# -*- coding: utf-8 -*-
"""
Created on Fri May 13 09:43:03 2016

@author: Alex
"""

import shelve
import os, sys
import pandas as pd

names = shelve.open('Molecule_Database')
names['+'].to_csv('pos.csv', index = False)
names['-'].to_csv('neg.csv', index = False)
names.close()
