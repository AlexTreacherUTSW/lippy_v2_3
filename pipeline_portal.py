# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 08:50:42 2016

@author: Alex
"""

import os, sys
import pandas as pd
from experiment_class import QTOF_Experiment
from Lookups.database_tables import db_login_info
import mysql
import utilities

import pdb



def run_job_experiment(job_experiment_id, job_cnx, lipid_cnx, rename = False, update=True):
    """
    This is used to complete a job experiment from the job database and output
    Takes the data from the database, filteres it, renames if needed and outputs
    the resuts to job_experiment output_msdata
    Parameters
    ----------
    job_experiment_id : This is the id for the job to be ran
    cnx : a connection to the database, this will not be closed by this function
    """
    pdb.set_trace()
    utilities.add_log_message('portal_pipelines - starting job with job_experiment_id = '+str(job_experiment_id))
    job_inputs_query = "SELECT * FROM job_experiment WHERE job_experiment_id = " + str(job_experiment_id) + ";"
    job_info_temp = pd.read_sql_query(job_inputs_query, job_cnx)
    job_series = job_info_temp.iloc[0]
    pdb.set_trace()
    if job_series['status_complete'] != 0:
        utilities.add_log_message('portal_pipelines - job_experiment_id '+str(job_experiment_id)+' is already complete')
        return None
    if job_series['setting_ic'] == True:
        import_raw = False
        import_ic = True
    else:
        import_raw = True
        import_ic = False
    utilities.update_job_loading(cnx, job_experiment_id, '10', 'Loading data from database', '0')
    experiment = QTOF_Experiment.from_database_experiment(cnx, job_experiment_id, 
                                                          rename = rename,
                                                          import_raw = import_raw,
                                                          import_ic = import_ic,
                                                          import_raw_norm = False,
                                                          import_ic_norm = False)
    utilities.update_job_loading(cnx, job_experiment_id, '', 'Applying Filters', '0')
    if job_series['setting_ic'] == True:
        main_data = experiment.data_ic
    else:
        main_data = experiment.data_raw
    


if '__main__' == __name__:
    cnx=mysql.connector.connect(user = db_login_info['user'], 
                            password = db_login_info['password'],
                            host = db_login_info['host'],
                            database = db_login_info['lipid_db'])
    job_cnx=mysql.connector.connect(user = db_login_info['user'], 
                            password = db_login_info['password'],
                            host = db_login_info['host'],
                            database = db_login_info['job_db'])
    row_value_dct = {'status_percent':'50', 'status_progress':'Filtering'}                     
    #update_db_value(job_cnx, 'job_experiment','2', 'job_experiment_id',row_value_dct)
    run_job_experiment(2, job_cnx, cnx)
    #cnx.close()
    #job_cnx.close()