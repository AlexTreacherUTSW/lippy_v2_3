# -*- coding: utf-8 -*-
"""
Created on Tue May 10 11:12:23 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
import MS_classes as ms
import pdb
import shutil
import runmarkerview
import Pipeline
import mysql.connector
import time
import Lookups.database_tables
import sample_classes as sc
import utilities as util
db_tables = Lookups.database_tables.db_tables 

class Database_functions():
    
    def __init__(self, cnx):
        """
        used for function for the utswlipiddb
        """
        self.cnx = cnx
        self.curser = cnx.cursor()
        
    def clear_tables(self):
        for table in db_tables:
            nocheck_query = "ALTER TABLE %s NOCHECK CONSTRAINT ALL"%(table)
            query = "TRUNCATE '%s';"%(table)
            check_query = "ALTER TABLE %s CHECK CONSTRAINT ALL"%(table)
            self.curser.execute(query)
            
    def get_samples_by_id(self):
        pass
        
    def delete_samples(self):
        pass
        
    def add_sample_set(self, sample_names, sample_meta, experiment_meta, ms_data, ic_ms_data, tof_data):      
        if type(sample_meta) == str:
            sample_df = pd.read_csv(sample_meta)
        else:
            sample_df = sample_meta.copy()
        if type(experiment_meta) == str:
            experiment_df = pd.read_csv(experiment_meta)
        else:
            experiment_df = experiment_meta
        for name in sample_names:
            sample = sc.Sample.init_from_data(ms_data = ms_data,
                                         ic_ms_data = ic_ms_data,
                                         tof_data = tof_data,
                                         sample_name = name,
                                         sample_meta = sample_df, 
                                         experiment_meta = experiment_df,
                                         cnx = self.cnx)
            sample.to_utswlipiddb()
        
        
 

class Process_for_database():
    
    def __init__(self, folder, ms_data, cnx, ic_ms_data = None, tof_data = None):
        """
        Used to process data for database
        assumes that the folder has the meta data in it
        """
        self.meta_paths = {'sample':os.path.join(folder,'sample_meta.csv'),
                           'contol':os.path.join(folder,'contol_meta.csv'),
                           'experiment':os.path.join(folder,'experiment_meta.csv')}
        self.meta_data = self.read_meta()
        self.cnx = cnx
        self.ms_data = self.check_ms(ms_data)
        self.ic_ms_data = self.check_ms(ic_ms_data)
        self.folder = folder
        
        def read_meta(self):
            data = {}
            for k in self.meta_paths.keys():
                if not os.path.exists(self.meta_paths[k]):
                    raise IOError ('Missing meta files, please add to folder!')
                else:
                    self.data[k] = pd.read_csv(self.meta_paths[k])
            return data
        
        def check_ms(self, ms_class):
            if ms_class.__class__.__name__ != 'MS_Data':
                raise TypeError ('class must be MS_Data')
            if 'Name' not in ms_class.data_frame.columns.tolist():
                new = ms_class.Name_Molecules()
                return new
            else:
                return ms_class
            
            
        def to_db_class(self):
            return Samples_to_database(named_ms_data = self.ms_data,
                                       sample_meta = self.meta_data['sample'],
                                       control_meta = self.meta_data['control'],
                                       experiment_meta = self.meta_data['experiment'],
                                       cnx = self.cnx,
                                       named_ic_ms_data = self.ic_ms_data,
                                       tof_data = self.tof_data)
            
    

class Samples_to_database():
    
    @staticmethod
    def process(self, folder, ms_data, cnx, ic_ms_data = None, tof_data = None):
        data = Process_for_database(folder, ms_data, cnx,
                                    ic_ms_data = ic_ms_data,
                                    to_data=tof_data)
        return data.to_db_class()
        
    @staticmethod    
    def from_raw_data(self):
        """
        needs building when the webportal is up
        input raw_data into ms_classes, name, fileter, ic, then input tp db.        
        """
        pass
        
    
    def __init__(self, named_ms_data, sample_meta, control_meta, experiment_meta,
                 cnx, named_ic_ms_data = None, tof_data = None, upload = False):
        """
        used to upload data from LipPy into the Database
        Note, this class needs to be passed and open connection,
        and DOES NOT close it at the end
        """
        self.experiment_meta = experiment_meta
        self.check_experiment_number()
        self.ms_data = named_ms_data
        self.ic_ms_data = named_ic_ms_data
        self.tof_data = tof_data
        self.cnx = cnx
        self.cursor = self.cnx.cursor()
        self.db_tables = get_database_tables(self.cnx)
        self.all_samples = self.ms_data.all_sample_cols
        self.sample_cols = self.ms_data.sample_cols
        self.control_cols = self.ms_data.control_cols
        self.sample_ids = self.make_sample_ids()
        self.sample_meta = self.add_sample_id(sample_meta)
        self.control_meta = self.add_sample_id(control_meta)
        self.organism = self.get_organism()
        self.error = None
        self.warning = None
        self.upload = upload
        if self.upload == True:
            self.execute
            
    def check_experiment_number():
        no = self.experiment_meta.loc[0]['experiment_id']
        query = 'SELECT MAX(experiment_id) FROM experiment_info'
        max_experiment_id = pd.read_sql_query(query).iloc[0,0]
        if no <= max_experiment_id:
            self.warning = 'Experiment already exists in the DB, data will not be loaded into the database'
            
    def make_sample_ids(self):
        sample_ids = {}
        max_id = pd.read_sql_query('SELECT MAX(sample_id) FROM sample_info', 
                                   self.cnx).iloc[0,0]
        if max_id == None:
            c = 1
        else:
            c = max_id+1
        for i in self.all_samples:
            sample_ids[i] = c
            c+=1        
        return sample_ids
        
    def add_sample_id(self, data_frame):
        df = data_frame.copy()
        df['sample_id'] = df.apply(lambda row: self.sample_ids[row['sample_name']], axis = 1)
        return df

    def get_organism(self):
        organism_lst = list(set(self.sample_meta.organism_classifier))
        if len(organism_lst) != 1:
            return 'error'
        else:
            return organism_lst[0]

    def upload_controls(self):
        control_dct = {}
        #control_types = list(set(self.control_meta.group_name.tolist()))
        df_cols = self.control_meta.columns.tolist()
        for index in self.control_meta.index:
            group = self.control_meta.loc[index]['organism_classifier']
            name = self.control_meta.loc[index]['sample_name']
            type_cols = intersect(self.db_tables[group],df_cols)
            sample_info_cols = intersect(self.db_tables['sample_info'],df_cols)
            run_info_cols = intersect(self.db_tables['run_info'],df_cols)
            type_df = self.control_meta[type_cols][self.control_meta.sample_name == name]
            sample_info_df = self.control_meta[sample_info_cols][self.control_meta.sample_name == name]
            run_info_df = self.control_meta[run_info_cols][self.control_meta.sample_name == name]
            sql_ms_data = self.ms_data.sql_format(name,'MSALL', self.sample_ids[name])
            if self.ic_ms_data != None:
                sql_ic_ms_data = self.ic_ms_data.sql_format(name,'MSALL', self.sample_ids[name])
                sql_ic_ms_data.to_sql(con=cnx, name = 'ic_ms_data', if_exists='append',
                                      flavor='mysql', index = False, chunksize = 200)
            else:
                sql_ic_ms_data = None
            control_dct[name] = {group:type_df,
                                 'sample_info':sample_info_df,
                                 'run_info':run_info_df,
                                 'ms_data':sql_ms_data,
                                 'ic_ms_data':sql_ic_ms_data}
            for key in control_dct[name].keys():
                if type(control_dct[name][key]) == type(pd.DataFrame()):
                    control_dct[name][key].to_sql(con=cnx, name = key, if_exists='append',
                                             flavor='mysql', index = False, chunksize = 200)

    def upload_samples(self, write=False):
        sample_cols = db_tables['sample_info']
        info_tables = {}
        for table in ['sample_info','run_info',self.organism]:
            for_db = self.sample_meta[self.db_tables[table]]
            info_tables[table] = for_db
        data_tables = {}
        #pdb.set_trace()
        for s in self.sample_cols:
            data_tables[s] = self.ms_data.sql_format(s, 'MSALL', self.sample_ids[s])
        #pdb.set_trace()
        ic_data_tables = {}
        if self.ic_ms_data != None:
            for s in self.sample_cols:
                ic_data_tables[s] = self.ms_data.sql_format(s, 'MSALL', self.sample_ids[s])
        #pdb.set_trace()
        if write == True:
            for key in info_tables.keys():
                df = info_tables[key]
                df.to_sql(con=cnx, name = key, if_exists='append',
                          flavor='mysql', index = False)
            for key in data_tables.keys():
                df = data_tables[key]
                df.to_sql(con=cnx, name = 'ms_data', if_exists='append',
                          flavor='mysql', index = False, chunksize = 200)
            for key in ic_data_tables.keys():
                df = ic_data_tables[key]
                df.to_sql(con=cnx, name = 'ic_ms_data', if_exists='append',
                          flavor='mysql', index = False, chunksize = 200)
    
    def upload_tof(self, write = False):
        pdb.set_trace()
        if self.tof_data != None and write == True:
            for sample in self.all_samples:
                sid = self.sample_ids[sample]
                sql_tof = self.tof_data.sql_formal(sample, sid)
                sql_tof.to_sql(con=cnx, name = 'tod_data', if_exists='append',
                               flavor='mysql', index = False, chuncksize = 200)
        else:
            pass
    
    def add_all(self):
        self.upload_samples(write = True)
        self.upload_controls(write = True)
        self.upload_tof(write = True)

    def execute(self):
        pass
        
    
def intersect(a,b):
    """
    returns the intersection of two list
    """
    return list(set(a) & set(b))
    
def get_database_tables(cnx):
    """
    reads the database and returs a dct with tables as keys and the fields of
    said tables as the vales.
    does not open or close the database!
    """
    db_tables = {}
    tables_df = pd.read_sql_query(r'SHOW TABLES', cnx)
    tables = tables_df[tables_df.columns[0]].tolist()
    for i in tables:
        table = pd.read_sql_query(r'DESCRIBE %s' %(i), cnx)['Field']
        db_tables[i] = table.tolist()
    return db_tables
    
if __name__ == '__main__2':
    
    folder = os.path.abspath(r'C:\Anaconda2\Lib\site-packages\LipPy_v2_1\Test_files\Names_Files')    
    pos_ms_dir = os.path.join('Test_files','Raw_data','022816_NASH13_std1+MSALL.txt')
    neg_ms_dir = os.path.join('Test_files','Raw_data','022816_NASH13_std1-MSALL.txt')
    name_file = os.path.join('Test_files','Names_Files','022816_NASH13_std1_NamesFile.csv')
    pos_ms = ms.MS_Data.from_raw_data_files(pos_ms_dir, name_file, '+')
    neg_ms = ms.MS_Data.from_raw_data_files(neg_ms_dir, name_file, '-')
    print ('filter')
    pos_fileterd = pos_ms.filter_set1()
    neg_filtered = neg_ms.filter_set1()
    print ('nameing')
    pos_named = pos_fileterd.Name_Molecules()
    neg_named = neg_filtered.Name_Molecules()
    named = pos_named.combine(neg_named)
    print ('ic')
    ic_pos = pos_named.isotope_correction(clean = True)
    ic_neg = pos_named.isotope_correction(clean = True)
    ic = ic_pos.combine(ic_neg)
    sample_meta = pd.read_csv(os.path.join(r'C:\Anaconda2\Lib\site-packages\LipPy_v2_0\Test_files\Names_Files','sample_meta.csv'))
    control_meta = pd.read_csv(os.path.join(r'C:\Anaconda2\Lib\site-packages\LipPy_v2_0\Test_files\Names_Files','control_meta.csv'))
    user = 'LipPy'
    password = 'MolGen'
    cnx=mysql.connector.connect(user = user, 
                                password = password,
                                host='localhost',
                                database = 'utswlipiddb')
    