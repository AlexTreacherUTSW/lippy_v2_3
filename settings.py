# -*- coding: utf-8 -*-
"""
Created on Tue Feb 17 19:03:13 2015
Contains parameters to be set for LipPy
@author: Mike
"""

"""
settings used mostly for pipeline
"""
#OMICS = 'Counts'
#LOG_SUMMARY = True
#SUMMARY_DIRECTORY = 'Summaries'
SIGNIFICANCE = 0.05
#Data baseline, any intensity under this will be set to this
DATA_BASELINE = 100
#Isotope base line is the smallest value that the isotope correction will correct for
ISOTOPE_BASELINE = 200
#MS1 name range
MS1_MASS_RANGE = .3
#MS2 name range
MS2_MASS_RANGE = .3 #.05

#filter1 used as the initial filter
FILTER1_AVG_THRESH = 100
FILTER1_MAX = 300
FILTER1_CHILD_PEAKS = [(1,10)]
FILTER1_NL_THRESH = -1300
FILTER1_GROUP_MEAN = 100
FILTER1_GROUP_QUANTILE = (30,100)

#filter2 used for the group comparison
FILTER2_CHILD_PEAKS = [(1,10)]
FILTER2_AVERAGE_THRESHOLD = 100
FILTER2_NL_THRESH = -1300
FILTER2_THRESH_TUPLE = (30.0, 100)

