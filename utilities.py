# -*- coding: utf-8 -*-
"""
Created on Fri Jan 23 16:29:57 2015

@author: JMLab1
"""
import os
import sys
from shutil import move, copy
from datetime import datetime
import pandas as pd
from random import sample
import re
import Lookups.database_tables
import time
import errors
import json
from collections import OrderedDict
import math
db_tables = Lookups.database_tables.db_tables


months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
          'August', 'September', 'October', 'November', 'December']

def move_file(filename, directory):
    """
    
    """
    error_message = 'FILENAME not found.  Please move this file to DIRR and try again.'
    if filename is None:
        return None
    if not filename in os.listdir(directory):
        try:
            move(filename, directory)
        except IOError:
            err = error_message.replace('FILENAME', filename)
            err = err.replace('DIRRR', os.getcwd()+os.sep+directory)
            raise IOError(err)
    return directory+os.sep+filename
    
def copy_file(filename, directory):
    """
    
    """
    error_message = 'FILENAME not found.  Please move this file to DIRR and try again.'
    if filename is None:
        return None
    if not filename in os.listdir(directory):
        try:
            copy(filename, directory)
        except IOError:
            err = error_message.replace('FILENAME', filename)
            err = err.replace('DIRRR', os.getcwd()+os.sep+directory)
            raise IOError(err)
    return directory+os.sep+filename
    
def get_month_dir(make_dir=False, directory=None):
    current_month = months[datetime.now().month-1]
    current_year = str(datetime.now().year)
    date_directory = current_month+'_'+current_year
    if directory is None:
        directory = os.getcwd()
    month_dir = directory+os.sep+date_directory
    if not date_directory in os.listdir(directory) and make_dir:
        os.makedirs(month_dir)
    return directory+os.sep+date_directory
    
def log_inputs(destination):
    try:
        try:
            copy('inputs.py', destination)
        except IOError:
            os.remove(destination+os.sep+'inputs.py')
            copy('inputs.py', destination)
    except WindowsError:
        ##WindowsError means no inputs.py, means test.  Pass.
        pass
    
def shrink_dataset(dataset, n):
    """
    Input: .txt file
    Output: None
    
    Side effect: creates a new .txt file consisting of n rows chosen at random
    from the input file.  Useful for generating small data sets to speed up
    testing.
    """
    try:
        table = pd.read_table(dataset)
    except IOError:
        dataset = dataset+'.txt'
        return shrink_dataset(dataset, n)
    new_idx = sample(table.index[5:], min([n, len(table.index[5:])]))
    table = table.ix[new_idx]
    fname = dataset.split('.')[0]
    table.to_csv(fname+'_mini.txt', sep='\t', index=False)
    return None
    
def assert_inputs_exist(data_files, names_file, TOF_files=[]):
    """
    Inputs:
        ---data_files, iterable of strings
        ---names_file, a string
        ---TOF_files, iterable of strings
    Raises errors if any of the strings input do not exist in the current
    directory of the directory that they will be moved to.
    """
    home_name= os.getcwd()
    home_dir = os.listdir(os.getcwd())
    msall_dir = os.listdir(home_name+os.sep+'Raw_Data')
    name_dir = os.listdir(home_name+os.sep+'Name_Files')
    tof_dir = os.listdir(home_name+os.sep+'TOF_data')
    for f in data_files:
        if f is not None:
            if f not in home_dir+msall_dir:
                raise IOError('File '+f+' does not exist.')
    if names_file not in home_dir+name_dir:
        raise IOError('File '+names_file+' does not exist.')
    for f in TOF_files:
        if f is not None:
            if f not in home_dir+tof_dir:
                raise IOError('File '+f+' does not exist.')
    return None

def quick_read(path_to_file, sheet_name=0):
    reader_dict = {'.csv' : pd.read_csv,
                   '.txt' : pd.read_table,
                   '.xls' : lambda x: pd.read_excel(x, sheet_name=sheet_name),
                   '.xlsx' : lambda x: pd.read_excel(x, sheet_name=sheet_name)}
    extensions = reader_dict.keys()  
    file_type = filter(lambda x: path_to_file.endswith(x), extensions)[0]
    reader = reader_dict[file_type]
    df = reader(path_to_file)
    return df

def make_root_dir(ext):
    if not os.path.exists(ext):
        return ext
    else:
        i = 1
        leni = len(str(i))
        while os.path.exists(ext):
            if i == 1:
                ext = ext +'_rerun' +str(i)
                i += 1
            else:
                ext = ext[:-leni] +str(i)
                leni = len(str(i))
                i += 1
        print 'new_directory name already used, data will be saved under the file extension ' + ext
        return ext
        
            
def read_time(time):
    if time >=3600:
        hours = int(time/3600)
        time = time - 3600*hours
        mins = int(time/60)
        secs = time % 60
    elif time >= 60 :
        hours = 0
        mins = int(time/60)
        secs = int(time % 60) 
    else:
        hours = 0
        mins = 0        
        secs = int (time)
    if hours < 10:
        hours = str(hours).zfill(2)    
    if mins < 10:
        mins = str(mins).zfill(2)
    else:
        pass
    if secs < 10:
        secs = str(secs).zfill(2)
    else:
        pass
    x = "%s:%s:%s" % (hours, mins, secs)
    return x
    
def names_file_checker(df, raise_error = True):
    error = None
    example_cols = ['sample_number','group_number','sample_name','group_name', 'sample_amount','standard_volume']
    forbidden_chars = ['.',',','\'','\\','/','\"','#',' ']
    cols = df.columns.tolist()
    for i in cols:
        if i not in example_cols:
            error = '%s not a correct column name, please edit and rerun'%(i)
    if df.isnull().values.any():
        error = 'Nans detected in names file, please remove them and rerun'
    if not df.sample_name.drop_duplicates().equals(df.sample_name):
        error = 'Duplicates detected in the Names column of the namesfile, please edit and rerun'
    groups = []
    for i in df.index:
        groups.append((df.group_number[i],df.group_name[i]))
    groups = list(set(groups))
    for l in groups:
        n = l[0]
        s = l[1]
        for g in groups:
            n1 = g[0]
            s1 = g[1]
            if (s1 == s and n1!=n) or (n1==n and s1!=s):
                error = 'Duplicate group_name used for group, please change and rerun.'
    forbidden_found = ''
    for c in [df.sample_name.tolist(), df.group_name.tolist()]:
        for s in c:      
            for f in forbidden_chars:
                if f in s:
                    forbidden_found += ('\''+f+'\''+',')
    if forbidden_found != '':
        error = '%s found in names_file, please remove and rerun'%(forbidden_found)
    for col in [df.sample_number, df.group_number]:
        try:
            col.apply(lambda x: int(x))
        except ValueError:
            error = 'Sample_Number and Group must be numbers'
    if error != None and raise_error == False:
        return error
    elif error != None and raise_error == True:
        raise ValueError(error)
    else:
        return None

def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))
        
def pack(file_loc):
    '''
    Used to comapct the file structure after a run into inputs and outputs folders
    '''
    input_file = os.path.join(file_loc, 'Input')
    if not os.path.exists(input_file):
        os.makedirs(input_file)
    #output_file = os.path.join(file_loc, 'Output')
    files = os.listdir(file_loc)
    if 'Output' in files:
        files.remove('Output')
    if 'inputs.py' in files:
        files.remove('inputs.py')
    for i in files:
        file_path = os.path.join(file_loc,i)
        shutil.move(file_path, input_file)
        
def unpack(file_loc):
    '''
    Used to up comapct the file structure in case of 
    '''
    input_file = os.path.join(file_loc, 'Input')   
    if not os.path.exists(input_file):
        pass
    else:
        files = os.listdir(input_file)
        for i in files:
            file_path = os.path.join(input_file,i)
            shutil.move(file_path, file_loc)
        os.rmdir(input_file)
        print 'Unpacking successful'

def format_date_sql(string, sep = '/'):
    """
    used to put a date into a format that sql can read
    """
    parts = string.split(sep)
    year = None
    for i in parts:
        if len(i) == 4:
            year = str(i)
            parts.remove(i)
    if year == None:
        year = str(int(parts[2])).zfill(2)
        parts.remove(parts[2])
    month = str(int(parts[0])).zfill(2)
    day = str(int(parts[1])).zfill(2)
    return year+'-'+month+'-'+day
    
def format_datetime_sql(string):
    """
    removes the ' at the begining of the datetime string. it is made this way
    so excel will not mess with it if it is opened in excel
    """
    if string.startswith('\''):
        return string[1:]
    else:
        return string

def df_format_date_sql(df):
    """
    used to change the date columns set by lookups/database_tables
    from excel format to mySQL format
    """
    date_cols = Lookups.database_tables.date_cols
    datetime_cols = Lookups.database_tables.datetime_cols
    new_df = df.copy()
    for col in date_cols:
        if col in new_df.columns.tolist():
            try:
                new_df[col] = new_df[col].apply(format_date_sql)
            except:
                new_df[col] = new_df[col]
        if col in datetime_cols:
            try:
                new_df[col] = new_df[col].apply(format_datetime_sql)
            except:
                new_df[col] = new_df[col]
    return new_df
    
default_error = os.path.join('Logs', 'errors.txt')
  
def add_error_message(error, error_file = default_error):
    datetime = time.strftime("%m/%d/20%y-%H:%M:%S")
    to_add = '['+datetime+']; ' + error +'\n'
    with open(error_file, "a") as myfile:
        myfile.write(to_add)
        
default_log = os.path.join('Logs', 'log.txt') 
def add_log_message(log, error_file = default_log):
    datetime = time.strftime("%m/%d/20%y-%H:%M:%S")
    to_add = '['+datetime+']; ' + log +'\n'
    with open(error_file, "a") as myfile:
        myfile.write(to_add)

def update_db_value(cnx, table, primary_key, primary_key_col, row_value_dct):
    """
    Used to update a mysql table with the priary key with the row_value_dct
    Example
    cnx = mysql_connection
    table = 'job_experiment'
    primary_key = 1
    primary_key_col = 'job_experiment_id'
    row_value_dct = {'status_percent':50, 'status_progress':'Filtering'}
    it will follow type, eg if percent is an int it will be put in as an int
    It seems my sql will adjust the type if possible, so I'd suggest to always 
    use a string
    """
    cursor = cnx.cursor()
    query = "UPDATE " + table + " SET "
    for key in row_value_dct:
        if type(row_value_dct[key]) != str:
            query += str(key) + '=' + str(row_value_dct[key]) + ', '
        else:
            query += str(key) + '=\'' + str(row_value_dct[key]) + '\', '
    query = query[:-2] + ' WHERE ' + str(primary_key_col) + ' = ' + str(primary_key) + ';'
    #print query
    cursor.execute(query)
    cnx.commit()

def update_job_loading(cnx, job_id, percent, progress, complete):
    cursor = cnx.cursor()
    query = "UPDATE job_experiment SET status_percent = '" + str(percent) +\
        "', status_progress = '" + str(progress) + "', status_complete = '" + str(complete) +\
        "' WHERE job_experiment_id = '" + str(job_id) + "';"
    #print query
    cursor.execute(query)
    cnx.commit()
    
def check_namefile(name_file, standard = False, raiseError = False):
    found_errors = []
    if type(name_file) == str:
        nf = pd.read_csv(name_file)
    else:
        nf = name_file.copy()
    #check the columns
    needed_cols = ['Sample_Number','Name','Group','Group_Name']
    if standard == True:
        needed_cols.extend(['Sample_Amount','Standard_Volume'])
    missing_columns = []
    for col in needed_cols:
        if col not in nf.columns.tolist():
            missing_columns.append(col)
    if missing_columns != []:
        found_errors.append('Names File is missing column(s) ' + str(missing_columns))
        if raiseError == True:
            raise errors.NameFileError(found_errors[0])
        else:        
            return found_errors
    #check for Nans
    if nf.isnull().values.any():
        found_errors.append('Nans found in namesfile')
    #check for duplicates in sample_number and ints
    sample_numbers = nf['Sample_Number'].tolist()
    if len(set(sample_numbers)) != len(sample_numbers):
        found_errors.append('Duplicates found in the Sample_Number')
    for num in sample_numbers:
        if int(num) != num:
            found_errors.append('Non interger values found for the Sample_Number')
            break
    #check for duplicates in Name
    sample_names = nf['Name']
    if len(set(sample_names)) != len(sample_names):
        found_errors.append('Duplicates found in the Sample_Number')
    #check for duplicate groups and group names
    groups = nf['Group'].tolist()
    group_names = nf['Group_Name'].tolist()
    group_dict = {}
    for num, name in zip(groups, group_names):
        if not num in group_dict.keys():
            group_dict[num] = name
        else:
            if group_dict[num] != name:
                found_errors.append('Duplicate Group or Group_Number')
        if raiseError == True:
            raise errors.NameFileError(str(found_errors))
        else:        
            return found_errors

def df_to_db_json(df):
    """
    takes a df and formats it for a the utsw database
    returns the data_cols and 
    """
    new_df = df.copy()
    cols = OrderedDict()
    cols_rename = OrderedDict()
    col_num = 0
    for c in df.columns.tolist():
        cols[col_num] = c
        cols_rename[c] = col_num
        col_num += 1
    col_str = json.dumps(cols)
    new_df.rename(columns = cols_rename, inplace = True)
    df_str = new_df.to_json(orient='records')
    return df_str, col_str
    
def json_to_df(df, col_key):
    data = pd.read_json(df)
    data_cols_temp = OrderedDict(json.loads(col_key))
    data_cols = OrderedDict()
    for key in data_cols_temp.keys():
        data_cols[int(key)] = data_cols_temp[key]
    data = data.rename(columns = data_cols)
    data.set_index('MS1', inplace = True, drop = True)
    data = data.sort_index(axis=1)
        
def round_sigfigs(num, sig_figs):
    try:
        if num != 0:
            return round(num, -int(math.floor(math.log10(abs(num))) - (sig_figs - 1)))
        else:
            return 0
    except:
        return num
    
    

    