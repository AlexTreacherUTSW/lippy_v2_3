# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 07:05:28 2016

@author: Alex
"""
import os, sys
import pandas as pd
import numpy as np
from LipPy_v2_1 import MS_classes as ms21
from LipPy_v2_2 import MS_classes as ms22
from LipPy_v2_3 import MS_classes as ms23
from LipPy_v2_3.Lookups import database_tables as db23
import experiment_class  as ec
import mysql.connector
import pdb

old_db_info = {'host':'129.112.144.138',
               'user':'LipPy',
               'password':'MolGen',
               'lipid_db':'utswlipiddb'}
               
new_db_info = db23.db_login_info

old_cnx = mysql.connector.connect(user = old_db_info['user'], 
                                password = old_db_info['password'],
                                host=old_db_info['host'],
                                database = old_db_info['lipid_db'])

new_cnx = mysql.connector.connect(user = new_db_info['user'], 
                                password = new_db_info['password'],
                                host= new_db_info['host'],
                                database = new_db_info['lipid_db'])
                                
#def transfere_experiment(eid):
if True:
    eid = 126
    experiment_id_str = str(eid)
    experiment_id = int(experiment_id_str)
    experiment_meta = pd.read_sql_query("SELECT * FROM experiment_info WHERE experiment_id = " + str(experiment_id) + ";", old_cnx)
    data_raw = ms23.MS_Data.from_old_db_experiment(experiment_id, old_cnx, iso_cor = False, std_norm = False, rename = True)
    print 'YAY naming'
    data_raw = data_raw.Name_Molecules()
    sample_query = "SELECT * FROM sample_info WHERE experiment_id = " + str(experiment_id) + ";"
    all_sample_info = pd.read_sql_query(sample_query, old_cnx)
    sample_info = all_sample_info[all_sample_info.group_number > 0]
    control_info = all_sample_info[all_sample_info.group_number < 0]
    name_table = data_raw.name_table.copy()
    samples = name_table[name_table.group_number > 0]
    controls = name_table[name_table.group_number < 0]
    sample_specific_info = pd.DataFrame()
    control_specific_info = pd.DataFrame()
    for sid in samples.sample_number.tolist():
        q1 = "SELECT organism_classifier FROM sample_info WHERE sample_id = " + str(sid) + ";"
        oc = pd.read_sql_query(q1, old_cnx).iloc[0]['organism_classifier']
        q2 = "SELECT * FROM " + oc + " WHERE sample_id = " + str(sid)
        sample_specific_info = sample_specific_info.append(pd.read_sql_query(q2, old_cnx))
    for cid in controls.sample_number.tolist():
        q1 = "SELECT organism_classifier FROM sample_info WHERE sample_id = " + str(cid) + ";"
        oc = pd.read_sql_query(q1, old_cnx).iloc[0]['organism_classifier']
        q2 = "SELECT * FROM " + oc + " WHERE sample_id = " + str(cid)
        control_specific_info = control_specific_info.append(pd.read_sql_query(q2, old_cnx))
    meta_sample = pd.concat([sample_info.set_index('sample_id', drop = True), sample_specific_info.set_index('sample_id', drop = True)], axis = 1).reset_index(drop = True)
    meta_control = pd.concat([control_info.set_index('sample_id', drop = True), control_specific_info.set_index('sample_id', drop = True)], axis = 1).reset_index(drop = True)
    meta_sample.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
    meta_control.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
    experiment = ec.QTOF_Experiment.from_raw_MS_Data(experiment_meta, meta_sample, meta_control, data_raw, None, None)
    #experiment.import_all_to_db(new_cnx)
    
                                
if False:
    print 'running'
    experiment_ids = pd.read_sql_query('SELECT * FROM experiment_info', old_cnx)['experiment_id'].tolist() 
    error_removed = []
    error_in = []
    correct_in = []
    for experiment_id_str in experiment_ids:
        try:
            experiment_id = int(experiment_id_str)
            experiment_meta = pd.read_sql_query("SELECT * FROM experiment_info WHERE experiment_id = " + str(experiment_id) + ";", old_cnx)
            data_raw = ms23.MS_Data.from_old_db_experiment(experiment_id, old_cnx, iso_cor = False, std_norm = False, rename = True)
            print 'YAY naming'
            data_raw = data_raw.Name_Molecules()
            sample_query = "SELECT * FROM sample_info WHERE experiment_id = " + str(experiment_id) + ";"
            all_sample_info = pd.read_sql_query(sample_query, old_cnx)
            sample_info = all_sample_info[all_sample_info.group_number > 0]
            control_info = all_sample_info[all_sample_info.group_number < 0]
            name_table = data_raw.name_table.copy()
            samples = name_table[name_table.group_number > 0]
            controls = name_table[name_table.group_number < 0]
            sample_specific_info = pd.DataFrame()
            control_specific_info = pd.DataFrame()
            for sid in samples.sample_number.tolist():
                q1 = "SELECT organism_classifier FROM sample_info WHERE sample_id = " + str(sid) + ";"
                oc = pd.read_sql_query(q1, old_cnx).iloc[0]['organism_classifier']
                q2 = "SELECT * FROM " + oc + " WHERE sample_id = " + str(sid)
                sample_specific_info = sample_specific_info.append(pd.read_sql_query(q2, old_cnx))
            for cid in controls.sample_number.tolist():
                q1 = "SELECT organism_classifier FROM sample_info WHERE sample_id = " + str(cid) + ";"
                oc = pd.read_sql_query(q1, old_cnx).iloc[0]['organism_classifier']
                q2 = "SELECT * FROM " + oc + " WHERE sample_id = " + str(cid)
                control_specific_info = control_specific_info.append(pd.read_sql_query(q2, old_cnx))
            meta_sample = pd.concat([sample_info.set_index('sample_id', drop = True), sample_specific_info.set_index('sample_id', drop = True)], axis = 1).reset_index(drop = True)
            meta_control = pd.concat([control_info.set_index('sample_id', drop = True), control_specific_info.set_index('sample_id', drop = True)], axis = 1).reset_index(drop = True)
            meta_sample.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
            meta_control.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
            experiment = ec.QTOF_Experiment.from_raw_MS_Data(experiment_meta, meta_sample, meta_control, data_raw, None, None)
            experiment.import_all_to_db(new_cnx)
            correct_in.append(experiment_id)        
        except:
            error_str = 'Issue with experiment ' + str(experiment_id)
            try:
                ec.QTOF_Experiment.delete_experiment_from_db(new_cnx, experiment_id)
                error_str += ': Removal Success'
                error_removed.append(experiment_id)
            except:
                error_str += ': Removal Failed'
                error_in.append(experiment_id)
            print error_str
    old_cnx.close()
    new_cnx.close()

def list_to_mysql_str(lst):
    return '('+str(lst)[1:-1]+')'

