# -*- coding: utf-8 -*-
"""
Created on Thu May 19 14:07:01 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
import pdb
import copy
import warnings
from scipy.stats import linregress
import utilities
import mysql
from Lookups.database_tables import db_login_info
from collections import OrderedDict
import json
idx = pd.IndexSlice

class TOF_process:
    
    def __init__(self, tof_data, names, mode, number_to_save = 15, standard_mix = None, omit = None):
        self.tof_path = tof_data
        self.names_file = names
        self.mode = mode
        self.number_to_save = number_to_save
        self.standard_mix = standard_mix
        self.omit = omit
    
    def read_names(self):
        """
        Parses name table, checking that it is properly formmated along the way.
        Returns the parsed table.
        """
        if type(self.names_file) == pd.DataFrame:
            name_table = self.names_file.copy()
        else:
            name_table = pd.read_csv(self.names_file, skiprows=self.omit)
        sanitize = lambda x : x.strip().replace(' ', '_').replace('.', '_')
        ##capitalize first letter of each word in name table columns and replace whitespace with underscores
        name_table.columns = map(sanitize, name_table.columns) 
        example_cols = ['sample_number','group_number','sample_name','group_name', 'sample_amount','standard_volume']
        #next couple of line change the names file from an old format to a new one if needed
        new_cols_dct = OrderedDict()
        new_cols_dct['Sample_Number'] = 'sample_number'
        new_cols_dct['sample_id'] = 'sample_number'
        new_cols_dct['Sample_ID'] = 'sample_number'
        new_cols_dct['Group'] = 'group_number'
        new_cols_dct['Name'] = 'sample_name'
        new_cols_dct['Group_Name'] = 'group_name'
        new_cols_dct['Sample_Amount'] = 'sample_amount'
        new_cols_dct['Standard_Volume'] = 'standard_volume'
        for col in new_cols_dct:
            if col in name_table:
                name_table[new_cols_dct[col]] = name_table[col]
                del name_table[col]
        #map(sanitize, pd.read_csv(lookup_dir+'names_checker.csv').columns)
        try:
            assert 'sample_number' in name_table.columns
            assert 'group_number' in name_table.columns
            assert 'sample_name' in name_table.columns
            assert 'group_name' in name_table.columns     
            for heading in name_table.columns:
                assert heading in example_cols
        except:
            raise IOError('Column headings in %s not properly formatted.  Column heads should be: \'sample_number\' \'group_number\', \'group_name\' followed by optional columns: \'sample_amount\', \'standard_volume\'')
            #name_table.columns = ['sample_number', 'group_number', 'sample_name', 'group_name', 'Type']
        if 'standard_volume' not in name_table.columns and self.standard_mix != None:
            name_table['standard_volume'] = 20
        name_table['group_name'] = name_table['group_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_')) ##Remove trailing whitespace in group names    
        name_table['sample_name'] = name_table['sample_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_'))
        type_dct = {'sample_number':int,
                    'sample_name':str,
                    'group_number':int,
                    'group_name':str,
                    'sample_amount':float,
                    'standard_volume':float}
        for col in type_dct.keys():
            if col in name_table.columns.tolist():
                name_table[col] = name_table[col].astype(type_dct[col])
        try:
            assert all([isinstance(name_table.sample_number[i], (int, float, np.int64, np.float64)) 
                        for i in name_table.index])
            assert all([isinstance(name_table.group_number[i], (int, float, np.int64, np.float64))
                        for i in name_table.index])
        except AssertionError:
            raise IOError('Not all entries in the Sample Number or group columns in your names file are integers.  Correct your names file and try again.')
        if name_table['sample_name'].duplicated().any():
            raise IOError('File '+self.names_file+' contains duplicates in the sample names column.  Give each sample a distinct name and run again.')
        return name_table
    
    def pre_process(self):
        """
        creates a multi index dataframe for the tof data  
        examples for selection
        df.loc[:,[('s09','16')]]
        df.xs('s09',level='name',axis =1)
        """
        try:
            df = pd.read_table(self.tof_path, skiprows = [1,2])
        except IOError:
            print 'TOF file not found: ', self.tof_path, '. Will continue without'
            return pd.DataFrame()
        header = df.columns.tolist()[1:]
        name_list = [x.split(' - ')[0] for x in header]
        names = []
        [names.append(x) for x in name_list if not x in names]
        timepoint = [x.split(' - ')[1] for x in header]
        info_cols = [('MS1','')]
        tuple_cols = zip(name_list, timepoint)
        all_cols = info_cols + tuple_cols
        header = pd.MultiIndex.from_tuples(all_cols)
        df.columns = header
        df.columns.names = ['name','time']
        df.insert(1,('Mode',''), self.mode)
        df_names =[]
        [df_names.append(x[0]) for x in df.columns.tolist() if x[0] not in df_names]
        timecols = list([x for x in range(1,self.number_to_save+1)])
        new_cols = [('MS1',''),('Mode','')]
        tof_cols = [('MS1',''),('Mode','')]
        for name in names:
            name_df = df.xs(name, level = 'name', axis = 1)
            [new_cols.append((name,x)) for x in name_df.columns.tolist()[-self.number_to_save:]]
            [tof_cols.append((name,tnum)) for tnum in timecols]
        tof_index = pd.MultiIndex.from_tuples(tof_cols)
        new_df = df.loc[:,new_cols]
        new_df.columns = tof_index
        new_df.columns.names = ['name','run']
        return new_df
        
    def to_TOF_Data(self):
        try:
            new_df = self.pre_process()
            if new_df.empty:
                return TOF_Data_None()
            else:
                return TOF_Data(new_df,self.read_names(),mode = self.mode) 
        except ValueError:
            print "TOF_classes - issue with the number of time intervals, it will be ignored"
            return TOF_Data_None()
        

class TOF_Data:
    
    @staticmethod
    def from_raw_data_files(tof_data, names, mode, number_to_save = 15):
        process = TOF_process(tof_data = tof_data,
                              names = names,
                              mode = mode,
                              number_to_save = number_to_save)
        data = process.to_TOF_Data()
        return data
        
    @staticmethod
    def from_db_experiment(experiment_id, cnx, name_file = None, mode = None):
        """ 
        Not complete!
        """
        if mode == None:
            pos = TOF_Data.from_db_experiment(experiment_id, cnx, name_file = name_file, mode = '+')
            neg = TOF_Data.from_db_experiment(experiment_id, cnx, name_file = name_file, mode = '-')
            return pos.combine(neg)
        else:
            query = "SELECT sample_id FROM meta_sample WHERE experiment_id = " + str(experiment_id) + ";"
            sample_ids = pd.read_sql(query, cnx)['sample_id'].tolist()
            query = "SELECT * FROM meta_sample WHERE experiment_id = " + str(experiment_id) + ";"
            meta_samples = pd.read_sql_query(query, cnx)
            if isinstance(name_file, type(None)):
                new_names = OrderedDict([('sample_id','sample_number'),
                                         ('sample_name','sample_name'),
                                         ('group_number','group_number'),
                                         ('group_name','group_name'),
                                         ('standard_volume','standard_volume'),
                                         ('sample_amount','sample_amount')])
                name_file = meta_samples.rename(columns = new_names)[new_names.values()]
            sample_dfs = []
            sample_names = []
            for sid in sample_ids:
                sample_name = name_file.sample_name[name_file.sample_number == sid].iloc[0]
                sample_names.append(sample_name)
                query = "SELECT data, data_cols FROM data_tof WHERE sample_id = " + str(sid) + " AND mode = '"+ mode +"';"
                tof_query_df = pd.read_sql_query(query, cnx)
                if tof_query_df.shape[0] != 1:
                    print 'No TOF data found for sample id ' + str(sid) + '; it will be skipped'
                    continue
                data = pd.read_json(tof_query_df.iloc[0]['data'])
                if data.empty:
                    return TOF_Data_None()
                data_cols_temp = OrderedDict(json.loads(tof_query_df.iloc[0]['data_cols']))
                data_cols = OrderedDict()
                for key in data_cols_temp.keys():
                    data_cols[int(key)] = data_cols_temp[key]
                data = data.rename(columns = data_cols)
                data.set_index('MS1', inplace = True, drop = True)
                data = data.sort_index(axis=1)
                sample_dfs.append(data)
            if sample_dfs != []:
                complete_data_df = pd.concat(sample_dfs, axis = 1, keys=sample_names)
            else:
                complete_data_df = pd.DataFrame()
            mode_df = pd.DataFrame([mode]*len(complete_data_df.index),index = complete_data_df.index, columns =pd.MultiIndex.from_tuples([('Mode','')]))
            complete_df = pd.concat([mode_df, complete_data_df], axis =1)
            complete_df.reset_index(inplace = True)
            return TOF_Data(complete_df, name_file, mode=mode)
            
    def __init__(self, data_frame, names, mode=None, ms_standards=None):
        self.data_frame = data_frame
        self.name_table = names
        self.sample_dict = dict(zip(self.name_table.sample_number, self.name_table.sample_name))
        self.sample_cols = self.name_table[self.name_table.group_number>0].sort_values(by = ['group_number']).sample_name.tolist()
        self.control_cols = self.name_table[self.name_table.group_number<=0].sort_values(by = ['group_number']).sample_name.tolist()
        self.all_sample_cols = self.sample_cols+self.control_cols  
        self.mode = mode
        self.names = self.get_names()
        self.name_cols = ['MS1','Mode']
    
    def __repr__(self):
        return self.data_frame.__repr__()
        
    def read_names(self):
        """
        Parses name table, checking that it is properly formmated along the way.
        Returns the parsed table.
        """
        if type(self.names_file) == pd.DataFrame:
            name_table = self.names_file.copy()
        else:
            name_table = pd.read_csv(self.names_file, skiprows=self.omit)
        sanitize = lambda x : x.strip().replace(' ', '_').replace('.', '_')
        ##capitalize first letter of each word in name table columns and replace whitespace with underscores
        name_table.columns = map(sanitize, name_table.columns) 
        example_cols = ['sample_number','group_number','sample_name','group_name', 'sample_amount','standard_volume']
        #next couple of line change the names file from an old format to a new one if needed
        new_cols_dct = OrderedDict()
        new_cols_dct['Sample_Number'] = 'sample_number'
        new_cols_dct['sample_id'] = 'sample_number'
        new_cols_dct['Sample_ID'] = 'sample_number'
        new_cols_dct['Group'] = 'group_number'
        new_cols_dct['Name'] = 'sample_name'
        new_cols_dct['Group_Name'] = 'group_name'
        new_cols_dct['Sample_Amount'] = 'sample_amount'
        new_cols_dct['Standard_Volume'] = 'standard_volume'
        for col in new_cols_dct:
            if col in name_table:
                name_table[new_cols_dct[col]] = name_table[col]
                del name_table[col]
        #map(sanitize, pd.read_csv(lookup_dir+'names_checker.csv').columns)
        try:
            assert 'sample_number' in name_table.columns
            assert 'group_number' in name_table.columns
            assert 'sample_name' in name_table.columns
            assert 'group_name' in name_table.columns     
            for heading in name_table.columns:
                assert heading in example_cols
        except:
            raise IOError('Column headings in %s not properly formatted.  Column heads should be: \'sample_number\' \'group_number\', \'group_name\' followed by optional columns: \'sample_amount\', \'standard_volume\'')
            #name_table.columns = ['sample_number', 'group_number', 'sample_name', 'group_name', 'Type']
        if 'standard_volume' not in name_table.columns and self.standard_mix != None:
            name_table['standard_volume'] = 20
        name_table['group_name'] = name_table['group_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_')) ##Remove trailing whitespace in group names    
        name_table['sample_name'] = name_table['sample_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_'))
        type_dct = {'sample_number':int,
                    'sample_name':str,
                    'group_number':int,
                    'group_name':str,
                    'sample_amount':float,
                    'standard_volume':float}
        for col in type_dct.keys():
            if col in name_table.columns.tolist():
                name_table[col] = name_table[col].astype(type_dct[col])
        try:
            assert all([isinstance(name_table.sample_number[i], (int, float, np.int64, np.float64)) 
                        for i in name_table.index])
            assert all([isinstance(name_table.group_number[i], (int, float, np.int64, np.float64))
                        for i in name_table.index])
        except AssertionError:
            raise IOError('Not all entries in the Sample Number or group columns in your names file are integers.  Correct your names file and try again.')
        if name_table['sample_name'].duplicated().any():
            raise IOError('File '+self.names_file+' contains duplicates in the sample names column.  Give each sample a distinct name and run again.')
        return name_table
        
    def get_names(self):
        """
        This will get the names from names file and compare it with the names
        on self.data_frame if the names dont match it will call columns_from_nf
        and rename the column headings to the names_file
        
        """
        names = []
        for col in self.data_frame.columns.tolist()[2:]:
            if not col[0] in names:
                names.append(col[0])
        nf_names = self.name_table['sample_name'].tolist()
        if len(names) != len(nf_names):
            print 'TOF_Classes - Please check your data, number of samples in TOF does not match number in names_file,'
            print 'TOF_Classes - LipPy will be using the names set in the wiff files'
            names = names
        elif set(names) != set(nf_names):
            print 'TOF_Classes - WARNING!!!!!! Names in Tof file do not match the name_table names.'
            print 'TOF_Classes - Probable alignment issues'
            self.columns_from_nf()
        return names
            
    def columns_from_nf(self):
        '''
        changes the columns in self.data_frame to those in names file,
        this is not suggested as it is not a stable method.
        '''
        names = []
        for col in self.data_frame.columns.tolist()[2:]:
            if not col[0] in names:
                names.append(col[0])
        nf_names = self.name_table['sample_name'].tolist()
        new_cols = [('MS1',''),('Mode','')]
        old_cols = self.data_frame.columns.tolist()[2:]
        cols_dct = {}
        for col in old_cols:
            i = names.index(col[0])
            new_cols.append((nf_names[i],col[1]))
        new_index = pd.MultiIndex.from_tuples(new_cols)
        self.data_frame.columns = new_index

    def combine(self, other):
        """
        combines self with another tof_data class
        """
        if other == None and self == None:
            return TOF_Data_None()
        elif other == None:
            return self
        elif self == None:
            return other
        else:
            if not self.name_table.equals(other.name_table):
                print 'TOF_classes - Names_tables do not match for combine'           
                return None
            elif False:
                pass
                #place to add other checks
            else:
                return combine(self, other)
            
    def restrict_to_mode(self, mode):
        df = self.data_frame.copy()
        new_df = df[df['Mode'] == mode]
        return TOF_Data(new_df, self.name_table, mode = mode)
        
    def single_sample_tof(self, sample):
        new_df = self.data_frame.copy()
        new_cols = self.name_cols+[sample]
        df = select_multi_index(new_df,[new_cols,[]])
        new_names = self.name_table[self.name_table.sample_name == sample]    
        return TOF_Data(df, new_names, mode = self.mode)       

    def to_json(self, sample):
        """
        This turns a sample into a json for the DB, this will remove mode as 
        that is saved seporatly in the db
        """
        if not self.mode in ['+','-']:
            raise ValueError('Mode must be + or -')
        df = self.data_frame.copy()
        sample_df = pd.DataFrame()
        sample_df['MS1'] = df['MS1']
        sample_df = pd.concat([sample_df, df[sample]],axis = 1)
        return utilities.df_to_db_json(sample_df)
        
    def sample_stats_json(self, sample):
        stats = self.sample_stats(sample)
        return utilities.df_to_db_json(stats)

    def sample_stats(self, sample):
        stats = tof_sample_stats(self,sample)
        return stats

    def sample_summary_stats(self, sample):
        if self == None or self.data_frame.empty:
            return pd.DataFrame()
        sample_data = self.data_frame[sample]
        sums = sample_data.apply(np.sum)
        slope, intercept, r_value, p_value, std_err = linregress(range(len(sums)),sums)
        mean = np.average(sums)
        stdev = np.std(sums, ddof = 1)
        total = np.sum(sums)
        mx = np.max(sums)
        stats = pd.Series(OrderedDict([('sum',total),
                                      ('mean',mean),
                                      ('stdev',stdev),
                                      ('slope',slope),
                                      ('intercept',intercept),
                                      ('r_value',r_value),
                                      ('p_value',p_value),
                                      ('std_err',std_err),
                                      ('max',mx),                      
                                      ]))
        to_return = pd.concat([sums, stats])
        to_return.name = sample
        return to_return

    def qc_stats(self, mode = None, output_path = None):
        """
        Stats for QC
        This gets the total signal for each sample
        
        """
        if mode == None and self.mode == None:
            raise ValueError('Please select a mode for the TOF_classes qc_stats')
        elif mode == None:
            mode_data = self
        else:
            mode_data = self.restrict_to_mode(mode)
        summary_sheet = pd.DataFrame()
        for i in mode_data.all_sample_cols:
            to_add = mode_data.sample_summary_stats(i)
            summary_sheet = pd.concat([summary_sheet, to_add], axis = 1)
        if output_path != None:
            summary_sheet.to_csv(output_path)
        return summary_sheet.reset_index(drop = False)

class TOF_Data_None(TOF_Data):
    """
    Used when there is no data, but allows for continuous use of the functions, they will always return None_MS_Data
    This is an MS_Data where this == None is True
    Useful when there is no data in the database for this
    """
    
    def __init__(self, data_frame = None, names = None, mode=None, ms_standards=None):
        self.data_frame = pd.DataFrame()
        self.name_table = None
        self.ms_standards = None
        self.mode = None
    
    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, type(None)):
            return True
        else:
            return False
            
    def __ne__(self, other):
        """Override the default not Equals behavior"""
        return not self.__eq__(other)
                
        

def select_multi_index(df, columns):
    """
    used to select from a df with multi index
    needs further testing, but the way its being used now works for the purpose
    
    """
    old_cols = df.columns.tolist()
    new_cols = []
    for level in range(len(columns)):
        for col in old_cols:
            if col[level] in columns[level]:
                new_cols.append(col)
    return df[new_cols]

def combine(tof_class1, tof_class2):
    """
    Used to combine 2 TOF_Data classes
    if passes two None return None
    if one class is None return the class that is not None
    """
    class1 = copy.deepcopy(tof_class1)
    class2 = copy.deepcopy(tof_class2)
    if class1 == None and class2 == None:
        return None
    elif class1 == None:
        return class2
    elif class2 == None:
        return class1
    elif type(class1) != type(class2):
        raise TypeError ('When combining 2 TOF_classes, both classed need to be tof_classes')
    elif class1.data_frame.columns.tolist() != class2.data_frame.columns.tolist():
        raise ValueError ('The two TOF dataframes columns must match in order to be combined')
    elif not class1.name_table.equals(class2.name_table):
        raise ValueError('the name_table of the tof classes must be the same in order to combine')
    elif class1.mode == class2.mode:
        warnings.warn('The tof classes you are trying to cobine have the same mode', UserWarning)
    else:
        name_table = class1.name_table
        df1 = class1.data_frame
        df2 = class2.data_frame
        new_df= df1.append(df2, ignore_index = True)
        return TOF_Data(new_df,name_table,mode = None)

def tof_stats(x, y):
    """
    stats used to apply to tof data
    y will most likley be [1,2,3,..,15]
    returns mean, stdev, slope, intercept, r_value, p_value, std_err
    """
    slope, intercept, r_value, p_value, std_err = linregress(x,y)
    mean = np.average(y)
    stdev = np.std(y, ddof = 1)
    return mean, stdev, slope, intercept, r_value, p_value, std_err

def tof_sample_stats(TOF_class, sample):
    if TOF_class.data_frame.empty:
        return pd.DataFrame()
    tof_sample = TOF_class.single_sample_tof(sample)
    data_cols = tof_sample.data_frame[sample].columns.tolist()
    stats_frame = pd.DataFrame()
    stats_frame['MS1'] = tof_sample.data_frame['MS1','']
    stats_frame['Mode'] = tof_sample.data_frame['Mode','']
    stats = tof_sample.data_frame[sample][data_cols].apply(lambda x: tof_stats(data_cols, x), axis = 1)
    stats_frame['mean'],stats_frame['stdev'],stats_frame['slope'],stats_frame['intercept'],stats_frame['rvalue'],stats_frame['p_value'],stats_frame['stderr']= zip(*stats)
    return stats_frame

if __name__ == '__main__':
    if False:
        pos_tof = os.path.join('Test_files','101116_test', 'Inputs','Tof_data','091816_LA3_LD_Dyn_M1_3+TOF.txt')
        neg_tof = os.path.join('Test_files','101116_test', 'Inputs','Tof_data','091816_LA3_LD_Dyn_M1_3-TOF.txt')
        names_path = os.path.join('Test_files','101116_test', 'Inputs','091816_LA3_LD_Dyn_M1_3_NamesFile.csv')
        print 'pos'
        pos = TOF_Data.from_raw_data_files(pos_tof,names_path,'+')
        print 'neg'
        neg = TOF_Data.from_raw_data_files(neg_tof,names_path,'-')
        tof = pos.combine(neg)
    if False:
        cnx=mysql.connector.connect(user = db_login_info['user'], 
                                password = db_login_info['password'],
                                host = db_login_info['host'],
                                database = db_login_info['lipid_db'])
        pos = TOF_Data.from_db_experiment(118, cnx, mode='+')
        neg = TOF_Data.from_db_experiment(118, cnx, mode='-')