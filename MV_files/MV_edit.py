# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 15:32:51 2016

@author: Alex
"""

import os, sys
import re

file_loc = r'D:\Alex Treacher\Lipid_Explorer_v2_0\MV_files'
MSMSloc = file_loc+os.sep+'ProcessMSMSOfEverything.xml'
TOFloc = file_loc+os.sep+'PrecessSpectra.xml'

with open(MSMSloc) as t:
    x = t.read()
MS_threshold_pat = re.compile('\n\s{2}<threshold\s{1}value=".{2}"\s/>')
MS_threshold_str = '\n  <threshold value="%i" />'
MS_IFW_pat  = re.compile('\n\s{2}<threshold\s{1}value=".{2}"\s/>')


def pattern_maker(setting):
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))
    return pattern
    
def change_MS_setting(setting, value):
    if setting not in ['threshold','ignorefragmentswindow',
                       'masstolerance','maxcompounds',
                       'usepeakarea','ignorefragmentsaboveprecuror']:
        raise ValueError('setting not found')
    text = open(MSMSloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(MSMSloc, 'w') as wr:
        wr.write(text)
        
def change_TOF_setting(setting, value):
    if setting not in ['Operation','masstolerance',
                       'masstoleranceinppm','processpeaklistsmasstolerance',
                       'processpeaklistsmasstoleranceinppm',
                       'massbinsize','massbinsizeinppm',
                       'baselinesubtract','baselinesubtractbins',
                       'minintensity','maxcompounds',
                       'useexclusion','period',
                       'experiment']:
        raise ValueError('setting not found')
    text = open(MSMSloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(MSMSloc, 'w') as wr:
        wr.write(text)