# -*- coding: utf-8 -*-
"""
Created on Fri May 13 09:43:03 2016

@author: Alex
"""

import os, sys
import pandas as pd
from collections import OrderedDict
import mysql.connector
import pdb

mysql_user = 'LipPy'
mysql_password = 'MolGen'
mysql_old_host = '129.112.145.252'
mysql_host = '129.112.145.204'
mysql_database = 'utswlipiddb'
mysql_job = 'jobs'

db_login_info = {'user': mysql_user,
                 'password':mysql_password,
                 'host':mysql_host,
                 'host_old':'129.112.149.252',
                 'lipid_db': mysql_database,
                 'job_db':mysql_job,
                 'name_lists':'name_lists'}

db_tables = OrderedDict([('meta_experiment',['experiment_id','experiment_name',
                                             'run_date', 'date_received',
                                             'extraction_date',
                                             'wiff_location', 'user',
                                             'pi_of_origin', 'contact_email']),
                         ('meta_sample',['sample_id','experiment_id',
                                         'sample_name','group_number',
                                         'group_name',
                                         'sample_classifier','position_on_plate',
                                         'plate_position','sequence_number',
                                         'internal_standard','standard_volume',
                                         'sample_amount','database_input_date']),
                         ('meta_cultured_cells',['sample_id','cell_line',
                                            'cell_type','species',
                                            'dish_size','medium',
                                            'serum','treatment',
                                            'confluence','cell_number']),
                         ('meta_human',['sample_id','sample_type',
                                   'volume','weight',
                                   'study_name','sex','age',
                                   'treatment','cohort']),
                         ('meta_liver_standard',['sample_id','batch',
                                            'vial','defrost_date',
                                            'note']),
                         ('meta_model_organism',['sample_id',
                                            'info1','info2','info3','info4']),
                         ('meta_nonhuman_mammal',['sample_id','species','sample_type',
                                             'weight','volume','treatment',
                                             'genotype','background','diet',
                                             'age','sex','feed_cycle',
                                             'feeding_status','time_of_harvest']),
                         ('meta_other',['sample_id','note']),
                         ('meta_sciex_standard',['sample_id','batch','vial',
                                            'lot','received_date',
                                            'open_date','note']),
                         ('meta_solvent_blank',['sample_id','note']),
                         ('meta_splash_standard',['sample_id','batch','vial','lot',
                                             'defrost_date','open_date',
                                             'date_prepared','prepared_by',
                                             'concentration', 'note']),
                         ('data_ms',['ms_id','sample_id','method',
                                     'mode','data_raw','data_raw_std_norm',
                                     'data_ic','data_ic_std_norm',
                                     'data_cols']),
                         ('data_tof',['tof_id','sample_id','method','mode','data','tof_qc']),
                         ('data_experiment_tech',['experiment_id','lippy_version','markerview_version','standard_data'])
                        ])
                                     
date_cols = ['run_date',
             'date_received',
             'extraction_date',
             'defrost_date',
             'date_recieved',
             'receive_date',
             'open_date',
             'defrost_date',
             'open_date',
             'date_prepared',]

datetime_cols = ['run_datetime']

def get_database_tables(cnx):
    """
    reads the database and returs a dct with tables as keys and the fields of
    said tables as the vales.
    does not open or close the database!
    """
    db_tables = {}
    tables_df = pd.read_sql_query(r'SHOW TABLES', cnx)
    tables = tables_df[tables_df.columns[0]].tolist()
    for i in tables:
        table = pd.read_sql_query(r'DESCRIBE %s' %(i), cnx)['Field']
        db_tables[i] = table.tolist()
    return db_tables
    
def check_tables(cnx, expected):
    """
    Used to check the tables in the data base with expected tables
    Warning will occur if tables do not match, but will not error
    """
    warning = 'Lookups - database tables from Lookups do not match tables from the database'
    observed = get_database_tables(cnx)
    for k in observed.keys():
        if k not in expected.keys():
            print warning, k
        if set(observed[k]) != set(expected[k]):
            print warning, k
    return observed
    
def make_db_connection(table = 'lipid'):
    if table == 'lipid' or table == 'lipids':
        cnx=mysql.connector.connect(user = db_login_info['user'], 
                                    password = db_login_info['password'],
                                    host= db_login_info['host'],
                                    database = db_login_info['lipid_db'])
    elif table == 'job' or table == 'jobs':
        cnx=mysql.connector.connect(user = db_login_info['user'], 
                                    password = db_login_info['password'],
                                    host= db_login_info['host'],
                                    database = db_login_info['lipid_db'])
    elif table == 'name' or table == 'names' or table == 'name_table' or table == 'name_tables':
        cnx=mysql.connector.connect(user = db_login_info['user'], 
                                    password = db_login_info['password'],
                                    host= db_login_info['host'],
                                    database = db_login_info['name_lists'])
    else:
        return None
    return cnx

def get_pos_names():
    """
    gets the positive name list from the database
    """
    cnx = make_db_connection('names')
    query = "SELECT * FROM positive_master"
    df = pd.read_sql_query(query, cnx)
    rename = {'precursor':'Precursor',
              'fragment':'Fragment',
              'lipid_name':'Name',
              'lipid_group':'Group'}
    df.rename(columns = rename, inplace = True)
    cnx.close()
    return df

def get_neg_names():
    """
    gets the positive name list from the database
    """
    cnx = make_db_connection('names')
    query = "SELECT * FROM negative_master"
    df = pd.read_sql_query(query, cnx)
    rename = {'precursor':'Precursor',
              'fragment':'Fragment',
              'lipid_name':'Name',
              'lipid_group':'Group'}
    df.rename(columns = rename, inplace = True)
    cnx.close()
    return df

def get_standard(standard_name):
    """
    gets the positive name list from the database
    """
    if type(standard_name) == type(None) or standard_name.lower() == 'none':
        return None
    cnx = make_db_connection('names')
    query = "SELECT * FROM " +standard_name
    try:   
        df = pd.read_sql_query(query, cnx)
    except:# mysql.connector.errors.DatabaseError:
        print 'database_tables - ' + str(standard_name) + ' is not a currently recognised standard'
        return None
    cnx.close()
    return df

if True:
    try:
        cnx=mysql.connector.connect(user = mysql_user, 
                                    password = mysql_password,
                                    host=mysql_host,
                                    database = mysql_database)
        x = check_tables(cnx, db_tables)                 
        cnx.close()
    except:
        print 'database_tables - Issue connecting to datbase to check the table settings'     

if '__main__' == __name__:
    job_cnx = mysql.connector.connect(user = mysql_user, 
                                    password = mysql_password,
                                    host=mysql_host,
                                    database = db_login_info['job_db'])
