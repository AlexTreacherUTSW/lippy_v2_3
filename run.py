# -*- coding: utf-8 -*-
"""
Created on Tue May 03 18:34:12 2016

@author: Alex
"""

"""
input info
    user
    output_dir
    input_dir
    pos_msall_name
    neg_msall_name
    
run checks on the data
select settings
run through pipelines
retrieve unfiltered_raw_counts and corected_counts from pipelines
add data to database
"""
import os, sys
import pandas as pd
import numpy as np
import pdb
import shutil
import runmarkerview
import pipeline_data_analysis as Pipeline
import experiment_class as ec
import database_classes
import mysql.connector
import utilities as util
import datetime, time
import Lookups.database_tables as database_tables
from collections import OrderedDict

lipidomics_path = os.path.abspath(r'X:\Lipidomics')

class Run():
    """
    THis class is used to:
        take in a folder from a specified location,
        get data from inputs (this allows the verables to be saved)
        check the source folder for needed cont
    """
    def __init__(self, user, output_file, source_path,
                 pos_MS, neg_MS, pos_TOF, neg_TOF, namesFile, 
                 DMS_modes, custom_runs,
                 header, standard_mix, groups_to_test,
                 to_database, ignore_missing_standard = False,
                 restrict_output_to_pos = True,
                 run_data_analysis = True):
        self.user = user
        #file locations
        self.source_path = os.path.abspath(source_path)
        self.source_file = os.path.basename(self.source_path)
        self.source_input_path = os.path.join(self.source_path,'Inputs')
        self.output_path = self.get_output_path(output_file)
        self.raw_ms_path = os.path.join(self.source_input_path,'Raw_data')
        self.raw_tof_path = os.path.join(self.source_input_path,'Tof_data')
        self.namesFile = namesFile
        self.namesFilePath = os.path.join(self.source_input_path,self.namesFile)
        self.sampleMetaPath = os.path.join(self.source_input_path,'sample_meta.csv')
        self.contolMetaPath = os.path.join(self.source_input_path,'control_meta.csv')
        self.experimentMetaPath = os.path.join(self.source_input_path,'experiment_meta.csv')
        if pos_MS != None:
            self.pos_MS = os.path.join(self.raw_ms_path,pos_MS)
        else:
            self.pos_MS = None
        if neg_MS != None:
            self.neg_MS = os.path.join(self.raw_ms_path,neg_MS)
        else:
            self.neg_MS = None
        if pos_TOF != None:
            self.pos_TOF = os.path.join(self.raw_tof_path,pos_TOF)
        else:
            self.pos_TOF = None
        if neg_TOF != None:
            self.neg_TOF = os.path.join(self.raw_tof_path,neg_TOF)
        else:
            self.neg_TOF = None
        self.DMS_modes = DMS_modes
        self.custom_runs = custom_runs
        self.header = header
        self.standard_mix = standard_mix
        self.groups_to_test = groups_to_test
        self.to_database = to_database
        #abitrary settings
        self.source_checked = False
        self.moved = False
        self.error  = None
        self.warning = None
        self.ignore_missing_standard = ignore_missing_standard
        self.restrict_output_to_pos = restrict_output_to_pos
        self.run_data_analysis = run_data_analysis
        self.file_errors = []
        
    def get_output_path(self, output_file):
        path = os.path.join(self.source_path,output_file)
        new_path = util.make_root_dir(path)
        return new_path

    def check_source(self, return_wiff_times = True):
        """
        checks to make sure needed files are there
        
        This will also get the time each will was made so it can be saved in 
        database mostly for graphing purposes
        """
        #checks to make sure all the .wiff files are there
        data_dct = {'pos_MS':{'data':self.pos_MS,
                              'command_file':'Command_%s+MSALL.txt'%(self.source_file),
                              'wiff_folder':'+MSALL',
                              'sample_name_extention':'+MSALL.wiff',
                              },
                    'neg_MS':{'data':self.neg_MS,
                              'command_file':'Command_%s-MSALL.txt'%(self.source_file),
                              'wiff_folder':'-MSALL',
                              'sample_name_extention':'-MSALL.wiff',
                              },
                    'pos_TOF':{'data':self.pos_TOF,
                              'command_file': 'Command_%s+TOF.txt'%(self.source_file),
                              'wiff_folder':os.path.join(self.source_path,'+TOF'),
                              'sample_name_extention':'+MSALL.wiff',
                              },
                    'neg_TOF':{'data':self.neg_TOF,
                              'command_file':'Command_%s-TOF.txt'%(self.source_file),
                              'wiff_folder':'-TOF',
                              'sample_name_extention':'-TOF.wiff',
                              },
                  }
        for key in data_dct.keys():
            info_dct = data_dct[key]
            if info_dct['data'] == None:
                #no key                
                print ('no %s data to check' %(key))
            else:
                if info_dct['command_file']:
                    command_ex = os.path.join(self.source_input_path, 'MarkerView_Files', info_dct['command_file'])
                    wiff_ex = os.path.join(self.source_input_path, info_dct['command_file'])
                    sample_ex = info_dct['sample_name_extention']
                    wiff_times = check_wiffs(command_ex, wiff_ex, self.source_input_path, sample_ex)
                    error = wiff_times[0]
                if error != None:
                    # do something drastic
                    self.file_errors.append('wiff_error '+error)
                else:
                    self.sample_run_datetime = wiff_times[1]
        files_to_check = [self.namesFile]
        if self.to_database == True:
            files_to_check.extend(['sample_meta.csv','control_meta.csv'])
        for f in files_to_check:
            if not os.path.exists(os.path.join(self.source_input_path,f)):
                self.file_errors.append(f+' missing')
        #check names file
        names_file = pd.read_csv(self.namesFilePath)
        nameError = util.names_file_checker(names_file, raise_error = False)
        if nameError != None:
            self.file_errors.append(nameError)
        #check meta_data
        if self.to_database == True:
            self.sample_meta = pd.read_csv(self.sampleMetaPath)
            self.control_meta = pd.read_csv(self.contolMetaPath)
            self.experiment_meta = pd.read_csv(self.experimentMetaPath)
            self.sample_meta['internal_standard'] = self.sample_meta['internal_standard'].apply(lambda x: str(x.lower()))
        else:
            self.sample_meta = None
        if self.file_errors == []:
            self.source_checked = True
            print "Run - Files checked, go grab a coffee!"
        else:
            raise ValueError('files missing from source' +str(self.file_errors))

    def pre_preprocessing(self):
        """
        set MarkerView settings
        run markerview
        check the markerView output files
        check and make output folder
        Also will set the datetime for the sample and control meta 
            (if to_database = True)
        """
        if self.moved == True and self.source_checked == True:
            print 'run - Running wiff files through markerview'
            #copy settings folder across, use default settings
            runmarkerview.change_MV_settings()
            #run marker view for all files
            runmarkerview.throughMV(self.source_input_path)
        #check the raw data files (sample_id in the header?)
        else:
            raise ValueError('Source needs to be checked before running, please run self.check_source')
        if self.to_database == True:
            print "run - Adding sample run timestamps"
            self.sample_meta['run_datetime'] = ''
            self.control_meta['run_datetime'] = ''
            for i in self.sample_meta.index:
                sample = self.sample_meta.loc[i, 'sample_name']
                if sample in self.sample_run_datetime.keys():
                    datetime = self.sample_run_datetime[sample]
                else:
                    self.experiment_meta
                    date = self.experiment_meta_path.iloc[0]['run_date']
                    date = date.replace('/','-')
                    datetime = date + " 00:00:00"
                self.sample_meta.loc[i, 'run_datetime'] = datetime
            self.sample_meta.to_csv(self.sampleMetaPath)
            for i in self.control_meta.index:
                sample = self.control_meta.loc[i, 'sample_name']
                if sample in self.sample_run_datetime.keys():
                    datetime = self.sample_run_datetime[sample]
                else:
                    self.experiment_meta
                    date = self.experiment_meta_path.iloc[0]['run_date']
                    date = date.replace('/','-')
                    datetime = date + " 00:00:00"
                self.control_meta.loc[i, 'run_datetime'] = datetime
            self.control_meta.to_csv(self.contolMetaPath)

    def run_Pipeline(self, force_run=False):
        """
        Once the source folder has been moved to the lipidomics folder
        run info though pipelines, this will make the local ouotput folders
        currently used.
        infomation from this will also me used to be put into the database
        """
        self.experiment.run_pipeline(self.output_path, self.header, self.groups_to_test, self.restrict_output_to_pos, self.ignore_missing_standard, force=force_run)

    def input_to_database(self, force_db=False):
        """
        Inputs the data into the database
        probably worth creating a new script for this
        """
        if self.to_database == True:
            self.cnx=database_tables.make_db_connection()
            self.experiment.import_all_to_db(self.cnx, force=force_db)
            self.cnx.close()
        else:
            print "run - attr to_database is set to False, so samples will not be added"

    def execute(self, to_database = True, restrict_to_pos=True, force_run = False, force_db = False):
        print 'Checking source files, please make sure all files are closed'
        self.check_source()
        print 'Source files present and correct'
        self.moved = True
        self.pre_preprocessing()
        print 'Running data through pipelines'
        self.experiment = ec.QTOF_Experiment.from_file(self.source_input_path, self.namesFilePath, ignore_missing_standard = self.ignore_missing_standard)
        #run checks here
        self.experiment_errors = self.experiment.check_for_data_errors()
        if self.experiment_errors != []:
            'Can not put into database or run pipelines as there are errors'
            self.experiment.make_error_output(self.output_path)
        if self.experiment_errors == [] or force_db:
            if self.to_database == True or force_db:
                self.input_to_database(force_db=force_db)
        if self.experiment_errors == [] or force_run:
            if self.run_data_analysis == True or force_run:
                self.run_Pipeline(force_run=force_run)
        
    def execute_goncalo(self):
        self.source_checked = True
        self.moved = True
        self.pre_preprocessing()
        self.run_Pipeline()
            
        
def check_wiffs(text_dir, wiff_path, inputs, sample_ex):
    """
    checks the command file to make sure all the needed wiffs are in the right
    place and none are missing.
    This will also look up the files time it was made and save it with the
        basename of the file with the sample_ex removed
    """
    df = pd.read_csv(text_dir, header = None)
    df.columns = ['path']
    timestamps = OrderedDict()
    for i in df.index:
        line = df.loc[i]['path']
        path = line.replace('Sample:temporary', inputs)
        if not os.path.exists(path):
            return [path, 'error']
        else:
            sample_plus_ex = os.path.basename(path)
            if sample_plus_ex.endswith(sample_ex):
                sample = sample_plus_ex[:-len(sample_ex)]
            else:
                sample = sample_plus_ex
            timestamps[sample] = reformat_datetime(path)
    return [None, timestamps]
    
def reformat_datetime(path):
    """
    gets the datetime stamp from a file and formats it for mySQL
    """
    dt = datetime.datetime.fromtimestamp(os.path.getmtime(path))   
    year = "%04d" % (dt.year)
    month = "%02d" % (dt.month)
    day = "%02d" % (dt.day)
    hour = "%02d" % (dt.hour)
    mins = "%02d" % (dt.minute)
    seconds = "%02d" % (dt.second)
    string = "'%s-%s-%s %s:%s:%s"%(year,
                                  month,
                                  day,
                                  hour,
                                  mins,
                                  seconds)
    return string

def check_sample_meta(path):
    """
    used to make sure the meta data is correct
    """
    error = None
    if not os.path.exists(path):
        error = 'No sample meta file located in your run folder'
        return error
    else:
        meta = pd.read_csv(path)
    organism_list = list(set(self.sample_meta.organism_classifier))
    if len(organism_list) != 1:
        error = 'Samples set can not contain more than one organism'
        return error
    else:
        organism = organism_list[0]


    
