# -*- coding: utf-8 -*-
"""
Created on Fri Jan 23 16:29:57 2015

@author: JMLab1
"""
import os
import sys
import os, sys
import pandas as pd
import numpy as np
import mysql.connector
import LipPy_v2_2 as LipPy
import LipPy_v2_2.MS_classes as ms2
import LipPy_v2_1.MS_classes as ms1
from LipPy_v2_1.Lookups.database_tables import db_login_info
import LipPy_v2_2.settings as settings 
from collections import OrderedDict
import pdb
    

def ms_data_to_sample_json(ms_data, sample):
    sql_df = ms_data.data_frame.copy()
    cols = ['Mass_Pair','Name','Group','Type', sample]
    sql_df = sql_df[cols]
    sql_df.rename(columns = {'Mass_Pair':'mass_pair',
                     'Name':'name',
                     'Group':'lipid_group',
                     'Type':'lipid_type',
                     sample:'counts'}, inplace = True)
           
    sql_df['name']=sql_df['name'].apply(lambda x: str(x.name))
    
    type_dct = {'mass_pair':str,
                'name':str,
                'lipid_group':str,
                'lipid_type':str,
                'counts':int}
    for key in type_dct.keys():
        if key in sql_df.columns.tolist():
            sql_df[key] = sql_df[key].astype(type_dct[key])
    return sql_df

def df_to_JSON(df):
    x = df.to_json(orient='records')
    return x

def df_to_txt_csv(fn, df):
    x = df.to_csv()
    with open(fn, "w") as text_file:
        text_file.write(x)

def df_to_txt_JSON(fn, df):
    x = df_to_JSON(df)
    with open(fn, "w") as text_file:
        text_file.write(x)

#if "__main__" == __name__:
if True:
    job_experiment_cnx=mysql.connector.connect(user = db_login_info['user'], 
                                password = db_login_info['password'],
                                host = db_login_info['host'],
                                database = db_login_info['job_db'])
    lipid_db_cnx=mysql.connector.connect(user = db_login_info['user'], 
                                password = db_login_info['password'],
                                host = db_login_info['host'],
                                database = db_login_info['lipid_db'])
    ms_data = ms1.MS_Data.from_db_experiment(1, lipid_db_cnx, iso_cor = False, std_norm = False)
    needed_cols = ['Mass_Pair','Name','Group','Type','counts']
    ms_data_pos = ms_data.restrict_to_mode('+')
    ms_data_neg = ms_data.restrict_to_mode('-')
    
        