# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 13:42:13 2015

@author: Alex
"""

def check_name_overlaps(msdata):
    """
    check to make sure that there is only one of each name,
    will return where the over lap is if there is one, if not return None
    """
    df = msdata.data_frame.copy()
    names = df['Name'].copy()
    if len(set(names)) != len(names):
        return 'Duplicate name in data'
    