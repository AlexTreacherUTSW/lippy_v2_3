# -*- coding: utf-8 -*-
"""
Created on Thu Dec 03 09:03:55 2015

@author: Alex
"""

import shelve
import pandas as pd
standards = shelve.open('Standards_DB')

#complete path to standard mix
file_path_for_new_standard = 'D:\Alex Treacher\Lipid_Explorer_v2_0\LipPy_v2_0\Lookups\splash_mix.csv'
#name of the standard, this will be used to call on it from LipPy UI
new_standard_name = 'SPLASH2'


# no need to touch anything below this!
#=================================
if not file_path_for_new_standard.endswith('.csv'):
    raise ValueError ('new_standard_mix must be in .csv format')
else:
    new_std = pd.read_csv(file_path_for_new_standard)

if new_standard_name in standards.keys():
    print standards.keys()
    raise ValueError ('Sorry this name of mix is already taken, list of taken names above')

def standard_checker(df):
    posstds = df[df.Polarity == '+']
    negstds = df[df.Polarity == '-']
    for p in df.Polarity.tolist():
        if p not in ['+','-']:
            raise ValueError('Polarity needs to be either \'+\' or \'-\'')
    if df.columns.tolist() != ['Name','MS1','MS2','Polarity','Group','Conc']:
        raise ValueError('columns are not correct')
    elif df.shape[1] != 6:
        raise ValueError('Wrong number of rows in the standards mix')
    elif 'default' not in posstds.Group.tolist():
        print posstds.Group.tolist()
        raise ValueError('There needs to be a default standard for the pos mode')
    elif 'default' not in negstds.Group.tolist():
        raise ValueError('There needs to be a default standard for the neg mode')
    elif df.isnull().values.any():
        raise ValueError('There are Nans in your table, please remove and try again')
    else:
        return 'OK'

x = standard_checker(new_std)
if x == 'OK':
    standards[new_standard_name] = new_std
        
standards.close()
