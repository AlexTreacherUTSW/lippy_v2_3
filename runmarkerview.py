# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 14:01:44 2015

@author: S23689
"""
#To make markerview.exe work in the command prompt, the file path was added to the environment variable path. Go to control panel->system and security->security->change settigns->advanced->environment variables
#Save this in LipPY
import os, sys
import shutil
from Tkinter import *
import tkFileDialog
import subprocess
import pandas as pd
import numpy as np
import collections
from collections import OrderedDict
import pdb
import re

#This function modifies strings in text files
def change_string(filename, old_string, new_string):
        s=open(filename).read()
        if old_string in s:
                s=s.replace(old_string, new_string)
                f=open(filename, 'w')
                f.write(s)
                f.close()

directory = r'N:\Alex_T\For_testing_RMV\120915_Plasma_DilSet_PBSvsW'
mv_setting_dir = os.path.abspath(r'C:\Users\JMLab1\AppData\Roaming\Sciex\MarkerView')
mv_setting_dir = os.path.abspath(r'D:\Alex Treacher\play\MarkerView')
ms_setting_loc = os.path.join(mv_setting_dir,r'ProcessMSMSOfEverything.xml')
tof_setting_loc = os.path.join(mv_setting_dir,r'ProcessSpectra.xml')

def change_MV_settings(loc = mv_setting_dir, method = 'workhorse', path_for_log = False, 
                       ms_file = 'MV_MS_Settings.csv', tof_file = 'MV_TOF_Settings.csv'):
    """
    Parameters
    ----------
    loc : the file location of the makerview settings
    method : this will call from the dictionary the method and where the files 
        are located. So workhorse will have a set of files, so will a method
        for DMS and custom runs etc. These files should be saved in Templates
        folder of LipPy
    path_for_log:
        Default False
        If False it will do nothing
        If not False it should be set to a path where it can create .csv files
            that hold the settings that markerview used to run the data
    ms_file:
        'MV_TOF_Settings'
        file name that the ms_settings will be saved to
    tof_file:
        Default 'MV_MS_Settings.csv'
        file name that the tof_settings will be saved to
    Examples
    --------
    MV_settings('C:\Users\JMLab1\AppData\Roaming\Sciex\MarkerView', 'worhorse')
    Default
    -------
    Set to use workhorse settings
    """
    mv_setting_files = {'workhorse':{'ms':'ProcessMSMSOfEverything.xml',
                                      'tof':'ProcessSpectra.xml'},
                        'DMS':{},
                        'CR1':{},}
    #if ran from script it will get run path, if not it will get relative path
    try:
        script_dir = os.path.dirname(__file__)
    except NameError:
        script_dir = os.path.realpath('.')
    ms_new = os.path.join(script_dir,'Templates', mv_setting_files[method]['ms'])
    tof_new = os.path.join(script_dir,'Templates', mv_setting_files[method]['tof'])
    ms_setting_loc = os.path.join(loc,r'ProcessMSMSOfEverything.xml')
    tof_setting_loc = os.path.join(loc,r'ProcessSpectra.xml')
    shutil.copy(ms_new,ms_setting_loc)
    shutil.copy(tof_new,tof_setting_loc)
    if path_for_log != False:
        ms_settings_path = os.path.join(path_for_log,'MV_MS_Settings.csv')
        tof_settings_path = os.path.join(path_for_log,'MW_TOF_Settings.csv')
#===================
    #get the ms and tof text to read
        with open(ms_setting_loc, 'r') as txt:
            ms_txt = txt.read()
        with open(tof_setting_loc, 'r') as txt:
            tof_txt = txt.read()
        ms_text = open()
        
        ms_settings = OrderedDict([('threshold',get_setting('threshold',ms_text)),
                                   ('masstolerance',get_setting('masstolerance',ms_text)),
                                   ('maxcompounds',get_setting('maxcompounds',ms_text)),
                                   ('usepeakarea',get_setting('usepeakarea',ms_text)),
                                   ('ignorefragmentsaboveprecuror',get_setting('ignorefragmentsaboveprecuror',ms_text))])
        tof_settings = OrderedDict([('Operation',get_setting('Operation',tof_text)),
                                    ('masstolerance', get_setting('masstolerance', tof_text)),
                                    ('masstoleranceinppm', get_setting('masstoleranceinppm')),
                                    ('processpeaklistsmasstolerance',get_setting('processpeaklistsmasstolerance')),
                                    ('processpeaklistsmasstoleranceinppm',get_setting('processpeaklistsmasstoleranceinppm')),
                                    ('massbinsize',get_setting('massbinsize')),
                                    ('massbinsizeinppm',get_setting('massbinsizeinppm')),
                                    ('baselinesubtract',get_setting('baselinesubtract')),
                                    ('baselinesubtractbins',get_setting('baselinesubtractbins')),
                                    ('minintensity',get_setting('minintensity')),
                                    ('maxcompounds',get_setting('maxcompounds')),
                                    ('useexclusion',get_setting('useexclusion')),
                                    ('period value',get_setting('period value')),
                                    ('experiment',get_setting('experiment')),])
        ms_df = pd.DataFrame(ms_settings)
        tof_df = ps.DataFrame(tof_settings)
        ms_df.to_csv(ms_settings_path)
        tof_df.to_csv(tof_settings_path)
                        
def get_setting(setting_name, text):
    p1 = setting+'\svalue="'
    p2 = setting+'\svalue=".*"'
    start = re.search(p1, text).end()
    stop = re.search(p2, text).end()-1
    return text[start:stop]
    
    

def throughMV(directory, run = True, additional_runs = None):
    #folder name manipulation
    dirName=directory.replace(r'/',os.sep)
    folderName=os.path.basename(directory)
    commandFilesDir = os.path.join(dirName, 'MarkerView_Files')
    #gets the bat file needed
    possible_bat = []
    for f in os.listdir(directory):
        if f.endswith('.bat'):
            possible_bat.append(f)
    if len(possible_bat) == 1:
        bat_file = possible_bat[0]
    else:
        for p in possible_bat:
            if p[:-4] in dirName:
                print "runmarkerview - more than one .bat file found, will use" + p
                bat_file = p
                break
    cmdFile = os.path.join(dirName,bat_file)
    names_file = os.path.join(dirName,folderName+'_NamesFile.csv')
    raw_data_file = os.path.join(dirName, 'Raw_data')
    tof_data_file = os.path.join(dirName, 'Tof_data')
    for direc in [raw_data_file,tof_data_file]:
        if not os.path.exists(direc):
            os.mkdir(direc)
    if os.listdir(raw_data_file) != []:
        print ('Files located in raw_data. Will not run sames though marker view. If you wish for the files to be made, please stop the program and delete or move the contents of the raw_data file in your folder and re-run')
        return None
    #change strings on command files
    all_files = os.listdir(commandFilesDir)
    text_files = [i for i in all_files if '.txt' in i]
    if all_files != text_files:
        raise IOError('Unexpected file in MarkerView_Files folder please remove or edit and try again')
    for i in text_files:
        change_string(os.path.join(commandFilesDir,i),'temporary', dirName)
    #change strings on CMD files 
    newCMDstr = os.path.join(dirName,'MarkerView_Files')+os.sep
    change_string(cmdFile, 'OutPutDir', newCMDstr)
    change_string(cmdFile, 'RawDataDir', raw_data_file)
    change_string(cmdFile, 'TofDataDir', tof_data_file)
    if run:
        p=subprocess.Popen(cmdFile, shell=True, stdout=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if p.returncode != 0:
            print ('Possible issue with the MarkerView files, please make sure they are correct and complete')
        for i in text_files:
            change_string(os.path.join(commandFilesDir,i), dirName,'temporary')
        change_string(cmdFile,newCMDstr, 'OutPutDir')
        change_string(cmdFile, raw_data_file, 'RawDataDir')
        change_string(cmdFile, tof_data_file, 'TofDataDir')

MSMSsettingsloc = ''
TOFsettingsloc = ''


def change_MS_setting(setting, value):
    if setting not in ['threshold','ignorefragmentswindow',
                       'masstolerance','maxcompounds',
                       'usepeakarea','ignorefragmentsaboveprecuror']:
        raise ValueError('setting not found')
    text = open(MSMSsettingsloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(MSMSsettingsloc, 'w') as wr:
        wr.write(text)
        
def change_TOF_setting(setting, value):
    if setting not in ['Operation','masstolerance',
                       'masstoleranceinppm','processpeaklistsmasstolerance',
                       'processpeaklistsmasstoleranceinppm',
                       'massbinsize','massbinsizeinppm',
                       'baselinesubtract','baselinesubtractbins',
                       'minintensity','maxcompounds',
                       'useexclusion','period',
                       'experiment']:
        raise ValueError('setting not found')
    text = open(TOFsettingsloc).read()
    replace = '\n  <%s value="%s" />'%(setting, value)
    guide = r'\n\s{2}<%s\s{1}value=".{2}"\s/>'
    pattern = re.compile(guide % (setting))    
    text=re.sub(pattern, replace,text)
    with open(TOFsettingsloc, 'w') as wr:
        wr.write(text)
        
    