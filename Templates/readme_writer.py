# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 09:53:39 2015

@author: Alex
"""

import pandas as pd
import pdfkit
import os, sys
root = 'D:\\Alex Treacher\\Lipid_Explorer_v2_0\\LipPy_v2_0\\'
 
def HTML_writer(title,group1,group2,df_table):
    f = open(title,'w')
    
    message1 = r"""<html>
    <head>
    <style>
    h1 {{
        color: #164476;
        text-decoration: underline;
        font-size: 24;
        
    }}
    table {{
        border-collapse: collapse;
        width: 40%;
    }}  
    th, td {{
        text-align: left;
        padding: 8px;
        border-bottom: 1px solid #ddd;
    }}
    th {{
        background-color: #164476;
        color: white;
    }}
    </style>
    </head>
    <h1>Comparison of {0} and {1}</h1>
    <body><p> Your lipidomics data has been analyzed and organized to make it easier to interpret. <br> 
    If you are a new user, we suggest that you begin with the table below and the {0} v {1} Norm to Counts file. <br>
    Here is a summary of data that can be identified from your samples: </p></body>
    """ .format(group1,group2)
    
    table1 = df_table.to_html(index = False)

    message2 = """<html>
    <head></head>
    <body><p>  
    In addition to the identifed metabolites, we identifed a total of %r potential metabolites in your samples. <br>
    Of these, %r are increasing and %r are decreasing. The identity of these metabolites is unknown and will likely be difficult to follow up. <br>
    The accompanying subfolders contain more detailed analysis for each group or class of lipids. <br>
    In addition, the Plots folder contains visual outputs that maybe serve as helpful starting points. <br>
    If there is any type of analysis you would like to see, have a lipid class you want added to this analysis, <br> 
    or need help navigating the directories, contact your neighborhood lipidomics specialist or send an e-mail to <br>
    Matthew.Mitsche@utsouthwestern.edu or Alex.Treacher@utsouthwestern.edu.<br>
    Your help is needed to improve LipidExplorer and will be greatly appreciated. <br>
    This analysis was done using LipidExplorer V2 0.<br>
    </p></body>
    </html>""" % (0,0,0)
    
    
    complete = message1+table1+message2
    complete = complete.replace('<table border="1" class="dataframe">','<table>')
    f.write(complete)
    f.close()
    print ('    HTML file written')
    
if '__main__' == __name__:
    df = pd.DataFrame({'a':[5,8,3,9,2],'b':[1,2,3,4,5],'c':[1,2,3,4,6]})
    HTML_writer('Hello World.html','a','b',df)
    #pdfkit.from_file(root + 'Hello World.html',root + 'out.pdf')
    print ('done')
