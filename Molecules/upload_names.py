# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 06:58:09 2016

@author: Alex
"""

#from LipPy_v2_2.Molecules import upload_names
import os, sys
import pandas as pd
import numpy as np
import mysql.connector
#from ..Lookups import database_tables
from LipPy_v2_2.Lookups import database_tables
from collections import OrderedDict
import pdb


def append_names_to_df(data, to_append, cnx):
    """
    Used to add names to the nametable mysql database    
    data can be a file location as a csv or a pandas.DataFrame
    to_append should be the name of the existing table.
    CURRENTLY DOES NOT CHECK FOR DUPLICATES!!!!!!!!!!!!!!!!!
    """
    if type(data) != type(pd.DataFrame()):
        try:
            df = pd.read_csv(data)
        except:
            ValueError("data can not be read as a csv")
    else:
        df = data.copy()
    rename_dict = {'Precursor':'precursor',
                   'Fragment':'fragment',
                   'Name':'lipid_name',
                   'Group':'lipid_group'}
    new_name = df.rename(columns = rename_dict)
    existing_query = "SELECT * FROM "+to_append
    existing_names = pd.read_sql_query(existing_query, cnx)
    total_names = existing_names.append(new_name, ignore_index = True)
    #pdb.set_trace()
    if total_names.isnull().values.any():
        raise ValueError('Nans in names list please remove them')
    col_error = []
    for col in total_names:
        if not col in existing_names.columns.tolist():
            col_error.append(total_names)
    if col_error != []:
        raise ValueError('additional_cols: ' + str(col_error))
    else:
        new_name.to_sql(to_append, cnx, if_exists='append', flavor='mysql',
                        index = False, chunksize = 200)

def add_standard_mix(data, standard_name, cnx):
    """
    used to add a standard to the database, does not check to see if it is already in
    IT WILL OVER WRITE!!!
    """
    if type(data) != type(pd.DataFrame()):
        try:
            df = pd.read_csv(data)
        except:
            ValueError("data can not be read as a csv")
    else:
        df = data.copy()
    df.to_sql(standard_name, cnx, if_exists='replace',  flavor='mysql', index = False)

def check_overlap(df):
    df = df.copy()
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    try:
        overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    except KeyError:
        overlaps = pd.DataFrame()
    return overlaps
    
def find(df, a, b, accuracy = .3):
    '''
    used to find a pre and fragment with accuracy
    '''  
    a1 = a-accuracy
    a2 = a+accuracy
    b1 = b-accuracy
    b2 = b+accuracy    
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    if check.empty and for_isotope == True:
        return None
    else:
        return check.index


if False:
    cnx=mysql.connector.connect(user = database_tables.db_login_info['user'], 
                                password = database_tables.db_login_info['password'],
                                host=database_tables.db_login_info['host'],
                                database = database_tables.db_login_info['name_lists'])
    pos_file = r'D:\Alex Treacher\Projects\new_names\test_set\pos_test.csv'
    neg_file = r'D:\Alex Treacher\Projects\new_names\test_set\neg_test.csv'
    append_names_to_df(pos_file,'positive_master', cnx)
    append_names_to_df(neg_file,'negative_master', cnx)
    