# -*- coding: utf-8 -*-
"""
Created on Wed Dec 21 07:18:09 2016

@author: Alex
"""

import os, sys
import time
import pandas as pd
import numpy as np
from collections import OrderedDict
import json
import mysql
import Lookups.database_tables as dt
import experiment_class as ex
import ast
import pdb
import subprocess
from pipeline_experiment_job import *

run = False

if run == True:
    while True:
        job_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                                password = dt.db_login_info['password'],
                                host = dt.db_login_info['host'],
                                database = dt.db_login_info['job_db'])
        
        lipid_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                                password = dt.db_login_info['password'],
                                host = dt.db_login_info['host'],
                                database = dt.db_login_info['lipid_db'])
        ids = get_incomplete_job_experiments(job_cnx)
        if ids != []:
            try:
                job_id = min(ids)
                complete_job_experiment(job_id, job_cnx, lipid_cnx)
            except Exception as e:
                update_job_progress(job_id, 0, e, job_cnx)
        else:
            time.sleep(15)
        job_cnx.close()
        lipid_cnx.close()
else:
    job_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                            password = dt.db_login_info['password'],
                            host = dt.db_login_info['host'],
                            database = dt.db_login_info['job_db'])
    
    lipid_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                            password = dt.db_login_info['password'],
                            host = dt.db_login_info['host'],
                            database = dt.db_login_info['lipid_db'])
    ids = get_incomplete_job_experiments(job_cnx)
    if ids != []:
        job_id = min(ids)
        complete_job_experiment(job_id, job_cnx, lipid_cnx)
    