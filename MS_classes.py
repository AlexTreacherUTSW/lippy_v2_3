# -*- coding: utf-8 -*-
"""
Created on Mon Sep 22 13:27:35 2014

@author: JMLab1
"""

import os, sys
import pandas as pd
import numpy as np
import time
#look into
import Stats.LipidFunctions as lf
import Stats.stats as stat
from Molecules.Lipids import *
from Molecules.mass_pairs import MassPair
from collections import defaultdict, OrderedDict
# look into
from scipy.stats import ttest_ind, ranksums
#for probability?
from itertools import combinations
#look into
from Stats.Rescaling import *
from settings import *
#?
from utilities import quick_read
import utilities
from Lookups import name_lookup
#for locate
from bisect import bisect_left
import copy
import pdb
import json
import matplotlib.pyplot as plt
import shelve
from random import sample
from Lookups import database_tables
import pdb
pd.options.mode.chained_assignment = None
cur_dir = os.path.dirname(os.path.realpath(__file__))+os.sep
template_dir = cur_dir+'Templates'+os.sep
test_dir = cur_dir+'TestData'+os.sep
lookup_dir = cur_dir+'Lookups'+os.sep
symbols = ['v', '>', '^', '<', 'o', 's', 'p']

class MS_Process:
    
    def __init__(self, data, names, mode, standard_mix = None, omit=None):#,std_amt = 20):
        """
        
        """
        self.names_file = names
        self.omit = omit
        self.mode = mode
        self.data_file = data
        self.mass_cols = ['Mass_Pair', 'Precursor', 'Fragment', 'neutral_loss', 'Mode']
        self.standard_mix = standard_mix
        self.name_table = self.read_names()
        self.sample_names = self.name_table.sample_name.tolist()
        self.clean_data = self.pre_process()

    def pre_process(self):
        """
        Processes MS/MS of everything raw text file into a pandas data frame
        with the correct columns.  Name table must be read before this is called.
        """
        if type(self.data_file) == pd.DataFrame:
            df = self.data_file.copy()
        else:
            df = pd.read_table(self.data_file, skiprows = [1,2], header=0, index_col = None)
        if df.columns.tolist() == ["This sample is corrupted. Select another sample or file. (Could not find file 'N:\\Alex_T\\For_testing_RMV\\120915_Plasma_DilSet_PBSvsW\\-MSALL\\Liver_Mix_Standard_Start-MSALL.wiff'.)"]:
            raise IOError('MarkerView file corrupted, please change and rerun')
        try:
            Pair = df['Sample Name'].apply(lambda x: MassPair.from_string(x, ascending=True))
        except AttributeError:
            raise IOError('Input MSAll file'+self.data_file+' flagged as likely TOF.')
        del df['Sample Name']
        Precursor = Pair.apply(lambda x : x.ms1)
        Fragment = Pair.apply(lambda x: x.ms2)
        Neutral_Loss = Precursor - Fragment
        self.sample_names = self.name_table.sort_values('sample_number').sample_name.tolist()
        if len(df.columns) != len(self.sample_names):
            raise ValueError('Different number of samples in data than in name table')
        try:
            df = df[self.sample_names]
            print('MS_Classes - Names file matched up with names on sample Data')
        except KeyError:
            print ('MS_Classes - Names file does not match up with names on sample Data, please check for alignment issues')
            df.columns = range(1,df.shape[1]+1)
            try:
                df = df[df.columns.intersection(self.name_table.sample_number)]
                df.columns = self.sample_names
            except:
                self.sample_names = map(lambda x : 's'+str(x), df.columns)
                table_gen = pd.DataFrame({'sample_number' : df.columns.tolist(),
                                          'group_number' : ['NA']*len(df.columns),
                                          'sample_name' : self.sample_names,
                                          'group_name' : ['NA']*len(df.columns)})
                self.name_table = table_gen
                df.columns = self.sample_names        
                           
        df['Mass_Pair'] = Pair
        df['Precursor'] = Precursor
        df['Fragment'] = Fragment
        df['neutral_loss'] = Neutral_Loss
        df['Mode'] = self.mode
        cols = self.mass_cols+self.sample_names
        '''
        df = pd.read_table(self.data_file, skiprows=
                            [0,1,2], header=None, index_col=None)
        df = df.drop_duplicates(0)
        try:
            Fragment = df[0].apply(lambda x : float(x.split('_')[0]))
        except AttributeError:
            raise IOError('Input MSAll file'+self.data_file+' flagged as likely TOF.')
        Pair = df[0].apply(lambda x: MassPair.from_string(x, ascending=True))
        del df[0]
        Precursor = Pair.apply(lambda x : x.ms1)
        Fragment = Pair.apply(lambda x: x.ms2)
        Neutral_Loss = Precursor - Fragment
        try:
            df = df[df.columns.intersection(self.name_table.sample_number)]
            df.columns = self.sample_names
        except:
            self.sample_names = map(lambda x : 's'+str(x), df.columns)
            table_gen = pd.DataFrame({'sample_number' : df.columns.tolist(),
                                      'group_number' : ['NA']*len(df.columns),
                                      'sample_name' : self.sample_names,
                                      'group_name' : ['NA']*len(df.columns)})
            self.name_table = table_gen
            df.columns = self.sample_names        
                    
        df['Mass_Pair'] = Pair
        df['Precursor'] = Precursor
        df['Fragment'] = Fragment
        df['neutral_loss'] = Neutral_Loss
        df['Mode'] = self.mode
        cols = self.mass_cols+self.sample_names
        '''
        return df[cols]

    def read_names(self):
        """
        Parses name table, checking that it is properly formmated along the way.
        Returns the parsed table.
        """
        if type(self.names_file) == pd.DataFrame:
            name_table = self.names_file.copy()
        else:
            name_table = pd.read_csv(self.names_file, skiprows=self.omit)
        sanitize = lambda x : x.strip().replace(' ', '_').replace('.', '_')
        ##capitalize first letter of each word in name table columns and replace whitespace with underscores
        name_table.columns = map(sanitize, name_table.columns) 
        example_cols = ['sample_number','group_number','sample_name','group_name', 'sample_amount','standard_volume']
        #next couple of line change the names file from an old format to a new one if needed
        new_cols_dct = OrderedDict()
        new_cols_dct['Sample_Number'] = 'sample_number'
        new_cols_dct['sample_id'] = 'sample_number'
        new_cols_dct['Sample_ID'] = 'sample_number'
        new_cols_dct['Group'] = 'group_number'
        new_cols_dct['Name'] = 'sample_name'
        new_cols_dct['Group_Name'] = 'group_name'
        new_cols_dct['Sample_Amount'] = 'sample_amount'
        new_cols_dct['Standard_Volume'] = 'standard_volume'
        for col in new_cols_dct:
            if col in name_table:
                name_table[new_cols_dct[col]] = name_table[col]
                del name_table[col]
        #map(sanitize, pd.read_csv(lookup_dir+'names_checker.csv').columns)
        try:
            assert 'sample_number' in name_table.columns
            assert 'group_number' in name_table.columns
            assert 'sample_name' in name_table.columns
            assert 'group_name' in name_table.columns     
            for heading in name_table.columns:
                assert heading in example_cols
        except:
            raise IOError('Column headings in %s not properly formatted.  Column heads should be: \'sample_number\' \'group_number\', \'group_name\' followed by optional columns: \'sample_amount\', \'standard_volume\'')
            #name_table.columns = ['sample_number', 'group_number', 'sample_name', 'group_name', 'Type']
        if 'standard_volume' not in name_table.columns and self.standard_mix != None:
            name_table['standard_volume'] = 20
        name_table['group_name'] = name_table['group_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_')) ##Remove trailing whitespace in group names    
        name_table['sample_name'] = name_table['sample_name'].apply(lambda x : x.strip().replace(' ', '_').replace('.', '_'))
        type_dct = {'sample_number':int,
                    'sample_name':str,
                    'group_number':int,
                    'group_name':str,
                    'sample_amount':float,
                    'standard_volume':float}
        for col in type_dct.keys():
            if col in name_table.columns.tolist():
                name_table[col] = name_table[col].astype(type_dct[col])
        try:
            assert all([isinstance(name_table.sample_number[i], (int, float, np.int64, np.float64)) 
                        for i in name_table.index])
            assert all([isinstance(name_table.group_number[i], (int, float, np.int64, np.float64))
                        for i in name_table.index])
        except AssertionError:
            raise IOError('Not all entries in the Sample Number or group columns in your names file are integers.  Correct your names file and try again.')
        if name_table['sample_name'].duplicated().any():
            raise IOError('File '+self.names_file+' contains duplicates in the sample names column.  Give each sample a distinct name and run again.')
        return name_table

    def saturation_check(self, filename, directory):
        from QualityControl.Saturation import sample_saturation, saturation_report
        mode_dict = {'+' : 'pos', '-' : 'neg'}
        df = self.clean_data.copy()
        if filename.endswith('.html'):
            filename = filename.strip('.html')+'_'+mode_dict[self.mode]+'.html'
        else:
            filename = filename+'_'+mode_dict[self.mode]+'.html'
        #df.Mass_Pair = df.Mass_Pair.apply(MassPair.from_string)
        sample_cols = df.columns.difference(self.mass_cols)
        df.index = df.Mass_Pair
        images = []
        for c in sample_cols:
            spectrum = df[c].copy()
            new_images = sample_saturation(spectrum, f_header=c)
            images.append(new_images)
        saturation_report(images, filename, directory)

    def to_MS_data(self):
        """
        
        """
        standards = database_tables.get_standard(self.standard_mix)
        if type(standards) == type(None):
            stand_dict = {'Observed' : {'+' : None, '-' : None}, 'Input' : {'+' : None, '-' : None}}
        else:
            stand_dict = {'Observed' : {'+' : None, '-' : None}, 'Input' : {'+' : None, '-' : None}}
            #standards.index = standards.Name
            stand_dict['Input']['+'] = standards[standards.Mode == '+']
            stand_dict['Input']['-'] = standards[standards.Mode == '-']
            #stand_dict['Input']['+'].index =  stand_dict['Input']['+'].Normalize_Group
            #stand_dict['Input']['-'].index = stand_dict['Input']['-'].Normalize_Group
        return MS_Data(self.clean_data, self.name_table, mode=self.mode, ms_standards=stand_dict)

class MS_Data:
    """
    Represents a dataset from a tandem mass spectrometry experiment.  The object
    is specified by two pandas DataFrames
        1) The data frame with the ms1, ms2, and sample intensities.
        2) The names_table data frame.
    
    Implements common procedures such as filtering and naming of molecules, 
    hypothesis tests, normalizations, restriction to types and groups of lipids, 
    data subsetting by dropping of groups, combination with other MS_data objects.
    
    
    Parameters
    ----------
    data_frame : pandas.DataFrame
        Processed and parsed data from the mass spectrometer.  Must have
        columns Mass_Pair, Precursor, Fragment, neutral_loss.
    
    names : pandas.DataFrame
        Contains sample numbers,sample names,  group names, group numbers, and
        optional columns that may contain information on standards and sample 
        weight.
    
    mode : string or NoneType -- '+', '-', or None -- default None
        The mode that the dataset has been obtained in.  This affects name and
        standard lookups.  Combined positive and negative mode data has mode None.
        
    ms_standards : dict or None -- default None
        If the data set was run without standards, this should be None.  If the
        data set was run with standards, the input format is as follows:
        
            self.standards = {'Observed' : {'+' : pos_observed_standards, '-' : neg_observed_standards}, 
                                  'Input' : {'+' : pos_input_standards, '-' : neg_input_standards}}

        All terminal values must be pandas DataFrames.  Note that the terminal 
        values for 'Observed' may be None if the molecules have not yet been named;
        these will be filled in when the Name_Molecules method is called.
        Initializing an object by passing files through the MS_Process object
        pipeline will keep the programmer from having to deal with the ms_standards
        input.
        
    
    Examples
    --------
    >>> pos_data_set = MS_data(pos_data, names, mode='+')
    >>> neg_data_set = MS_data(neg_data, names, mode='-')
    >>> data_set = pos_data_set.combine(neg_data_set)
    >>> named_data = data_set.Name_Molecules()
    >>> normed_named_data = named_data.normalize()
    
    See also
    --------
    LipPy.MS_data.quick_read_from_files
    LipPy.MS_data.from_raw_data_files
    LipPy.MS_Process
    """

    @staticmethod
    def from_raw_data_files(raw_data, name_file, mode, standard_mix = None, omit=None):
        """
        Constructs an MS_data object from a raw data files.
        
        Returns
        ----------
        An MS_data object
    
        Parameters
        ----------
        raw_data : string
            The name of the file containing the raw experimental data.
            
        name_file : string
            The name of the file with the name table.  This can be none.
            
        mode : string or NoneType -- '+', '-', or None -- default None
            The mode that the dataset has been obtained in.  This affects name and
            standard lookups.
            
        Examples
        --------
            >>> data_set = MS_data.from_raw_data_files('data_name.txt', 'names.csv', '+')
        """
        process = MS_Process(raw_data, name_file, mode, standard_mix = standard_mix, omit=omit)
        data = process.to_MS_data()
        return data
        
    @staticmethod
    def from_db_experiment(experiment_id, cnx, iso_cor = True, std_norm = True, rename = False, name_file = None, mode=None):
        """
        used to create a ms_class from the database
        requires valid connection to the database and will not close it at the end
        
        rename
        ------
        Used to apply the most recent names list to the data, usually only used when
            the data is being moved to a newer database
        """
        if mode == None:
            pos = MS_Data.from_db_experiment(experiment_id, cnx, iso_cor, std_norm, rename, name_file, '+')            
            neg = MS_Data.from_db_experiment(experiment_id, cnx, iso_cor, std_norm, rename, name_file, '-')
            if type(neg) == type(None):
                return pos
            elif type(pos) == type(None):
                return neg
            elif type(pos) == type(None) and type(neg) == type(None):
                return None
            else:
                comb = pos.combine(neg)
                return comb
        else:
            query1 = "SELECT * FROM meta_sample WHERE experiment_id = " + str(experiment_id) + ";"
            meta_samples = pd.read_sql_query(query1, cnx)
            if name_file == None:
                new_names = OrderedDict([('sample_id','sample_number'),
                                         ('sample_name','sample_name'),
                                         ('group_number','group_number'),
                                         ('group_name','group_name'),
                                         ('standard_volume','standard_volume'),
                                         ('sample_amount','sample_amount')])
                name_file = meta_samples.rename(columns = new_names)[new_names.values()]
            else:
                name_file = name_file.copy()
            if iso_cor == True and std_norm == True:
                data_col = 'data_ic_std_norm'
            elif iso_cor == True and std_norm == False:
                data_col = 'data_ic'
            elif iso_cor == False and std_norm == True:
                data_col = 'data_raw_std_norm'
            elif iso_cor == False and std_norm == False:
                data_col = 'data_raw'
            else:
                raise ValueError('Value combination for iso_cor and std_norm invalid')
            sids = meta_samples['sample_id'].tolist()
            sample_data = OrderedDict()
            for sid in sids:
                sample_name = meta_samples[meta_samples.sample_id == sid].iloc[0]['sample_name']
                data_cols_query = 'SELECT data_cols FROM data_ms WHERE sample_id = ' + str(sid) + ' AND mode = "' + mode + '";'
                data_cols_str = pd.read_sql(data_cols_query, cnx).loc[0]['data_cols']
                data_cols_temp= OrderedDict(json.loads(data_cols_str))
                data_cols = OrderedDict()
                for col in data_cols_temp:
                    data_cols[int(col)] = data_cols_temp[col]
                data_query = 'SELECT ' + data_col + ' FROM data_ms WHERE sample_id = ' + str(sid) + ' AND mode = "' + mode + '";'
                data_str = pd.read_sql_query(data_query, cnx).loc[0][data_col]
                if data_str == 'None':
                   sample_data[sample_name] = None
                else:
                    data = pd.read_json(data_str)
                    data.rename(columns = data_cols, inplace= True)
                    data.sort_values(by = ['Precursor','Fragment'], inplace = True)
                    data['Mass_Pair'] = data.apply(make_mass_pair_str, axis = 1)
                    data.set_index('Mass_Pair', inplace = True, drop = True)
                    sample_data[sample_name] = data
            for sample_name in sample_data:
                try:
                    data = sample_data[sample_name]
                    base_df = data[['Name','Group','Precursor','Fragment']]
                    break
                except TypeError:
                    pass
            try: base_df
            except NameError:
                return None
            for sample_name in sample_data:
                to_add = sample_data[sample_name][sample_name]
                base_df = pd.concat([base_df, to_add], axis = 1)
            base_df['neutral_loss'] = np.subtract(base_df['Precursor'],base_df['Fragment'])
            base_df.reset_index(inplace=True, drop=False)
            col_order = ['Mass_Pair','Name','Group','Precursor','Fragment','neutral_loss'] + sample_data.keys()
            data_df = base_df[col_order]
            #need to get standards still
            only_samples = meta_samples[meta_samples.group_number > 0]
            standard_name_list = list(set(only_samples.internal_standard.tolist()))
            if len(standard_name_list) > 1:
                standard_name = None
            elif standard_name_list[0] == 'None':
                standard_name = None
            else:
                standard_name = standard_name_list[0]
            input_standard = database_tables.get_standard(standard_name)
            standards = {'Observed' : {'+' : None, '-' : None}, 
                         'Input' : {'+' : None, '-' : None}}
            if type(input_standard) != type(None):
                standards['Input']['+'] = input_standard[input_standard.Mode == '+']
                standards['Input']['-'] = input_standard[input_standard.Mode == '-']
                mode_standards = standards['Input'][mode]
                standard_names = mode_standards.Name.tolist()
                is_std_df = data_df.Name.apply(lambda x: x in standard_names)
                std_df = data_df[is_std_df]
                if std_df.empty:
                    standards['Observed'][mode] = None
                else:
                    standards['Observed'][mode] = data_df[is_std_df]
            return MS_Data(data_df, name_file, mode = mode, ms_standards=standards)

    @staticmethod
    def from_old_db_experiment(experiment_id, cnx, iso_cor = True, std_norm = True, rename = False):
        """
        NOTE: THIS SHOULD BE REDUDENT NOW, USED TO TRANSFER DATA FROM AN OLD DATABASE TO THE CURRENT        
        
        used to create a ms_class from the database
        requires valid connection to the database and will not close it at the end
        
        rename
        ------
        Used to apply the most recent names list to the data, usually only used when
            the data is being moved to a newer database
        """
        query1 = "SELECT * FROM sample_info WHERE experiment_id = " + str(experiment_id) + ";"
        sample_ids = pd.read_sql(query1, cnx)
        possible_internal_standards = []
        sample_data = OrderedDict()
        for i in sample_ids.index:
            sample_id = sample_ids.loc[i]['sample_id']
            sample_name = sample_ids.loc[i]['sample_name']
            sample_group = sample_ids.loc[i]['group_number']
            if sample_group >= 1:
                possible_internal_standards.append(sample_ids.loc[i]['internal_standard'])
            if iso_cor == False:
                query2 = "SELECT * FROM ms_data WHERE sample_id = " + str(sample_id) + ";";
            else:
                query2 = "SELECT * FROM ic_ms_data WHERE sample_id = " + str(sample_id) + ";";
            #startt = time.time()
            df = pd.read_sql(query2, cnx)
            #stopt = time.time()
            #print "time for sample" + str(sample_id) +  " : " + str(stopt-startt)
            sample_data[sample_name] = process_db(df ,sample_name, std_norm = std_norm)
        all_samples = pd.concat(sample_data.values(), axis = 1)
        all_samples.reset_index(inplace = True)
        all_samples.fillna(0, inplace= True)  
        all_samples.sort_values('name', inplace = True)
        sample_ids['sample_id']
        #to_sub = min(sample_ids['sample_id'])
        #if to_sub <= 0 :
        #    sample_ids['Sample_Number'] = sample_ids['sample_id']
        #else:
        #    sample_ids['Sample_Number'] = sample_ids['sample_id']-(to_sub-1)
        new_names = {'sample_id':'Sample_Number',
                     'sample_name':'Name',
                     'group_number':'Group',
                     'group_name':'Group_Name',
                     'standard_volume':'Standard_Volume',
                     'sample_amount':'Sample_Amount'}
        new_data_names = {'mass_pair': 'Mass_Pair',
                          'mode':'Mode',
                          'name':'Name',
                          'lipid_group':'Group',
                          'lipid_type':'Type',
                          'precursor':'Precursor',
                          'fragment':'Fragment',
                          'neutral_loss':'neutral_loss'}
        all_samples.rename(columns = new_data_names, inplace = True)
        all_samples.Name = all_samples.apply(make_lipid_from_col, axis = 1)
        pos = all_samples[all_samples.Mode == '+']
        neg = all_samples[all_samples.Mode == '-']
        pos_raw = format_to_raw_file(pos)
        neg_raw = format_to_raw_file(neg)
        name_table_query = "SELECT sample_id, sample_name, group_number, group_name, standard_volume, sample_amount FROM sample_info WHERE experiment_id = " + str(experiment_id) + ';'
        name_table = pd.read_sql_query(name_table_query, cnx)
        std_table_query = "SELECT internal_standard FROM sample_info WHERE experiment_id = " + str(experiment_id) + ';'
        std = pd.read_sql_query(std_table_query, cnx)['internal_standard'].tolist()[0].lower()
        if std == 'none' or std == 'None':
            std = None
        pos = MS_Data.from_raw_data_files(pos_raw, name_table, '+', std)
        neg = MS_Data.from_raw_data_files(neg_raw, name_table, '-', std)
        comb = pos.combine(neg)
        return comb

    @staticmethod
    def combine_ms_samples(ms_classes):
        """
        might need to be added, combine single sample_ms classes
        standards and mode need to be the same
        """
        pass
        
    def __init__(self, data_frame, names, mode=None, ms_standards=None):
        self.data_frame = data_frame
        self.name_table = names
        self.mass_cols = ['Mass_Pair', 'Precursor', 'Fragment', 'neutral_loss', 'Mode']
        self.sample_dict = dict(zip(self.name_table.sample_number, self.name_table.sample_name))
        self.group_num_dict = dict(zip(self.name_table.group_number, self.name_table.group_name))
        self.groups_dict = self.get_all_groups_dict()
        self.mode = mode
        #samples that are not a blank or standard
        self.sample_groups_dict = self.get_sample_groups_dict()
        self.sample_cols = self.name_table[self.name_table.group_number>0].sort_values(by = ['group_number']).sample_name.tolist()
        self.control_cols = self.name_table[self.name_table.group_number<=0].sort_values(by = ['group_number']).sample_name.tolist()
        self.all_sample_cols = self.sample_cols+self.control_cols  
        if ms_standards is None:
            self.standards = {'Observed' : {'+' : None, '-' : None}, 
                              'Input' : {'+' : None, '-' : None}}
        else:
            self.standards = ms_standards
            for k in self.standards['Observed'].keys():
                if isinstance(self.standards['Observed'][k], pd.DataFrame):
                    observed = self.standards['Observed'][k].copy()
                    observed_keys = observed.columns.tolist()
                    for s in self.all_sample_cols:
                        observed_keys.remove(s)
                    observed = observed[observed_keys+self.all_sample_cols]
                    self.standards['Observed'][k] = observed
        self.name_cols = ['Name', 'Group', 'Type', 'MS1_Formula', 'MS2_Formula']
        if mode != None or 'Mode' not in self.data_frame.columns and mode != 'Mixed':
            self.data_frame['Mode'] = self.mode
        try:
            self.data_frame = self.data_frame[self.name_cols+self.mass_cols+self.all_sample_cols]
        except KeyError:
            try:
                self.data_frame = self.data_frame[['Name', 'Group'] + self.mass_cols +self.all_sample_cols]
                self.data_frame['Name'] = self.data_frame.apply(lambda x: Lipid.from_group(x['Group'],x['Name'],x['Mode'],mass_pair=([x['Precursor'],x['Fragment']])), axis = 1)
                self.data_frame['Type'] = self.data_frame.apply(lambda x: x['Name'].get_type(), axis = 1)
                formulas = self.data_frame.apply(lambda x: x['Name'].formula_string(), axis = 1)
                self.data_frame['MS1_Formula'] = formulas.apply(lambda x: x[0])
                self.data_frame['MS2_Formula'] = formulas.apply(lambda x: x[1])
                self.data_frame = self.data_frame[self.name_cols+self.mass_cols+self.all_sample_cols]
            except KeyError:   
                self.data_frame = self.data_frame[self.mass_cols+self.all_sample_cols]
        self.data_frame.sort_values(by=['Mode','Precursor','Fragment'], inplace=True)
        
    def shape(self):
        """
        Parameters
        ----------
        None
        
        Returns
        ----------
        A numpy array containing the dimensions of the experimental data set.
        """
        return np.shape(self.data_frame)
        
    def __len__(self):
        return len(self.data_frame.index)
        
    def __repr__(self):
        return self.data_frame.__repr__()
            
    def combine(self, other):
        """
        Combines self with another MS_Data object with the same name_table into
        a single MS_Data object.
        Parameters
        ----------
        other : MS_Data
            The MS_Data object that will be combined with self.  self and other
            should have different modes, else a warning will be raised -- though
            the code will still run.
        
        Returns
        ----------
        An MS_Data object with the experimental data combined into one DataFrame
        with the modes labeled and the standard dictionaries combined.
        """
        new_mode = None
        new_standards = self.standards.copy()
        if self == None and other == None:
            return MS_Data_None()
        elif self == None:
            return other
        elif other == None:
            return self.copy()
        else:
            try:
                assert self.mode != other.mode
            except AssertionError:
                print "MS_Classes - Warning: modes of MS_Data objects to be combined are both "+self.mode+"."
                new_mode = self.mode
            new_df = pd.concat([self.data_frame, other.data_frame])
            if new_df.index.duplicated().any():
                try:
                    assert new_df.index.is_integer()
                    new_df = new_df.reset_index(drop=True)
                except AssertionError:
                    raise ValueError('Duplicates in non-integer index.  Find Mike immediately!')
            for i in ['Observed', 'Input']:
                new_standards[i][self.mode] = self.standards[i][self.mode]
                new_standards[i][other.mode] = other.standards[i][other.mode]
            if new_df.index.duplicated().any():
                new_df = new_df.reset_index(drop=True)
            return MS_Data(new_df, self.name_table, mode=new_mode, ms_standards=new_standards)
            
    def copy_standards(self):
        new_stds = {}
        for i_o in self.standards.keys():
            new_stds[i_o] = {}
            for mode in self.standards[i_o].keys():
                try:
                    new_stds[i_o][mode] = self.standards[i_o][mode].copy()
                except AttributeError:
                    new_stds[i_o][mode] = self.standards[i_o][mode]
        return new_stds
    
    def remove_sample(self, samples):
        """
        Takes a list of sample(s) and returns a MS_class with out those samples
        can also accept a single samples name
        """
        if not isinstance(samples, list):
            samples = [samples]
        df = self.data_frame.copy()
        nt = self.name_table.copy()
        new_stds = self.copy_standards()
        for samp in samples:
            if not samp in df.columns.tolist():
                print str(samp) + 'sample not in data'
                return MS_Data(df, nt, mode=self.mode[:], ms_standards=new_stds)
            else:
                del df[samp]
                nt = nt[nt.sample_name != samp]
            for mode in new_stds['Observed'].keys():
                if isinstance(new_stds['Observed'][mode], pd.DataFrame):
                    del new_stds['Observed'][mode][samp]
        return MS_Data(df, nt, mode=self.mode, ms_standards=new_stds)
                    
    def groups_present(self, as_str=True):
        """
        Returns
        ----------
        A list of lipid groups present in the data set.  If data are not named,
        the Name_Molecules method will be called to determine which groups are
        present.
        
        Parameters
        ----------
        as_str : bool, default True
            If True, a list of strings is returned, if False, the class objects
            themselves populate the list.
        
        See also
        ----------
        MS_data.types_present()
        """
        if self == None:
            return []
        if not 'Group' in self.data_frame.columns:
            return self.Name_Molecules().groups_present()
        else:
            groups = self.data_frame.Group.drop_duplicates().tolist()
            for g in groups:
                if 'standard' in g.lower():
                    groups.remove(g)
            try:
                groups.remove('Unknown')
            except ValueError:
                pass
            if as_str:
                if not all([isinstance(g, str) for g in groups]):
                    groups = map(lambda x : x.__name__ 
                                            if type(x)==type else str(x),
                                                                  groups)
        return groups
            
    def types_present(self, as_str=True):
        """
        Returns
        ----------
        A list of lipid types present in the data set.  If data are not named,
        the Name_Molecules method will be called to determine which groups are
        present.
        
        Parameters
        ----------
        as_str : bool, default True
            If True, a list of strings is returned, if False, the class objects
            themselves populate the list.
        
        See also
        ----------
        MS_data.groups_present()
        MS_data.restrict_to_group
        MS_data.restrict_to_type
        """
        if self == None:
            return []
        if not 'Group' in self.data_frame.columns:
            return self.Name_Molecules().types_present()
        else:
            types = self.data_frame.Type.drop_duplicates().tolist()
            try:
                types.remove('Unknown')
            except ValueError:
                pass
            if as_str:
                if not all([isinstance(t, str) for t in types]):
                    types = map(lambda x : x.__name__ 
                                          if type(x)==type else str(x), types)
            return types

    def get_all_groups_dict(self):
        """
        Constructs group dictionary with the group names as keys.
        Use attribute self.groups_dict instead.
        """
        groups_dict = defaultdict(list)
        for group in self.name_table.group_number.drop_duplicates():
            groups_dict[group] = self.name_table[self.name_table.group_number == group].sample_name.tolist()    
        return groups_dict
    
    def get_sample_groups_dict(self):
        """
        Contructs groups dictionary with the group names as keys,
        Uses the self.groups_dict and only takes the keys with positive numbers
        """
        dct = {}
        for k in self.groups_dict.keys():
            if k > 0:
                dct[k] = self.groups_dict[k]
        return dct
    
    def get_all_sample_groups_dict(self):
        dct = {}
        for k in self.groups_dict.keys():
            dct[k] = self.groups_dict[k]
        return dct

    def groups_in_types_dict(self , exclude_standards = True):
        '''
        returns a dict of each type as the key, and groups in each type 
        as the values 
        
        if exclude_standards == Ture remove the standards from the list
        '''        
        dct = {}
        to_skip = ['Isotope', 'Misc','Unknown' , 'PA']
        for t in self.types_present():
            if t not in to_skip:            
                t_df = self.data_frame[self.data_frame['Type'] == t]
                groups_list = list(set((t_df['Group'])))
                for r in to_skip:
                    if r in groups_list:
                        groups_list.remove(r)
                dct[t] = groups_list
        if exclude_standards == True:
            dct = {k:[elem for elem in v if 'standard' not in elem.lower()] for k,v in dct.iteritems()}
        return dct

    def center_samples(self):
        samples = self.data_frame[self.all_sample_cols].copy()
        centered = samples - samples.mean(axis=1)
        other_cols = [c for c in self.data_frame.columns if c not in self.all_sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([centered, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def standardize_samples(self):
        samples = self.data_frame[self.all_sample_cols].copy()
        standardized = (samples - samples.mean(axis=1)) / samples.std(axis=1)
        other_cols = [c for c in self.data_frame.columns if c not in self.all_sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([standardized, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def pareto_scale_samples(self):
        samples = self.data_frame[self.all_sample_cols].copy()
        par_scaled = (samples - samples.mean(axis=1)) / np.sqrt(samples.std(axis=1))
        other_cols = [c for c in self.data_frame.columns if c not in self.all_sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([par_scaled, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def log_transform_samples(self, base='exp'):
        samples = self.data_frame[self.all_sample_cols].copy()
        if base == 'exp':
            b = np.e
        log_transformed = np.log(samples) / np.log(b) ##denominator is for log change-of-base formula
        other_cols = [c for c in self.data_frame.columns if c not in self.all_sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([log_transformed, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)
        
    def range_scale_data(self):
        samples = self.data_frame[self.all_sample_cols].copy()
        centered = samples - samples.min(axis=1)
        rng = samples.max(axis=1) - samples.min(axis=1)
        range_scaled = centered / rng
        other_cols = [c for c in self.data_frame.columns if c not in self.all_sample_cols]
        to_merge = self.data_frame[other_cols]
        merged = pd.concat([range_scaled, to_merge], axis=1)
        ordered = merged[self.data_frame.columns]
        return MS_Data_Like(ordered, self)

    def NL_Filter(self, threshold=-1):
        """
        Neutral loss filter.  Returns MS_data where the new data_frame contains
        only entries where the neutral loss is greather than threshold.
        By default, threshold = -1
        """
        if self == None:
            return MS_Data_None()
        df = self.data_frame
        filtered_data = df[df.neutral_loss > threshold]
        filtered_data = filtered_data#.reset_index(drop=True)
        return MS_Data(filtered_data, self.name_table, self.mode, self.standards)

    def Average_and_Max(self, avg_thresh = 100, max_thresh = 300, samples_to_filter = None):
        """
        
        """
        if self == None:
            return MS_Data_None()
        if samples_to_filter == None:
            samples_to_filter = self.sample_cols
        return MS_Data(lf.Remove_Average_Max(self.data_frame,
                                             samples_to_filter,
                                             average = avg_thresh,
                                             maximum = max_thresh),
                        self.name_table, self.mode, self.standards)
    
    def Low_Average_Filter(self, threshold = 100, samples_to_filter = None):
        """
        
        """
        if self == None:
            return MS_Data_None()
        if samples_to_filter == None:
            samples_to_filter = self.sample_cols
        return MS_Data(lf.Remove_Low_Average(self.data_frame,
                                             samples_to_filter,
                                             average_threshold=threshold),
                       self.name_table, self.mode, self.standards)
    
    def Low_Average_Filter_byGroup(self, threshold = 100, groups_dict = None):
        """
        
        """
        if self == None:
            return MS_Data_None()
        if groups_dict == None:
            groups_dict = self.sample_groups_dict
        return MS_Data(lf.Remove_Low_Average_byGroup(self.data_frame,
                                                     groups_dict,
                                                     average_threshold=threshold),
                       self.name_table, self.mode, self.standards)        

    def Quantile_Filter(self, x_gt_y, samples_to_filter = None):
        """
        
        """
        if self == None:
            return MS_Data_None()
        if samples_to_filter == None:
            samples_to_filter = self.sample_cols
        return MS_Data(lf.Remove_Low_Freq(self.data_frame,
                                          samples_to_filter,
                                          under_thresh=x_gt_y),
                       self.name_table, self.mode, self.standards)
    
    def Quantile_Filter_byGroup(self, x_gt_y, groups_dict = None):
        """
        
        """
        if self == None:
            return MS_Data_None()
        if groups_dict  == None:
            groups_dict = self.sample_groups_dict
        return MS_Data(lf.Remove_Low_Freq_byGroup(self.data_frame,
                                                  groups_dict,
                                                  x_gt_y),
                       self.name_table, self.mode, self.standards)
    
    def Remove_Child_Peaks(self, peaks_tuple):
        """
        
        """
        if self == None:
            return MS_Data_None()
        return MS_Data(lf.Child_Peak_Filter(self.data_frame, peaks_tuple), self.name_table, self.mode, self.standards)

    #filter sets primarily used in different parts of pipelines        
    def filter_set1(self, samples=None, groups_dict=None,
                    avg_thresh = 100, maximum = 300,
                    child_peaks=[(1, 10)], NL_thresh=-1300,
                    avg_thresh_group = 100, x_gt_y = (30,100)):
        """
        Used for the all group comparison and before the group comparison
        This set is geared toward set with multiple groups
        """
        if self == None:
            return MS_Data_None()
        if samples == None:
            samples = self.sample_cols
        if groups_dict == None:
            groups_dict = self.sample_groups_dict
        filtered_data = self.Average_and_Max(avg_thresh, maximum, samples)
        filtered_data = filtered_data.Remove_Child_Peaks(child_peaks)
        filtered_data = filtered_data.NL_Filter(NL_thresh)
        filtered_data = filtered_data.Low_Average_Filter_byGroup(avg_thresh_group, groups_dict)
        filtered_data = filtered_data.Quantile_Filter_byGroup(x_gt_y, groups_dict)
        return filtered_data
        
    def filter_set2(self, avg_thresh=100, NL_thresh=-1, x_gt_y=(30, 30), child_peaks=[(1, 10)], samples=None):
        """
        This is used for the filters designed for the group comparisons
        """
        if self == None:
            return MS_Data_None()
        if samples == None:
            samples = self.sample_cols
        filtered_data = self.Remove_Child_Peaks(child_peaks)
        filtered_data = filtered_data.Low_Average_Filter(avg_thresh, samples)
        filtered_data = filtered_data.NL_Filter(NL_thresh)
        filtered_data = filtered_data.Quantile_Filter(x_gt_y, samples)
        return filtered_data
    
    def set_baseline(self, baseline, change_standards = False):
        if self == None:
            return MS_Data_None()
        new_df = self.data_frame.copy()
        info_cols = new_df.columns.tolist()
        for i in self.all_sample_cols:
            if i in info_cols:
                info_cols.remove(i)        
        info_df = new_df[info_cols]
        sample_df = new_df[self.all_sample_cols]
        num = sample_df._get_numeric_data()       
        num[num < baseline] = baseline
        baseline_df = pd.concat([info_df,sample_df],axis = 1)
        #do standards
        new_stds = {}
        for bk in self.standards.keys():
            new_stds[bk] = {}
            for mk in self.standards[bk].keys():
                try:
                    new_stds[bk][mk] = self.standards[bk][mk].copy()
                except AttributeError:
                    new_stds[bk][mk] = self.standards[bk][mk]
        if change_standards == True:
            for mode in new_stds['Observed'].keys():
                if isinstance(new_stds['Observed'][mode], pd.DataFrame):            
                    stand_df = new_stds['Observed'][mode].copy()
                    std_info_cols = stand_df.columns.tolist()
                    for i in self.all_sample_cols:
                        if i in std_info_cols:
                            std_info_cols.remove(i)
                    std_info_df = stand_df[std_info_cols]
                    std_sample_df = stand_df[self.all_sample_cols]
                    std_num = std_sample_df._get_numeric_data()
                    std_num[std_num < baseline] = baseline
                    std_baseline_df = pd.concat([std_info_df, std_sample_df],axis = 1)
                    stand_df = std_baseline_df
                    new_stds['Observed'][mode] = stand_df
        return MS_Data(baseline_df, self.name_table, self.mode, new_stds) 
    
    def Name_Molecules(self, by_name=True, to_name = None, ms1_accuracy = MS1_MASS_RANGE, ms2_accuracy = MS2_MASS_RANGE):
        """
        names molecules based on Name_Data in lipidfunctions
        to_name should be a sting that is a key of the name_lookup DB
        if not it will default to name_lookup[self.mode]
        
        """
        if self == None:
            return MS_Data_None()
        if self.mode not in ['-', '+']:
            pos = self.restrict_to('Mode', '+', '==')
            neg = self.restrict_to('Mode', '-', '==')
            pos.mode = '+'
            neg.mode = '-'
            pos_named = pos.Name_Molecules(by_name=by_name)
            neg_named = neg.Name_Molecules(by_name=by_name)
            if by_name:
                return pos_named.combine(neg_named).sort('Name')
            else:
                return pos_named.combine(neg_named)
        else:
            if to_name == None:
                naming_table = name_lookup[self.mode]
            else:
                try:
                    naming_table = name_lookup[to_name]
                except KeyError:
                    naming_table = name_lookup[self.mode]
                    print 'MS_Classes - specific name table not found, will use %s instead' %(self.mode)
            ##lookup_df has the standards concatenated with the names list
            lookup_df = concat_names_standards(naming_table,
                                               self.standards['Input'][self.mode],
                                               self.mode)
            #named = lf.Name_Data_old(self.data_frame, lookup_table_handle=lookup_df)
            named = lf.Name_Data(self.data_frame, lookup_table=lookup_df, ms1_accuracy = ms1_accuracy, ms2_accuracy=ms2_accuracy)
            if named.empty:
                cols = self.name_cols + named.columns.tolist()
                df = pd.DataFrame(columns = cols)                
                return MS_Data(df, self.name_table, self.mode, self.standards)
            else:
                #named = named[named.Group != 'Standard']
                names_and_groups = named[['Group', 'Name']].copy()
                masses = named[['Precursor', 'Fragment']].copy()
                mass_pairs = masses.apply(lambda x: MassPair(x[0], x[1]), axis=1)
                lipid_info = pd.concat((names_and_groups, mass_pairs), axis=1)
                lipid_info.columns = ['Group', 'Name', 'Masses']
                #next line turns name in to lipid class
                names = lipid_info.apply(lambda x: Lipid.from_group(x.Group,
                                                                    x.Name,
                                                                    self.mode,
                                                                    mass_pair=x.Masses), 
                                                                    axis=1)
                named['Name'] = names
                named['MS1_Formula'] = names.apply(lambda x: x.formula_string()[0])
                named['MS2_Formula'] = names.apply(lambda x: x.formula_string()[1])
                named['Type'] = names.apply(lambda x: x.get_type()) 
                #named = named[named.Group != 'Labels']
                #named = named[named.Group != 'labels']
                new_stand_dict = self.standards.copy()
                if type(self.standards['Input'][self.mode]) != type(None):
                    new_standards = named[named.Group.str.contains('Standard')]
                    new_stand_dict['Observed'][self.mode] = new_standards
                    new_stand_dict['Observed'][self.mode].index = new_standards.Name
                    new_stand_dict['Observed'][self.mode] = new_stand_dict['Observed'][self.mode][self.mass_cols+self.all_sample_cols] 
                    new_stand_dict['Observed'][self.mode].reset_index(inplace = True)
                else:
                    new_stand_dict['Observed'][self.mode] = None
                return MS_Data(named, self.name_table, self.mode, new_stand_dict)
                
    def check_standards(self, baseline = 100, soft = False):
        """
        Checks the standards in the sample
        returns list if issues if something is wrong
        returns True if ok
        if soft == True this will only check that the default standard is present
        if soft == False (recomended) this will check all standards
        """
        if self.mode == None:
            #do pos and neg
            pos = self.restrict_to_mode('+').check_standards(baseline=baseline, soft=soft)
            neg = self.restrict_to_mode('-').check_standards(baseline=baseline, soft=soft)
            if isinstance(pos, bool) and isinstance(neg, bool):
                if pos == True and neg == True:
                    return True
            else:
                if isinstance(pos, bool):
                    return neg
                elif isinstance(neg, bool):
                    return pos
                else:
                    return pos+neg
            pass
        else:
            if isinstance(self.standards['Input'][self.mode], type(None)):
                return True
            errors = []
            try:
                std_input = self.standards['Input'][self.mode].copy()
            except AttributeError:
                std_input = self.standards['Input'][self.mode]
            try:
                std_observed = self.standards['Input'][self.mode].copy()
            except AttributeError:
                std_observed = self.standards['Input'][self.mode]
            input_names = [str(x) for x in std_input['Name'].tolist()]
            observed_names = [str(x) for x in std_observed['Name'].tolist()]
            #check that the default standard is present
            default_std_df = std_input[std_input['Normalize_Group']=='default']['Name']
            if default_std_df.shape[0] != 1:
                errors.append(default_std + ' which is the default standard was found multiple times in the input')
            else:
                default_std = default_std_df.iloc[0]
                if not default_std in observed_names:
                    errors.append(default_std + ' which is the default standard is missing') 
            #check the rest of the standards
            if not soft:
                for std_name in std_input['Name']:
                    if (not std_name in observed_names) and (std_name != default_std):
                        errors.append(std_name+" standard completely missing (or filtered out) in mode:" + self.mode)
                    else:
                        values = std_observed[std_observed.Name == std_name][self.sample_cols]
                        if values.shape[0] != 1:
                            errors.append('Multiple peaks found for ' + std_name + ','+ + 'in mode: '+ self.mode)
                        else:
                            min_value = np.min(values.iloc[0].tolist())
                            if min_value <= baseline:
                                errors.append(std_name+" standard below baseline in mode: " + self.mode)
                #check outliers in standards
                for g_num in self.sample_groups_dict.keys():
                    g_samples = self.sample_groups_dict[g_num]
                    for i in std_observed.index:
                        std_name = std_observed.loc[i]['Name']
                        group_series = std_observed.loc[i][g_samples]
                        outlier_samples = stat.get_outlier_names(group_series)
                        if len(outlier_samples) != 0:
                            group_name = self.group_num_dict[g_num]
                            error_str = str(std_name)+" standard has outliers in samples:"
                            for o_s in outlier_samples:
                                error_str+=(' '+o_s+',')
                            errors.append(error_str + " in group: " + group_name + ', and mode:' + self.mode +'.')
            #check standards are greater than baseline
            if len(errors) == 0:
                return True
            else:
                return errors
    
    def check_saturation(self, error = .1, min_checks = 3, max_checks=100, check_lower_limit = False, safe_roof = 1000000):
        """
        Used the isotope distibution of the largest named peak to decide if the
        data is saturated
        Preferable that data is not filtered.
        Data CAN NOT be isotope corrected.
        error should be the percent in decimal form
        """
        if not 'Name' in self.data_frame.columns.tolist():
            raise ValueError('MS_Class must be named before calling check_sat')
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            return {'+':pos.check_saturation(error=error,min_checks=min_checks,max_checks=max_checks,check_lower_limit=check_lower_limit),
                    '-':neg.check_saturation(error=error,min_checks=min_checks,max_checks=max_checks,check_lower_limit=check_lower_limit)}
        else:
            sample_saturation = OrderedDict()
            simple_saturation = OrderedDict()
            ms = self.data_frame
            named = self.remove_unknowns()
            named_ms = named.data_frame
            for sample_name in named.sample_cols:
                #print 'checking saturation on ' + sample_name
                #check each sample
                sample_saturation[sample_name] = OrderedDict()
                sample_ms = ms[named.mass_cols+named.name_cols+[sample_name]].sort_values(by = sample_name, ascending = False)
                sample_named_ms = named_ms[named.mass_cols+named.name_cols+[sample_name]].sort_values(by = sample_name, ascending = False)
                #only want to look at the mass pairs where the intensity is grater than the safe_roof                
                above_roof = sample_named_ms[sample_named_ms[sample_name] > safe_roof]             
                #if the intensity of the largest intensity is below safe roof assume that its not saturated
                #pdb.set_trace()
                if above_roof.empty:
                    for i in range(min_checks):
                        sample_saturation[sample_name][sample_named_ms.iloc[i]['Name']] = False
                        simple_saturation[sample_name] = False
                for i in above_roof.index[:max_checks]:
                    #only check the most abundant peaks up to max_checks
                    #sample_saturation[sample_name]
                    #pdb.set_trace()
                    row = above_roof.loc[i]
                    lipid = Lipid.from_group(row['Group'],row['Name'],row['Mode'],mass_pair=(row['Precursor'],row['Fragment']))
                    sample_saturation[sample_name][str(lipid)] = lipid.is_saturated(sample_ms, sample_name, error = error, soft_check=True, check_lower_limit=check_lower_limit)
                    if len(sample_saturation[sample_name].keys()) == min_checks:
                        if (not True in sample_saturation[sample_name].values()) and (False in sample_saturation[sample_name].values()):
                            #if the fist checks up to min_checks have at least one True and no Falses then stop checking
                            #print 'passed' 
                            simple_saturation[sample_name] = False
                            break
                        else:
                            simple_saturation[sample_name] = True
                            #print 'failed'                            
            return sample_saturation
    
    def get_saturated_samples(self, error = .1):
        saturated_samples = OrderedDict()
        if self.mode == None:
            modes= ['+','-']
        else:
            modes = [self.mode]
        for m in modes:
            ms = self.restrict_to_mode(m)
            saturation_info = ms.check_saturation()
            saturated_samples[m] = OrderedDict()
            for sample_name in self.sample_cols:
                if (len(saturation_info[sample_name].keys()) == 3) and (not True in saturation_info[sample_name].values()):
                    pass
                else:
                    saturated_samples[m][sample_name] = saturation_info[sample_name]
        return saturated_samples
    
    def check_isotope_correction(self):
        pass
    
    def check_for_contamination(self, to_check = 5, upto = 3):
        """
        This will check to see if the top n(to_check) are named and if upto are
        unknowns it will flag sample with a possible contaminant        
        """
        df = self.data_frame.copy()
        contaminants = OrderedDict()
        for samp in self.sample_cols:
            count = 0
            top_20 = df.sort_values(by = samp, ascending = False).head(to_check)
            for i in top_20.index:
                if top_20.loc[i]['Group'] == 'Unknown':
                    count += 1
            if count > upto:
                contaminants[samp] = top_20['Name'].tolist()
        return contaminants
            
    def estimate_baseline(self):
        """
        This data should be isotope corrected
        Uses the lowest named peaks with isotope distributions to determine
        the lower baseline
        """
        pass

    def samples_missing_standard(self):
        '''
        returns the samples that are calculated to me missing the standard
        (uses outlier quartile test)
        be carefull that if it is a pos and neg run that both have the same
        missing standards!
        ''' 
        if self.standards == None:
            return []
        else:
            pos_missing = []
            neg_missing = []
            if self.mode == None or self.mode == '+':
                outliers = stat.is_outlier_df(self.standards['Observed']['+'], self.sample_cols)
                pos_missing.extend(outliers[1])
                if self.mode == '+':
                    return pos_missing
            if self.mode == None or self.mode == '-':
                outliers = stat.is_outlier_df(self.standards['Observed']['-'], self.sample_cols)
                neg_missing.extend(outliers[1])
                if self.mode == '-':
                    return neg_missing
            if self.mode == None:
                if set(pos_missing) != set(neg_missing):
                    print ('MS_Classes - WARNING! in pos mode %r are missing standard, \n and in neg mode %r are missing standard' %(pos_missing, neg_missing))
                    print ('MS_Classes - Only samples that are missing std in both modes will be ignored')
                both_missing = set(pos_missing)&set(neg_missing)
                return both_missing
                
    def get_all_spectra(self, reindex=None):
        df = self.data_frame.copy()
        if reindex is not None:
            try:
                assert reindex in self.data_frame.columns
            except AssertionError:
                raise IndexError(str(reindex)+' not in data frame columns.')
            df.index = df[reindex]
        return df[self.all_sample_cols]
        
    def deconvolve_single_spectrum(self, sample):
        if self.mode is None:
            raise ValueError('Cannot deconvolve spectrum with NoneType mode.')
        spec = self.get_spectrum(sample, reindex='Mass_Pair')
        observed_lipids = self.data_frame.Name
        for lpd in observed_lipids:
            num_M0M0 = spec[lpd.mass_pair]
            if isinstance(num_M0M0, (float, int, np.int64)) or num_M0M0 == 0:
                predicted = lpd.mass_pair_dist(N=num_M0M0)
                if len(predicted) > 1:
                    try:
                        assert (predicted > -0.1).all()
                    except AssertionError:
                        raise ValueError('method deconvolve_single_spectrum not working as intended.')
                    corrected = spec.ix[predicted.index][1:] - predicted[1:]
                    for l in corrected.index:
                        if l in spec.index:
                            spec.loc[l] = corrected[l]
                        else:
                            spec[l] = -predicted[l]
            else:
                print num_M0M0
                raise ValueError('')
        return spec
        
    def deconvolve_all_spectra(self, cols='all'):
        if self.mode is None:
            pos = self.restrict_to('mode', '+').deconvolve_all_spectra()
            neg = self.restrict_to('mode', '-').deconvolve_all_spectra()
            return pos.combine(neg)
        if cols == 'all':
            cols = self.all_sample_cols
        all_spectra = pd.DataFrame()
        for c in cols:
            assert c in self.all_sample_cols or type(c) == int
            all_spectra[c] = self.deconvolve_single_spectrum(c)
        spectra = all_spectra.replace(np.nan, 0)
        molecule_info = self.data_frame[self.data_frame.columns.difference(self.all_sample_cols)]
        molecule_info.index = molecule_info.Mass_Pair
        df = pd.concat([molecule_info, spectra], axis=1)
        df.reset_index()
        ###
        df = df[self.name_cols+self.mass_cols+spectra.columns.tolist()].reset_index(drop=True)
        names = self.name_table[self.name_table.sample_name.apply(lambda x: x in spectra.columns.tolist())]
        return MS_Data(df, names, mode=self.mode, ms_standards=self.standards)

    def get_spectrum(self, sample, reindex=None):
        """
        Gets the spectrum
        """
        df = self.data_frame.copy()
        if reindex is not None:
            try:
                assert reindex in self.data_frame.columns
            except AssertionError:
                raise IndexError(str(reindex)+' not in data frame columns.')
            df.index = df[reindex]
        if sample in self.all_sample_cols:
            return df[sample]
        elif isinstance(sample, int):
            sample = self.all_sample_cols[sample]
            return self.get_spectrum(sample, reindex=reindex)
        else:
            raise IndexError("Bad data type for get_spectrum")
            
    def sort(self, column, ascending=False):
        return MS_Data(self.data_frame.sort_values(column, ascending=ascending), self.name_table, self.mode, self.standards)

    def sort_by_pre_frag(self, ascending = True, reset_index = False):
        new_df = self.data_frame.copy()
        new_df.sort_values(by = ['Precursor','Fragment'], ascending=ascending, inplace = True)
        if reset_index == True:
            new_df.reset_index(inplace = True, drop = True)
        return MS_Data(new_df, self.name_table, self.mode, self.standards)

    def normalize(self, separate_modes=False):
        if self == None:
            return MS_Data_None()
        if separate_modes and self.mode == None:
            pos = self.restrict_to('Mode', '+', '==').normalize(separate_modes=False)
            neg = self.restrict_to('Mode', '-', '==').normalize(separate_modes=False)
            return pos.combine(neg)
        normalized = lf.normalizer(self.data_frame, self.all_sample_cols)
        return MS_Data(normalized, self.name_table, self.mode, self.standards)
    
    def mol_normalize(self):
        normalized = lf.molar_norm(self.data_frame, self.all_sample_cols)
        return MS_Data(normalized, self.name_table, self.mode, self.standards)
        
    def _get_subset(self, N):
        """
        Returns an MS_data object that contains only the first N entries in 
        self.data_frame.  Useful for testing.
        """
        shrunken_df = self.data_frame.ix[self.data_frame.index[:N]]
        return MS_Data(shrunken_df, self.name_table, self.mode, self.standards)
        
    def remove_samples_without_standard(self):
        if self == None:
            return MS_Data_None()
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            pos_removed = pos.remove_samples_without_standard()
            neg_removed = neg.remove_samples_without_standard()
            pos_len = len(pos_named.sample_cols)
            neg_len = len(neg_named.sample_cols)
            if pos_len >= neg_len:
                return pos_removed.combine(neg_removed)
            elif pos_len <= neg_len:
                return neg_removed.combine(pos_removed)
            else:
                raise ValueError('Issue with remove_samples_without_standard')
        samples_to_ignore = self.samples_missing_standard()
        new_df = self.data_frame.copy()
        new_name_table = self.name_table.copy()
        #new_standards = copy.deepcopy(self.standards)
        for s in samples_to_ignore:
            del new_df[s]
            new_name_table = new_name_table[new_name_table.sample_name != s]
            #del new_standards['Observed'][self.mode][s]
            #del new_standards['Input'][self.mode][s]
        return MS_Data(new_df, new_name_table, self.mode, self.standards)
    
    def standard_normalize(self, ignore_missing_standard = False):
        """
        Running self.check_standards is advidesed before using this to make sure the standards
        are ok and present before normalization. However only the default standard is
        nessicary in observed to carry out standard_normalize
        
        """
        if self == None:
            return MS_Data_None()
        if 'Name' not in self.data_frame.columns:
            print ('MS_Classes - Naming data before it can be normalized')
            named = self.Name_Molecules()
            return named.standard_normalize()
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            pos_norm = pos.standard_normalize(ignore_missing_standard = ignore_missing_standard)
            neg_norm = neg.standard_normalize(ignore_missing_standard = ignore_missing_standard)
            try:
                comb = pos_norm.combine(neg_norm)
            except:
                comb = neg_norm.combine(pos_norm)
            return comb
        else:
            if isinstance(self.standards['Input'][self.mode], type(None)):
                return MS_Data_None()
            to_norm = self
            default_standards = to_norm.standards['Input'][self.mode][to_norm.standards['Input'][self.mode].Normalize_Group == 'default'].Name
            if (default_standards.shape[0] != 1):
                print ('Issue with input default standard in mode:' + self.mode)
                return MS_Data_None()
            elif not str(default_standards.iloc[0]) in [str(x) for x in to_norm.standards['Observed'][to_norm.mode].Name.tolist()]:
                print ('Default standard midding in mode:' + self.mode)
                return MS_Data_None()
            df = to_norm.data_frame.copy()
            samples = df[to_norm.sample_cols]
            stds_observed = to_norm.standards['Observed'][to_norm.mode].copy()
            #stds_observed.index =stds_observed.index.map(lambda x: x.name)
            #series of the samples
            stand_series = to_norm.data_frame.Name.apply(lambda x : x.get_standard(to_norm.standards['Input'][self.mode], to_norm.standards['Observed'][self.mode]))
            #series of the concentrations from to_norm.standards['Input']
            stand_conc = stand_series.apply(lambda x: to_norm.standards['Input'][to_norm.mode].set_index('Name')['Conc'][x])
            for samp in samples.columns:
                intensity_dct = {}
                for std_index in stds_observed.index:
                    intensity_dct[stds_observed.loc[std_index]['Name']] = float(stds_observed.loc[std_index][samp])
                std_vol = to_norm.name_table.set_index('sample_name')['standard_volume'][samp]
                sample_amt = to_norm.name_table.set_index('sample_name')['sample_amount'][samp]
                stdC_times_stdV = stand_conc.multiply(float(std_vol))
                constant = stdC_times_stdV.divide(float(sample_amt))
                #std_intensities = stand_series.apply(lambda x : float(stds_observed[stds_observed.Name == x][samp]))
                std_intensities = stand_series.apply(lambda x : intensity_dct[x])
                samples[samp] = samples[samp].divide(std_intensities)
                samples[samp] = samples[samp].multiply(constant)
                samples[samp] = samples[samp].apply(lambda x: utilities.round_sigfigs(x,5))
            new_df = pd.concat([df[df.columns.difference(to_norm.sample_cols)], samples], axis=1)
            return MS_Data(new_df, to_norm.name_table, to_norm.mode, to_norm.standards)
        
    def check_normalization_groups(self):
        """
        Returns a df that has an example of each group and what lipid from the
        standard mix it was normalized to
        """
        group_standard_df = pd.DataFrame(columns = ['Group','Standard'])        
        for g in self.groups_present():
            gdf = self.restrict_to_group(g)
            item = gdf.data_frame.Name.iloc[0]
            standard = item.get_standard(self)
            group_standard_df = group_standard_df.append(pd.DataFrame({'Group':[item],'Standard':[standard]}))
        return group_standard_df
    
    def restrict_to_group(self, Group):
        """
        Returns
        ----------
        A new MS_data object with the same name and standard information, but
        with a data frame that contains only records for the specified lipid group.
        
        Parameters
        ----------
        Group : str
            The lipid group that the dataset will be restricted to.
            
        Examples
            >>>dataset.restrict_to_group('TAG')
            >>>dataset.restrict_to_group('PC')
        ----------
        
        
        See also
        ----------
        MS_data.groups_present
        MS_data.restrict_to_type
        MS_data.restrict_to
        """
        if self == None:
            return MS_Data_None()
        tdf = self.data_frame[self.data_frame.Group == Group]
        if Group == 'PL':
            tdf['not PA'] = tdf['Name'].apply(lambda x : not x.name.startswith('PA'))
            tdf = tdf[tdf['not PA']]
            del tdf['not PA']
        return MS_Data(tdf, self.name_table, self.mode, self.standards)
    
    def restrict_to_type(self, Type):
        if self == None:
            return MS_Data_None()
        tdf = self.data_frame[self.data_frame.Type == Type]
        if 'Group' in tdf.columns.tolist():
            tdf = tdf[tdf.Group != 'Isotope']
        if Type == 'PL':
            tdf['not PA'] = tdf['Name'].apply(lambda x : not x.name.startswith('PA'))
            tdf = tdf[tdf['not PA']]
            del tdf['not PA']
        return MS_Data(tdf, self.name_table, self.mode, self.standards)
        
    def remove_unknowns(self, remove_isotopes = True):
        """
        This should NOT reset the values of the index
        """
        if self == None:
            return MS_Data_None()
        if remove_isotopes == True:
            no_iso = self.restrict_to('Group', 'Isotope', '!=')
            return no_iso.restrict_to('Group', 'Unknown', '!=')
        else:
            return self.restrict_to('Group','Unknown', '!=')
        
    def restrict_to(self, lhs, rhs, rel='=='):
        """
        Not recommended for use unless you can figure out what the code does.
        """
        if self == None:
            return MS_Data_None()
        R = {'==' : lambda a, b : a == b, '!=' : lambda a, b : a != b}
        return MS_Data(self.data_frame[R[rel](self.data_frame[lhs], rhs)], self.name_table, mode=self.mode, ms_standards = self.standards)
        
    def restrict_to_mode(self, Mode):
        if self == None:
            return MS_Data_None()
        if Mode == '+':
            mdf = self.data_frame[self.data_frame['Mode'] == '+']
        elif Mode == '-':
            mdf = self.data_frame[self.data_frame['Mode'] == '-']
        else:
            raise ValueError('Mode should = "-" or "+".')
        return MS_Data(mdf, self.name_table, Mode, self.standards)
        
    def Fold_Changes(self, group_nums, append_group_names=False, sort=False):
        g1, g2 = self.groups_dict[group_nums[0]], self.groups_dict[group_nums[1]]
        gname1, gname2 = self.group_num_dict[group_nums[0]], self.group_num_dict[group_nums[1]]
        app = append_group_names*('_'+gname2+'_/_'+gname1)
        df = self.data_frame[g1+g2]
        stats_frame = self.data_frame[self.name_cols+self.mass_cols]
        mean_g1 = df[g1].mean(axis=1)
        mean_g2 = df[g2].mean(axis=1)
        FC = mean_g2 / mean_g1
        return FC
    
    def Hypothesis_Tests(self, group_nums, paired=False, append_group_names=False, sort=False):
        '''        
        STDEV = Sqrt(SUM(x-ave(x))/(n-1)) x is mean average and n is sample size

        '''         
        stats_frame = pd.DataFrame()
        g1, g2 = self.groups_dict[group_nums[0]], self.groups_dict[group_nums[1]]
        gname1, gname2 = self.group_num_dict[group_nums[0]], self.group_num_dict[group_nums[1]]
        app = append_group_names*('_'+gname2+'_/_'+gname1)
        df = self.data_frame[g1+g2]
        stats_frame = self.data_frame[self.name_cols+self.mass_cols]
        stats_frame['Mean_'+gname1] = df[g1].mean(axis=1)
        stats_frame['Mean_'+gname2] = df[g2].mean(axis=1)
        stats_frame['Stdev_'+gname1] = df[g1].std(axis=1)
        stats_frame['Stdev_'+gname2] = df[g2].std(axis=1)
        stats_frame['Fold_Change'+app] = stats_frame['Mean_'+gname2].div(stats_frame['Mean_'+gname1])
        if len(g1) >= 3 and len(g2) >= 3:
            if not paired:        
                stats_frame['RS_p_value'+app] = df.apply(lambda x : ranksums(x[g1], x[g2])[1], axis=1)
                stats_frame['t_test_p_value'+app] = ttest_ind(df[g1], df[g2], axis=1)[1]
            if paired:
                stats_frame['t_test_p_value'+app] = ttest_rel(df[g1], df[g2], axis=1)[1]
        stats_frame['Median_'+gname1] = df[g1].median(axis=1)
        stats_frame['Median_'+gname2] = df[g2].median(axis=1)
        stats_frame['Range_'+gname1] = df[g1].max(axis=1) - df[g1].min(axis=1)
        stats_frame['Range_'+gname2] = df[g2].max(axis=1) - df[g2].min(axis=1)
        cols = self.name_cols+self.mass_cols+['Fold_Change'+app, 'RS_p_value'+app,
                                              't_test_p_value'+app,
                                              'Mean_'+gname1, 'Stdev_'+gname1,
                                              'Median_'+gname1, 'Range_'+gname1,
                                              'Mean_'+gname2, 'Stdev_'+gname2,
                                              'Median_'+gname2, 'Range_'+gname2]
        if len(g1) < 3 or len(g2) < 3:
            cols.remove('t_test_p_value'+app)
            cols.remove('RS_p_value'+app)
        if paired:
            cols = cols.remove('RS_p_value'+app)
        if sort:
            stats_frame = stats_frame.sort_values(by = 'Fold_Change')
        self.hypothesis_tests = stats_frame[cols]
        self.stats_cols = [c for c in cols if c not in self.name_cols+self.mass_cols]
        return self.hypothesis_tests
        
    def Compare_All_Groups(self, normalize = False):
        '''        
        Used to make the all groups_sheet, 
        '''
        if normalize == False:
            df = self.data_frame.copy()
        elif normalize == True:
            df1 = self.normalize()
            df = df1.data_frame
        stats_frame = pd.DataFrame()
        stats_frame = df[self.name_cols+self.mass_cols]
        for g in self.groups_dict:
            group_name = self.group_num_dict[g]
            stats_frame['Mean_'+group_name] = df[self.groups_dict[g]].mean(axis = 1)
            stats_frame['Stdev_'+group_name] = df[self.groups_dict[g]].std(axis = 1)
        return stats_frame
            
    def Describe_Unknowns(self):
        ###FIX THIS
        df = self.data_frame[self.data_frame.Name == 'Unknown']
        new_name = pd.Series(dict([(i, 'Unknown_'+df.mode[i]+'_'+str(df.Precursor[i])[:5]+'_'+str(df.Fragment[i])[:5]) for i in df.index]))
        df['Name'] = new_name
        final_df = pd.concat([self.data_frame[self.data_frame.Name != 'Unknown'], df])
        return MS_Data(final_df, self.name_table, self.mode, self.standards)
        
    def summarize_group_feature(self, group, groups_to_test, paired=False, MS1_group = True, MS2_features = True, chain_features = False):
        if not group in self.groups_present():
            return None
        restricted = self.restrict_to_group(group)
        features = restricted.data_frame.Name.apply(lambda x : 'LOLWTF'.join(x.features.keys())) #join features in string so they can be compared (can't as a list)
        to_analyze = features[features.first_valid_index()]
        try:
            assert len(features.drop_duplicates()) == 1 ##throw an error if the features are not all the same
            first = features[features.first_valid_index()]
            assert (features == first).all()
            common_to_all = first.split('LOLWTF')
        except AssertionError:
            ##get features common to all
            common_to_all = set(to_analyze)
            for i in features.index:
                next_set = set(features[i].split('LOLWTF'))
                common_to_all = common_to_all.intersection(next_set)
                try:
                    assert len(common_to_all) > 0
                except AssertionError:
                    print 'MS_Classes - Error: group', group, 'has no features common to all species.'                    
                    return None
        ###perform the groupby and tests
        excel_dict = OrderedDict()
        df = restricted.data_frame.copy()
        df['Count'] = 1
        features = list(common_to_all)
        for f in features:
            df[f]= df.Name.apply(lambda x : x.features[f])
        to_group = df[self.name_cols+features+self.mass_cols+self.all_sample_cols+['Count']] #organize columns for data file
        stats = restricted.Hypothesis_Tests(groups_to_test)
        stats_cols = restricted.stats_cols
        for f in features:
            stats[f]= stats.Name.apply(lambda x : x.features[f])
        stats = stats[self.name_cols+self.mass_cols+features+stats_cols]
        to_data = df.copy()
        for f in features:
            del to_data[f]
        del to_data['Count']
        excel_dict['Data'] = to_data
        excel_dict['Stats'] = stats.copy()
        for f in features:
            fdata = to_group.groupby(f).sum()
            g1, g2 = self.groups_dict[groups_to_test[0]], self.groups_dict[groups_to_test[1]]
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            gname1, gname2 = self.group_num_dict[groups_to_test[0]], self.group_num_dict[groups_to_test[1]]
            df = fdata[g1+g2]
            fdata = to_group.groupby(f).sum()
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            stats_frame = make_stats_frame(df, gname1, gname2, g1, g2, paired = paired)
            excel_dict[f+'_Statistics'] = stats_frame
        #enter sarahs sheets here
        if MS1_group:
            MS1_bin = restricted.bin_and_sum('Precursor', normalize=False)
            MS1_bin_stats = make_stats_frame(MS1_bin, gname1, gname2, g1 ,g2, paired = paired)
            excel_dict['MS1_bin'] = MS1_bin
            excel_dict['MS1_bin_Statistics'] = MS1_bin_stats
        if MS2_features:
            if group not in ['DAG','TAG']:
                pass
            else:
                ms2_features = restricted.data_frame.Name.apply(lambda x: x.MS2_features(as_str = True))
                to_group = restricted.data_frame.copy()
                to_group['Count'] = 1
                to_group['MS2_features'] = ms2_features
                MS2_grouped = to_group.groupby('MS2_features').sum()
                MS2_grouped.reset_index(range(len(MS2_grouped.index)), inplace = True)
                cols = ['MS2_features','Count']+self.all_sample_cols
                MS2_grouped = MS2_grouped[cols]
                MS2_grouped_stats = make_stats_frame(MS2_grouped, gname1, gname2, g1 ,g2, paired = paired)
                excel_dict['MS2_bin'] = MS2_grouped
                excel_dict['MS2_bin_stats'] = MS2_grouped_stats
        if chain_features:
            pass
        return excel_dict
        
    def summarize_type_feature(self, typ, groups_to_test, append_group_names = False, paired=False):
        if not typ in self.types_present():
            return None
        restricted = self.restrict_to_type(typ)
        if restricted.data_frame.empty:
            return None
        features = restricted.data_frame.Name.apply(lambda x : 'LOLWTF'.join(x.features.keys())) #join features in string so they can be compared (can't as a list)
        to_analyze = features[features.first_valid_index()]
        try:
            first = features[features.first_valid_index()]
            assert (features == first).all()
            common_to_all = first.split('LOLWTF')
        except:
            ##get features common to all
            common_to_all = set(to_analyze.split('LOLWTF'))
            for i in features.index:
                next_set = set(features[i].split('LOLWTF'))
                common_to_all = common_to_all.intersection(next_set)
                try:
                    assert len(common_to_all) > 0
                    #features = list(common_to_all)
                except AssertionError:
                    print 'MS_Classes - Error: type', typ, 'has no features common to all species.'
                    print common_to_all
                    return None
        ###perform the groupby and tests
        excel_dict = OrderedDict()
        df = restricted.data_frame.copy()
        df['Count'] = 1
        #features = df.Name[df.Name.first_valid_index()].features.keys()
        features = list(common_to_all)
        for f in features:
            df[f]= df.Name.apply(lambda x : x.features[f])
        to_group = df[self.name_cols+features+self.mass_cols+self.all_sample_cols+['Count']] #organize columns for data file
        stats = restricted.Hypothesis_Tests(groups_to_test,
                                            append_group_names=append_group_names)
        stats_cols = restricted.stats_cols
        for f in features:
            stats[f]= stats.Name.apply(lambda x : x.features[f])
        stats = stats[self.name_cols+self.mass_cols+features+stats_cols]
        to_data = df.copy()
        for f in features:
            del to_data[f]
        del to_data['Count']
        excel_dict['Data'] = to_data
        excel_dict['Stats'] = stats.copy()
        for f in features:
            fdata = to_group.groupby(f).sum()
            g1, g2 = self.groups_dict[groups_to_test[0]], self.groups_dict[groups_to_test[1]]
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            gname1, gname2 = self.group_num_dict[groups_to_test[0]], self.group_num_dict[groups_to_test[1]]
            df = fdata[g1+g2]
            fdata = to_group.groupby(f).sum()
            excel_dict[f+'_data'] = fdata[['Count']+g1+g2].reset_index()
            stats_frame = make_stats_frame(df, gname1, gname2, g1, g2, paired = paired,
                                           append_group_names=append_group_names)
            excel_dict[f+'_Statistics'] = stats_frame 
        MS1_bin = restricted.bin_and_sum('Precursor',normalize=False)
        MS1_bin_stats = make_stats_frame(MS1_bin, gname1, gname2, g1, g2, paired = paired,
                                         append_group_names=append_group_names)
        excel_dict['MS1_bin'] = MS1_bin
        excel_dict['MS1_bin_Statistics'] = MS1_bin_stats
        return excel_dict
        
    def get_general_info(self):
        if self.mode == None:
            pos_info = self.restrict_to_mode('+').get_general_info()
            neg_info = self.restrict_to_mode('-').get_general_info()
            info = OrderedDict()
            info['+'] = pos_info['+']
            info['-'] = neg_info['-']
            return info
        else:
            info = OrderedDict()
            info[self.mode] = OrderedDict()
            df = self.data_frame.copy()
            info[self.mode]['named'] = self.remove_unknowns().data_frame.shape[0]
            info[self.mode]['unknown'] = df[df.Group == 'Unknown'].shape[0]
            groups = self.groups_present()
            types = self.types_present()
            info[self.mode]['types'] = OrderedDict()
            info[self.mode]['groups'] = OrderedDict()
            for t in types:
                if not 'standard' in t.lower():
                    info[self.mode]['types'][t] = df[df.Type == t].shape[0]
            for g in groups:
                if not 'standard' in g.lower():
                    info[self.mode]['groups'][g] = df[df.Group == g].shape[0]
            return info
    
    def get_general_stats(self):
        pass
    
    def qc_stats(self, mode = None, baseline = 100):
        if mode == None and self.mode == None:
            raise ValueError('Please select a mode for the MS_classes qc_stats')
        if mode == None:
            df = self.data_frame.copy()
        else:
            resticted = self.restrict_to_mode(mode)
            df = resticted.data_frame.copy()
        #pdb.set_trace()
        stats_df = pd.DataFrame()
        for sample_name in self.all_sample_cols:
            sample_df = df[self.name_cols + [sample_name]]
            over_baseline = sample_df[sample_df[sample_name] > baseline].sort_values(by=sample_name, ascending = False)
            sample_stats = pd.Series(name = sample_name)
            sample_stats['sum'] = np.sum(sample_df[sample_name])
            sample_stats['max_name'] = str(over_baseline.iloc[0]['Name'])
            sample_stats['max_value'] = over_baseline.iloc[0][sample_name]
            no_unknowns = over_baseline[over_baseline['Group'] != 'Unknown']
            sample_stats['number_named'] = no_unknowns[no_unknowns['Group'] != 'Isotope'].shape[0]
            sample_stats['number_peaks'] = over_baseline.shape[0]
            stats_df = pd.concat([stats_df, sample_stats], axis = 1)
        return stats_df.reset_index(drop = False)

    def to_excel(self, writer, **kwargs):#, float_format='%.4f'):
        if kwargs.get('index') is None:
            kwargs['index'] = False
        df = self.data_frame.copy()
        if 'Mass_Pair' in df.columns:
            df.Mass_Pair = df.Mass_Pair.apply(str)
        if 'Name' in df.columns:
            df.Name = df.Name.apply(str)
        df.to_excel(writer, **kwargs)
    
    def to_csv(self, path):
        self.data_frame.to_csv(path)

    def drop_group_of_samples(self, num=None, name=None):
        if name is not None:
            new_names = self.name_table[self.name_table.group_name != name]
            new_data = self.data_frame[self.name_cols+self.mass_cols+new_names.Name.tolist()]
            return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
        elif num is not None:
            new_names = self.name_table[self.name_table.Group != num]
            new_data = self.data_frame[self.name_cols+self.mass_cols+new_names.Name.tolist()]
            return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
            
    def drop_sample_groups(self, nums=None, names=None):
        if nums is not None:
            nums = list(nums)
        if names is not None:
            names = list(names)
        new_mdata = self.copy()
        while nums:
            to_drop = nums.pop()
            new_mdata = new_mdata.drop_group_of_samples(num=to_drop)
        while names:
            to_drop = names.pop()
            new_mdata = new_mdata.drop_group_of_samples(name=to_drop)
        return new_mdata

    def to_skl_classifier(self):
        """
        Y, X = self.to_skl_classifier()
        """
        data = np.array(self.data_frame[self.all_sample_cols].T)
        labels = np.array(self.name_table.group_number)
        return labels, data
    
    def copy(self):
        new_data = self.data_frame.copy()
        new_names = self.name_table.copy()
        return MS_Data(new_data, new_names, mode=self.mode, ms_standards=self.standards)
    
    def Volcano(self, saveas=None):
        
        if not hasattr(self, 'hypothesis_tests'):
            print('MS_Classes - Fold change and p-value not yet calculated.  Cannot make volcano plot.')
            return None
        df = self.hypothesis_tests[self.hypothesis_tests['Type'] != 'Unknown']
        fig, ax = plt.subplots()
        plot_groups = df['Group'].drop_duplicates().tolist()
        colormap = plt.cm.Set1
        plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, np.exp(1)/np.pi, len(plot_groups))])
        for group in plot_groups:
            restricted_df = df[df['Group'] == group]
            pvals = -np.log10(restricted_df['t_test_p_value'])
            FCs = np.log2(restricted_df['Fold_Change'])
            if group != 'Unknown':
                ax.plot(FCs, pvals, sample(symbols, 1)[0], ms=7.5, label=group)
            else:
                pass#ax.plot(pvals, FCs, '*', ms=5, label=group)
        lgd = ax.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        restricted_df = df[df['Group'] == group]
        ax.set_xlabel('log2(Fold Change)')
        ax.set_ylabel('-log10(p-value)')
        fig.tight_layout()
        if saveas is not None:
            fig.savefig(saveas, bbox_extra_artists=lgd)
        return None
        
    def Manhattan(self, saveas=None):
        
        if not hasattr(self, 'hypothesis_tests'):
            raise AttributeError('Statistics not done.')
        grouped = self.hypothesis_tests.sort_values(by = 'Group').reset_index(drop=True)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for g in grouped.Group.drop_duplicates():
            pvals = grouped[grouped.Group == g]['t_test_p_value']
            ax.plot(pvals.index, -np.log10(pvals), label=g)
        ax.set_ylim(bottom=0)
        if saveas is not None:
            fig.savefig(saveas)
        return fig
        
    def fit_skl_model(self, skl_model, **kwargs):
        Y, X = self.to_skl_classifier()
        skl_model.fit(X, Y)
        return skl_model
        
    def score_skl_model(self, skl, **kwargs):
        Y, X = self.to_skl_classifier()
        skl_model.score(X, Y)
        return skl_model
            
    def raw_counts(self):
        return self.data_frame[self.all_sample_cols]
    
    def isotope_distributions(self):
        try:
            dist = self.data_frame.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
            return dist
        except AttributeError:
            raise AttributeError('Data needs to be named before isotope_distibutions is called')
         
    def test(self):
        return self.data_frame
        
    def isotope_correction(self, treat = '0', missing_value = 200, baseline = 200, max_m = 4, concidered_iso_fract = .4, name_isotopes = True, clean = False):
        """
        Takes the samples and correctes them isotopically.
        this will be done on self.data_frame
        data_frame needs to have been named
        missing_value:
            is the isotope distibution times the signal is larger than this 
            number it will be added to the missing signal list
        baseline:
            the smallest value that it will correct for.
            decreasing this increases the runtime but increases the accuracy
            increasing this decreases the runtime but decreases the accuracy 
                
        this function makes a huge dataframe with all of the info from the isotopes
        including:
            each isotope distribution for each mass pair
            each of the fragtion for each distibution for each mass pair
            isotope corrected
            isotope adjusted (whcih is the same as isotope corrected but only pos numbers)
        
        Use isotope_filter to reduce the self.data_frame to only the sample info
        
        mcs aka Matts Crazy Sheet:
            returns a dataframe that shows what fraction of a mass pairs Mxy 
            come from its isotopes
            =(Raw-correction_value)/Raw
        
        note1: this does not remove any mass pairs, this only corrects the data.
            no fractions or graphs or analysis, just correction.
            
        note2: This will not alter the standards
        """
        if self == None:
            return [MS_Data_None(),pd.DataFrame(),pd.DataFrame()]
        if type(self.data_frame) == type(None) or self.data_frame.empty:
            return self.copy(), pd.DataFrame(), pd.DataFrame()
        #write some checks to make sure it is named data and only one mode!
        t1 = 0
        t2 = 0
        t3 = 0
        t4 = 0
        total_time_start = time.time()
        if self.mode == None:
            raise ValueError('Data must be seperated by mode before calling')
            #needs to be tested
            #pos = self.restrict_to('Mode', '+', '==')
            #neg = self.restrict_to('Mode', '-', '==')
            #pos.mode = '+'
            #neg.mode = '-'
            #pos_cor = pos.isotope_correction(treat, missing_value, baseline, max_m, concidered_iso_fract, name_isotopes)
            #neg_cor = neg.isotope_correction(treat, missing_value, baseline, max_m, concidered_iso_fract, name_isotopes)
            #print type(pos_cor)
            #print type(neg_cor)
            #mcs = pd.concat([pos_cor[2],neg_cor[2]])
            #missing = pd.concat([pos_cor[1],neg_cor[1]])
            #new_df = pd.concat([pos_cor[0].data_frame,neg_cor[0].data_frame])
            #new_ms_class = pos_cor[0].combine(neg_cor[0])
            #new_ms_class.data_frame = new_df
            #return new_ms_class, missing, mcs
            
        print '    MS_Classes - Calculating isotope distributions for %s mode'%(self.mode)        
        try:
            ratios = self.data_frame.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
        except AttributeError:
            raise AttributeError('Data needs to be named before isotope_distibutions is called')    
        t1_start = time.time()
        iso_df = self.data_frame.copy()        
        iso_df.sort_values(by = ['Precursor','Fragment'], inplace=True)
        time_dist_start = time.time()
        ratios = iso_df.Name.apply(lambda x: x.mass_pair_dist(from_zero=True))
        time_dist_stop = time.time()
        dist_time = time_dist_stop - time_dist_start
        print 'Time for isotope distributions = ' + str(dist_time)
        molecule_info = iso_df[self.name_cols+self.mass_cols]
        raw_data = iso_df[self.all_sample_cols]
        #adj stands for adjusted, this will be the dataframe that ends up with
        #the isotopically corrcted data        
        iso_df = pd.concat([molecule_info, ratios, raw_data], axis=1).replace(np.nan, 0)
        iso_df.sort_values(by = ['Precursor','Fragment'], inplace=True)
        
        '''
        first makes the dataframe with all of the columns needed for correction
        the in the for i in iso_df.index loop
            for s in samples
                first it sums the Int Mxy for each sample and sumtracts that from 
                the raw to give the adjusted value
                then stores that value along with the fraction value for each sample
            if iso_df
                uses fractions to see if it should be classified as an isotope
                if so changes the name, group and type to reflect that.
            for M, Ms in zip(m_num,m_str):
                does Mxy * cor, adds it to a dictionary if that value is larger than baseline
                then if that dictionary is not empty,
                finds each of the isotopes precursor and fragments an puts in the Mxy*cor value
            
        '''
        m_num = []
        m_str = []
        for m in range(1, max_m+1):
            for n in range(0, m+1):
                m_num.append((m,n))
                m_str.append('Int M'+str(m)+str(n))
        t1 += time.time()-t1_start
        print '    MS_Classes - Constructing the dataframes'
        #mcs stands for matts_crazy_sheet
        t2_start = time.time()
        mcs = iso_df[['Name','Precursor','Fragment','Mode']]
        missing_signals = OrderedDict([])
        samples = self.all_sample_cols
        for s in samples:
            iso_df[s +' Adjusted Intensity'] = iso_df[s]
            mcs[s+ 'Raw'] = iso_df[s]
            missing_signals[s] = pd.DataFrame(columns=[s+' Precursor', s+' Fragment',s+' From',s+' Counts'])
            ########################## adding mcs need to add Mxy for each s
            for x in m_str:
                iso_df[s+' '+x] = 0.0
            for x in m_num:
                mcs[s+' M'+str(x[0])+str(x[1])] = 0.0
            iso_df[s +' Isotope Correction'] = 0.0
            iso_df[s +' Cor/Raw'] = 0.0
        print'    MS_Classes - Correcting, this can take a few minutes'
        t2 += time.time() - t2_start
        #loops though each index for isotope correction         
        for i in iso_df.index:
            t3_start = time.time()
            pre = iso_df['Precursor'][i]
            frag = iso_df['Fragment'][i]
            mass_pair_mode = iso_df['Mode'][i]
            #used for the naming function       
            fractions = []            
            sum_Mxy = {}            
            for m in m_str:
                sum_Mxy[m] = 0
            #calculated the adj and correctd for each sample   
            for s in samples:
                sumM = 0.0
                for Ms in m_str:
                    sumM += iso_df[s +' '+ Ms][i]
                    sum_Mxy[Ms] += (iso_df[s +' '+ Ms][i])
                corrected_subtraction = iso_df[s][i] - sumM
                iso_df[s +' Isotope Correction'][i] = int(corrected_subtraction)
                #TAKE OUT THE SIMILAR LINES IF YOU DONT WANT INT!           
                #iso_df[s +' Isotope Correction'][i] = corrected_subtraction
                if iso_df[s +' Isotope Correction'][i] <= 0:
                    iso_df[s +' Adjusted Intensity'][i] = 0
                else:
                    iso_df[s +' Adjusted Intensity'][i] = int(corrected_subtraction)
                    #iso_df[s +' Adjusted Intensity'][i] = corrected_subtraction
                iso_df[s +' Cor/Raw'][i] = iso_df[s +' Isotope Correction'][i]/iso_df[s][i]
                fractions.append(iso_df[s +' Cor/Raw'][i])
            if name_isotopes == True:
                #used for naming 
                highestMxy = keywithmaxval(sum_Mxy)
                fract_median = round(np.median(fractions),2)
                if fract_median < concidered_iso_fract:
                    iso_df['Group'][i] = 'Isotope'
                    x = int(highestMxy[-2:-1])
                    y = int(highestMxy[-1:])
                    iso_name =  'Isotope from ' + str(pre-x) +'_'+ str(frag-y) + ' '+ highestMxy[-3:] + ' ('+str(fract_median) +')'
                    isotope = Lipid.from_group('Isotope',iso_name, mode = mass_pair_mode, mass_pair = MassPair(pre,frag))
                    iso_df['Name'][i] = isotope
                    mcs['Name'][i] = isotope
            #calculates the dist* Isotope Correction and puts it in the correct
            #place for each Mxy and each sample
            t3 += time.time() - t3_start
            t4_start = time.time()
            for M, Ms in zip(m_num,m_str):
                samples_for_correction={}      
                for s in samples:
                    if treat in {'neg','Neg','negative','Nagative','-'}:
                        adjMxy = iso_df[s+ ' Isotope Correction'][i]*iso_df[MassPair.from_list(M)][i]
                    elif treat in {'zero','Zero','0'}:
                        adjMxy = iso_df[s+ ' Adjusted Intensity'][i]*iso_df[MassPair.from_list(M)][i]
                    else:
                        raise ValueError('treat needs to be one of the following "neg", "Neg", "Negative", "negative" or "-", "Zero", "zero", "0"')
                    if abs(adjMxy) >= baseline:
                        samples_for_correction[s] = adjMxy
                if samples_for_correction.keys():
                    isotope_ind = find(iso_df, pre+M[0], frag+M[1])
                    for v in samples_for_correction:
                        if isotope_ind is not None:
                            iso_df[v + ' '+Ms][isotope_ind] = samples_for_correction[v]
                            mcs_num = (iso_df[v][isotope_ind]-samples_for_correction[v])/iso_df[v][isotope_ind]
                            mcs[v + ' M'+str(M[0])+str(M[1])][i] = mcs_num
                        else: #add to missing signals!
                            missing_pre = pre+M[0]
                            missing_frag = frag+M[1]
                            missing_intensity = samples_for_correction[v]
                            missing_from = str(pre)+'_'+str(frag)+' M'+str(M[0])+str(M[1])
                            to_add = pd.DataFrame([[missing_pre,missing_frag,missing_from,missing_intensity]],columns=[v+' Precursor', v+' Fragment',v+' From',v+' Counts'])
                            missing_signals[v] = missing_signals[v].append(to_add, ignore_index=True)
            t4+= time.time()-t4_start
        print ('    MS_Classes - Correction complete')
        missing_signals_dfs = []
        for df in missing_signals.values():
            missing_signals_dfs.append(df)
        missing_signals_concat = pd.concat(missing_signals_dfs,axis = 1) 
        new_MS = MS_Data(iso_df, self.name_table, self.mode, self.standards)
        new_MS.data_frame = iso_df
        print  'times' + str([t1,t2,t3,t4,time.time()-total_time_start]) 
        if clean == False:
            return new_MS, missing_signals_concat, mcs
        else:
            ic = new_MS.isotope_filter(report = '0')
            no_iso = ic.remove_isotopes()
            base_line = no_iso.set_baseline(100)
            return base_line
            
        
    def isotope_raw(self):
        '''
        returns a MS_class object that has the raw data, and correlating sample info
        benefit of this is that isotopes are now named
        '''
        if self == None:
            return MS_Data_None()
        if self.mode == None:
            raise ValueError('Sample must be resticted to one mode first')
        new_df = self.data_frame[self.name_cols+self.mass_cols+self.all_sample_cols]
        return MS_Data(new_df, self.name_table, self.mode, self.standards)
        
    def creat_db_isotope_sheet(self):
        pass

    def isotope_filter(self, report = 'zero'):       
        #write in tests to make sure it is isotope corrected            
        '''
        iso_df the corrected columns from the complete isotope dataframe
        if iso_df = 'zero' negative numbers will be reported as 0
        if iso_df != 'zero' negative numbers will be reported
        '''
        if self == None:
            return MS_Data_None()
        if type(self.data_frame) == None or self.data_frame.empty:
            return self.copy()
        if self.mode == None:
            raise ValueError('Sample must be resticted to one mode first')
        corrected_cols = []
        if report in {'zero', 'Zero', '0'}:
            for i in self.all_sample_cols:
                corrected_cols.append(i+' Adjusted Intensity')            
        elif report in {'neg', 'Neg', 'negative', 'Negative','-'}:
            for i in self.all_sample_cols:
                corrected_cols.append(i+' Isotope Correction')
        else:
            raise ValueError('treat needs to be one of the following "neg", "Neg", "Negative", "negative" or "-", "Zero", "zero", "0"')
        #get the new standards
        new_df = self.data_frame[self.name_cols+self.mass_cols+corrected_cols]
        new_df.columns = [self.name_cols+self.mass_cols+self.all_sample_cols]
        new_stds = copy.deepcopy(self.standards)
        new_standards_df = new_df[new_df.Group.str.contains('Standard')]
        new_standards_df.set_index('Name', drop = True, inplace = True)
        new_standards_df = new_standards_df[self.mass_cols+self.all_sample_cols]
        new_standards_df.reset_index(inplace = True)
        #new_standards_df.index = new_standards_df.index.map(lambda x: x.name)
        new_stds['Observed'][self.mode] = new_standards_df
        return MS_Data(new_df, self.name_table, self.mode, new_stds)                
        #change the corrected
    
    def remove_isotopes(self):
        if self == None:
            return MS_Data_None()
        new_df = self.data_frame[self.data_frame['Group'] != 'Isotope']
        new_df = new_df[self.data_frame['Type'] != 'Isotope']
        return MS_Data(new_df, self.name_table, self.mode, self.standards)
        
    def clean_isotope_correction(self, baseline = 200, report_zero = True, treat_zero = True):
        if self == None:
            return MS_Data_None()
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            print 'ic pos'
            pos_iso = pos.clean_isotope_correction(baseline=baseline, report_zero=report_zero, treat_zero=treat_zero)
            neg_iso = neg.clean_isotope_correction(baseline=baseline, report_zero=report_zero, treat_zero=treat_zero)
            combined = pos_iso.combine(neg_iso)
            return combined
        else:
            if treat_zero == True:
                isotope_output = self.isotope_correction(baseline = baseline, 
                                                        treat = '0')
            else:
                isotope_output = self.isotope_correction(baseline = baseline, 
                                                        treat = '-')
            ic = isotope_output[0].isotope_filter(report = '0')
            no_iso = ic.remove_isotopes()
            return no_iso
                                                    
        
    def bin_and_sum(self, component, accuracy = .5, normalize = True):
        '''
        bins together and sums the intensity of the specified component
        eg component = 'Fragment'
        this will bin the fragments with the + or - accuracy and then does stats
        change the poap increment of the poap file changes!
        '''
        if self == None:
            return pd.DataFrame()
        msc_modes = {'+':self.restrict_to_mode('+').data_frame,
                     '-':self.restrict_to_mode('-').data_frame}
        msc_normalized = {}
        msc_data = {}
        poap = []
        start_up = 150
        start_down = 150
        while start_up <= 1202 or start_down >=0:
            if start_up <= 1202:
                poap.append(start_up)
                start_up += 1.007825
            if start_down >= 0:
                poap.append(start_down)
                start_down -= 1.007825
        poap = [ round(elem, 3) for elem in poap ]
        neg_poap = [-x for x in poap]
        poap.extend(neg_poap)
        poap.append(0)
        poap.sort()
        for k in msc_modes.keys():
            msc = msc_modes[k]
            msc.sort_values(by = component,inplace = True)
            msc.reset_index(drop= True,inplace = True)
            samples = self.all_sample_cols
            used_indexes = []
            comp_col = [component, '# examples', 'Mode']+samples
            component_df = pd.DataFrame(columns = comp_col)
            for i in msc.index:
                if i not in used_indexes:
                    comp_val = msc[component][i]
                    center_point = take_closest(poap, comp_val)
                    spread = abs(comp_val-center_point) 
                    if spread > accuracy:
                        pass#print 'WARNING!!!!',comp_val,center_point    
                    found = locate(msc, component, center_point, accuracy = accuracy)
                    if found is not None:
                        used_indexes.extend(found)
                        if len(found) > 1:
                            comp_row = OrderedDict([(component,[center_point])])
                            comp_row['# examples'] = [len(found)]
                            comp_row['Mode'] = k
                            for s in samples:
                                total = 0
                                for f in found:
                                    total += msc[s][f]
                                comp_row[s] = [total]
                            component_df = component_df.append(pd.DataFrame(comp_row),ignore_index = True)
                    else:
                        used_indexes.extend([i])    
                else:
                    used_indexes.extend([i])
            #component_df = component_df[comp_col]
            normalized = lf.normalizer(component_df, self.all_sample_cols)
            msc_data[k] = component_df            
            msc_normalized[k] = normalized
        msc_data_df = pd.concat([msc_data['+'],msc_data['-']])
        msc_normalized_df = pd.concat([msc_normalized['+'],msc_normalized['-']]) 
        msc_normalized_df = msc_normalized_df[msc_data_df.columns]    
        if normalize == True:
            return msc_normalized_df
        else:
            return msc_data_df
    
    def single_sample_ms(self, sample):
        if self == None:
            return MS_Data_None()
        if not sample in self.all_sample_cols:
            return MS_Data_None()
        else:
            new_cols = self.name_cols+self.mass_cols+[sample]
            df = self.data_frame[new_cols]
            new_names = self.name_table[self.name_table.sample_name == sample]            
            return MS_Data(df,new_names,self.mode,self.standards)
    
    def data_as_json(self, column_key):
        """
        this will  return a json string for the database for the sample given.
        if there is a column key then the columns will be renamed to these first
        column_key should be a OrderedDict to make sure the order is kept
        example
        -----------------
        if self.data_frame = 
        mass_pair   name    s1  s2  s3
        630.6_184   PC16:0  50  100 200

        column_key = OrderedDict([(0,'mass_pair'),(2,'name'),(3,'s1')])
        
        would return
        "[{0:630.6_184,1:PC16:0,2:50,3:100,4:200}]"
        """
        if self == None:
            return ''
        df = self.data_frame.copy()
        df.Name = df.Name.apply(lambda x: x.name)
        df.Mass_Pair = df.Mass_Pair.apply(lambda x: str(x))
        new_df = pd.DataFrame()
        for key in column_key.keys():
            new_df[key] = df[column_key[key]]
        json_str = df_to_JSON(new_df)
        return json_str        
            
    def to_json(self, column_key):
        df = self.data_frame.copy()
        df.Name = df.Name.apply(lambda x: str(x.name))
        df.Mass_Pair = df.Mass_Pair.apply(lambda x: str(x))
        dct = OrderedDict()
        all_cols = self.mass_cols+self.name_cols+self.all_sample_cols
        for col in all_cols:
            dct[col] = df[col].tolist()
        return df_to_JSON(pd.DataFrame(dct))

    def format_df_for_db(self):
        """
        returns the self.dataframe with columns renamed as numbers, and an
        OrderedDict for the keys of the dataframe
        """
        if self == None:
            return ['','']
        df = self.data_frame.copy()
        cols = OrderedDict()
        col_num = 0
        for c in df.columns.tolist():
            cols[col_num] = c
            col_num += 1
        col_str = json.dumps(cols)
        df_str = self.data_as_json(column_key = cols)
        return df_str, col_str
        
    def make_TIC(self):
        """
        Takes the mass spec and outputs a total Ion count dataframe
        """
        if self.mode == None:
            pos = self.restrict_to_mode('+')
            neg = self.restrict_to_mode('-')
            pos_tic = pos.make_TIC()
            neg_tic = neg.make_TIC()
            return pd.concat([pos_tic,neg_tic], axis = 0)
        else:
            new_cols = ['Precursor']+self.all_sample_cols
            df = self.data_frame.copy()[new_cols]
            TIC_df = df.groupby('Precursor').sum().reset_index(drop = False)
            TIC_df['Mode'] = self.mode
            final_cols = ['Precursor','Mode']+self.all_sample_cols
            return TIC_df[final_cols].sort_values(by = 'Precursor').reset_index(drop = True)
                   
class MS_Data_None(MS_Data):
    """
    Used when there is no data, but allows for continuous use of the functions, they will always return None_MS_Data
    This is an MS_Data where this == None is True
    Useful when there is no data in the database for this
    """
    
    def __init__(self, data_frame = None, names = None, mode=None, ms_standards=None):
        self.data_frame = pd.DataFrame()
        self.name_table = None
        self.ms_standards = None
        self.mode = None
        self.standards = {'Observed' : {'+' : None, '-' : None}, 
                          'Input' : {'+' : None, '-' : None}}
    
    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, type(None)):
            return True
        else:
            return False
            
    def __ne__(self, other):
        """Override the default not Equals behavior"""
        return not self.__eq__(other)
                
def find(df, a, b, accuracy = .3, for_isotope = True):
    '''
    used in iso_correction to find the desired parent and daughter isotope by precursor and fragment size
    if it is found return [ture, index] if not found return [false, none]
    '''  
    a1 = a-accuracy
    a2 = a+accuracy
    b1 = b-accuracy
    b2 = b+accuracy    
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    if check.empty and for_isotope == True:
        return None
    elif for_isotope == False:
        '''
        this is used for concat_names_standards
        this should never be used for isotope correction as each mass pair should be unique
        '''
        return check.index
    elif len(check.index) > 1 and for_isotope == True:
        ms2diff = check.Fragment.apply(lambda x:abs(x-b))
        ms2diff.sort_values(inplace = True)
        return ms2diff.index[0]
    else:
        return check.index[0]

def locate(df, component, a, accuracy = .3):
    '''
    this will locate all of the indexes where 'a' is found in column 'component'
    on DataFrame df
    '''
    df = df   
    a1 = a-accuracy
    a2 = a+accuracy   
    check = df.loc[((df[component] >= a1) & (df[component] <= a2))]
    if check.empty:
        return None
    else:
        return list(check.index)

def keywithmaxval(d):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the max value"""  
     v=list(d.values())
     k=list(d.keys())
     return k[v.index(max(v))]   

def take_closest(myList, myNumber):
    """
    Assumes myList is sorted. Returns closest value to myNumber.

    If two numbers are equally close, return the smallest number.
    """
    pos = bisect_left(myList, myNumber)
    if pos == 0:
        return myList[0]
    if pos == len(myList):
        return myList[-1]
    before = myList[pos - 1]
    after = myList[pos]
    if after - myNumber < myNumber - before:
       return after
    else:
       return before

def concat_names_standards(names_table, standards_table, mode):
    """
    Adds standards to a names list for naming with the group name 'Standard'
    """    
    if standards_table is None:
        return names_table
    standards_table = standards_table[standards_table.Mode == mode]
    to_append = standards_table[['MS1', 'MS2', 'Name', 'Lipid_Group']]
    to_append.columns = ['Precursor', 'Fragment', 'Name', 'Group'] 
    assert to_append.columns.all() in names_table.columns
    #need to remove and mass pairs that match the standards from the names table
    for i in to_append.index:
        pre = to_append['Precursor'][i]
        frag = to_append['Fragment'][i]
        found = find(names_table, pre,frag, for_isotope=False)
        if len(found) != 0:
            names_table.drop(found, inplace = True)
        else:
            pass
    #remove any name from the origonal names list that shows up in the standard mix
    for name in to_append.Name:
        names_table = names_table[names_table.Name != name]
    return pd.concat([names_table, to_append]).reset_index(drop=True)
    
def combine_MS_data(pos_data, neg_data):
    '''
    This does not work if the class.data_frame has more columns than that in
    the names list    
    '''
    if pos_data is None and neg_data is None:
        raise TypeError('Both inputs are None type.')
    else:
        try:
            return pos_data.combine(neg_data)
        except:
            return neg_data.combine(pos_data)
            
def MS_Data_Like(data_frame, ms_data):
    return MS_Data(data_frame, ms_data.name_table, ms_data.mode, ms_data.standards)
    
def _test_isotope_qc():
    MS_Process(test_dir+'111814_Pano_MSMSall_pos.txt', test_dir+'Names_Pano_111814.csv', '+').saturation_check('TEST_SATURATION', 'isotope_qc_check')
    #MS_Process(test_dir+'111814_Pano_MSMSall_neg.txt', test_dir+'Names_Pano_111814.csv', '-').to_MS_data()

def make_stats_frame(df, g1_name, g2_name, g1_cols, g2_cols, paired = False, append_group_names = False):
    app = append_group_names*('_'+g2_name+'_/_'+g1_name)
    g1,g2 = g1_cols, g2_cols
    init_columns = list([x for x in df.columns.tolist() if not x in g1+g2])
    stats = df[init_columns]
    stats['Mean_'+g1_name] = df[g1].mean(axis=1)
    stats['Mean_'+g2_name] = df[g2].mean(axis=1)
    stats['Stdev_'+g1_name] = df[g1].std(axis=1)
    stats['Stdev_'+g2_name] = df[g2].std(axis=1)
    stats['Fold_Change'+app] = stats['Mean_'+g2_name].div(stats['Mean_'+g1_name])
    if len(g1) >= 3 and len(g2) >= 3:
        if not paired:        
            stats['RS_p_value'+app] = df.apply(lambda x : ranksums(x[g1], x[g2])[1], axis=1)
            stats['t_test_p_value'+app] = ttest_ind(df[g1], df[g2], axis=1)[1]
        if paired:
            stats['t_test_p_value'+app] = ttest_rel(df[g1], df[g2], axis=1)[1]
    stats['Median_'+g1_name] = df[g1].median(axis=1)
    stats['Median_'+g2_name] = df[g2].median(axis=1)
    stats['Range_'+g1_name] = df[g1].max(axis=1) - df[g1].min(axis=1)
    stats['Range_'+g2_name] = df[g2].max(axis=1) - df[g2].min(axis=1)
    cols = init_columns + ['Fold_Change'+app, 'RS_p_value'+app,
                          't_test_p_value'+app,
                          'Mean_'+g1_name, 'Stdev_'+g1_name,
                          'Median_'+g1_name, 'Range_'+g1_name,
                          'Mean_'+g2_name, 'Stdev_'+g2_name,
                          'Median_'+g2_name, 'Range_'+g2_name]
    if len(g1) < 3 or len(g2) < 3:
        cols.remove('t_test_p_value'+app)
        cols.remove('RS_p_value'+app)
    if paired:
        cols = cols.remove('RS_p_value'+app)
    stats = stats[cols]
    return stats

def make_lipid_from_col(row):
    """
    used to make lipid from row
    used in MS_class.form_db_experiment
    
    """
    name = row['Name']
    group = row['Group']
    mode = row['Mode']
    ms1 = row['Precursor']
    ms2 = row['Fragment']
    lp = Lipid.from_group(group, name, mode, mass_pair = (ms1,ms2))
    return lp
    
def process_db(df, sample_name, std_norm = True):
    """
    used to format df from db to ms_class
    used in MS_class.form_db_experiment
    
    """
    if std_norm == False:
        new_cols = ['mass_pair','mode','name','lipid_group','lipid_type','precursor','fragment','neutral_loss','counts']
    else:
        new_cols = ['mass_pair','mode','name','lipid_group','lipid_type','precursor','fragment','neutral_loss','std_norm_counts']    
    
    index_cols = ['mass_pair','mode','name','lipid_group','lipid_type','precursor','fragment','neutral_loss']
    new_df = df[new_cols]
    if std_norm == False:
        new_df.rename(index=str, columns={'counts':sample_name}, inplace = True)
    else:
        new_df.rename(index=str, columns={'std_norm_counts':sample_name}, inplace = True)
    new_df.set_index(index_cols, drop = True, inplace = True)
    return new_df
    
def df_to_JSON(df):
    x = df.to_json(orient='records')
    return x

def make_mass_pair_str(row):
    return str("%.2f" % row['Precursor'])+'_'+str("%.4f" % row['Fragment'])
    
def mv_formatting_sample_name(row):
    ms1 = '%.2f' % row.Precursor
    ms2 = '%.4f' % row.Fragment
    return ms2+'_'+ms1

def format_to_raw_file(df, drop_cols = ['Fragment', 'Mode', 'Group', 'Precursor', 'neutral_loss', 'Mass_Pair', 'Type', 'Name']):
    """
    Warning Does not account for mode!!! 
    """
    if len(list(set(df.Mode.tolist()))) == 2:
        raise ValueError('Mode can only have one value')
    new_df = df.copy()
    if not new_df.empty:
        new_df.insert(0, 'Sample Name', new_df.apply(mv_formatting_sample_name, axis = 1))
    else:
        new_df.insert(0, 'Sample Name', [])
    for n in drop_cols:
        new_df.drop(n, axis = 1, inplace = True)
    new_df.reset_index(inplace = True, drop = True)
    return new_df

def standards_different(ms_1, ms_2, mode=None, pvalue = .05):
    if mode == None:
        pos = standards_different(ms_1, ms_2, '+')
        neg = standards_different(ms_1, ms_2, '-')
        if pos == False and neg == False:
            return False
        else:
            return True
    else:
        #check if inputs or observed are different
        if isinstance(ms_1.standards['Input'][mode], type(None)) and isinstance(ms_2.standards['Input'][mode], type(None)):
            return False
        if isinstance(ms_1.standards['Input'][mode], type(None)) and not isinstance(ms_2.standards['Input'][mode], type(None)):
            return True
        if not isinstance(ms_1.standards['Input'][mode], type(None)) and isinstance(ms_2.standards['Input'][mode], type(None)):
            return True
        inputs = not ms_1.standards['Input'][mode].equals(ms_2.standards['Input'][mode])
        ms_1_observed = ms_1.standards['Observed'][mode]
        ms_2_observed = ms_2.standards['Observed'][mode]
        observed = False
        ms_1_stds = ms_1_observed['Name'].tolist()
        ms_1_stds.sort()
        ms_2_stds = ms_2_observed['Name'].tolist()
        ms_2_stds.sort()
        if ms_1_stds != ms_2_stds:
            observed = True
        else:
            for name in ms_1_stds:
                ms_1_values = ms_1_observed[ms_1_observed.Name == name][ms_1.sample_cols].iloc[0].tolist()
                ms_2_values = ms_2_observed[ms_2_observed.Name == name][ms_2.sample_cols].iloc[0].tolist()
                calc_pvalue = stat.ttest(ms_1_values, ms_2_values, paired = False)
                if calc_pvalue <= pvalue:
                    observed = True
                    break
        if observed or inputs:
            return True
        else:
            return False
        
    

##TESTS
if __name__ == '__main__2':
    pos_ms_dir = r'X:\Lipidomics\AHT\102616_ELOVL_Livers\Inputs\Raw_data\102616_ELOVL_Livers+MSALL.txt'
    neg_ms_dir = r'X:\Lipidomics\AHT\102616_ELOVL_Livers\Inputs\Raw_data\102616_ELOVL_Livers-MSALL.txt'
    name_file = r'X:\Lipidomics\AHT\102616_ELOVL_Livers\Inputs\102616_ELOVL_Livers_NamesFile.csv'
    pos_ms = MS_Data.from_raw_data_files(pos_ms_dir, name_file, '+', standard_mix='splash')
    neg_ms = MS_Data.from_raw_data_files(neg_ms_dir, name_file, '-', standard_mix='splash')
    ms_data = pos_ms.combine(neg_ms)
    fil1 = ms_data.filter_set1()
    samples = fil1.sample_groups_dict[1]+fil1.sample_groups_dict[2]
    fil2 = fil1.filter_set2(samples = samples)
    named = fil2.Name_Molecules()
    std_norm = named.standard_normalize(ignore_missing_standard=False)
    std_norm.to_csv('test3.csv')
    #filter_set1 = pos_ms.filter_set1()
    #pos_named = filter_set1.Name_Molecules()
    
if __name__ == '__main__':
    import mysql
    from Lookups.database_tables import db_login_info
    cnx=mysql.connector.connect(user = db_login_info['user'], 
                            password = db_login_info['password'],
                            host = db_login_info['host'],
                            database = db_login_info['lipid_db'])
    
    ms = MS_Data.from_db_experiment(1,cnx, iso_cor = False, std_norm = False)
    
    
    
    
