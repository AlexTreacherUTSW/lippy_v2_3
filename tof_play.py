# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 14:26:28 2017

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
import pdb
import copy
import warnings
from scipy.stats import linregress
import utilities
import mysql
from Lookups.database_tables import db_login_info
from collections import OrderedDict
import json
from TOF_classes  import *
import time

def stats(lst):
    slope, intercept, r_value, p_value, std_err = linregress(range(len(lst)),lst)
    mean = np.average(lst)
    stdev = np.std(lst, ddof = 1)
    total = np.sum(lst)
    mx = np.max(lst)
    return pd.Series(OrderedDict([('sum',total),
                                  ('mean',mean),
                                  ('stdev',stdev),
                                  ('slope',slope),
                                  ('intercept',intercept),
                                  ('r_value',r_value),
                                  ('p_value',p_value),
                                  ('std_err',std_err),
                                  ('max',mx)                        
                                  ]))

cnx=mysql.connector.connect(user = db_login_info['user'], 
                        password = db_login_info['password'],
                        host = db_login_info['host'],
                        database = db_login_info['lipid_db'])
start = time.time()
pos = TOF_data.from_db_experiment(118, cnx, mode='+')
neg = TOF_data.from_db_experiment(118, cnx, mode='-')
stop = time.time()

def tof_stats(tof, sample):
    sample_data = tof.data_frame[sample]
    sums = sample_data.apply(np.sum)
    stat = stats(sums)
    stat = sums.append(stat)
    return stat
    
if False:
    sheet_dct = OrderedDict()
    summary_sheet = pd.DataFrame()
    for i in pos.all_sample_cols:
        to_add = tof_stats(pos, i)
        to_add.name = i
        summary_sheet = pd.concat([summary_sheet, to_add], axis = 1)
    
if False:
    writer = pd.ExcelWriter(os.path.abspath(r'D:\\Temp\\tof_pilot_with_max.xlsx'))
    sheet_dct = OrderedDict()
    summary_sheet = pd.DataFrame()
    for i in pos.all_sample_cols:
        sheet_dct[i] = tof_sample_stats(pos, i)
        sheet_dct[i].to_excel(writer, i)
        sample_stats = stats(sheet_dct[i]['mean'])
        sample_stats.name = i
        summary_sheet = pd.concat([summary_sheet, sample_stats], axis = 1)
    
    #summary_sheet.to_excel(writer, 'summary')
    #writer.save()