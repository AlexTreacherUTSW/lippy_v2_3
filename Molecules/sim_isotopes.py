# -*- coding: utf-8 -*-
"""
Created on Thu Jan 29 11:23:11 2015

@author: Mike
"""
from __future__ import division
from collections import OrderedDict
import numpy as np
'''
ABUNDANCES = OrderedDict([('C', [.9893, .0107]),
                          ('H', [.999885, .000115]),
                          ('O', [.99757, .00038, .00205]),
                          ('N', [.99636, .00364]),
                          ('P', [1.0]),
                          ('S', [.9499, .0075, .0425, .0001])])                       
MASSES = OrderedDict([('C', 12.0),
                      ('H', 1.0),
                      ('O', 16.0),
                      ('P', 15.0),
                      ('S', 32.0)])
TRIOLEIN = OrderedDict([('C', 57),
                        ('H', 104),
                        ('O', 6)])
B_TRIOLE = OrderedDict([('C', 20),
                        ('H', 54),
                        ('O', 2)])            
'''
#c13 = .0109
c13 = .0115

ABUNDANCES = OrderedDict([('C', [1-c13, c13]),
                          ('H', [.999844, .000156]),
                          ('O', [.99759, .00037, .00204]),
                          ('N', [.99635, .00365]),
                          ('P', [1.0]),
                          ('S', [.9502, .0075, .0421, 0,.0002]),
                          ('Cl',[.7553,0,.2447]),
                          ('Na',[1.0]),
                          ('F',[1.0]),
                          ('D',[1.0]),
                        ])
                        
MASSES = OrderedDict([('C', 12.0),
                      ('H', 1.0),
                      ('O', 16.0),
                      ('P', 15.0),
                      ('S', 32.0),
                      ('P', 31.0),
                      ('Cl',35.0),
                      ('Na',23.0),
                      ('F',19.0),
                      ('D',2.0),
                      ('N',14.0)
                    ])
                    
TRIOLEIN = OrderedDict([('C', 57),
                        ('H', 104),
                        ('O', 6)])
B_TRIOLE = OrderedDict([('C', 20),
                        ('H', 54),
                        ('O', 2)]) 

def calc_MS1(chem_formula):
    if isinstance(chem_formula, str):
        return calc_MS1(chform_to_dict(chem_formula))
    else:
        mod_masses = OrderedDict([(k, v) for k, v in MASSES.items() 
                                  if k in chem_formula.keys()])
        base_weights = chform_to_vec(mod_masses)
        ordered_form = chform_to_vec(chem_formula)
        base_mass = base_weights.dot(ordered_form)
    return base_mass

def sim_MS1(chem_formula, neglect_thresh=None, N=1, full_mass=False):
    if N > 1:
        return [sim_MS1(chem_formula, neglect_thresh=neglect_thresh) 
                for i in range(N)]
    chem_formula = order_formula(chem_formula)
    state = build_state_matrix(chem_formula, neglect_thresh=neglect_thresh)
    i = 0
    nrows, ncols = np.shape(state)
    weights = np.arange(ncols)
    #print weights
    for i in range(nrows):
        key = chem_formula.keys()[i]
        pad_zeros = lambda x : x+(ncols - len(ABUNDANCES[key]))*[0]
        multinom_probs = pad_zeros(ABUNDANCES[key])
        #print multinom_probs
        multinom_N = chem_formula.values()[i]
        #print multinom_N
        state[i] = np.random.multinomial(multinom_N, multinom_probs)
    mass = np.sum(np.dot(state, weights))
    if full_mass:
        base_mass = calc_MS1(chem_formula)
        mass += base_mass
    return [mass, state]
    
def sim_MS2(MS1_state, chem_formula):
    chem_formula = order_formula(chem_formula)
    MS2_state = np.zeros_like(MS1_state)
    probs = make_prob_matrix(MS1_state)
    nrows, ncols = np.shape(probs)
    for i in range(nrows):
        bernoulli_N = chem_formula.values()[i]
        urn = MS1_state[i]
        MS2_state[i] = sim_bernoulli_NR(urn, bernoulli_N)
    weights = np.arange(ncols)
    mass = np.sum(np.dot(MS2_state, weights))
    return [mass, MS2_state]
    
def sim_pair(chem_formulas):
    ms1_form, ms2_form = dict_merge(chem_formulas[0], chem_formulas[1])
    #ms1_form, ms2_form = map(lambda x : order_formula(x), chem_formulas)
    m1_mass, m1_state = sim_MS1(ms1_form)
    m2_mass, m2_state = sim_MS2(m1_state, ms2_form)
    return [int(m1_mass), int(m2_mass)]
 
def MC_MS1(chem_formula, N=10000, neglect_thresh=None):
    results = sim_MS1(chem_formula, neglect_thresh=neglect_thresh, N=N)
    masses = [m[0] for m in results]
    counts = {num : masses.count(num) for num in set(masses)}
    return counts
    
def MC_pairs(chem_formulas, N=1000):
    pairs = [sim_pair(chem_formulas) for i in range(N)]
    pairs = map(lambda x : str(x), pairs)
    counts = {pair : pairs.count(pair) for pair in set(pairs)}
    return counts

def make_prob_matrix(counts):
    row_sum = counts.sum(axis=1)
    prob_matrix =  np.zeros_like(counts)
    for i in range(np.shape(counts)[0]):
        prob_matrix[i] = counts[i] / row_sum[i]
    nrows = np.shape(counts)[0]
    assert abs(prob_matrix.sum() - nrows) < 10**-6
    return prob_matrix
    
def build_state_matrix(chem_formula, neglect_thresh=None):
    nrows = len(chem_formula)
    elements = chem_formula.keys()
    ncols = max([len(ABUNDANCES[i]) for i in elements])
    return np.zeros((nrows, ncols))

def chform_to_dict(chem_formula):
    pass

def chform_to_vec(chem_formula):
    elements = filter(lambda x : x in chem_formula.keys(), MASSES.keys())
    return np.array([chem_formula[key] for key in elements])

def check_normalization():
    for el, ab in ABUNDANCES.items():
        try:
            assert abs(sum(ab) - 1.0) < 10**-6
        except AssertionError:
            print '{0} probabilities do not sum to 1'.format(el)
            
def pair_probs(ms1_form, ms2_form):
    pass

def order_formula(chem_formula):
    new_form = OrderedDict()
    for i in ABUNDANCES.keys():
        if i in chem_formula.keys():
            new_form[i] = chem_formula[i]
    return new_form
    
def dict_merge(d1, d2):
    for key in list(set(d1.keys()).symmetric_difference(d2.keys())):
        if key not in d1.keys():
                d1[key] = [0]*len(d2[key])
        if key not in d2.keys():
                d2[key] = [0]*len(d1[key])
    return order_formula(d1), order_formula(d2)

#@jit
def sim_bernoulli_NR(a, N):
#    if num_trials > 1:
#        return np.array([sim_bernoulli_NR(a, N, num_trials=1) 
#                         for i in range(num_trials)])
    hand = np.zeros_like(a)
    urn = np.array(a).copy()
    for i in range(N):
        probs = urn.cumsum() / urn.sum() #compute probabilities
#        draw = bisect_left(probs, np.random.rand()) #choose randomly
        draw = None
        i = 0
        while draw is None:
            if probs[i] > np.random.rand() :
                draw = i
            i += 1
        hand[draw] += 1 #update hand
        urn[draw] -= 1 #update urn
    return hand
    
def ntrials_bernoulli():
    pass

def test_bernoulli():
    num_draw = 1
    urn = [2,1]
    z =[]
    for i in range(1000):
        z += [sim_bernoulli_NR(urn, num_draw, num_trials=100).sum(axis=0)[0] / 100.0]
    z = np.array(z)
    mean = 2.0/3
    std = np.sqrt(mean*(1-mean)/100)
    return [mean, std], [z.mean(), z.std()]
    
        
    
   
    
            
check_normalization()
triolein_mass, triolein_state = sim_MS1(TRIOLEIN)
METHANE = {'C' : 1, 'H' : 4}
BREAK = {'C' : 1, 'H' : 1}
