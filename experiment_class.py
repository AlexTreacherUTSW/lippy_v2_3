# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 13:43:00 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
from collections import OrderedDict
from Lookups import database_tables
from Lookups.database_tables import db_tables
import MS_classes as ms
import TOF_classes as tof
from collections import OrderedDict
from Lookups.database_tables import db_login_info
import mysql
import json
import pdb
import time
import pipeline_data_analysis as pda
import shutil
import settings


class QTOF_Experiment():
    
    def __init__(self, meta_experiment, meta_samples, meta_controls, data_raw, data_raw_std_norm, data_ic, data_ic_std_norm, data_tof, AR, **kwargs):
        self.meta_experiment = meta_experiment
        self.meta_samples = meta_samples
        self.meta_controls = meta_controls
        self.data_raw = data_raw
        self.data_raw_std_norm = data_raw_std_norm
        self.data_ic = data_ic
        self.data_ic_std_norm = data_ic_std_norm
        self.data_tof = data_tof
        self.AR = AR
        self.internal_standard = self.get_standard()
        self.modes = self.get_modes()
        self.standard = self.get_standard()
        self.name_table = self.get_name_table()
        self.sample_ids = OrderedDict()
        self.experiment_id = self.get_experiment_id()
        self.reputable = False
        for key, value in kwargs.items():
          setattr(self, key, value)
        
        
    @staticmethod
    def from_file(file_loc = r'X:\Lipidomics\AHT\test_data\101116_test\Inputs', name_table = None, iso_baseline = 200, iso_report_zero = True, iso_treat_zero = True, **kwargs):
        #need to run a names file check!
        #get meta files
        meta_experiment = pd.read_csv(os.path.join(file_loc,'experiment_meta.csv'))       
        meta_samples = pd.read_csv(os.path.join(file_loc,'sample_meta.csv'))
        if 'organism_classifier' in meta_samples.columns.tolist():
            meta_samples.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
        meta_controls = pd.read_csv(os.path.join(file_loc,'control_meta.csv'))
        if 'organism_classifier' in meta_controls.columns.tolist():
            meta_controls.rename(columns = {'organism_classifier':'sample_classifier'}, inplace = True)
        if name_table != None:
            if os.path.exists(name_table):
                name_file_name = name_table
            elif os.path.exists(os.path.join(file_loc, name_table)):
                name_file_name = os.path.join(file_loc, name_table)
            else:
                print "Warning selected name_file location does not exist, the default one will in the file be used"
                name_table = None
        if name_table == None:
            possible_name_files = []
            for f in os.listdir(file_loc):
                if 'NamesFile.csv' in f:
                    possible_name_files.append(f)
            if len(possible_name_files) == 0:
                raise IOError('No default name file is located in your experiment_file')
            elif len(possible_name_files) > 1:
                raise IOError('Too many options for the default name_file have been found, please select one in the inputs, or delete others')
            else:
                name_file_name = possible_name_files[0]
        #name_file = pd.read_csv(os.path.join(file_loc,name_file_name))
        #get standard
        all_standards = list(set(meta_samples['internal_standard']))
        if len(all_standards) == 1:
            standard = all_standards[0]
            if standard == 'None':
                standard = None
        else:
            standard = None
        raw_data_loc = os.path.join(file_loc,'Raw_data')
        for f in os.listdir(raw_data_loc):
            if '+MSALL.txt' in f:
                pos_ms_file = f
            if '-MSALL.txt' in f:
                neg_ms_file = f
        try:
            pos_ms_file
        except NameError:
            pos_ms = None
        else:
            pos_ms = ms.MS_Data.from_raw_data_files(os.path.join(file_loc, 'Raw_data' ,pos_ms_file),name_file = os.path.join(file_loc,name_file_name),  mode = '+', standard_mix=standard)
        try:
            neg_ms_file
        except NameError:
            neg_ms = None
        else:
            neg_ms = ms.MS_Data.from_raw_data_files(os.path.join(file_loc,'Raw_data', neg_ms_file),name_file = os.path.join(file_loc,name_file_name),  mode = '-', standard_mix=standard)
        data_raw_unnamed = pos_ms.combine(neg_ms)
        print 'Nameing'
        if 'name_molecules_inputs' in kwargs.keys():
            data_raw = data_raw_unnamed.Name_Molecules(**kwargs['name_molecules_inputs'])
        else:
            data_raw = data_raw_unnamed.Name_Molecules()
        AR = None
        print 'Importing Tof'
        tof_data_loc = os.path.join(file_loc,'Tof_data')
        pos_tof_file = ''
        neg_tof_file = ''
        for f in os.listdir(tof_data_loc):
            if '+TOF.txt' in f:
                pos_tof_file = os.path.join(tof_data_loc,f)
            if '-TOF.txt' in f:
                neg_tof_file = os.path.join(tof_data_loc,f)
        data_tof_pos = tof.TOF_Data.from_raw_data_files(pos_tof_file, os.path.join(file_loc,name_file_name), mode = '+')
        data_tof_neg = tof.TOF_Data.from_raw_data_files(neg_tof_file, os.path.join(file_loc,name_file_name), mode = '-')
        data_tof = data_tof_pos.combine(data_tof_neg)
        input_files = OrderedDict()
        input_files['name_file'] = os.path.join(file_loc,name_file_name)
        input_files['pos_ms_raw_txt'] = os.path.join(file_loc,'Raw_data', pos_ms_file)
        input_files['neg_ms_raw_txt'] = os.path.join(file_loc,'Raw_data', neg_ms_file)
        input_files['pos_tof_raw_txt'] = pos_tof_file
        input_files['neg_tof_raw_txt'] = neg_tof_file
        return QTOF_Experiment.from_raw_MS_Data(meta_experiment = meta_experiment,
                                                meta_samples = meta_samples,
                                                meta_controls = meta_controls,
                                                data_raw = data_raw,
                                                data_tof = data_tof,
                                                AR = AR,
                                                iso_baseline = 200,
                                                iso_report_zero = True,
                                                iso_treat_zero = True,
                                                input_files = input_files,
                                                **kwargs)
                                
    @staticmethod
    def from_classes(meta_experiment_df, meta_samples_df, meta_controls_df, data_raw, data_raw_std_norm, data_ic, data_ic_std_norm, data_tof, AR, **kwargs):
        """
        This needs to be written to accept the 4 different ms_data (raw, raw_IC, std_norm, std_norm_IC)
        along with the meta data
        """
        return QTOF_Experiment(meta_experiment=meta_experiment,
                               meta_samples = meta_samples,
                               meta_controls = meta_controls,
                               data_raw = data_raw,
                               data_raw_std_norm = data_raw_std_norm,
                               data_ic = data_ic,
                               data_ic_std_norm = data_ic_std_norm,
                               data_tof = data_tof,
                               AR = AR,
                               **kwargs)
    
    @staticmethod                          
    def from_raw_MS_Data(meta_experiment, meta_samples, meta_controls, data_raw, data_tof, AR, iso_baseline = 200, iso_report_zero = True, iso_treat_zero = True, **kwargs):
        std = list(set(list(meta_samples.internal_standard.tolist())))[0]
        print 'std_norm'
        if 'ignore_missing_standard' in kwargs.keys():
            data_raw_std_norm = data_raw.standard_normalize(ignore_missing_standard=kwargs['ignore_missing_standard'])
        else:
            data_raw_std_norm = data_raw.standard_normalize()
        #this is a light filter for the isotope correction, used to save time, it can be removed once the isotope correction is parallelised 
        print 'ic'
        data_for_ic = data_raw.Average_and_Max(avg_thresh = 30, max_thresh = 300)
        pos_for_iso = data_for_ic.restrict_to_mode('+')
        neg_for_iso = data_for_ic.restrict_to_mode('-')
        if iso_treat_zero == True:
            pos_iso_data = pos_for_iso.isotope_correction(baseline = iso_baseline, 
                                                            treat = '0')
            neg_iso_data = neg_for_iso.isotope_correction(baseline = iso_baseline, 
                                                            treat = '0')
        else:
            pos_iso_data = pos_for_iso.isotope_correction(baseline = iso_baseline, 
                                                            treat = '-')
            neg_iso_data = neg_for_iso.isotope_correction(baseline = iso_baseline, 
                                                            treat = '-')
        if iso_report_zero == True:
            pos_iso_filtered = pos_iso_data[0].isotope_filter(report = '0')
            neg_iso_filtered = neg_iso_data[0].isotope_filter(report = '0')
        else:
            pos_iso_filtered = pos_iso_data[0].isotope_filter(report = '-')
            neg_iso_filtered = neg_iso_data[0].isotope_filter(report = '-')
        pos_iso = pos_iso_filtered.remove_isotopes()
        neg_iso = neg_iso_filtered.remove_isotopes()
        data_ic = pos_iso.combine(neg_iso)
        print 'IC norm'
        if 'ignore_missing_standard' in kwargs.keys():
            data_ic_std_norm = data_ic.standard_normalize(ignore_missing_standard=kwargs['ignore_missing_standard'])
        else:
            data_ic_std_norm = data_ic.standard_normalize()
        return QTOF_Experiment(meta_experiment=meta_experiment,
                               meta_samples = meta_samples,
                               meta_controls = meta_controls,
                               data_raw = data_raw,
                               data_raw_std_norm = data_raw_std_norm,
                               data_ic = data_ic,
                               data_ic_std_norm = data_ic_std_norm,
                               data_tof = data_tof,
                               AR = AR,
                               pos_isotope_data = pos_iso_data,
                               neg_isotope_data = neg_iso_data,
                               **kwargs)
    
    @staticmethod
    def from_database_experiment(cnx, experiment_id, rename = False, import_raw = True, import_ic = True, import_raw_norm = True, import_ic_norm = True, import_tof = True):
        """
        rename does not currently work
        """
        eid = str(experiment_id)
        experiment_meta_query = "SELECT * FROM meta_experiment WHERE experiment_id = " + eid
        experiment_meta = pd.read_sql_query(experiment_meta_query, cnx)
        all_samples_query = "SELECT * FROM meta_sample WHERE experiment_id = " + eid
        all_samples = pd.read_sql_query(all_samples_query, cnx)
        all_samples.set_index('sample_id', inplace=True, drop=True)
        samples = all_samples[all_samples.group_number > 0]
        control_samples = all_samples[all_samples.group_number < 0]
        sample_specific_meta = pd.DataFrame()
        control_sample_specific_meta = pd.DataFrame()
        for sid in samples.index:
            classifier = str(samples.loc[sid]['sample_classifier'])
            classifier_col = 'meta_'+classifier
            classifier_query = "SELECT * FROM " + classifier_col  + " WHERE sample_id = " + str(sid)
            class_specific_data = pd.read_sql_query(classifier_query,cnx)
            class_specific_data.set_index('sample_id', inplace=True, drop=True)
            sample_specific_meta = sample_specific_meta.append(class_specific_data)
        sample_meta = pd.concat([samples, sample_specific_meta], axis = 1)
        for sid in control_samples.index:
            classifier = str(control_samples.loc[sid]['sample_classifier'])
            classifier_col = 'meta_'+classifier
            classifier_query = "SELECT * FROM " + classifier_col  + " WHERE sample_id = " + str(sid)
            class_specific_data = pd.read_sql_query(classifier_query,cnx)
            class_specific_data.set_index('sample_id', inplace=True, drop=True)
            control_sample_specific_meta = control_sample_specific_meta.append(class_specific_data)
        control_sample_meta = pd.concat([control_samples, control_sample_specific_meta], axis = 1)
        print 'raw'
        if import_raw == True:
            data_raw = ms.MS_Data.from_db_experiment(experiment_id, cnx, iso_cor=False, std_norm=False)
        else:
            data_raw = ms.MS_Data_None()
        print 'raw sn'
        if import_raw_norm == True:
            data_raw_std_norm = ms.MS_Data.from_db_experiment(experiment_id, cnx, iso_cor=False, std_norm=True)
        else:
            data_raw_std_norm = ms.MS_Data_None()
        print 'ic'
        if import_ic == True:
            data_ic = ms.MS_Data.from_db_experiment(experiment_id, cnx, iso_cor=True, std_norm=False)
        else:
            data_ic = ms.MS_Data_None()
        print 'ic sn'
        if import_ic_norm == True:
            data_ic_std_norm = ms.MS_Data.from_db_experiment(experiment_id, cnx, iso_cor=True, std_norm=True)
        else:
            data_ic_std_norm = ms.MS_Data_None()
        print 'TOF'
        if import_tof == True:
            data_tof = tof.TOF_Data.from_db_experiment(experiment_id, cnx)
        else:
            data_tof = None
        print 'done with data'
        return QTOF_Experiment(meta_experiment=experiment_meta,
                               meta_samples = sample_meta,
                               meta_controls = control_sample_meta,
                               data_raw = data_raw,
                               data_raw_std_norm = data_raw_std_norm,
                               data_ic = data_ic,
                               data_ic_std_norm = data_ic_std_norm,
                               data_tof = data_tof,
                               AR = None)

    @staticmethod
    def from_database_samples(cnx, samples_ids, names_file):
        """
        create a experiment from samples in the database
        This needs and alignment algorythem before it is put in place
        """
        pass
    
    @staticmethod
    def delete_experiment_from_db(cnx, experiment_id):
        query = "SELECT * FROM meta_sample WHERE experiment_id = " + str(experiment_id) +';'
        sample_info = pd.read_sql_query(query, cnx)
        sample_ids = sample_info['sample_id'].tolist()
        cursor = cnx.cursor()
        for sid in sample_ids:
            sample_classifier = sample_info[sample_info.sample_id == sid].iloc[0]['sample_classifier']
            query = "DELETE FROM meta_" + sample_classifier + " WHERE sample_id = " + str(sid) +";"
            cursor.execute(query)
            cnx.commit()
            query = "DELETE FROM data_ms WHERE sample_id = " + str(sid) +";"
            cursor.execute(query)
            cnx.commit()
            query = "DELETE FROM data_tof WHERE sample_id = " + str(sid) +";"
            cursor.execute(query)
            cnx.commit()
            query = "DELETE FROM meta_sample WHERE sample_id = " + str(sid) +";"
            cursor.execute(query)
            cnx.commit()        
        query = "DELETE FROM data_experiment_tech WHERE experiment_id = " + str(experiment_id)
        cursor.execute(query)
        cnx.commit()
        query = "DELETE FROM meta_experiment WHERE experiment_id = " + str(experiment_id)
        cursor.execute(query)
        cnx.commit()                  

    def get_standard(self):
        #use meta_samples to get the standard
        std_list_all = self.meta_samples['internal_standard']
        std_list = list(set(std_list_all))
        if len(std_list) != 1:
            return None
        else:
            return str(std_list[0])
        
    def get_modes(self):
        #use data to see if there is pos or neg modes in here
        if type(self.data_raw) != type(None) and not self.data_raw.data_frame.empty:
            if self.data_raw.mode == None:
                modes = ['+','-']
            else:
                modes = [self.data_raw.mode]
        elif type(self.data_raw_std_norm) != type(None) and not self.data_raw_std_norm.data_frame.empty:
            if self.data_raw_std_norm.mode == None:
                modes = ['+','-']
            else:
                modes = [self.data_raw_std_norm.mode]
        elif type(self.data_ic) != type(None):
            if self.data_ic.mode == None:
                modes = ['+','-']
            else:
                modes = [self.data_ic.mode]
        elif type(self.data_ic_std_norm) != type(None):
            if self.data_ic_std_norm.mode == None:
                modes = ['+','-']
            else:
                modes = [self.data_ic_std_norm.mode]
        else:
            modes = None
        return modes
    
    def get_name_table(self):
        """
        check to make sure all the datasets have the sample name_table then
        return it
        """
        if hasattr(self.data_raw, 'name_table'):
            return self.data_raw.name_table
        if hasattr(self.data_raw_std_norm, 'name_table'):
            return self.data_raw_std_norm.name_table
        if hasattr(self.data_ic, 'name_table'):
            return self.data_ic.name_table
        if hasattr(self.data_ic_std_norm, 'name_table'):
            return self.data_ic.name_table
        else:
            return None
    
    def get_experiment_id(self):
        if 'experiment_id' in self.meta_experiment.columns.tolist():
            eids = list(set(self.meta_experiment['experiment_id'].tolist()))
            if len(eids) > 1:
                raise ValueError('experiment_meta can not have more than one experiment_id')
            else:
                return int(eids[0])
    
    def check_for_data_errors(self):
        """
        This is used to make sure that data is ok to be uploaded to the database
        """
        errors = []
        #check raw standards
        std_errors = self.data_raw.check_standards()
        if isinstance(std_errors, list):
            errors.extend(self.data_raw.check_standards())
        #check saturation        
        saturated_samples = self.data_raw.get_saturated_samples()
        for mode in saturated_samples.keys():
            if saturated_samples[mode].keys()!=[]:
                for ss in saturated_samples[mode].keys():
                    error_message = ss+' is calculated to be saturated based on: '
                    for name in saturated_samples[mode][ss].keys()[:3]:
                        if saturated_samples[mode][ss][name] == True:
                            error_message += str(name) +','
                    error_message = error_message[:-1]+'.'
                    errors.append(error_message)
        #check is isotope_standards are close to non-iso standards
        stds_differ = ms.standards_different(self.data_raw, self.data_ic)
        if stds_differ:
            errors.append('The standards are not in a clean space')
        if errors == []:
            self.reputable = True
        else:
            self.reputable = False
        self.data_errors = errors
        return errors

    def database_get_sample_ids(self, cnx, experiment_id):
        query = "SELECT * FROM meta_sample WHERE experiment_id = " + str(experiment_id) + ";"
        info = pd.read_sql_query(query, cnx)
        self.sample_ids = OrderedDict()
        for sid in info.sample_id.tolist():
            self.sample_ids[sid] = info[info.sample_id == sid].iloc[0]['sample_name']
    
    def database_input_experiment_meta(self, cnx):
        """"
        load experiment meta into the database
        """
        in_cols = self.meta_experiment.columns.tolist()
        db_cols = db_tables['meta_experiment']
        cols = list(set(in_cols).intersection(set(db_cols)))
        meta_experiment_for_db = self.meta_experiment[cols]
        meta_experiment_for_db.to_sql(name = 'meta_experiment', con = cnx, flavor = 'mysql',
                                      if_exists='append', index = False, chunksize = 200)
        self.experiment_id = get_last_id(cnx)
        
    def database_input_sample_meta(self, cnx):
        """"
        load sample_meta into the database, this does not include the control samples
        """
        in_cols = self.meta_samples.columns.tolist()
        db_cols = db_tables['meta_sample']
        cols = list(set(in_cols).intersection(set(db_cols)))
        self.sample_classifiers = OrderedDict()
        for index in self.meta_samples.index:
            df_for_db = self.meta_samples.loc[index][cols]
            df_for_db_clean = convert_series_for_db(df_for_db)
            df_for_db_clean.to_sql(name = 'meta_sample', con = cnx,
                             if_exists='append', flavor='mysql',
                             index = False, chunksize = 200)
            sample_name = self.meta_samples.loc[index]['sample_name']
            self.sample_ids[sample_name] = get_last_id(cnx)
            sample_class = self.meta_samples.loc[index]['sample_classifier']
            sample_class_name = 'meta_'+sample_class
            sample_specific_cols = list(set(in_cols).intersection(set(db_tables[sample_class_name])))
            sample_specific_meta = self.meta_samples[self.meta_samples.sample_name == sample_name][sample_specific_cols]
            sample_specific_meta['sample_id'] = self.sample_ids[sample_name]
            sample_specific_meta.to_sql(name = sample_class_name, con = cnx,
                                        if_exists='append', flavor='mysql',
                                        index = False, chunksize = 200)
                                        
    def database_input_control_sample_meta(self, cnx):
        in_cols = self.meta_controls.columns.tolist()
        db_cols = db_tables['meta_sample']
        cols = list(set(in_cols).intersection(set(db_cols)))
        self.sample_classifiers = OrderedDict()
        for index in self.meta_controls.index:
            df_for_db = self.meta_controls.loc[index][cols]
            df_for_db_clean = convert_series_for_db(df_for_db)
            df_for_db_clean.to_sql(name = 'meta_sample', con = cnx,
                             if_exists='append', flavor='mysql',
                             index = False, chunksize = 200)
            sample_name = self.meta_controls.loc[index]['sample_name']
            self.sample_ids[sample_name] = get_last_id(cnx)
            sample_class = self.meta_controls.loc[index]['sample_classifier']
            sample_class_name = 'meta_'+sample_class
            sample_specific_cols = list(set(in_cols).intersection(set(db_tables[sample_class_name])))
            sample_specific_meta = self.meta_controls[self.meta_controls.sample_name == sample_name][sample_specific_cols]
            sample_specific_meta['sample_id'] = self.sample_ids[sample_name]
            sample_specific_meta.to_sql(name = sample_class_name, con = cnx,
                                        if_exists='append', flavor='mysql',
                                        index = False, chunksize = 200)
                                      
    def database_input_msdata(self, cnx, method = 'MSMSALL'):
        data_names = ['data_raw', 'data_raw_std_norm', 'data_ic', 'data_ic_std_norm']
        data_tables = [self.data_raw, self.data_raw_std_norm, self.data_ic, self.data_ic_std_norm]
        for sample_name in self.sample_ids:
            for mode in ['+','-']:
                sid = self.sample_ids[sample_name]
                sample_series = pd.Series()
                sample_series['sample_id'] = sid
                sample_series['method'] = method
                sample_series['mode'] = mode
                data_cols = OrderedDict()
                data_cols['0'] = 'Name'
                data_cols['1'] = 'Group'
                data_cols['2'] = 'Precursor'
                data_cols['3'] = 'Fragment'
                data_cols['4'] = sample_name
                sample_series['data_cols'] = json.dumps(data_cols)
                for data_name, data_table in zip(data_names, data_tables):
                    if type(data_table) != type(None):
                        mode_data = data_table.restrict_to_mode(mode)
                    else:
                        mode_data = None
                    if type(mode_data) != type(None):
                        sample_series[data_name] = mode_data.data_as_json(data_cols)
                    else:
                        sample_series[data_name] = 'None'
                data_to_db = convert_series_for_db(sample_series)
                data_to_db.to_sql(name = 'data_ms', con = cnx,
                                  if_exists='append', flavor='mysql',
                                  index = False, chunksize = 200)
    
    def database_input_tofdata(self, cnx, method = 'TOF'):
        if self.data_tof != None:
            for sample_name in self.sample_ids:
                for mode in ['+','-']:
                    sid = self.sample_ids[sample_name]
                    sample_series = pd.Series()
                    sample_series['sample_id'] = sid
                    sample_series['method'] = method
                    sample_series['mode'] = mode
                    mode_data = self.data_tof.restrict_to_mode(mode)
                    json_data = mode_data.to_json(sample_name)
                    sample_series['data'] = json_data[0]
                    sample_series['data_cols'] = json_data[1]
                    json_stats = mode_data.sample_stats_json(sample_name)
                    sample_series['tof_stats'] = json_stats[0]
                    sample_series['tof_stats_cols'] = json_stats[1]
                    data_to_db = convert_series_for_db(sample_series)
                    data_to_db.to_sql(name = 'data_tof', con = cnx,
                                      if_exists='append', flavor='mysql',
                                      index = False, chunksize = 200)
        else:
            pass
        
    def make_error_output(self, output_folder):
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        #make tech data
        tech_folder = os.path.join(output_folder, 'tech_data')
        if not os.path.exists(tech_folder):
            os.mkdir(tech_folder)
        error_df = pd.DataFrame()
        for e in self.data_errors:
            error_df = error_df.append(pd.DataFrame([e]))
        error_df.to_csv(os.path.join(tech_folder, 'errors.txt'), index = False, header = False)
        standards_folder = os.path.join(tech_folder,'standards')
        if not os.path.exists(standards_folder):
            os.mkdir(standards_folder)
        if self.standard != None:
            if isinstance(self.data_raw.standards['Input']['+'], pd.DataFrame):
                self.data_raw.standards['Input']['+'].to_csv(os.path.join(standards_folder, 'Input_pos.csv'))
            if isinstance(self.data_raw.standards['Input']['-'], pd.DataFrame):
                self.data_raw.standards['Input']['-'].to_csv(os.path.join(standards_folder, 'Input_neg.csv'))
            if isinstance(self.data_raw.standards['Observed']['+'], pd.DataFrame):
                self.data_raw.standards['Observed']['+'].to_csv(os.path.join(standards_folder, 'Observed_pos.csv'))
            if isinstance(self.data_raw.standards['Observed']['-'], pd.DataFrame):
                self.data_raw.standards['Observed']['-'].to_csv(os.path.join(standards_folder, 'Observed_neg.csv'))
            if isinstance(self.data_ic.standards['Observed']['+'], pd.DataFrame):
                self.data_ic.standards['Observed']['+'].to_csv(os.path.join(standards_folder, 'Observed_pos_isotope.csv'))
            if isinstance(self.data_ic.standards['Observed']['-'], pd.DataFrame):
                self.data_ic.standards['Observed']['-'].to_csv(os.path.join(standards_folder, 'Observed_neg_isotope.csv'))

    def import_all_to_db(self, cnx, force = False):
        #run checks
        #make sure all the data is here
        #check if experiment is in already
        if self.reputable == True or force == True:
            self.database_input_experiment_meta(cnx)
            self.database_input_sample_meta(cnx)
            self.database_input_control_sample_meta(cnx)
            self.database_input_msdata(cnx)
            self.database_input_tofdata(cnx)
        else:
            print 'Data can not be put into database becasue it has either not been tested or failed the test. Please run self.check_for_data_errors to see if there are issues'

    def run_pipeline(self, output_folder, header, groups_to_compare, restrict_to_pos = True, ignore_missing_standard = False, force = False):
        """
        run the major pipeline
        order (data will be named already)
        filter
        set_baseline
        normalize                
        """
        if self.reputable != True and force != True:
            print 'Please check data before running, this data is not reputable yet'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        #make tech data
        #tech data meta
        tech_folder = os.path.join(output_folder, 'tech_data')
        if not os.path.exists(tech_folder):
            os.mkdir(tech_folder)
        tech_series = pd.Series()
        tech_series['Tech Data'] = ''
        tech_series['Run Date'] = time.strftime("%m/%d/%y")
        tech_series['Run Time'] = time.strftime("%X")
        tech_series['LipPy Version'] = 'LipPy_v2_3'
        if hasattr(self, 'experiment_id') and hasattr(self, 'sample_ids'):
            tech_series['sent_to_db'] = 'True'
            tech_series['experiment_id'] = self.experiment_id
            for sid in self.sample_ids.keys():
                tech_series['Sample_id: '+ str(sid)] = self.sample_ids[sid]
        else:
            tech_series['sent_to_db'] = 'False'
            if hasattr(self, 'experiment_id'):
                tech_series['experiment_id'] = self.experiment_id         
        tech_series.to_csv(os.path.join(tech_folder, 'run_meta.csv'))
        #tech inputs
        input_folder = os.path.join(tech_folder, 'Data_Inputs')
        if not os.path.exists(input_folder):
            os.mkdir(input_folder)
        if hasattr(self, 'input_files'):
            for i in self.input_files.values():
                try:
                    shutil.copy(i, input_folder)
                except IOError:
                    print "experiment_class - could not copy file: " + i
        #tech isotopes
        if hasattr(self, 'pos_isotope_data') or hasattr(self, 'neg_isotope_data'):
            isotope_analysis_path = os.path.join(tech_folder, 'Isotope_analysis')
            if not os.path.exists(isotope_analysis_path):
                os.makedirs(isotope_analysis_path)
            for data, mode in zip(['pos_isotope_data','neg_isotope_data'],['+','-']):
                if hasattr(self, data):
                    iso_data = getattr(self, data)
                    df = iso_data[0].data_frame
                    df.to_csv(os.path.join(isotope_analysis_path,mode+'isotope_data.csv'))
                    iso_data[1].to_csv(os.path.join(isotope_analysis_path,mode+'missing_signals.csv'))
                    iso_data[2].to_csv(os.path.join(isotope_analysis_path,mode+'Deuterium_Test.csv'))
        #tech standards
        standards_folder = os.path.join(tech_folder,'standards')
        if not os.path.exists(standards_folder):
            os.mkdir(standards_folder)
        if self.standard != None:
            if isinstance(self.data_raw.standards['Input']['+'], pd.DataFrame):
                self.data_raw.standards['Input']['+'].to_csv(os.path.join(standards_folder, 'Input_pos.csv'))
            if isinstance(self.data_raw.standards['Input']['-'], pd.DataFrame):
                self.data_raw.standards['Input']['-'].to_csv(os.path.join(standards_folder, 'Input_neg.csv'))
            if isinstance(self.data_raw.standards['Observed']['+'], pd.DataFrame):
                self.data_raw.standards['Observed']['+'].to_csv(os.path.join(standards_folder, 'Observed_pos.csv'))
            if isinstance(self.data_raw.standards['Observed']['-'], pd.DataFrame):
                self.data_raw.standards['Observed']['-'].to_csv(os.path.join(standards_folder, 'Observed_neg.csv'))
            if isinstance(self.data_ic.standards['Observed']['+'], pd.DataFrame):
                self.data_ic.standards['Observed']['+'].to_csv(os.path.join(standards_folder, 'Observed_pos_isotope.csv'))
            if isinstance(self.data_ic.standards['Observed']['-'], pd.DataFrame):
                self.data_ic.standards['Observed']['-'].to_csv(os.path.join(standards_folder, 'Observed_neg_isotope.csv'))
        #filter and apply baseline to data
        if restrict_to_pos == True:
            #restrict to mode, filter, then set baseline
            self.pipeline_data_raw = self.data_raw.restrict_to_mode('+').filter_set1(avg_thresh = settings.FILTER1_AVG_THRESH,
                                                                  maximum = settings.FILTER1_MAX,
                                                                  child_peaks= settings.FILTER1_CHILD_PEAKS,
                                                                  NL_thresh=settings.FILTER1_NL_THRESH,
                                                                  avg_thresh_group = settings.FILTER1_GROUP_MEAN,
                                                                  x_gt_y = settings.FILTER1_GROUP_QUANTILE
                                                                  ).set_baseline(settings.DATA_BASELINE)
            
            self.pipeline_data_ic = self.data_ic.restrict_to_mode('+').filter_set1(avg_thresh = settings.FILTER1_AVG_THRESH,
                                                                maximum = settings.FILTER1_MAX,
                                                                child_peaks= settings.FILTER1_CHILD_PEAKS,
                                                                NL_thresh=settings.FILTER1_NL_THRESH,
                                                                avg_thresh_group = settings.FILTER1_GROUP_MEAN,
                                                                x_gt_y = settings.FILTER1_GROUP_QUANTILE
                                                                ).set_baseline(settings.DATA_BASELINE)
        else:
            #filter, then set baseline
            self.pipeline_data_raw = self.data_raw.filter_set1(avg_thresh = settings.FILTER1_AVG_THRESH,
                                            maximum = settings.FILTER1_MAX,
                                            child_peaks= settings.FILTER1_CHILD_PEAKS,
                                            NL_thresh=settings.FILTER1_NL_THRESH,
                                            avg_thresh_group = settings.FILTER1_GROUP_MEAN,
                                            x_gt_y = settings.FILTER1_GROUP_QUANTILE
                                            ).set_baseline(settings.DATA_BASELINE)
            
            self.pipeline_data_ic = self.data_ic.filter_set1(avg_thresh = settings.FILTER1_AVG_THRESH,
                                          maximum = settings.FILTER1_MAX,
                                          child_peaks= settings.FILTER1_CHILD_PEAKS,
                                          NL_thresh=settings.FILTER1_NL_THRESH,
                                          avg_thresh_group = settings.FILTER1_GROUP_MEAN,
                                          x_gt_y = settings.FILTER1_GROUP_QUANTILE
                                          ).set_baseline(settings.DATA_BASELINE)
        self.pipeline_data_raw_std = self.pipeline_data_raw.standard_normalize()
        self.pipeline_data_ic_std = self.pipeline_data_ic.standard_normalize()
        #========================================================================
        #make all group comparison
        self.run_all_group_comparison(output_folder,
                                      header,
                                      restrict_to_pos = restrict_to_pos)
        #make group comparisons
        if groups_to_compare != None:
            for cg in groups_to_compare:
                self.run_group_comparison(output_folder, 
                                          header,
                                          cg,
                                          restrict_to_pos = restrict_to_pos,
                                          ignore_missing_standard = ignore_missing_standard)

    def run_all_group_comparison(self, root_dir, header, restrict_to_pos = True):
        agc = pda.AllGroupComparison(raw_named_data = self.pipeline_data_raw,
                                     iso_cor_data = self.pipeline_data_ic,
                                     root_dir = root_dir,
                                     header = header,
                                     raw_std_norm_data= self.pipeline_data_raw_std,
                                     iso_std_norm_data= self.pipeline_data_ic_std)
        agc.init_dir()
        agc.write_joyces_file()

    def run_group_comparison(self, root_dir, header, groups, restrict_to_pos = True, ignore_missing_standard = False):
        assert groups[0] in self.name_table.group_number and groups[1] in self.name_table.group_number
        gc = pda.GroupComparison(raw_named_data = self.pipeline_data_raw,
                                 iso_cor_data = self.pipeline_data_ic,
                                 root_dir = root_dir,
                                 header = header,
                                 comparison_groups = groups,
                                 ignore_missing_standard = ignore_missing_standard)
        gc.init_dir()
        gc.create_normalizations_file()
        gc.create_sub_group_analysis_file()
        gc.create_unknown_folder()
        gc.readme_file()
        
    
    
def convert_series_for_db(series):
    """
    used to convert a df.Series to a importable series for the database
    """
    s = series.copy()
    s_new = pd.Series()
    for k in s.keys():
        if type(s[k]) == np.int64:
            s_new[k] = float(s[k])
        else:
            s_new[k] = s[k]
    df = s_new.to_frame().transpose()
    return df

def make_json_cols_key(keys, columns):
    """
    takes 2 lists and makes a json string for the database
    """
    d = OrderedDict()
    for key, col in zip(keys, columns):
        d[str(key)] = col
    return json.dumps(d)
    
def get_last_id(cnx):
    df = pd.read_sql_query('SELECT LAST_INSERT_ID();', cnx)
    returned_id = df.loc[0]['LAST_INSERT_ID()']
    return returned_id

if __name__ == '__main__':
    cnx=mysql.connector.connect(user = db_login_info['user'], 
                            password = db_login_info['password'],
                            host = db_login_info['host'],
                            database = db_login_info['lipid_db'])
    if False:
        experiment = QTOF_Experiment.from_database_experiment(cnx, 1)
    if False:
        experiment = QTOF_Experiment.from_database_experiment(cnx, 117)
    if False:
        
        experiment = QTOF_Experiment.from_file(r'X:\Lipidomics\AHT\test_data\121416_ELOVL_Kidneys\Inputs')
        #experiment.run_pipeline(r'X:\Lipidomics\AHT\test_data\122016_ELOVL5_7_Kidney_II\v2_3', 'v2_3',[(1,2),(3,4)], restrict_to_pos=True)

    