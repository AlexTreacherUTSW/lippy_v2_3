# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 15:25:23 2016

@author: Alex
"""

import os, sys
import time
import pandas as pd
import numpy as np
from collections import OrderedDict
import json
import mysql
import Lookups.database_tables as dt
import experiment_class as ex
import ast
import pdb
import subprocess
import utilities

def update_job_progress(job_id, percent, string, cnx):
    if percent != None and string != None:
        query = 'UPDATE job_experiment_progress SET status_percent_complete = '+str(percent)+', status_progress = "'+str(string)+'" WHERE job_experiment_id = ' + str(job_id) + ';'
    elif percent == None and string != None:
        query = 'UPDATE job_experiment_progress SET status_progress = "'+str(string)+'" WHERE job_experiment_id = ' + str(job_id) + ';'
    elif percent != None and string == None:
        query = 'UPDATE job_experiment_progress SET status_percent_complete = '+str(percent)+' WHERE job_experiment_id = ' + str(job_id) + ';'
    else:
        query = ''
    print query
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()

def update_job_status_complete(job_id, complete, cnx):
    query = 'UPDATE job_experiment_progress SET status_complete = '+ str(complete) +' WHERE job_experiment_id = ' + str(job_id) + ';'
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()

def update_job_started(job_id, started, cnx):
    query = 'UPDATE job_experiment_progress SET status_started = '+ str(started) +' WHERE job_experiment_id = ' + str(job_id) + ';'
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    
def input_job(series):
    if 'filter_group_quantile' in series.keys():
        series['filter_group_quantile'] = ast.literal_eval(series['filter_group_quantile'])
    if 'filter_child_peaks' in series.keys():
        series['filter_child_peaks'] = ast.literal_eval(series['filter_child_peaks'])
    return series
    
def convert_series_for_db(series):
    """
    used to convert a df.Series to a importable series for the database
    """
    s = series.copy()
    s_new = pd.Series()
    for k in s.keys():
        if type(s[k]) == np.int64:
            s_new[k] = float(s[k])
        else:
            s_new[k] = s[k]
    df = s_new.to_frame().transpose()
    return df

def data_as_json(df, column_key):
    """
    this will  return a json string for the database for the sample given.
    if there is a column key then the columns will be renamed to these first
    column_key should be a OrderedDict to make sure the order is kept
    example
    -----------------
    if self.data_frame = 
    mass_pair   name    s1  s2  s3
    630.6_184   PC16:0  50  100 200

    column_key = OrderedDict([(0,'mass_pair'),(2,'name'),(3,'s1')])
    
    would return
    "[{0:630.6_184,1:PC16:0,2:50,3:100,4:200}]"
    """
    df = df.copy()
    new_df = pd.DataFrame()
    for key in column_key.keys():
        new_df[key] = df[column_key[key]]
    json_str = df_to_JSON(new_df)
    return json_str
    
def format_df_for_db(df):
    """
    returns the self.dataframe with columns renamed as numbers, and an
    OrderedDict for the keys of the dataframe
    """
    df = df.copy()
    cols = OrderedDict()
    col_num = 0
    for c in df.columns.tolist():
        cols[col_num] = c
        col_num += 1
    col_str = json.dumps(cols)
    df_str = data_as_json(df, column_key = cols)
    return df_str, col_str

def df_to_JSON(df):
    x = df.to_json(orient='records')
    return x
    
def get_incomplete_job_experiments(job_cnx):
    query1 = "SELECT * FROM job_experiment_progress WHERE status_started = 0"
    job_ids = pd.read_sql(query1, job_cnx)['job_experiment_id'].tolist()
    return job_ids
    
def heavy_df_to_sql(df, cnx, name, key= None):
    """
    Can find the key if theres only one key
    """
    cursor = cnx.cursor()
    #pdb.set_trace()
    if type(key) == type(None):
        table_info = pd.read_sql("DESCRIBE "+name, cnx)
        keys = table_info[table_info.Key == 'PRI']['Field'].tolist()
        assert len(keys) == 1
        key = keys[0]
    cols = df.columns.tolist()
    for index in df.index:
        key_val = df.loc[index][key]
        for col in cols:
            query = "UPDATE " + str(name) + " SET " + str(col) + " = '" + str(df.loc[index][col]) +"' WHERE " + str(key) + ' = ' + str(key_val) +';'
            cursor.execute(query)
            cnx.commit()
    
def complete_job_experiment(job_id, job_cnx, lipid_cnx, force = False):
    query = "SELECT * FROM job_experiment_progress WHERE job_experiment_id = " + str(job_id) + ";"
    job_progress = pd.read_sql(query, job_cnx)
    output_error = 'None'
    if not job_progress.loc[0]['status_started'] == True or force == True:
        update_job_started(job_id, 1, job_cnx)
        update_job_progress(job_id, 0, 'Started', job_cnx)
        job_input_query = "SELECT * FROM job_experiment_input WHERE job_experiment_id = " + str(job_id) + ';'
        update_job_started(job_id, 1, job_cnx)
        update_job_progress(job_id, 0, 'Started', job_cnx)
        job_input_query = "SELECT * FROM job_experiment_input WHERE job_experiment_id = " + str(job_id) + ';'
        job_inputs = input_job(pd.read_sql(job_input_query, job_cnx).iloc[0])
        import_raw = False
        import_ic = False
        import_raw_norm = False
        import_ic_norm = False
        if job_inputs['setting_ic'] ==  True:
            import_ic = True
        else:
            import_raw = True
        update_job_progress(job_id, 10, 'Importing Data', job_cnx)
        experiment = ex.QTOF_Experiment.from_database_experiment(lipid_cnx, 
                                                                 job_inputs['experiment_id'],
                                                                 rename = False,
                                                                 import_raw = import_raw,
                                                                 import_ic = import_ic,
                                                                 import_raw_norm = import_raw_norm,
                                                                 import_ic_norm = import_ic_norm,
                                                                 import_tof = True)
        if job_inputs['setting_ic'] ==  True:
            ms_data = experiment.data_ic
        else:
            ms_data = experiment.data_raw
        if ms_data == None:
            raise ValueError('No Data Found')
        update_job_progress(job_id, 30, 'filtering_data', job_cnx)
        filtered = ms_data.filter_set1(samples=ms_data.sample_cols,
                            avg_thresh = job_inputs['filter_ave_thresh'], 
                            maximum = job_inputs['filter_ave_max'],
                            child_peaks= job_inputs['filter_child_peaks'], 
                            NL_thresh= job_inputs['filter_nl_threshold'],
                            avg_thresh_group = job_inputs['filter_group_mean'],
                            x_gt_y = job_inputs['filter_group_quantile'])
        update_job_progress(job_id, 40, 'setting_baseline', job_cnx)
        baseline = filtered.set_baseline(job_inputs['setting_baseline'])
        update_job_progress(job_id, 50, 'applying normalizations', job_cnx)
        if not(experiment.standard == 'None' or experiment.standard == None or experiment.standard == 'null'):
            raw = baseline
            std_norm = baseline.standard_normalize()
            std_lst = std_norm.format_df_for_db()
        else:
            raw = baseline
            std_norm = None
            std_lst = ['[]','[]'] 
        raw_lst = raw.format_df_for_db()
        name_table_list = format_df_for_db(raw.name_table)
        front_page_info = raw.get_general_info()
        experiment_output_series = pd.Series()
        experiment_output_series['job_experiment_id'] = job_id
        experiment_output_series['data_msall_raw'] = raw_lst[0]
        experiment_output_series['data_msall_raw_cols'] = raw_lst[1]
        experiment_output_series['data_msall_norm_std'] = std_lst[0]
        experiment_output_series['data_msall_norm_std_cols'] = std_lst[1]
        #TOF
        experiment_output_series['name_table'] = name_table_list[0]
        experiment_output_series['name_table_cols'] = name_table_list[1]
        experiment_output_series['front_page_info'] = json.dumps(front_page_info)
        experiment_output_series['output_error'] = output_error
        #pdb.set_trace()
        job_sql_df = convert_series_for_db(experiment_output_series)
        #pdb.set_trace()
        update_job_progress(job_id, 75, 'exporting data', job_cnx)
        temp_df = pd.DataFrame({'job_experiment_id':[job_id]})
        try:
            temp_df.to_sql(con=job_cnx, name = 'job_experiment_output', if_exists='append',
                                          flavor='mysql', index = False, chunksize = 200)
        except:
            pass
        heavy_df_to_sql(df = job_sql_df, cnx = job_cnx, name = 'job_experiment_output', key = 'job_experiment_id')
        update_job_progress(job_id, 90, 'Running QC', job_cnx)
        #pdb.set_trace()
        experiment_job_qc = pd.Series()
        experiment_job_qc['job_experiment_id'] = job_id
        if ms_data != None:
            pos_ms_qc = ms_data.qc_stats(mode = '+')
            neg_ms_qc = ms_data.qc_stats(mode = '-')
            pos_ms_qc_strs = utilities.df_to_db_json(pos_ms_qc)
            neg_ms_qc_strs = utilities.df_to_db_json(neg_ms_qc)
            experiment_job_qc['pos_ms_qc'] = pos_ms_qc_strs[0]
            experiment_job_qc['pos_ms_qc_cols'] = pos_ms_qc_strs[1]
            experiment_job_qc['neg_ms_qc'] = neg_ms_qc_strs[0]
            experiment_job_qc['neg_ms_qc_cols'] = neg_ms_qc_strs[1]
        if experiment.data_tof != None:
            pos_tof_qc = experiment.data_tof.qc_stats(mode = '+')
            neg_tof_qc = experiment.data_tof.qc_stats(mode = '-')
            pos_tof_qc_strs = utilities.df_to_db_json(pos_tof_qc)
            neg_tof_qc_strs = utilities.df_to_db_json(neg_tof_qc)
            experiment_job_qc['pos_tof_qc'] = pos_tof_qc_strs[0]
            experiment_job_qc['pos_tof_qc_cols'] = pos_tof_qc_strs[1]
            experiment_job_qc['neg_tof_qc'] = neg_tof_qc_strs[0]
            experiment_job_qc['neg_tof_qc_cols'] = neg_tof_qc_strs[1]
        #pdb.set_trace()
        experiment_job_qc_df = experiment_job_qc.to_frame().transpose()
        experiment_job_qc_df.to_sql('job_quality_control', job_cnx, flavor='mysql', index = False, if_exists='append')
        update_job_status_complete(job_id, 1, job_cnx)
        update_job_progress(job_id, 100, 'Done', job_cnx)
    else:
        pass

def reset_job_experiment(job_id, cnx):
    """
    hasnt been tested yet
    """
    to_remove = ['job_experiment_output','job_quality_control']
    cursor = cnx.cursor()
    for table in to_remove:
        query = "DELETE FROM " + table + " WHERE job_experiment_id = " + str(job_id) +";"
        cursor.execute(query)
        cnx.commit()
    update_job_progress(job_id, '0', 'In Queue', cnx)
    update_job_started(job_id, 0, cnx)
    update_job_status_complete(job_id, 0, cnx)
    

def remove_job_experiment(job_id, cnx, remove_inputs = False):
    to_remove = ['job_experiment_progress','job_experiment_output','job_quality_control','job_experiment_input']
    cursor = cnx.cursor()
    for table in to_remove:
        query = "DELETE FROM " + table + " WHERE job_experiment_id = " + str(job_id) +";"
        cursor.execute(query)
        cnx.commit()
    
    
class job_experiment_loop():
    
    def __init__(self, job_cnx = None, lipid_cnx = None):
        self.job_cnx = job_cnx
        self.lipid_cnx = lipid_cnx
        self.running = False        
        
    def start(self, force_start = False):
        if self.running == False or force_start == True:
            self.running = True
            self.sub = subprocess.Popen('python job_experiment_loop.py', shell = True)
        else:
            print 'already running'
    
    def stop(self):
        if self.running == True:
            self.running = False            
            self.sub.kill()
        
    
if __name__ == '__main__':
    start_t = time.time()
    job_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                            password = dt.db_login_info['password'],
                            host = dt.db_login_info['host'],
                            database = dt.db_login_info['job_db'])
    
    lipid_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                            password = dt.db_login_info['password'],
                            host = dt.db_login_info['host'],
                            database = dt.db_login_info['lipid_db'])
    if False:
        ids = get_incomplete_job_experiments(job_cnx)
        if ids != []:
            job_id = min(ids)
            complete_job_experiment(job_id, job_cnx, lipid_cnx)
            #try:
            #    complete_job_experiment(job_id, job_cnx, lipid_cnx)
            #except:
            #    update_job_progress(job_id, 0, 'Error, please check your inputs and try again', job_cnx)
                
        print time.time() - start_t
        job_cnx.close()
        lipid_cnx.close()