# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 07:53:01 2015

@author: Alex Treacher
"""

import os ,sys
import MS_classes as ms
import TOF_classes as tof
from settings import *
import time
from scipy.stats import ttest_rel, ttest_ind
import Stats.stats as st
import clean_up as cu
import utilities as util
from collections import OrderedDict
import Stats.LipidFunctions as lf
from Templates.readme_writer import HTML_writer
import pandas as pd
import numpy as np
sys.path.append('..')
import pdb
import warnings
#import inputs

class QTOF_Pipeline():
    
    def __init__(self, output_path, input_path,                 
                 pos_mode_data = None, pos_mode_tof = None, 
                 neg_mode_data = None, neg_mode_tof = None, 
                 header = '', name_table_string = None, 
                 groups_to_test = None, to_omit = None,
                 standard_mix = None):
        """
        QTOF experiment has all fuctions to make the output files
        Order,
            name
            filter
            iso_correct
            allgroup comparison
            
        """
        if not os.path.exists(input_path):
            raise IOError('Input path not found on local machine')
        self.output_root = util.make_root_dir(os.path.join(output_path))
        self.input_root = input_path
        self.raw_data_dir = os.path.join(self.input_root,'Raw_Data')
        self.names_file_dir = os.path.join(self.input_root)
        if pos_mode_data is not None:
            self.pos_ms = pos_mode_data
        else: self.pos_ms = None
        if neg_mode_data is not None:
            self.neg_ms = neg_mode_data
        else: self.neg_ms = None
        if pos_mode_tof is not None:
            self.pos_tof = pos_mode_tof
        else: self.pos_tof = None
        if neg_mode_tof is not None:
            self.neg_tof = neg_mode_tof
        else: self.neg_tof = None
        if name_table_string is not None:
            self.name_file = os.path.join(self.names_file_dir,name_table_string)
        else:
            self.name_file = None
        self.groups_to_test = groups_to_test
        self.omit = to_omit
        self.header = header
        self.std_name = standard_mix
        self.input_data = OrderedDict([('pos_ms' , self.pos_ms),
                      ('neg_ms' , self.neg_ms),
                      ('pos_tof', self.pos_tof),
                      ('neg_tof', self.neg_tof),
                      ('name_file', self.name_file),
                      ('groups_to_test', self.groups_to_test),
                      ('omit', self.omit),
                      ('header', self.header),
                      ('new_dir', self.output_root),
                      ('std_name', self.std_name),
                      ])
        self.altered_dir = self.input_data['new_dir']

    def initDir(self):
        """
        make the new Dir
        """
        os.makedirs(self.input_data['new_dir'])

    def check_data(self):
        """
        Currently used to chech tha names file, but more checks can and will/should
        be added
        """
        name_file = pd.read_csv(self.input_data['name_file'])
        util.names_file_checker(name_file)
        if self.input_data['groups_to_test'] != None:
            for g in self.input_data['groups_to_test']:
                for i in g:
                    if i not in name_file.Group.tolist():
                        print 'Pipeline - group in groups to test, not in name_file'
        
    def importData(self):
        """
        Imports ms_data then tof_data if they are not None
        returns the tof data
        """
        #MS_data
        print 'Pipeline - Importing Data'
        if self.input_data['pos_ms'] is not None:
            self.msall_pos = ms.MS_Data.from_raw_data_files(self.input_data['pos_ms'], 
                                                            self.input_data['name_file'],
                                                            standard_mix = self.input_data['std_name'],
                                                            mode='+')
                
        else:
            self.msall_pos = None                                                
        if self.input_data['neg_ms'] is not None:      
            self.msall_neg = ms.MS_Data.from_raw_data_files(self.input_data['neg_ms'], 
                                                            self.input_data['name_file'],
                                                            standard_mix = self.input_data['std_name'],
                                                            mode='-')
        else:
            self.msall_neg = None
        #TOF Data
        if self.input_data['pos_tof'] != None:
            try:
                self.pos_tof = tof.TOF_data.from_raw_data_files(self.input_data['pos_tof'],
                                                                self.input_data['name_file'], 
                                                                mode = '+')
            except:
                self.pos_tof = None
                print 'Pipeline - Issue with pos TOF, it will be ignored'
        else:
            self.pos_tof = None
        if self.input_data['neg_tof'] != None:
            try:
                self.neg_tof = tof.TOF_data.from_raw_data_files(self.input_data['neg_tof'],
                                                                self.input_data['name_file'],
                                                                mode = '-')
            except:
                self.neg_tof = None
                print 'Pipeline - Issue with neg TOF, it will be ignored'
        else:
            self.neg_tof = None
        self.tof_data = tof.combine(self.pos_tof, self.neg_tof)           
 
 
    def nameData(self):
        """
        Named data
        
        """
        print 'Pipeline - Naming Data'
        if self.input_data['pos_ms'] is not None:
            self.pos_msall_named = self.msall_pos.Name_Molecules()
        else:
            self.pos_msall_named = None
        if self.input_data['neg_ms'] is not None:
            self.neg_msall_named = self.msall_neg.Name_Molecules()
        else:
            self.neg_msall_named = None
        if self.pos_msall_named is not None and self.neg_msall_named is not None:
            self.msall_named = self.pos_msall_named.combine(self.neg_msall_named)
        elif self.pos_msall_named is not None and self.neg_msall_named is None:
            self.msall_named = self.pos_msall_named
        elif self.neg_msall_named is not None and self.pos_msall_named is None:
            self.msall_named = self.neg_msall_named
        else:
            raise ValueError ('Please put in either pos or neg data')
            self.msall_named = None
        self.for_dataBase = self.msall_named

    def filterData(self):
        """
        filters the data from namedata
        """
        print 'Pipeline - Filtering Data'
        if self.input_data['pos_ms'] is not None:
            self.pos_filtered_noBaseline = self.pos_msall_named.filter_set1(
                                                avg_thresh = FILTER1_AVG_THRESH,
                                                maximum = FILTER1_MAX,
                                                child_peaks=FILTER1_CHILD_PEAKS,
                                                NL_thresh=FILTER1_NL_THRESH,
                                                x_gt_y=FILTER1_GROUP_QUANTILE)
            self.pos_msall_filtered = self.pos_filtered_noBaseline.set_baseline(DATA_BASELINE)
        else:
            self.pos_filtered_noBaseline = None
            self.pos_msall_filtered = None
        if self.input_data['neg_ms'] is not None:
            self.neg_filtered_noBaseline = self.neg_msall_named.filter_set1(
                                                avg_thresh = FILTER1_AVG_THRESH,
                                                maximum = FILTER1_MAX,
                                                child_peaks=FILTER1_CHILD_PEAKS,
                                                NL_thresh=FILTER1_NL_THRESH,
                                                avg_thresh_group=FILTER1_GROUP_MEAN,
                                                x_gt_y=FILTER1_GROUP_QUANTILE)
            self.neg_msall_filtered = self.neg_filtered_noBaseline.set_baseline(DATA_BASELINE)
        else:
            self.neg_filtered_noBaseline = None
            self.neg_msall_filtered = None

    def isotopeCorrectMsall(self):
        """
        isotope corrects the data from filtered data
        """
        if not hasattr(self,'msall_named'):
            self.nameData()
        start_iso_time = time.time()
        if self.input_data['pos_ms'] is not None:
            print 'Pipeline - Isotopically correcting positive data'
            print 'Pipeline - '
            iso_cor_pos_dfs = self.pos_msall_filtered.isotope_correction(baseline = ISOTOPE_BASELINE, 
                                                                      treat = '0')
            self.iso_cor_master_pos_msall = iso_cor_pos_dfs[0]
            self.missing_pos_msall = iso_cor_pos_dfs[1]
            self.mcs_pos_msall = iso_cor_pos_dfs[2]
            self.pos_msall_raw_isotopes_named = self.iso_cor_master_pos_msall.isotope_raw()
            self.pos_msall_iso_filtered = self.iso_cor_master_pos_msall.isotope_filter(report = '0')
            self.pos_msall_iso_cor_temp = self.pos_msall_iso_filtered.remove_isotopes()
            self.pos_msall_iso_cor = self.pos_msall_iso_cor_temp.set_baseline(DATA_BASELINE)
        else:
            iso_cor_pos_dfs = None
            self.iso_cor_master_pos_msall = None
            self.missing_pos_msall = None
            self.mcs_pos_msall = None
            self.pos_msall_raw_isotopes_named = None
            self.pos_msall_iso_filtered = None
            self.pos_msall_iso_cor = None
        if self.input_data['neg_ms'] is not None:
            print 'Pipeline - Isotopically correcting negative data'
            iso_cor_neg_dfs = self.neg_msall_filtered.isotope_correction(baseline = ISOTOPE_BASELINE, 
                                                                      treat = '0')
            self.iso_cor_master_neg_msall = iso_cor_neg_dfs[0]
            self.missing_neg_msall = iso_cor_neg_dfs[1]
            self.mcs_neg_msall = iso_cor_neg_dfs[2]
            self.neg_msall_raw_isotopes_named = self.iso_cor_master_neg_msall.isotope_raw()
            self.neg_msall_iso_filtered = self.iso_cor_master_neg_msall.isotope_filter(report = '0')
            self.neg_msall_iso_cor_temp = self.neg_msall_iso_filtered.remove_isotopes()
            self.neg_msall_iso_cor = self.neg_msall_iso_cor_temp.set_baseline(DATA_BASELINE)
        else:
            iso_cor_neg_dfs = None
            self.iso_cor_master_neg_msall = None
            self.missing_neg_msall = None
            self.mcs_neg_msall = None
            self.neg_msall_raw_isotopes_named = None
            self.neg_msall_iso_filtered = None
            self.neg_msall_iso_cor = None
        self.mcs_msall = pd.concat([self.mcs_pos_msall,self.mcs_neg_msall],axis = 1)
        stop_iso_time = time.time()
        self.iso_time = util.read_time(stop_iso_time-start_iso_time)
        print 'Pipeline - Time to iso_correct data, %s' %(self.iso_time)
        #raw_data_with_the_isotopes_named
        try:
            self.msall_raw_cor_data = self.pos_msall_raw_isotopes_named.combine(self.neg_msall_raw_isotopes_named)        
            self.msall_iso_cor_data = self.pos_msall_iso_cor.combine(self.neg_msall_iso_cor)
        except AttributeError:
            self.msall_raw_cor_data = self.neg_msall_raw_isotopes_named.combine(self.pos_msall_raw_isotopes_named)        
            self.msall_iso_cor_data = self.neg_msall_iso_cor.combine(self.pos_msall_iso_cor)
        self.ic_for_dataBase = self.msall_iso_cor_data
        
    def normalizeToStandard(self, ignore_missing_standard = False):
        """
        funtion used to normalize to standard if there is a standard present
        MC_classes will check to see if there is an outlyer in the standards
        """
        if self.input_data['std_name'] != None:
            name_table = self.msall_raw_cor_data.name_table.copy()
            if 'Standard_Volume' not in name_table.columns.tolist():
                self.rawNormToStandard = None
                self.isoNormToStandard = None
                print ('Pipeline - Warning, Standard_Volume not in names table so will not standardize')
            if 'Sample_Amount' not in name_table.columns.tolist():
                self.rawNormToStandard = None
                self.isoNormToStandard = None
                print ('Pipeline - Warning, Sample_Amount not in names table so will not standardize')
            else:
                #std_norm the raw_data
                if hasattr(self,'msall_named'):
                    if self.input_data['pos_ms'] is not None and self.input_data['neg_ms'] is not None:
                        #checking the raw data to see if its ok to stdNorm
                        missing_pos_raw = self.pos_msall_named.samples_missing_standard()
                        missing_neg_raw = self.neg_msall_named.samples_missing_standard()
                        if set(missing_pos_raw) == set(missing_neg_raw) or ignore_missing_standard == True:
                            self.rawNormToStandard = self.msall_raw_cor_data.standard_normalize(ignore_missing_standard == True)
                        else:
                            self.rawNormToStandard = None
                            print 'Pipeline - Miss match missing iso_cor standards so can not normalize to standard'
                            print 'Pipeline - %r missing from pos, %r missign from neg' %(missing_pos_raw,missing_neg_raw) 
                    elif self.input_data['pos_ms'] is not None and self.input_data['neg_ms'] is None:
                        self.rawNormToStandard = self.msall_raw_cor_data.standard_normalize()
                        print 'Pipeline - Only positive data found to standardize'
                    elif self.input_data['neg_ms'] is not None and self.input_data['pos_ms'] is None:                 
                        self.rawNormToStandard = self.msall_raw_cor_data.standard_normalize()
                        print 'Pipeline - Only negative data found to standardize'
                        #checking the cor data to see if its ok to stdNorm
                else:
                    print 'Pipeline - Please call self.nameData() before trying to notmalize to Standard'
                #std_nrom the iso corrected data, but only if raw data can be corrected
                if hasattr(self,'msall_iso_cor_data') and self.rawNormToStandard != None:
                    if self.input_data['pos_ms'] is not None and self.input_data['neg_ms'] is not None:
                        missing_pos_cor = self.pos_msall_iso_cor.samples_missing_standard()
                        missing_neg_cor = self.neg_msall_iso_cor.samples_missing_standard()
                        if set(missing_pos_cor) == set(missing_neg_cor) or ignore_missing_standard == True:
                            self.isoNormToStandard = self.msall_iso_cor_data.standard_normalize(ignore_missing_standard == True)
                        else:
                            print 'Pipeline - miss match missing iso_cor standards so can not normalize to standard'
                            print 'Pipeline - %r missing from pos, %r missign from neg' %(missing_pos_cor,missing_neg_cor)                       
                            self.isoNormToStandard = None
                    elif self.input_data['pos_ms'] is not None and self.input_data['neg_ms'] is None:
                        self.isoNormToStandard = self.msall_iso_cor_data.standard_normalize()
                        print 'Pipeline - Only positive data found in iso_cor data to standardize'
                    elif self.input_data['neg_ms'] is not None and self.input_data['pos_ms'] is None:
                        self.isoNormToStandard = self.msall_iso_cor_data.standard_normalize()
                        print 'Pipeline - Only negative data found in iso_cor data to standardize'
                else:
                    self.isoNormToStandard = None
        else:
            self.rawNormToStandard = None
            self.isoNormToStandard = None
            print('Pipeline - No standard found to normalize to.')
            
    def additional_run_file(self, method, filter_settings):
        """
        Needs work, used to 
        """
        if '+' in method:
            ARmethod = '+'
        elif '-' in method:
            ARmethod = '-'
        else:
            raise ValueError ('method must have \'+\' or \'-\' in to be a valid AR method')
        ARoutput = os.path.join(self.output_root,'Additional Run Analysis',method)
        if not os.path.exists(ARoutput):
            os.makedirs(ARoutput)
        txt_data_loc = os.path.join(self.raw_data_dir,method)
        ARUnfilteredData = ms.MS_Data.from_raw_data_files(txt_data_loc,
                                                          self.input_data['name_file'],
                                                          standard_mix = self.input_data['std_name'],
                                                          mode = ARmethod)
        ARfiltered = ARUnfilteredData.all_filters(filter_settings)
        ARnamedData = ARfiltered.Name_Molecules(to_name = method)
        ARmasterisocor = ARnamedData.isotope_correction()[0]
        ARisocor = ARmasterisocor[0].isotope_filter(report = '0')
        ARisocor.data_frame.to_csv('lol.csv')
        #what to do now, stats, iso, subgroup?
        #coninue writing===============================================================================
    
    def run_AR(self, ARs):
        filter_settings = 1
        for method in ARs:
            self.additional_run_file(method, filter_settings)
            
    def writeTechFiles(self):
        '''
        isotopeCorrectionMsall needs to be called before this!
        writes tech files usefull for post running checking
        '''
        dirs = {'inputs_data':os.path.join(self.altered_dir,'tech_data','Data_Inputs'),
                'isotope_data':os.path.join(self.altered_dir,'tech_data','Isotope_analysis'),
                'standards':os.path.join(self.altered_dir,'tech_data','Standards'),
                'qc':os.path.join(self.altered_dir,'tech_data','QC')}
        for v in dirs.values():
            if not os.path.exists(v):
                os.makedirs(v)
        raw_data = ['pos_ms','neg_ms','pos_tof','neg_tof']
        name_data = ['name_file']        
        for i in self.input_data:
            if (i in raw_data+name_data) and (self.input_data[i] != None):
                util.copy_file(self.input_data[i],dirs['inputs_data'])
        if self.input_data['pos_ms'] != None:
            self.iso_cor_master_pos_msall.data_frame.to_csv(os.path.join(dirs['isotope_data'],self.input_data['header']+'_pos_isotope_data'+'.csv'))
            self.missing_pos_msall.to_csv(os.path.join(dirs['isotope_data'],self.input_data['header'] + '_Missing_Pos'+'.csv'))
        if self.input_data['neg_ms'] != None:
            self.iso_cor_master_neg_msall.data_frame.to_csv(os.path.join(dirs['isotope_data'],self.input_data['header']+'_neg_isotope_data'+'.csv'))
            self.missing_neg_msall.to_csv(os.path.join(dirs['isotope_data'],self.input_data['header'] + '_Missing_Neg'+ '.csv'))
        self.mcs_msall.to_csv(os.path.join(dirs['isotope_data'],self.input_data['header']+'_Deuterium_Test'+'.csv'))
        if hasattr(self, 'msall_named'):
            if self.input_data['std_name'] is not None:
                try:
                    input_std = pd.concat([self.msall_named.standards['Input']['+'],
                                           self.msall_named.standards['Input']['-']])
                    observed_std = pd.concat([self.msall_named.standards['Observed']['+'],
                                           self.msall_named.standards['Observed']['-']])
                    input_std.to_csv(os.path.join(dirs['standards'],self.header+'_Input_Standards.csv'))
                    observed_std.to_csv(os.path.join(dirs['standards'],self.header+'_Observed_Standards.csv'))
                except ValueError:
                    print('Pipeline - No standards to write in tech files')
            else:
                print 'Pipeline - No standards to log'
        if hasattr(self, 'msall_iso_cor_data'):
            if self.input_data['std_name'] is not None:
                try:
                        iso_observed_std = pd.concat([self.msall_iso_cor_data.standards['Observed']['+'],
                                                      self.msall_iso_cor_data.standards['Observed']['-']])
                        iso_observed_std.to_csv(os.path.join(dirs['standards'],self.header+'_Iso_Observed_Standards.csv'))
                except ValueError:
                    print('Pipeline - No standards to write in tech files')
            

    def performGroupAnalysis(self, ignore_missing_standard):
        '''
        calls performSeficicGroupAnalysis on each of the groups
        '''
        print 'Pipeline - Group Analysis in progress'
        if self.input_data['groups_to_test']:
            if self.input_data['name_file'] != None:
                for g in self.input_data['groups_to_test']:
                    print 'Pipeline - Performing analysis on groups '+str(g[0])+' and '+str(g[1])
                    self.comparison = GroupComparison(raw_named_data = self.msall_raw_cor_data,
                                                      iso_cor_data = self.msall_iso_cor_data,
                                                      raw_std_norm_data=self.rawNormToStandard,
                                                      iso_std_norm_data=self.isoNormToStandard,
                                                      root_dir = self.altered_dir,
                                                      header = self.input_data['header'],
                                                      comparison_groups = g,
                                                      ignore_missing_standard = ignore_missing_standard)
                    self.comparison.init_dir()
                    self.comparison.create_normalizations_file()
                    self.comparison.create_sub_group_analysis_file()
                    self.comparison.create_unknown_folder()
                    self.comparison.readme_file()
        else:
            print ('Pipeline - No group analysis to complete, as no groups given.')
            
    def performSpecificGroupAnalysis(self, g, ignore_missing_standard = False):
        """
        Uses the class group comparison to compare two groups that are given
        as a iterable
        """
        assert len(g) == 2 and type(g[0]) == int and type(g[1]) == int
        print 'Pipeline - Performing analysis on groups '+str(g[0])+' and '+str(g[1])
        self.comparison = GroupComparison(raw_named_data = self.msall_raw_cor_data,
                                          iso_cor_data = self.msall_iso_cor_data,
                                          raw_std_norm_data=self.rawNormToStandard,
                                          iso_std_norm_data=self.isoNormToStandard,
                                          root_dir = self.altered_dir,
                                          header = self.input_data['header'],
                                          comparison_groups = g,
                                          ignore_missing_standard=self.ignore_missing_standard)
        self.comparison.init_dir()
        self.comparison.create_normalizations_file()
        self.comparison.create_sub_group_analysis_file()
        self.comparison.create_unknown_folder()
        self.comparison.readme_file()
        

    def performAllGroupAnalysis(self):
        """
        makes the all group comparson for the output
        """
        if self.input_data['name_file'] != None:
            self.all_data_comparison = AllGroupComparison(raw_named_data=self.msall_raw_cor_data,
                                                          iso_cor_data=self.msall_iso_cor_data,
                                                          raw_std_norm_data=self.rawNormToStandard,
                                                          iso_std_norm_data=self.isoNormToStandard,
                                                          root_dir=self.altered_dir,
                                                          header=self.input_data['header'])
            self.all_data_comparison.init_dir()
            self.all_data_comparison.write_joyces_file()

    def noNameTableAnalysis(self):
        """
        analysis when no names file is given
        needs testing and implamenting. might be taken out and option built into
        run
        """
        if self.input_data['name_file'] == None:
            files_to_write = OrderedDict()
            raw_path = os.path.join(self.new_dir,self.header+'_Raw_Data.csv')
            mass_cols = self.msall_raw_cor_data.name_cols + self.msall_raw_cor_data.mass_cols
            sample_cols = self.msall_raw_cor_data.sample_cols
            raw_stats = self.msall_raw_cor_data.data_frame[mass_cols]
            raw_stats['Mean'] = self.msall_raw_cor_data.data_frame[sample_cols].mean(axis=1)
            raw_stats['Stdev'] = self.msall_raw_cor_data.data_frame[sample_cols].std(axis=1)
            files_to_write[raw_path] = OrderedDict([('Data',self.msall_raw_cor_data),
                                                    ('Stats',raw_stats)])
            if hasattr(self,'msall_iso_cor_data'):
                iso_path = os.path.join(self.new_dir,self.header+'_Iso_Cor_Data.csv')
                iso_stats = self.msall_iso_cor_data.data_frame[mass_cols]
                iso_stats['Mean'] = self.msall_iso_cor_data.data_frame[sample_cols].mean(axis=1)
                iso_stats['Stdev'] = self.msall_iso_cor_data.data_frame[sample_cols].std(axis=1)
                files_to_write[iso_path] = OrderedDict([('Data', self.msall_iso_cor_data),
                                                        ('Stats', iso_stats)])
                                                        
            xlsx_files = [files_to_write]
            for files in xlsx_files:
                for sheet in files:
                    iterable_to_excel(files[sheet],sheet)    
        else:
            raise ValueError('Intended only for use if names file not given')
        
    def execute(self, ignore_missing_standard = False):
        """
        Used to run a sample set through pipelines and output the data
        """
        if not self.input_data['name_file'] == None:
            self.check_data()            
            self.initDir()
            self.importData()
            self.nameData()
            self.filterData()
            self.isotopeCorrectMsall()
            self.normalizeToStandard(ignore_missing_standard = ignore_missing_standard)
            self.writeTechFiles()
            self.performAllGroupAnalysis()
            self.performGroupAnalysis(ignore_missing_standard = ignore_missing_standard)
            #self.load_to_database()
        elif self.input_data['name_file'] == None:
            self.initDir()
            self.importData()
            self.nameData()
            self.filterData()
            self.isotopeCorrectMsall()
            self.normalizeToStandard(ignore_missing_standard = ignore_missing_standard)
            self.writeTechFiles()
            self.noNameTableAnalysis()
        return self.for_dataBase, self.ic_for_dataBase, self.tof_data

class AllGroupComparison():
    """
    Used for tha all group analysis with a namesfile
    normalizes and performs statistics. 
    """
    def __init__(self, raw_named_data, iso_cor_data, root_dir, header, raw_std_norm_data=None, iso_std_norm_data=None):
        self.root_dir = root_dir
        self.folder_root = os.path.join(self.root_dir,'All_group_comparison')
        self.raw_data = raw_named_data
        self.iso_data = iso_cor_data
        self.raw_std_norm_data = raw_std_norm_data
        self.iso_std_norm_data = iso_std_norm_data
        self.sample_cols = self.raw_data.sample_cols
        self.groups_dict = self.raw_data.groups_dict
        self.group_num_dict = self.raw_data.group_num_dict
        self.groups_present = self.raw_data.groups_present()
        self.types_present = self.raw_data.types_present()
        self.header = header
        
    def init_dir(self):
        """
        Creates the directory
        """
        'Creating subfolders for All_Group Analysis.'
        dirs_to_init = [self.folder_root]
        for d in dirs_to_init:
            if not os.path.exists(d):        
                os.makedirs(d)
            else:
                print 'Pipeline - Warning duplicate test being run, may over write files in %s' %(d) 
    
    def joyce_fun(self):
        """
        makes the files for the all group analysis
        """
        All_group_NIC = self.raw_data.Compare_All_Groups()
        All_group_norm_NIC = self.raw_data.Compare_All_Groups(normalize = True)
        All_group = self.iso_data.Compare_All_Groups()
        All_group_norm = self.iso_data.Compare_All_Groups(normalize = True)
        files_to_make = OrderedDict([(os.path.join(self.folder_root,self.header+'_All_Groups_Comparison_NIC'),
                                           OrderedDict([('Data',self.raw_data),
                                                        ('Stats',All_group_NIC)])),
                                     (os.path.join(self.folder_root,self.header+'_Norm_All_Groups_Comparison_NIC'),
                                           OrderedDict([('Data',self.raw_data.normalize()),
                                                        ('Stats',All_group_norm_NIC)])),
                                     (os.path.join(self.folder_root,self.header+'_Norm_All_Groups_Comparison'),
                                           OrderedDict([('Data',self.iso_data.normalize()),
                                                        ('Stats',All_group_norm)])),
                                     (os.path.join(self.folder_root,self.header+'_All_Groups_Comparison'),
                                           OrderedDict([('Data',self.iso_data),
                                                        ('Stats',All_group)]))
                                    ])
        if self.raw_std_norm_data != None and self.iso_std_norm_data != None:
            All_norm_to_std_NIC = self.raw_std_norm_data.Compare_All_Groups()
            All_norm_to_std = self.iso_std_norm_data.Compare_All_Groups()
            NTS_NIC_file = os.path.join(self.folder_root,self.header+'_Normalized_to_standard_comparison_NIC')
            NTS_file = os.path.join(self.folder_root,self.header+'_Normalized_to_standard_comparison')
            files_to_make[NTS_NIC_file] = OrderedDict([('Data',self.raw_std_norm_data),
                                                      ('Stats',All_norm_to_std_NIC)])
            files_to_make[NTS_file] = OrderedDict([('Data',self.iso_std_norm_data),
                                                   ('Stats',All_norm_to_std)])
        elif self.raw_std_norm_data == None and self.iso_std_norm_data != None:
            print ('Pipeline - Standard normalized data not found for allGroupComparison')
        elif self.raw_std_norm_data != None and self.iso_std_norm_data == None:
            print ('Pipeline - Standard normalized data not found for allGroupComparison')
        elif self.raw_std_norm_data == None and self.iso_std_norm_data == None:
            print ('Pipeline - No norm_to_standard data to write')
        else:
            pass
        return files_to_make
    
    def write_joyces_file(self):
        files_to_make = self.joyce_fun()
        for f in files_to_make:
            iterable_to_excel(files_to_make[f],f)
    
class GroupComparison():
    """
    Used to perform and create files for a group comparison
    """
    def __init__(self, raw_named_data, iso_cor_data, root_dir, header, comparison_groups, ignore_missing_standard = False):
        self.root_dir = root_dir
        self.raw_data = raw_named_data
        self.iso_data = iso_cor_data
        self.cg = comparison_groups
        self.sample_cols = self.raw_data.sample_cols
        #worth checking if raw and iso data have same groups?
        self.groups_dict = self.raw_data.groups_dict
        self.g1, self.g2 = self.groups_dict[self.cg[0]], self.groups_dict[self.cg[1]]
        self.group_num_dict = self.raw_data.group_num_dict
        self.g1_name = self.group_num_dict[self.cg[0]]
        self.g2_name = self.group_num_dict[self.cg[1]]
        self.groups_present = self.raw_data.groups_present()
        self.types_present = self.raw_data.types_present()
        self.group_string = str(self.group_num_dict[self.cg[0]]+'v'+self.group_num_dict[self.cg[1]])       
        self.group_root = os.path.join(self.root_dir,self.group_string)
        self.comparison_col = self.g1+self.g2
        self.header = header + '_' + self.group_string
        # apply filter to raw_named and iso_cor_data
        self.samples_in_cg = self.groups_dict[self.cg[0]]+self.groups_dict[self.cg[1]]
        self.raw_filtered = self.raw_data.filter_set2(avg_thresh=FILTER2_AVERAGE_THRESHOLD, 
                                                      NL_thresh=FILTER2_NL_THRESH, 
                                                      x_gt_y=FILTER2_THRESH_TUPLE, 
                                                      child_peaks=FILTER2_CHILD_PEAKS,
                                                      samples = self.samples_in_cg)
        self.iso_filtered = self.iso_data.filter_set2(avg_thresh=FILTER2_AVERAGE_THRESHOLD, 
                                                      NL_thresh=FILTER2_NL_THRESH, 
                                                      x_gt_y=FILTER2_THRESH_TUPLE, 
                                                      child_peaks=FILTER2_CHILD_PEAKS,
                                                      samples = self.samples_in_cg)
        self.groups_in_types_dict = self.iso_filtered.groups_in_types_dict()
        self.raw_std_norm_data = self.raw_filtered.standard_normalize(ignore_missing_standard = ignore_missing_standard)
        self.iso_std_norm_data = self.iso_filtered.standard_normalize(ignore_missing_standard = ignore_missing_standard)
        self.comparison_change_dct = {}
    
    def init_dir(self):
        'Creating subfolders for Subgroup Analysis.'
        dirs_to_init = [self.group_root,
                        os.path.join(self.group_root,'DMS'),
                        os.path.join(self.group_root,'Lipid_Class_Analysis'),
                        os.path.join(self.group_root,'Normalizations'),
                        os.path.join(self.group_root,'Normalizations','Pos_only'),
                        os.path.join(self.group_root,'Normalizations','Neg_only'),
                        os.path.join(self.group_root,'Plots'),
                        os.path.join(self.group_root,'Tutorials')]
        for d in dirs_to_init:
            if not os.path.exists(d):        
                os.makedirs(d)
            else:
                print 'Pipeline - Warning duplicate test being run, may over write files in %s' %(d)
                
    def norm_sheets(self, msclass, type_of_norm='counts', Raw_data = False, apply_normalization = True):
        '''
        Makes the data_frames for the sheets for norm_files,
        used mostly for norm_files function in class
        '''
        filtered = msclass        
        named = msclass.remove_unknowns()
        if apply_normalization == True:
            if type_of_norm == 'counts':
                named_norm = named.normalize()
                filtered_norm = filtered.normalize()
            elif type_of_norm == 'moles':
                named_norm = named.mol_normalize()
                filtered_norm = filtered.mol_normalize()
        elif apply_normalization == False:
            named_norm = named
            filtered_norm = filtered
        else:
            raise ValueError('apply_normalization in norm_sheets needs to be True or False.')
        named_data_sheet = named_norm.data_frame.drop('Mass_Pair', axis = 1)
        named_stats_sheet = named_norm.Hypothesis_Tests(self.cg).drop('Mass_Pair', axis = 1)
        sheets_to_create = OrderedDict([('Named_data', named_data_sheet),
                                        ('Filtered_data', filtered_norm.data_frame.drop('Mass_Pair', axis = 1)),
                                        ('Named_stats', named_stats_sheet),
                                        ('Filtered_stats', filtered_norm.Hypothesis_Tests(self.cg).drop('Mass_Pair', axis = 1))])
        if Raw_data:
            sheets_to_create['Raw_data'] = msclass.data_frame.drop('Mass_Pair', axis = 1)
        return sheets_to_create

    def norm_files(self, raw, iso, save_loc, raw_std_norm = None, iso_std_norm = None, omics_file = False):
        '''
        takes 2 MS_Classes and makes the sheets for the normalization files
        '''        
        Norm_to_counts = self.norm_sheets(iso)
        Norm_to_counts_NIC = self.norm_sheets(raw)
        Norm_to_moles = self.norm_sheets(iso, type_of_norm='moles')
        iso_cor = self.norm_sheets(iso, apply_normalization = False)     
        raw_cor = self.norm_sheets(raw, apply_normalization = False)
        files_to_make = {os.path.join(save_loc,self.header+'_Norm_to_counts'): Norm_to_counts,
                         os.path.join(save_loc,'Raw-er_data',self.header+'_Norm_to_counts_NIC'): Norm_to_counts_NIC,
                         os.path.join(save_loc,'Raw-er_data',self.header+'_Norm_to_moles'): Norm_to_moles,
                         os.path.join(save_loc,'Raw-er_data',self.header+'_Raw_Counts'):iso_cor,
                         os.path.join(save_loc,'Raw-er_data',self.header+'_Raw_Counts_NIC'):raw_cor}
        if raw_std_norm != None and iso_std_norm != None:
            files_to_make[os.path.join(save_loc,self.header+'_Norm_std')] = self.norm_sheets(raw_std_norm, apply_normalization=False)
            files_to_make[os.path.join(save_loc,self.header+'_Norm_std_NIC')] = self.norm_sheets(iso_std_norm, apply_normalization=False)
        return files_to_make

    def create_normalizations_file(self):
        '''
        creates a list of dictionarys, with dictionarys in them
        example:
        xlsx_files = 
        [0] = {root+'Norm_to_couts':{'Named_data': named_norm.data_frame.drop('Mass_Pair', axis = 1),
                                 'Filtered_Data': filtered_norm.data_frame.drop('Mass_Pair', axis = 1),
                                 'Named_Stats': named_norm.Hypothesis_Tests(self.cg).drop('Mass_Pair', axis = 1),
                                 'Filtered_stats': filtered_norm.Hypothesis_Tests(self.cg).drop('Mass_Pair', axis = 1)}}
        [1]...
        '''
        print 'Pipeline - Creating normalizations_file'
        pos_raw = self.raw_filtered.restrict_to_mode('+')
        pos_iso = self.iso_filtered.restrict_to_mode('+')
        neg_raw = self.raw_filtered.restrict_to_mode('-')
        neg_iso = self.iso_filtered.restrict_to_mode('-')
        all_raw = self.raw_filtered
        all_iso = self.iso_filtered
        #add norm to standard as needed#
        if self.raw_std_norm_data != None and self.iso_std_norm_data != None:
            pos_raw_std_norm = self.raw_std_norm_data.restrict_to_mode('+')
            neg_raw_std_norm = self.raw_std_norm_data.restrict_to_mode('-')
            pos_iso_std_norm = self.iso_std_norm_data.restrict_to_mode('+')
            neg_iso_std_norm = self.iso_std_norm_data.restrict_to_mode('-')
            raw_std_norm = self.raw_std_norm_data
            iso_std_norm = self.iso_std_norm_data
        else:
            pos_raw_std_norm = None
            neg_raw_std_norm = None
            pos_iso_std_norm = None
            neg_iso_std_norm = None
            raw_std_norm = None
            iso_std_norm = None
        #folder root
        norm_folder = os.path.join(self.group_root,'Normalizations')
        omics_folder = self.group_root
        #files to make
        combined_norm_file = self.norm_files(all_raw,
                                             all_iso,
                                             norm_folder,
                                             raw_std_norm=raw_std_norm,
                                             iso_std_norm=iso_std_norm)
        pos_norm_folder = self.norm_files(pos_raw,
                                          pos_iso,
                                          os.path.join(norm_folder,'Pos_only'),
                                          raw_std_norm=pos_raw_std_norm,
                                          iso_std_norm=pos_iso_std_norm)
        neg_norm_folder = self.norm_files(neg_raw,
                                          neg_iso,
                                          os.path.join(norm_folder,'Neg_only'),
                                          raw_std_norm=neg_raw_std_norm,
                                          iso_std_norm=neg_iso_std_norm)
        #makes the omics file from combined_norm
        Omics_sheets =  ['Named_stats','Filtered_stats']
        Omics_file = {os.path.join(self.group_root+'Group_Lipidomics'):make_omics_file(combined_norm_file[os.path.join(norm_folder,self.header+'_Norm_to_counts')],
                                     Omics_sheets)}
        xlsx_files = [pos_norm_folder, neg_norm_folder, combined_norm_file, Omics_file]
        #for the readme
        self.norm_named = combined_norm_file[os.path.join(norm_folder,self.header+'_Norm_to_counts')]['Named_data']
        self.norm_named_stats = combined_norm_file[os.path.join(norm_folder,self.header+'_Norm_to_counts')]['Named_stats']
        #return xlsx_files
        for files in xlsx_files:
            for sheet in files:
                iterable_to_excel(files[sheet],sheet)
                
    def comparison_change_fun(self, omics_file, group, significance = SIGNIFICANCE):
        '''
        this is to add a True or False to the comparison_change_dict.
        True if one or more of the p-values are below the significance
        used for goups, not used or tested for types
        '''
        if 't_test_p_value' in omics_file.columns.tolist():
            p_value = omics_file['t_test_p_value']
            P_above_significnce = [p for p in p_value if p <= significance]
            nu_changed = len(P_above_significnce)
            if nu_changed > 0:
                self.comparison_change_dct[group] = 'True'
            else:
                self.comparison_change_dct[group] = 'False'
        else:
            self.comparison_change_dct[group] = 'NA (n<3)'
        
        
    def sub_type_files(self, raw, iso, typ, save_loc, raw_std_norm_data=None, iso_std_norm_data=None):
        """
        Makes the sub_type files for the group comparison
        """
        if typ in iso.types_present():
            Per_of_Class = iso.restrict_to_type(typ).normalize().summarize_type_feature(typ, self.cg)
            Per_of_Named = iso.remove_unknowns().normalize().summarize_type_feature(typ, self.cg)
            Omics = OrderedDict([(('Percent_of_class'),Per_of_Class['Stats'])])
            self.comparison_change_fun(Omics['Percent_of_class'],typ)
        else:
            Per_of_Class = {}
            Per_of_Named = {}
            Omics = {}
        NIC_Per_of_Class = raw.restrict_to_type(typ).normalize().summarize_type_feature(typ, self.cg)
        NIC_Per_of_Named = raw.remove_unknowns().normalize().summarize_type_feature(typ, self.cg)
        NIC_Omics = OrderedDict([(('NIC_Percent_of_class'),NIC_Per_of_Class['Stats'])])       
        to_drop = ['MS1_Formula','MS2_Formula','Precursor','Fragment','neutral_loss','Group','Type','Mode','Carbons','Double_Bonds','Fold_Change']
        for gn in self.g1_name,self.g2_name:
            for m in ['Mean_','Stdev_','Median_','Range_']:
                to_drop.append(m+gn)
        for d in to_drop:
            try:
                NIC_Omics['NIC_Percent_of_class'].drop(d, inplace = True)
                Omics['Percent_of_class'].drop(d, inplace = True)
            except ValueError:
                pass
            
        files_to_make = {os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Lipidomics_NIC'):NIC_Omics,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Norm_to_Counts_Percent_of_Class_NIC'):NIC_Per_of_Class,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Norm_to_Counts_Precent_of_Named_NIC'):NIC_Per_of_Named,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,self.header+'_'+typ+'_Lipidomics'): Omics,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,self.header+'_'+typ+'_Norm_to_Counts_Percent_of_Class'):Per_of_Class,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Norm_to_Counts_Precent_of_Named'):Per_of_Named,}
        
        if raw_std_norm_data != None and iso_std_norm_data != None:
            std_per_of_class = iso_std_norm_data.restrict_to_type(typ).normalize().summarize_type_feature(typ, self.cg)
            std_per_of_named = iso_std_norm_data.remove_unknowns().normalize().summarize_type_feature(typ, self.cg)
            NIC_std_per_of_class = raw_std_norm_data.restrict_to_type(typ).normalize().summarize_type_feature(typ, self.cg)
            NIC_std_per_of_named = raw_std_norm_data.remove_unknowns().normalize().summarize_type_feature(typ, self.cg)
            files_to_make[os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Standardized_Norm_to_Counts_Percent_of_Class')] = std_per_of_class
            files_to_make[os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Standardized_Norm_to_Counts_Percent_of_Named')] = std_per_of_named
            files_to_make[os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Standardized_Norm_to_Counts_Percent_of_Class_NIC')] = NIC_std_per_of_class
            files_to_make[os.path.join(save_loc,'Lipid_Class_Analysis',typ,'Raw-er_data',self.header+'_'+typ+'_Standardized_Norm_to_Counts_Percent_of_Named_NIC')] = NIC_std_per_of_named
        return files_to_make

    def sub_group_files(self, raw, iso, group, save_loc, typ):
        """
        makes the sub_group files for the group comparisons
        """
        if group in iso.groups_present():
            Per_of_Named = iso.remove_unknowns().normalize().summarize_group_feature(group, self.cg)
            Per_of_Subclass = iso.restrict_to_group(group).normalize().summarize_group_feature(group, self.cg)
            Per_of_Class = iso.restrict_to_type(typ).normalize().summarize_group_feature(group, self.cg)
            Omics = OrderedDict([(('Percent_of_class'),Per_of_Subclass['Stats'].drop(['MS1_Formula','MS2_Formula','Precursor','Fragment','neutral_loss'],axis = 1))])
            self.comparison_change_fun(Omics['Percent_of_class'],group)        
        else:
            Per_of_Subclass = {}
            Per_of_Named = {}
            Per_of_Class = {}
            Omics = {}
        NIC_Per_of_Subclass = raw.restrict_to_group(group).normalize().summarize_group_feature(group, self.cg)
        NIC_Per_of_Class = raw.restrict_to_type(typ).normalize().summarize_group_feature(group, self.cg)
        NIC_Per_of_Named = raw.remove_unknowns().normalize().summarize_group_feature(group, self.cg)
        NIC_Omics = OrderedDict([(('NIC_Percent_of_class'),NIC_Per_of_Subclass['Stats'].drop(['MS1_Formula','MS2_Formula','Precursor','Fragment','neutral_loss'],axis = 1))])
        files_to_make = {os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Lipidomics_NIC'):NIC_Omics,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Norm_to_Counts_Percent_of_Subclass_NIC'):NIC_Per_of_Subclass,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Norm_to_Counts_Percent_of_Class_NIC'):NIC_Per_of_Class,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Norm_to_Counts_Precent_of_Named_NIC'):NIC_Per_of_Named,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,self.header+'_'+group+'_Lipidomics'): Omics,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,self.header+'_'+group+'_Norm_to_Counts_Percent_of_Subclass'):Per_of_Subclass,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Norm_to_Counts_Percent_of_Class'):Per_of_Class,
                         os.path.join(save_loc,'Lipid_Class_Analysis',typ,group,'Raw-er_data',self.header+'_'+group+'_Norm_to_Counts_Precent_of_Named'):Per_of_Named,}
        return files_to_make

    def create_sub_group_analysis_file(self):
        '''
        use raw_data for types, as it is possible the isotopes will have the
        type or group filtered out
        '''
        types_present = self.raw_filtered.types_present()
        groups_present = self.raw_filtered.types_present()
        type_files = []
        group_files = []
        to_skip = ['Isotope', 'Misc','Unknown' , 'PA']
        for s in to_skip:
            if s in types_present:
                types_present.remove(s)
            if s in groups_present:
                groups_present.remove(s) 
        for t in types_present:
            if 'standard' not in t.lower():
                save_loc = os.path.join(self.group_root,'Lipid_Class_Analysis',t)
                print '    Pipeline - Executing analysis on ' + t
                if not os.path.exists(save_loc):        
                    os.makedirs(save_loc)
                type_files.append(self.sub_type_files(self.raw_filtered,self.iso_filtered, t, self.group_root, raw_std_norm_data=self.raw_std_norm_data, iso_std_norm_data=self.iso_std_norm_data))

        #return xlsx_files            
        groups_present = self.raw_filtered.groups_present()
        for g in groups_present:
            if 'standard' not in g.lower():
                group_ms = self.raw_filtered.restrict_to_group(g)
                print '        Pipeline - Executing analysis on ' + g
                try:
                    typ = group_ms.data_frame['Type'][group_ms.data_frame.index[0]]
                except IndexError:
                    typ = ''
                if typ not in to_skip:
                    save_loc = os.path.join(self.group_root,'Lipid_Class_Analysis',typ,g)
                    if not os.path.exists(save_loc):        
                        os.makedirs(save_loc)
                    group_files.append(self.sub_group_files(self.raw_filtered,self.iso_filtered, g, self.group_root, typ))
        xlsx_files = type_files+group_files       
        for files in xlsx_files:
            for sheet in files:
                iterable_to_excel(files[sheet],sheet)

    def row_for_read_me(self,lipid_class):
        """        
        this makes the row for the read me table.
        # changed is how many p values between the group is less than the significance from the Norm_to_counts_nic
        composition change is True if there is a at least one p value that is significant in the Norm _to_counts percant of class
        """
        if lipid_class in list(self.norm_named['Type']): 
            data_t = self.norm_named[self.norm_named['Type'] == lipid_class]
            lipid_str = lipid_class
        elif lipid_class in list(self.norm_named['Group']):
            data_t = self.norm_named[self.norm_named['Group'] == lipid_class]
            lipid_str = '____'+lipid_class
        else:
            raise AttributeError ('lipid_class not in type or group of comaprison.norm_named')
        sums = data_t[self.g1+self.g2].sum(axis = 0)
        g1_sums = sums[self.g1]
        g2_sums = sums[self.g2]
        t_g1_ave = np.average(g1_sums)
        t_g2_ave = np.average(g2_sums)
        t_fold_change = t_g2_ave/t_g1_ave
        t_ttest = ttest_ind(g1_sums, g2_sums)[1]
        all_p_values = ttest_ind(data_t[self.g1], data_t[self.g2], axis=1)[1]
        P_above_significnce = [p for p in all_p_values if p <= SIGNIFICANCE]
        nu_changed = len(P_above_significnce)
        nu_observed = data_t.shape[0]
        row = [lipid_str,
               t_g1_ave,
               str(round(t_fold_change,3))+'('+str(round(t_ttest,3))+')',
               nu_observed,
               nu_changed,
               self.comparison_change_dct[lipid_class]]
        df = pd.DataFrame([row],columns = ['Lipid Class','% of Lipids','Fold Change','# Observed','# Changed','Composition Change'])
        return df
    
    def create_unknown_folder(self):
        """
        Makes the unknown folder in the group comparison output
        """
        unknown_root = os.path.join(self.group_root,'Lipid_Class_Analysis','Unknowns')
        if not os.path.exists(unknown_root):        
            os.makedirs(unknown_root)
        unknowns = self.iso_filtered.restrict_to_type('Unknown')
        component_dct = {}
        xlsx_files = []
        for c in ['Precursor','Fragment','neutral_loss']:
            c_sheet = unknowns.bin_and_sum(c)
            c_sheet.sort('# examples', inplace = True, ascending = False)
            component_dct[os.path.join(unknown_root,self.header+c+'.xlsx')] = OrderedDict ([('sample_data',c_sheet),
                        ('stats',st.Hypothesis_Tests_Sheet(c_sheet,self.g1,self.g2,g1_name = self.g1_name, g2_name = self.g2_name, cols_to_keep = [c,'# examples','Mode'], paired=False, append_group_names=False, sort=False))
                        ])
            xlsx_files.append(component_dct)

        for files in xlsx_files:
            for sheet in files:
                iterable_to_excel(files[sheet],sheet)

    def readme_file(self):
        read_me_df_cols = ['Lipid Class', '% of Lipids', 'Fold Change', '# Observed', '# Changed', 'Composition Change']
        readme_df = pd.DataFrame(columns = read_me_df_cols)
        for t in self.groups_in_types_dict.keys():
            readme_df=readme_df.append(self.row_for_read_me(t), ignore_index=True)
            for v in self.groups_in_types_dict[t]:
                readme_df=readme_df.append(self.row_for_read_me(v), ignore_index=True)
        HTML_writer(os.path.join(self.group_root,'README.html'),self.g1_name,self.g2_name,readme_df)

def iterable_to_excel(iterable, saveas, sort_by_name = True):
    '''
    used to pass dictionarys of and sigle objects of MS_classes to excel
    must not have 'mass_pairs' column in to work
    used lipids_to_str to change the class of lipids to a stirng to it can write
    '''
    if iterable is None: #do nothing if passed None-type
        return None
    if not saveas.endswith('.xlsx'):
        saveas = saveas.split('.')[0]+'.xlsx'
    dir_path = os.path.dirname(saveas)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    if len(iterable) > 0:
        with pd.ExcelWriter(saveas) as writer:
            if hasattr(iterable, 'keys'):
                to_iter = iterable.items()
            else:
                to_iter = iterable
            for key, val in to_iter:
#                print key
                if hasattr(val, 'data_frame'):
                    write_index = not val.data_frame.index.name is None ##want to write index if it's of type liipd
                    
                elif hasattr(val, 'index'):
                    write_index = not val.index.name is None
                else:
                    raise AttributeError(val.__class__.__name__+' has no attribute index or data_frame.')
                df = lipids_to_str(val)
                if 'Mass_Pair' in df.columns.tolist():
                    df.drop('Mass_Pair', axis = 1, inplace = True)
                if 'Name' in df.columns.tolist() and sort_by_name == True:
                    df.sort_values(by = 'Name', axis = 0, inplace=True)
                df.to_excel(writer, sheet_name=key, index=write_index, float_format='%.4f')
            writer.save()
    return None

def lipids_to_str(ms):
    '''
    used by iterable_to_excel to make lipids classes into strings so it can 
    write to excel
    '''    
    if hasattr(ms, 'data_frame'):
        return lipids_to_str(ms.data_frame)
    else:
        df = ms.copy()
        if 'Name' in df.columns:
            df['Name'] = df.Name.apply(lambda x : str(x))
        if 'Group' in df.columns:
            df['Group'] = df.Group.apply(lambda x : str(x))
        return df
        
def changes_in_p(df,g1,g2,significance):
    all_p_values = ttest_ind(df[g1], df[g2], axis=1)[1]
    P_above_significnce = [p for p in all_p_values if p <= significance]
    nu_changed = len(P_above_significnce)
    if nu_changed > 0:
        changed = True
    else:
        changed = False
    return changed, nu_changed, all_p_values

def make_omics_file(file_dct,sheets, to_drop = None):
    """
    Used to make an omics file from specified sheets, and drops specified columns
    default columns set if to_drop = None
    """
    omics_file = OrderedDict()
    if to_drop == None:
        to_drop = ['Group','Type','neutral_loss','Mode','MS1_Formula','MS2_Formula']
    for sheet in sheets:
        if sheet in file_dct.keys():
            df = file_dct[sheet].copy()
            old_cols = df.columns.tolist()
            new_cols = []
            for col in old_cols:
                if col in to_drop:
                    pass
                elif 'median' in col.lower() or 'range' in col.lower():
                    pass
                else:
                    new_cols.append(col)
            new_df = df[new_cols]
            omics_file[sheet] = new_df
    return omics_file
