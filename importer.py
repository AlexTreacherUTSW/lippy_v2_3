# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 12:25:00 2016

@author: Alex
"""
import pandas as pd
import numpy as np
import shelve
import os, sys
import LipPy_v2_0.MS_classes as ms
import mysql.connector

'''
rules for input tables
for sample_info
    if sample has been ran before put in sample id number
    if not put in 'Null'
    sample_info.sample_id MUST be a int or 'Null'
    no need to input all sample_info if it has been ran before
    if duplicate sample_name, the rest opf the sample_info must match up

'''

#open up the Database and create a cursor for it
con=mysql.connector.connect(user = 'root', 
                             password = 'UTSWLipidDB',
                             host='localhost',
                             database = 'utswlipiddb')
cursor=con.cursor()


#raw_data file locations
pos_ms = r'D:\Alex Treacher\mySQL\SWLDB\for_DB\020416_PNPLA3_neg.txt'
neg_ms = r'D:\Alex Treacher\mySQL\SWLDB\for_DB\020416_PNPLA3_neg.txt'
names = r'D:\Alex Treacher\mySQL\SWLDB\for_DB\020616_PNPLA3_names.csv'
chrono = r'D:\Alex Treacher\mySQL\SWLDB\for_DB\020616_PNPLA3_CHRONOS.csv'
sample_info = r'D:\Alex Treacher\mySQL\SWLDB\for_DB\sample_info.csv'
lst = [pos_ms,neg_ms,names,sample_info]

#import into ms_classes and into dataframes
#msall_pos = ms.MS_Data.from_raw_data_files(pos_ms, names, mode='+')
#msall_neg = ms.MS_Data.from_raw_data_files(neg_ms, names, mode='-')
pos_named = msall_pos.Name_Molecules()
#pos_cor = pos_named.iso
neg_named = msall_neg.Name_Molecules()
sidf = pd.read_csv(sample_info)
chdf = pd.read_csv(chrono)

#organize into tables for the dataframe
#sample_info dataframe, remember that this has all samples in it!
all_sample_info_df = sidf[['sample_id','sample_name','user','date_received','pi_of_origin','sample_contact','organism_classifier']]
samples = list(set(all_sample_info_df.sample_name.tolist()))

for index in all_sample_info_df.index:
    info = all_sample_info_df.iloc[[index]]
    if info.sample_id[index] == 'Null':
        sample_name = info.sample_name[index]
        new_id = pd.read_sql_query('SELECT MAX(sample_id) FROM sample_info', con).iloc[0,0]+1
        all_sample_info_df.loc[all_sample_info_df.sample_name == sample_name, 'sample_id'] = new_id
        info.sample_id = new_id
        info.to_sql(con=con, name = 'sample_info', if_exists='append', flavor='mysql', index = False)
    
    
    
#add samples_to_database and insert sample_id into all_sample_info_df
for s in samples:
    #assign id's    
    info = all_sample_info_df[all_sample_info_df.sample_name == s].head(1)
    if sample_id == 'Null':
        sample_id = df['ID'] = pd.read_sql_query('select ifnull(max(id),0)+1 from db_table',con).iloc[0,0]+range(len(df))
        info.to_sql(con=con, name = 'sample_info', if_exists='append', flavor='mysql', index = False)
        organism_classifier = info.organism_classifier.values[0]
        #add to databse and get back id
    else:
        check_data
        #check the sample_id and make sure the info match if given.
        #insert infomation into sample_info_df
        pass
    #get sample_id for the sample names 



'''
con=mysql.connector.connect(user = 'root', 
                             password = 'UTSWLipidDB',
                             host='localhost',
                             database = 'utswlipiddb')
c=conn.cursor()
c.execute("""INSERT INTO table VALUES
(data)""")
conn.commit()
df.to_sql(con = con, name = 'table1', if_exists='append', flavor='mysql', index = False)
con.close()
'''
