# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 07:23:18 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
from Compounds import MASSES
from mass_pairs import *
from collections import OrderedDict
import pdb
import create_neg_lipids as neg_functions


neg_adducts = []
neg_adducts.append(OrderedDict([('C',-1),('H',-3)]))
neg_adducts.append(OrderedDict([('H',-1)]))
neg_adducts.append(OrderedDict([('F',1)]))
neg_adducts.append(OrderedDict([('H',1),('F',2)]))
neg_adducts.append(OrderedDict([('CL',1)]))



input_csv = r'D:\Alex Treacher\Projects\new_names\for_meeting\neg_rules_adducts'
os.chdir(input_csv)
files_adducts = OrderedDict()
files_adducts['PC'] = {'H':1,'F':2}
files_adducts['LPC'] = {'H':1,'F':2}
files_adducts['PE'] = {'H':-1}
files_adducts['LPE'] = {'H':-1}
files_adducts['PG'] = {'H':-1}
files_adducts['LPG'] = {'H':-1}
files_adducts['PS'] = {'H':-1}
files_adducts['LPS'] = {'H':-1}
files_adducts['PA'] = {'H':-1}
files_adducts['LPA'] = {'H':-1}
files_adducts['PI'] = {'H':-1}
files_adducts['LPI'] = {'H':-1}



def formula_to_mass(formula):
    return sum(mul_dict(formula,MASSES).values())    

def adduct_to_name(adduct):
    mass = formula_to_mass(adduct)
    if mass >= 0:
        sign = '+'
    else:
        sign = '-'
    string = '['+sign
    for k in adduct.keys():
        string+= k
        num = adduct[k]
        if abs(num) > 1:
            string += str(abs(num))
    string += ']'
    return string

def add_adduct(df, adduct):
    if type(df) == str:
        new_df = pd.read_csv(df)
    else:
        new_df = df.copy()
    adduct_str = adduct_to_name(adduct)
    for i in df.index:
        new_df = new_df.set_value(i,'Name', new_df.loc[i]['Name']+adduct_str)
    return new_df
    
def new_name(name, adduct_str):
    if ' or ' in name:
        s = name.split(' or ')
        new_name = ''
        for name in s:
            new_name += name+adduct_str+ ' or '
        return new_name[:-4]
    else:
        return name+adduct_str
        
def PL_adduct(df, current_adduct, adduct_list = neg_adducts):   
    if type(df) == str:
        old_df = pd.read_csv(df)
        new_df = add_adduct(pd.read_csv(df), current_adduct)
    else:
        old_df = df.copy()
        new_df = add_adduct(df, current_adduct)
    calc_adducts = []
    for adduct in adduct_list:
        if current_adduct != adduct:
            calc_adducts.append(adduct)
    for adduct in calc_adducts:
        adduct_mass = formula_to_mass(adduct)
        adduct_str = adduct_to_name(adduct)
        for i in old_df.index:
            try:
                to_append = pd.Series()
                to_append['Precursor'] = old_df.loc[i]['Precursor'] + adduct_mass
                to_append['Fragment'] = old_df.loc[i]['Fragment']
                to_append['Name'] = new_name(old_df.loc[i]['Name'], adduct_str)
                to_append['Group'] = old_df.loc[i]['Group']
                new_df = new_df.append(to_append, ignore_index = True)
            except:
                pdb.set_trace()
    return new_df
            
    
if __name__ == "__main__2":        
    for f in files_adducts.keys():
        current_adduct = files_adducts[f]
        file_name = os.path.join(input_csv,f+'.csv')
        new_file_name = os.path.join(input_csv,f+'_adducts.csv')
        new_df = PL_adduct(file_name, current_adduct)
        new_df.to_csv(new_file_name, index = False)
        os.remove(file_name)
    full = neg_functions.concat_file(input_csv, ignore=['complete.csv','complete_rules.csv','overlaps.csv','overlaps_rules.csv','overlaps_after_priority.csv'])
    full.to_csv(os.path.join(input_csv,'complete.csv'), index = False)
    ol = neg_functions.check_overlap(full)
    ol.to_csv(r'overlaps.csv')
    
if __name__ == "__main__":
        old_names = pd.read_csv('complete_rules.csv')
        to_append = []
        for group in files_adducts.keys():
            group_df = old_names[old_names.Group == group]
            old_names.drop(group_df.index.tolist(), inplace = True)
            old_names.to_csv('del.csv')
            adduct_df = PL_adduct(group_df, files_adducts[group])
            to_append.append([adduct_df])
            x = 1
        new_names = old_names.copy()
        for df in to_append:
            new_names = new_names.append(df, ignore_index = True)
        new_names.to_csv('names_with_adducts.csv')
        ol = neg_functions.check_overlap(new_names)
        ol.to_csv('adduct_overlaps.csv')