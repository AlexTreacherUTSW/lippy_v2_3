# -*- coding: utf-8 -*-
"""
Created on Thu Jun 02 16:19:30 2016

@author: Alex
"""


import os, sys
import pandas as pd
import numpy as np
import Lookups.database_tables
db_tables = Lookups.database_tables.db_tables
import MS_classes as ms
import TOF_classes as tof
import mysql
from collections import OrderedDict
import pdb
import utilities as util

class Sample():
    
    def __init__(self, sample_meta, experiment_meta, ms_data, ic_ms_data, 
                 tof_data = None, cnx = None, retrieve_id = False):
        #this will be changed thoughout the program if it is deemed the sample
        #is not eligible for the db, eg the sample_id is already taken
        self.db_eligible = True
        self.ms_data = ms_data
        self.ic_ms_data = ic_ms_data
        self.tof_data = tof_data
        self.sample_meta = sample_meta
        self.experiment_meta = experiment_meta
        self.cnx = cnx
        self.get_sample_id()
        self.check_SE_id()

    @staticmethod
    def init_from_data(sample_name, sample_meta, experiment_meta, ms_data, ic_ms_data,
                       tof_data = None, sample_id = None, cnx = None):
        """
        initiate calss with sample_id
        if sample_id (regardless if cnx) is given sample_id will be set to value
            it can only be added it db if this key is not taken
        if cnx is not none and sample_id is none, the parent class will look
            at the experiment number and decide what to set it as
        if both none, sample_id will be set to unknown, and will not be able to
            be put into the db
        if retrieve set to True, if experiment number is already used the class
            will go into the database and seach for the correct sample_id
            if not found sample_id set to unknown
        if retrieve set to False and experiment number is already used
            the sample_id will be set to unknown
        """
        if sample_id != None:
            sample_id = int(sample_id)
        else:
            sample_id = 'Null'
        #load sample_meta    
        if type(sample_meta) == pd.DataFrame:
            all_sample_df = sample_meta
        else:
            try:
                all_sample_df = pd.read_csv(sample_meta)
            except:
                raise TypeError('sample_meta must be a dataframe or path to a readable file')
        sample_df = all_sample_df[all_sample_df.sample_name == sample_name]
        sample_df.dropna(axis = 1, inplace = True)
        sample_df = util.df_format_date_sql(sample_df)
        if sample_df.shape[0] > 1:
            raise ValueError ('Sample_classes - Can not be samples with the same name in sample_meta')
        else:
            s_fvi = sample_df.first_valid_index()
        #load experiment_meta
        if type(experiment_meta) == pd.DataFrame:
            experiment_df = experiment_meta
        else:
            try:
                experiment_df = pd.read_csv(experiment_meta)
            except:
                raise TypeError('experiment_meta must be a dataframe or path to a readable file')
        if experiment_df.shape[0] > 1:
            raise ValueError ('experiment_meta can not have more than one row')
        else:
            ex_fvi = experiment_df.first_valid_index()
            experiment_id = experiment_df.loc[ex_fvi]['experiment_id']
            experiment_df = util.df_format_date_sql(experiment_df)
        #gets organism classifier
        organism_classifier = sample_df.loc[s_fvi]['organism_classifier']
        #gets the attibutes from the dbt and sets them
        sample_db_attr = union(db_tables['sample_info'],db_tables[organism_classifier])
        #puts the sample_meta into a OrderedDict to be passes to main class
        sample_meta_dct = OrderedDict()
        sample_df_cols = sample_df.columns.tolist()
        missing_attr = []
        extra_attr = []
        for attr in union(sample_db_attr,sample_df_cols):
            if attr == 'sample_id' and sample_id in [None,'Null','null']:
                sample_meta_dct[attr] = 'Null'
            elif attr == 'sample_id' and not sample_id in [None,'Null','null']:
                sample_meta_dct[attr] = int(sample_id)
            elif attr == 'experiment_id':
                sample_meta_dct[attr] = experiment_id
            elif (attr in sample_df_cols) and (not attr in sample_db_attr) and (attr not in ['sample_number']):
                extra_attr.append(attr)
            else:
                if not attr in sample_df_cols:
                    sample_meta_dct[attr] = 'Null'
                    if attr not in ['sample_number','group_number','group_name']:#possibly needed to add constrains
                        missing_attr.append(attr)
                else:
                    sample_meta_dct[attr] = sample_df.loc[s_fvi][attr]
        if missing_attr != []:
            print 'sample_classes - ' + sample_name +' is missing attr:' + str(missing_attr)
        if extra_attr != []:
            print 'sample_classes - ' + sample_name +' has extra attr:' + str(extra_attr)
        #puts the experiment_meta into OrderedDict to be passed to the main class
        experiment_meta_dct = OrderedDict()
        experiment_df_cols = experiment_df.columns.tolist()
        experiment_db_attr = db_tables['experiment_info']
        missing_attr = []
        extra_attr = []
        for attr in union(experiment_db_attr,experiment_df_cols):
            if (attr in experiment_df_cols) and (not attr in experiment_db_attr):
                extra_attr.append(attr)
            else:
                if not attr in experiment_df_cols:
                    experiment_meta_dct[attr] = 'Null'
                    missing_attr.append(attr)
                else:
                    experiment_meta_dct[attr] = experiment_df.loc[ex_fvi][attr]    
        if missing_attr != []:
            print 'sample_classes - ' + sample_name +' is missing experiment attr:' + str(missing_attr)
        if extra_attr != []:
            print 'sample_classes - ' + sample_name +' has extra experiment attr:' + str(extra_attr)
        # get the ms as ic_ms sample data_frames
        ms_class = ms_data.single_sample_ms(sample_name)
        ic_ms_class = ms_data.single_sample_ms(sample_name)
        if tof_data != None:
            tof_class = tof_data.single_sample_tof(sample_name)
        else:
            tof_class = None
        return Sample(sample_meta = sample_meta_dct,
                      experiment_meta=experiment_meta_dct,
                      ms_data = ms_class, 
                      ic_ms_data = ic_ms_class,
                      tof_data = tof_class,
                      cnx = cnx)
        
    def get_sample_id(self):
        """
        
        """
        if type(self.sample_meta['sample_id']) == int:
            if sample_id_inuse(self.cnx, self.sample_meta['sample_id']):
                self.db_eligible = False   
        elif self.cnx == None:
            self.sample_meta['sample_id'] == 'unknown'
            self.db_eligible = False
        elif self.sample_meta['sample_id'].lower() == 'null':
            self.sample_meta['sample_id'] = get_next_valid_sample_id(self.cnx)
        
    def check_SE_id(self, warning = True):
        """
        Parameters
        ----------
        warning : boolean, default True
            if True it will display a warning in the concole if the experiment
            id and sample_name are in use
        """
        if self.experiment_meta['experiment_id'] != self.sample_meta['experiment_id']:
            raise ValueError('experiment_ids for '+ self.sample_meta['sample_name'] + 'do not match')
        inuse = SE_inuse(self.cnx, 
                         self.sample_meta['sample_name'],
                         self.experiment_meta['experiment_id'])
        if inuse == True:
            if warning == False:
                print 'sample_classes - ' + self.sample_meta['sample_name'] +' experiment and sample id combo in use'
            self.db_eligible = False
        else:
            pass

    def to_utswlipiddb(self, cnx=None):
        """
        
        """
        self.check_SE_id()
        if cnx == None:
            #change cnx to the classes cnx
            cnx = self.cnx
        if cnx != None and self.db_eligible == True:
            db_dct = OrderedDict()
            organism = self.sample_meta['organism_classifier']
            experiment_df = auto_fill_df(pd.DataFrame(self.experiment_meta, index = [0]),
                                         db_tables['experiment_info'],
                                         fill = 'Null')
            if not experiment_id_inuse(cnx, self.experiment_meta['experiment_id'], experiment_df):
                db_dct['experiment_info'] = experiment_df
            db_dct['sample_info'] = auto_fill_df(pd.DataFrame(self.sample_meta,index = [0]),
                                                 db_tables['sample_info'],
                                                 fill = 'Null')
            db_dct[organism] = auto_fill_df(pd.DataFrame(self.sample_meta,index = [0]),
                                            db_tables[organism],
                                            fill = 'Null')
            db_dct['ms_data'] = self.ms_data.sql_format(self.sample_meta['sample_name'],
                                                        method  = 'MSMSALL',
                                                        sample_id = self.sample_meta['sample_id'])
            db_dct['ic_ms_data'] = self.ic_ms_data.sql_format(self.sample_meta['sample_name'],
                                                              method  = 'MSMSALL',
                                                              sample_id = self.sample_meta['sample_id'],
                                                              ic = True)
            if self.tof_data != None:
                db_dct['tof_data'] = self.tof_data.sql_format(self.sample_meta['sample_name'],
                                                              self.sample_meta['sample_id'])
            for table, df in zip(db_dct.keys(),db_dct.values()):
                df.to_sql(con=cnx, name = table, if_exists='append',
                          flavor='mysql', index = False, chunksize = 200)
        elif cnx == None:
            print "sample_classes - No valid connection found so can not add %s to db" %(self.sample_meta['sample_name'])
        else:
            print '''Issue loading sample into database, please check that the
                     sample name is not already used with this experiment_id'''

def auto_fill_df(df, columns, fill = 'Null'):
    """
    Used to make a dataframe that will have the columns that are not in the old 
    dataframe filled with fill
    """
    new_df = pd.DataFrame()
    for col in columns:
        if col in df.columns.tolist():
            new_df[col] = df[col]
        else:
            new_df[col] = fill
    return new_df
            

def get_next_valid_sample_id(cnx):
    """
    gets the next sample_id
    """
    max_id = pd.read_sql_query('SELECT MAX(sample_id) FROM sample_info', 
                                cnx).iloc[0,0]
    if max_id == None:
        return 1
    else:
        return int(max_id)+1

def get_last_valid_experiment_id(cnx):
    """
    gets the last valid experiment id from the db
    """
    max_id = pd.read_sql_query('SELECT MAX(experiment_id) FROM experiment_info', 
                               cnx).iloc[0,0]
    if max_id == None:
        return 1
    else:
        return int(max_id)

def experiment_id_inuse(cnx, experiment_id, experiment_df):
    """
    Used to check if the experiment_id is in use.
    If it is makes sure that the experiment_dfs match
    If it has not been used or if it has but the data matched return False
    If not return True
    """
    query = """
            SELECT * FROM experiment_info
            WHERE experiment_id = "%s"
            """%(experiment_id)
    df = pd.read_sql(query,cnx)
    if df.empty or df.equals(experiment_df):
        return False
    else:
        return True

def sample_id_inuse(cnx, sample_id):
    """
    returns True or False if sample_id is in use
    """
    df = pd.read_sql_query('SELECT sample_id FROM sample_info', cnx)    
    ids = df['sample_id'].tolist()
    if sample_id in ids:
        return True
    else:
        return False

def get_next_sample_id(cnx, experiment_id, sample_name):
    db_max_experiment = get_last_valid_experiment_id(cnx)
    if experiment_id > db_max_experiment:
        sample_id = get_next_valid_sample_id(cnx)
        return sample_id
    else:
        print 'sample_classes - experiment_found, extracting sample_ids'
        query = """SELECT sample_id
                FROM sample_info 
                INNER JOIN experiment_info 
                    ON sample_info.experiment_id
                WHERE sample_info.experiment_id = %s
                AND sample_info.sample_name = "%s"
                """%(str(experiment_id),str(sample_name))
        sample_id_df = pd.read_sql_query(query,cnx)
        if sample_id_df.empty:
            print sample_name + ' is not found in database, null given'
            return 'unknown'
        else:
            sample_id_df.loc[0,0]
            
def SE_inuse(cnx, sample_name, experiment_id):
    """
    Checks if the sample_id and experiment_id are already in use
    """
    query = """SELECT *
            FROM sample_info
            WHERE experiment_id = %s
            AND sample_name = "%s"
            """%(str(experiment_id),str(sample_name))
    df = pd.read_sql(query, cnx)
    if df.empty:
        return False
    else:
        return True
        
                
def intersect(a,b):
    """
    returns the intersection of two list
    """
    return list(set(a) & set(b))

def union(a,b):
    """
    returns the union of two list, keeps order
    """
    lst = a+b
    return sorted(set(lst), key=lambda x: lst.index(x))

if __name__ == '__main__':
    folder = os.path.abspath(r'C:\Anaconda2\Lib\site-packages\LipPy_v2_1\Test_files\Names_Files')    
    pos_ms_dir = os.path.join('Test_files','Raw_data','022816_NASH13_std1+MSALL.txt')
    neg_ms_dir = os.path.join('Test_files','Raw_data','022816_NASH13_std1-MSALL.txt')
    name_file = os.path.join('Test_files','Names_Files','022816_NASH13_std1_NamesFile.csv')
    pos_ms = ms.MS_Data.from_raw_data_files(pos_ms_dir, name_file, '+')
    neg_ms = ms.MS_Data.from_raw_data_files(neg_ms_dir, name_file, '-')
    print ('filter')
    pos_fileterd = pos_ms.filter_set1()
    neg_filtered = neg_ms.filter_set1()
    print ('Nameing')
    pos_named = pos_fileterd.Name_Molecules()
    neg_named = neg_filtered.Name_Molecules()
    named = pos_named.combine(neg_named)
    #print ('ic')
    #ic_pos = pos_named.isotope_correction(clean = True)
    #ic_neg = pos_named.isotope_correction(clean = True)
    #ic = ic_pos.combine(ic_neg)
    pos_tof_path = os.path.join('Test_files','Tof_data','pos_tof.txt')
    neg_tof_path = os.path.join('Test_files','Tof_data','pos_tof.txt')
    pos_tof = tof.TOF_data.from_raw_data_files(pos_tof_path, name_file, mode = '+')
    neg_tof = tof.TOF_data.from_raw_data_files(neg_tof_path, name_file, mode = '-')
    tof_data = pos_tof.combine(neg_tof)
    sample_meta_path = os.path.join(folder, 'sample_meta.csv')
    sample_meta = pd.read_csv(sample_meta_path)
    control_meta_path = os.path.join(folder,'control_meta.csv')
    control_meta = pd.read_csv(control_meta_path)
    experiment_meta_path = os.path.join(folder,'experiment_meta.csv')
    experiment_meta = pd.read_csv(experiment_meta_path)
    user = 'LipPy'
    password = 'MolGen'
    cnx=mysql.connector.connect(user = user, 
                                password = password,
                                host='localhost',
                                database = 'utswlipiddb')
    print 'input'
    s1 = Sample.init_from_data(ms_data = named,
                              ic_ms_data = named,
                              tof_data = tof_data,
                              sample_name = 's01',
                              sample_meta = sample_meta_path, 
                              experiment_meta = experiment_meta_path,
                              sample_id = 1,
                              cnx = cnx)
    s1.to_utswlipiddb()
    s2 = Sample.init_from_data(ms_data = named,
                              ic_ms_data = named, 
                              sample_name = 's02',
                              sample_meta = sample_meta_path, 
                              experiment_meta = experiment_meta_path,
                              sample_id = None,
                              cnx = cnx)
    if False:
        s3 = Sample.init_from_data(ms_data = named,
                                  ic_ms_data = named, 
                                  sample_name = 'Solvent_Blank_Start',
                                  sample_meta = control_meta_path, 
                                  experiment_meta = experiment_meta_path,
                                  sample_id = None,
                                  cnx = cnx)
    cnx.close()