# -*- coding: utf-8 -*-
"""
Created on Tue Oct 07 12:48:05 2014
Functions for rescaling metabolomics data.
@author: Mike Trenfield
"""
import numpy as np

def Center(array, axis=0):
    return array - array.mean(axis=axis)
    
def AutoScale(array, axis=0):
    return Center(array) / np.std(array, axis=axis)
    
def RangeScale(array, axis=0):
    return Center(array) / (array.max(axis=axis) - array.min(axis=axis))
    
def ParetoScale(array, axis=0):
    return Center(array, axis=axis) / np.sqrt(np.std(array, axis=axis))
    
def VastScale(array, axis=0):
    return AutoScale(array, axis=axis) * (array.mean(axis=axis) / array.std(axis=axis))
    
def LevelScale(array, axis=0):
    return Center(array, axis=axis) / array.mean(axis=axis)
    

scale_tag = {Center : 'centered',
             RangeScale : 'range-scaled',
             ParetoScale : 'Pareto-scaled',
             VastScale : 'vast-scaled',
             LevelScale : 'level-scaled',
             AutoScale : 'auto-scaled'}