# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 10:51:08 2015

@author: Alex
"""
'''
used to change the naming database
f['+'],df['-'] are df's that hold the pos and neg data respectivly.
changing this will change the data_base

add_to_pos will add to the positive df
add_to_neg will add to the negative df 
'''
import shelve
import pandas as pd
from collections import OrderedDict
from Usefull.find import find

#BE SURE TO CLOSE G! g.close()!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

add_to_pos = OrderedDict([('Precursor',[]),
                          ('Fragment', []),
                          ('Name',     []),
                          ('Group',    [])
                         ])
add_to_neg = OrderedDict([('Precursor',[]),
                          ('Fragment', []),
                          ('Name',     []),
                          ('Group',    [])
                         ])

##############################################################################
for g in add_to_neg:
    test = len(add_to_neg['Name'])    
    if len(add_to_neg[g]) != test:
        raise TypeError('lengths of the neg lists are not equal')
        
for g in add_to_pos:
    test = len(add_to_pos['Name'])    
    if len(add_to_pos[g]) != test:
        raise TypeError('lengths of the pos lists are not equal')

df_add_to_pos = pd.DataFrame(add_to_pos)
df_add_to_neg = pd.DataFrame(add_to_neg)

g = shelve.open('Molecule_Database')
g['+']
g['-']

#to remove from list, choose index 
#g['+'] = g['+'].drop(g['+'].index[[1765]])
#g['-'] = g['-'].drop(g['+'].index[[1765]])

g['+'] = g['+'].append(df_add_to_pos, ignore_index = True)
g['-'] = g['-'].append(df_add_to_neg, ignore_index = True)
g.close()