# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 08:08:33 2017

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
import pdb
import copy
import warnings
from scipy.stats import linregress
import utilities
import mysql
from Lookups.database_tables import db_login_info
from collections import OrderedDict
import json
from MS_classes import *
import mysql
from Lookups.database_tables import db_login_info

baseline = 100
cnx=mysql.connector.connect(user = db_login_info['user'], 
                        password = db_login_info['password'],
                        host = db_login_info['host'],
                        database = db_login_info['lipid_db'])
                        
if False:
    pos = MS_Data.from_raw_data_files(r'X:\Lipidomics\KME\2016 Data\102616_ELOVL_plasma2\Inputs\Raw_data\102616_ELOVL_plasma2+MSALL.txt',name_file=r'X:\Lipidomics\KME\2016 Data\102616_ELOVL_plasma2\Inputs\102616_ELOVL_plasma2_NamesFile.csv', mode ='+')
    pos_named = pos.Name_Molecules()
    #pos_sat_named.data_frame.to_csv(r'D:/Temp/sat.csv')

if False:
    #trying to find an algorithm to find high intensity contaminants
    print 'importing'
    ms = MS_Data.from_db_experiment(118,cnx, iso_cor = False, std_norm = False)
    tic = tic = ms.make_TIC()
    pos_tic = tic[tic.Mode == '+']
    sample_name = 'Il_906'
    sample_tic = pos_tic[['Precursor', sample_name]]
    top_ms1_int = sample_tic.sort_values(by = sample_name, ascending = False).head(20)
    top_ms1_pre = top_ms1.sort_values(by = 'Precursor')
    for i in top_ms1_int.index:
        current_loc = top_ms1_pre.index.tolist().index(i)
        if current_loc == 0:
            pass
        else:
            test_ms1 = top_ms1_pre.loc[i]['Precursor']
            below_i = top_ms1_pre.index[current_loc-1]
            above_i = top_ms1_pre.index[current_loc+1]
            diff1 = abs(test_ms1-top_ms1_pre.loc[below_i]['Precursor'])
            diff2 = abs(test_ms1-top_ms1_pre.loc[above_i]['Precursor'])
            for diff in [diff1, diff2] :           
                found_pos = True
                found_neg = True
                n_plus = 1
                n_minus = 1
                while found_pos == True:
                    if top_ms1_pre.index[current_loc+n_plus] in top_ms1_pre.index:
                        
                        n_plus += 1 
                    else:
                        found_pos = False
                        break
                    
                    
                    
                while found_neg == True:
                    pass
            


if False:
    print 'importing'
    ms = MS_Data.from_db_experiment(118,cnx, iso_cor = False, std_norm = False)
    print 'done importing'
    filtered = ms.filter_set1()
    named = filtered.remove_unknowns()
    msdata = filtered.set_baseline(baseline)
    ms_stats = pd.DataFrame()
    df = msdata.data_frame
    named_df = named.data_frame
    samples = msdata.data_frame[msdata.all_sample_cols]
    sample_sum = samples.apply(lambda x: np.sum(x), axis = 0)
    sample_sum.name = 'Sum'
    sample_max = samples.apply(lambda x: np.max(x), axis = 0)
    sample_max.name = 'Max'
    no_filtered = pd.Series()
    no_filtered.name = 'Number_Filtered'
    no_named = pd.Series()
    no_named.name = 'Number_Named'
    for sample in msdata.all_sample_cols:
        no_filtered[sample] = len(df[sample][df[sample]>baseline].tolist())
        no_named[sample] = len(named_df[sample][named_df[sample]>baseline].tolist())
    df = msdata.data_frame.copy()
    ms1_sum = msdata.bin_and_sum('Precursor', normalize = False)
    ms_stats = pd.concat([sample_sum, sample_max,no_filtered, no_named], axis = 1)
    ms_stats.to_csv(r'D:/Temp/QC pilot/ms_stats.csv')