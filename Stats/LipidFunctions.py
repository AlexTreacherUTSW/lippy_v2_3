# -*- coding: utf-8 -*-
"""
Created on Mon Jun 30 16:11:39 2014

@author: JMLab1
"""
import pandas as pd
import numpy as np
import os, re
from scipy.stats import ttest_ind, ranksums
from collections import defaultdict
pd.options.mode.chained_assignment = None
import pdb
"""
Contains default settings as well as dictionaries and variables that we
would like to be able to call without initializing in a script.
"""
#Check the normailze function, keep getting 'FutureWarning: using '-' to provide set differences with Indexes is deprecated, use .difference()  "use .difference()",FutureWarning)


remove_child_peaks = True
child_peaks_N = [(1, 10)] #Neutral losses between these numbers will be ignored.

remove_low_averages = True
average_thresh = 30

remove_mass_defects = True   #This filter still needs to be written.
mass_defect_thresh = .9

remove_NL = True
NL_thresh = 0 #Mass threshold under which values are removed

remove_under_thresh = True
under_thresh_tuple = (45.0, 30.0) #under_thresh = (X, Y) means X percent of the samples must be above Y.

thresh_to_name = (-0.3, 0.3) 



lipid_dict = {'Neutral_Lipid' : ('TAG', 'CE', 'DAG', 'MAG'),
              'Phospholipid' : ('PC', 'PS', 'PE', 'PA', 'PI', 'PG'),
              'Lyso_Phospholipid' : ('lysoPC', 'lysoPE', 'lysoPI',
                       'lysoPS', 'lysoPG', 'lysoPA',
                       'LPE', 'LPC', 'LPI'),
              'Sphingolipid' : ('SM', 'SG')
                       }
                       
norm_methods = {'mol' : None, 'class' : None,
                'mol_class' : None, 'mol_filter' : None,
                'None' : None}

name_cols = ['Name', 'Group']
mass_cols = ['Precursor', 'Fragment', 'neutral_loss']

def NL_Filter(data_frame, threshold=NL_thresh):
    """
    NL_Filter(table, threshold) returns a new data frame
    in which all rows where the neutral loss is less than 
    threshold removed.
    
    table should be of type DataFrame
    threshold should be a float or integer.
    
    Example:np.median(data_frame[g1], axis=1)
    NL_Filter(data_frame, 0) would return a data
    frame consisting only of the rows with positive neutral
    loss.
    """
    NL_Filter=data_frame[data_frame.neutral_loss >= threshold].reset_index(drop=True)
    return NL_Filter
    
def Child_Peak_Filter(data_frame, list_of_tuples=child_peaks_N):
    """
    Child_Peak_Takes as input a list of doubles and a
    data frame and returns a data frame consisting of
    the rows of data_frame where the neutral loss falls
    between any of the pairs.
    
    Example:return 
    Child_Peak_Filter(data_frame, [(1,10), (-10,-5)]) would 
    return a table in which all of the neutral losses between 
    1 and 10 and between -10 and -5 removed
    
    """
    #list_of_tuples += [(0,0)]
    df_cpf = data_frame
    for pair in list_of_tuples:
        df_cpf_temp = df_cpf[df_cpf.neutral_loss > max(pair)]
        df_cpf = df_cpf[df_cpf.neutral_loss < min(pair)]
        df_cpf = pd.concat([df_cpf_temp, df_cpf])
        
    return df_cpf#.reset_index(drop=True)
    
def Remove_Average_Max(df, cols, average, maximum):
    """
    for each row checks if a column has an average OR maxium above set values,
    if it fails BOTH it is removed.    
    """
    mean_col = np.mean(df[cols], axis = 1)
    max_col = np.max(df[cols], axis = 1)
    mean_pass = mean_col > average
    max_pass =  max_col >= maximum
    new_df = df[mean_pass | max_pass]
    return new_df
    
def Remove_Low_Average(data_frame, samples_names, average_threshold=average_thresh):
    """
    Removes each row in the data_frame that does not have an average of the 
    selected samples above the set average
    """
    #pdb.set_trace()
    df = data_frame.copy()
    ave = np.mean(df[samples_names], axis = 1)
    new_df = df[ave > average_threshold]
    return new_df

def Remove_Low_Average_byGroup(data_frame, groups_dct, average_threshold=average_thresh):
    """
    checks the average 
    """    
    df = data_frame.copy()
    pass_dct = {}
    for k in groups_dct.keys():
        sample_names = groups_dct[k]
        ave = np.mean(df[sample_names], axis = 1)
        pass_dct[k] = ave > average_threshold
    pass_df = pd.concat(pass_dct.values(), axis = 1)
    truth_series = pass_df.apply(lambda x: any(x), axis = 1)
    new_df = df[truth_series]
    return new_df
    
def Remove_Low_Freq(data_frame, sample_names, under_thresh=under_thresh_tuple):
    """
    Remove_Low_Freq(data_frame, under_thresh, sample_names)
    takes as input a data frame, a tuple (X, Y), and a list
    of sample names.  It returns a new data frame consisting
    of the rows of data_frame in which X percent of the samples
    have intensity greater than Y.
    """
    df = data_frame.copy()
    min_n = len(sample_names)*(under_thresh[0]/100.0)
    sum_above = df[sample_names].apply(lambda x: sum(x >= under_thresh[1]), axis = 1)
    new_df = df[sum_above >= min_n]
    return new_df

def Remove_Low_Freq_byGroup(data_frame, group_dct, under_thresh=under_thresh_tuple):
    """
    Same as the Remove_Low_Freq however checks group wise and if a single group
    passes then the mass_pair is kept
    """
    df = data_frame.copy()
    pass_dct = {}
    for k in group_dct.keys():
        sample_names = group_dct[k]
        min_n  = len(sample_names)*(under_thresh[0]/100.0)
        sum_above = df[sample_names].apply(lambda x: sum(x >= under_thresh[1]), axis = 1)
        pass_dct[k] = sum_above >= min_n
    #return pass_dct
    pass_df = pd.concat(pass_dct.values(), axis = 1)
    truth_series = pass_df.apply(lambda x: any(x), axis = 1)
    new_df = df[truth_series]
    return new_df
    
def all_filters(data_frame, sample_names):
    """
    runs all filters with default settings
    """    
    
    df = Remove_Low_Freq(data_frame, sample_names)
    df = Remove_Low_Average(df, sample_names)
    df = Child_Peak_Filter(df)
    df = NL_Filter(df)
    return df

def get_name_and_group(ms1, ms2, name_df, ms1_accuracy = .3, ms2_accuracy = .1):
    possible_names = name_df[(name_df['Precursor'] <= ms1 + ms1_accuracy) &
                             (name_df['Precursor'] >= ms1 - ms1_accuracy) &
                             (name_df['Fragment'] <= ms2 + ms2_accuracy) &
                             (name_df['Fragment'] >= ms2 - ms2_accuracy)]
    if possible_names.shape[0] == 1:
        return [possible_names.iloc[0]['Name'], possible_names.iloc[0]['Group']]
    elif possible_names.shape[0] == 0:
        return ['Unknown','Unknown']
    else:
        #not sure what to do here
        #perhaps return multiple, or the indexs?
        pdb.set_trace()
        return ['Unknown','Unknown']

def Name_Data(data_frame, lookup_table, ms1_accuracy = .3, ms2_accuracy = .1):
    to_name = data_frame.copy()
    names = pd.Series(name = 'Name')
    groups = pd.Series(name = 'Group')
    for i in to_name.index:
        row = to_name.loc[i]
        n_g = get_name_and_group(row['Precursor'], row['Fragment'], lookup_table, ms1_accuracy = ms1_accuracy, ms2_accuracy = ms2_accuracy)
        names.set_value(i, n_g[0])
        groups.set_value(i, n_g[1])
    return pd.concat([names, groups, to_name], axis = 1).sort_values(by = 'Name', axis = 0) 
    
    
def Name_Data_old(data_frame, lookup_table_handle='Mast_List_100814.txt', naming_thresh=0.1):
    """
    Name_Data inputs:
    
        data_frame
        
        naming_thresh: the maximum amount for both Precursor
        and Fragment masses to deviate from a lookup entry
        for naming.
        
        lookup_table_handle: the string name of the table for
        looking up the names
        
    Outputs:
        A data frame consisting of named molecules and unknowns
    """
    #data_frame.to_csv('wattt.csv')
    #raw_input('hello')
    naming_thresh = abs(naming_thresh)
    if type(lookup_table_handle) == str:
        lookup_table = pd.read_table(lookup_table_handle)
    else:
        assert isinstance(lookup_table_handle, pd.DataFrame)
        lookup_table = lookup_table_handle.copy()
    best_match_names = []
    best_match_groups = []
    error_dict = {}
    data_frame = data_frame.reset_index(drop=True)
    warn_count = 0
    for row in data_frame.index:
        lookup_table['dPrecursor'] = (data_frame.loc[row]['Precursor'] - lookup_table['Precursor'])
        lookup_table['dFragment'] = (data_frame.loc[row]['Fragment'] - lookup_table['Fragment'])
        temp_table = lookup_table[lookup_table['dPrecursor'] < naming_thresh]
        temp_table = temp_table[temp_table['dPrecursor'] > -naming_thresh]
        temp_table = temp_table[temp_table['dFragment'] < naming_thresh]
        temp_table = temp_table[temp_table['dFragment'] > -naming_thresh]
        if np.shape(temp_table)[0] > 1:
            #print(temp_table)
            warn_count += 1
            error_dict[row] = [temp_table.Name.tolist()]
            best_match_index = temp_table.index[0]
            best_match_names += [temp_table.loc[best_match_index]['Name']]
            best_match_groups += [temp_table.loc[best_match_index]['Group']]
            #print 'Error', temp_table, data_frame.loc[row]['Precursor'], data_frame.loc[row]['Fragment']
        elif np.shape(temp_table)[0] == 1:
            #print temp_table, data_frame.loc[row]['Precursor'], data_frame.loc[row]['Fragment']
            best_match_index = temp_table.index[0]
            best_match_names += [temp_table.loc[best_match_index]['Name']]
            best_match_groups += [temp_table.loc[best_match_index]['Group']]
        else:
            best_match_names += ['Unknown']
            best_match_groups += ['Unknown']          
        del temp_table
    data_frame['Name'] = pd.Series(best_match_names)
    data_frame['Group'] = pd.Series(best_match_groups)
    data_frame.sort_values(by = 'Name', inplace=True, axis = 0)   
    return data_frame
    
def normalizer(df, samp_cols):
    """
    normalizer(df, samp_cols) takes as input a data frame and a
    list-like (e.g. pd.Series or list or tuple) corresponding to
    the columns to normalize along
    """    
    sc = pd.Series(samp_cols)
    dfa = df[df.columns.difference(sc)]
    dfb = df[samp_cols]
    norm = lambda x : 100*x/(x.sum() + .000001)
    dfb = dfb.apply(norm, axis = 0)
    df_normed = pd.concat([dfa, dfb], axis=1)
    return df_normed
    
def molar_norm(data_frame, sample_columns):
    """
    Normalizes sample intensities to the molar mass, or
    in math speak maps an intensity I_j with precursor mass M_j
    to I_j -> (I_j / M_j) / (sum(I_i/M_i))
    """
    samples = data_frame[sample_columns]
    samples_div_by_mass = samples.div(data_frame['Precursor'], axis='index')
    samples_norm = normalizer(samples_div_by_mass, sample_columns)
    return pd.concat([data_frame[data_frame.columns - pd.Series(sample_columns)], samples_norm], axis='index')
    
def omit(data_frame, name_lookup, omitted):
    """
    omit takes a data_frame, a name_lookup table, and a list of integers corresponding sample numbers to omit
    and returns two data frames: one corresponding to the data frame and lookup table with the samples omitted.
    """
    if len(omitted) != 0:
        omit_indices = pd.Series(omitted) - 1
        name_lookup = name_lookup.ix[name_lookup.index - omit_indices] #selects the columns that are not in omit_indices
        data_frame = data_frame[data_frame.columns.difference(pd.Series(omitted))]
    return data_frame, name_lookup