# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 10:11:14 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
from Lipids import *
from collections import OrderedDict
from mass_pairs import *
from Compounds import *
import pdb
from mass_pairs import *
import itertools

project_folder = os.path.abspath(r'D:\Alex Treacher\Projects\new_names')
chains_folder = os.path.join(project_folder, 'chains.txt')
output_folder = r'D:\Alex Treacher\Projects\new_names\no_wax_test'

if not os.path.exists(output_folder):
    os.mkdir(output_folder)

chains_df = pd.read_table(chains_folder)
chains = OrderedDict()
highchains = chains_df[['High',"HighMass"]].copy()
highchains.dropna(inplace=True)
highchains.columns = ['FA','MASS']
midchains = chains_df[['Mid','MidMass']].copy()
midchains.dropna(inplace=True)
midchains.columns = ['FA','MASS']
lowchains = chains_df[['Low','LowMass']].copy()
lowchains.dropna(inplace=True)
lowchains.columns = ['FA','MASS']

PLHighSat = ["16:0","16:1","18:0","18:1"]
PLHighPUFA = ["18:2","20:4","20:5","22:5","22:6"]
PLLowSat = ["20:0","20:1","22:0","22:1","22:2","24:0","24:1","14:0","14:1","15:0","15:1","17:0",]
PLLowPUFA = ["18:3","20:3","22:4","24:5","24:6"]

HS = pd.DataFrame({'FA':PLHighSat,'MASS':['']*len(PLHighSat)})
HP = pd.DataFrame({'FA':PLHighPUFA,'MASS':['']*len(PLHighPUFA)})
LS = pd.DataFrame({'FA':PLLowSat,'MASS':['']*len(PLLowSat)})
LP = pd.DataFrame({'FA':PLLowPUFA,'MASS':['']*len(PLLowPUFA)})

SM_R1 = pd.DataFrame({'FA':["18:1","18:0","16:0","16:1","18:2"],'MASS':['','','','','']})

cer_r1 = pd.DataFrame({'FA':["18:1","18:2","18:0","16:1"],'MASS':['','','','']})
cer_r2_chains = PLHighSat+PLHighPUFA
cer_r2 = pd.DataFrame({'FA':cer_r2_chains,'MASS':['']*len(cer_r2_chains)})

dct_template = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[])
                          ])
#builders =====================================================================
class fatty_acid_chain_builder():
    """
    class used for fatty acid composition and formula
    """
    
    def __init__(self, carbons, double_bonds):
        self.carbons = int(carbons)
        self.double_bonds = int(double_bonds)
        self.formula = self.get_formula()
        self.mass = self.get_mass()
        
    def get_formula(self):
        formula = OrderedDict()
        formula['C'] = self.carbons
        formula['O'] = 2
        formula['H'] = (self.carbons*2) - (self.double_bonds*2) - 2 + 1;
        return formula
        
    def get_mass(self):
        mass = sum(mul_dict(self.formula,MASSES).values())
        return mass
        
class DAG_builder():
    """
    creates 2 dags from 2 chains and produces necessary infomation to make a names list 
    """
    
    def __init__(self, chain1_str, chain2_str):
        self.backbone = {'C':3,'H':6,'O':1}
        self.adduct = {'N':1,'H':4}
        self.chain1 = fatty_acid_chain_builder(chain1_str.split(':')[0],chain1_str.split(':')[1])
        self.chain2 = fatty_acid_chain_builder(chain2_str.split(':')[0],chain2_str.split(':')[1])
        self.ms1_formula = self.get_ms1()
        self.ms1_mass = get_mass(self.ms1_formula)
        #formula and mass with chain 1 as the NL
        self.ms2_1_formula = self.get_ms2(self.chain1.formula)
        self.ms2_1_mass = get_mass(self.ms2_1_formula)        
        #formula and mass with chain 2 as the NL
        self.ms2_2_formula = self.get_ms2(self.chain2.formula)
        self.ms2_2_mass = get_mass(self.ms2_2_formula)
        #ms2 masses in a tuple
        self.ms2_mass = (self.ms2_1_mass, self.ms2_2_mass)
        #the two dag classes dag1 with chain1 being the NL and dag2 with the NL of chain2
        self.dag1 = Lipid.from_group("DAG",self.make_name(self.chain1),"+",mass_pair= MassPair(self.ms1_mass, self.ms2_1_mass))
        self.dag2 = Lipid.from_group("DAG",self.make_name(self.chain2),"+",mass_pair= MassPair(self.ms1_mass, self.ms2_2_mass))
        #this is used so pure numbers(that have not been manipulated by lipids) can be used
        self.dag1_dct = {}
        self.dag1_dct['Group'] = "DAG"
        self.dag1_dct['Name'] = self.make_name(self.chain1)
        self.dag1_dct['Precursor'] = self.ms1_mass
        self.dag1_dct['Fragment'] = self.ms2_1_mass
        self.dag2_dct = {}
        self.dag2_dct['Group'] = "DAG"
        self.dag2_dct['Name'] = self.make_name(self.chain2)
        self.dag2_dct['Precursor'] = self.ms1_mass
        self.dag2_dct['Fragment'] = self.ms2_2_mass
        
    def get_ms1(self):
        full = add_dict(self.backbone, self.adduct)
        full = add_dict(full, self.chain1.formula)
        full = add_dict(full, self.chain2.formula)
        return full
        
    def get_ms2(self, chain_formula):
        NL = add_dict(chain_formula, self.adduct)
        ms2 = sub_dict(self.ms1_formula, NL)
        return ms2
    
    def make_name(self, nl_chain):
        totalC = self.chain1.carbons + self.chain2.carbons
        totalDB = self.chain1.double_bonds + self.chain2.double_bonds
        name = "DAG("+str(totalC)+":"+str(totalDB)+") NL-"+str(nl_chain.carbons)+":"+str(nl_chain.double_bonds)
        return name
        
class TAG_builder():
    
    def __init__(self, chain1_str, chain2_str, chain3_str):
        self.backbone = {'C':3,'H':5}
        self.adduct = {'N':1,'H':4}
        self.chain1 = fatty_acid_chain_builder(chain1_str.split(':')[0],chain1_str.split(':')[1])
        self.chain2 = fatty_acid_chain_builder(chain2_str.split(':')[0],chain2_str.split(':')[1])
        self.chain3 = fatty_acid_chain_builder(chain3_str.split(':')[0],chain3_str.split(':')[1])
        self.ms1_formula = self.get_ms1_formula()
        self.ms1_mass = get_mass(self.ms1_formula)
        #info for chain 1 as the NL
        self.ms2_1_formula = self.get_ms2(self.chain1.formula)
        self.ms2_1_mass = get_mass(self.ms2_1_formula)
        self.name_1 = self.make_name(self.chain1)
        self.tag1 = Lipid.from_group("TAG",self.name_1,"+",mass_pair=MassPair(self.ms1_mass, self.ms2_1_mass))
        self.tag1_dct = {}
        self.tag1_dct['Group'] = "TAG"
        self.tag1_dct['Name'] = self.name_1
        self.tag1_dct['Precursor'] = self.ms1_mass
        self.tag1_dct['Fragment'] = self.ms2_1_mass
        self.tag1_dct['NL_str'] = chain1_str
        #info for cahin 2 as the NL
        self.ms2_2_formula = self.get_ms2(self.chain2.formula)
        self.ms2_2_mass = get_mass(self.ms2_2_formula)
        self.name_2 = self.make_name(self.chain2)
        self.tag2 = Lipid.from_group("TAG",self.name_2,"+",mass_pair=MassPair(self.ms1_mass, self.ms2_2_mass))
        self.tag2_dct = {}
        self.tag2_dct['Group'] = "TAG"
        self.tag2_dct['Name'] = self.name_2
        self.tag2_dct['Precursor'] = self.ms1_mass
        self.tag2_dct['Fragment'] = self.ms2_2_mass
        self.tag2_dct['NL_str'] = chain2_str
        #info for chain 3 as the NL
        self.ms2_3_formula = self.get_ms2(self.chain3.formula)
        self.ms2_3_mass = get_mass(self.ms2_3_formula)
        self.name_3 = self.make_name(self.chain3)
        self.tag3 = Lipid.from_group("TAG",self.name_3,"+",mass_pair=MassPair(self.ms1_mass, self.ms2_3_mass))
        self.tag3_dct = {}
        self.tag3_dct['Group'] = "TAG"
        self.tag3_dct['Name'] = self.name_3
        self.tag3_dct['Precursor'] = self.ms1_mass
        self.tag3_dct['Fragment'] = self.ms2_3_mass
        self.tag3_dct['NL_str'] = chain3_str
        
    def get_ms1_formula(self):
        full = add_dict(self.backbone, self.adduct)
        full = add_dict(full, self.chain1.formula)
        full = add_dict(full, self.chain2.formula)
        full = add_dict(full, self.chain3.formula)
        return full
    
    def get_ms2(self, chain_formula):
        NL = add_dict(chain_formula, self.adduct)
        ms2 = sub_dict(self.ms1_formula, NL)
        return ms2

    def make_name(self, nl_chain):
        totalC = self.chain1.carbons + self.chain2.carbons + self.chain3.carbons
        totalDB = self.chain1.double_bonds + self.chain2.double_bonds + self.chain3.double_bonds
        name = "TAG("+str(totalC)+":"+str(totalDB)+") NL-"+str(nl_chain.carbons)+":"+str(nl_chain.double_bonds)
        return name
        
class pos_PL_builder():
    
    def __init__(self, chain1_str, chain2_str, group):
        self.chain1C = int(chain1_str.split(':')[0])
        self.chain1db = int(chain1_str.split(':')[1])
        self.chain2C = int(chain2_str.split(':')[0])
        self.chain2db = int(chain2_str.split(':')[1])
        self.totalC = self.chain1C + self.chain2C
        self.totaldb = self.chain1db + self.chain2db
        self.group = group
        self.name = group + "(" + str(self.totalC) + ":" + str(self.totaldb) + ")"
        self.lipid = Lipid.from_group(self.group, self.name, mode = "+")
        self.ms1_mass = self.lipid.ms1_mass()
        self.ms2_mass = self.lipid.ms2_mass()
        self.ms1_formula = self.lipid.ms1_formula()
        self.ms2_formula = self.lipid.ms2_formula()
        
class pos_lysoPL_builder():
    
    def __init__(self, chain_str, group):
        self.totalC = int(chain_str.split(':')[0])
        self.totaldb = int(chain_str.split(':')[1])
        self.group = group
        self.name = group + "(" + str(self.totalC) + ":" + str(self.totaldb) + ")"
        self.lipid = Lipid.from_group(self.group, self.name, mode = "+")
        self.ms1_mass = self.lipid.ms1_mass()
        self.ms2_mass = self.lipid.ms2_mass()
        self.ms1_formula = self.lipid.ms1_formula()
        self.ms2_formula = self.lipid.ms2_formula()
    
class pos_Fatty_Acyl_builder():
    
    def __init__(self, chain_str, group):
        self.chain_str = chain_str
        self.chainC = int(chain_str.split(':')[0])
        self.chaindb = int(chain_str.split(':')[1])
        self.group = group
        self.name = self.group + "(" + chain_str + ")"
        self.lipid = Lipid.from_group(self.group, self.name, mode = "+")

        
        

#utilities ====================================================================
def get_mass(formula):
    return sum(mul_dict(formula,MASSES).values())

def check_masses_df(df, error = .3):
    """
    used to check the masses on a Systematic Set List
    it lists fatty acid carbons and double bonds, this will check the masses
    """
    name_col = df.columns[0]
    mass_col = df.columns[1]
    for i in df.index:
        string = df.loc[i][name_col]
        carbons = string.split(':')[0]
        double_bonds = string.split(':')[1]
        mass = df.loc[i][mass_col]
        fac = fatty_acid_chain(carbons, double_bonds)
        fac_mass = fac.mass+18
        if fac_mass > mass+error or fac_mass < mass-error:
            print "does not match " + name_col +" "+ string + " " + str(fac_mass) + " " + str(mass);
    
def TAG_rules_passed(tag):
    ms2carbons = tag.other_chains_formula()['C']
    ms2db = tag.features['Double_Bonds'] - tag.features['FA_Double_Bonds']
    check1_carbons = [28, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 44]
    check1_db = range(13)
    check2_carbons = [30,32,33,34,35,36,37,38,40]
    check2_db = range(9)
    check3_carbons = [32,34,36]
    check3_db = range(4)
    if tag.features['FA_Carbons'] % 2 == 0 and tag.features['Carbons'] % 2 != 0:
        return False
    elif tag.features['Fatty_Acid'] in highchains.FA.tolist() and(ms2carbons in check1_carbons) and (ms2db in check1_db):
        return True
    elif (tag.features['Fatty_Acid'] in midchains.FA.tolist()) and (ms2carbons in check2_carbons) and (ms2db in check2_db):
        return True
    elif (tag.features['Fatty_Acid'] in lowchains.FA.tolist()) and (ms2carbons in check3_carbons) and (ms2db in check3_db):
        return True
    else:
        return False

def DAG_rules_passed(dag):
    ms1carbons = dag.features['Carbons']
    #remove if even odd
    if dag.features['Carbons']%2 == 0 and dag.features['FA_Carbons']%2 != 0:
        return False
    #remove if odd even
    if dag.features['Carbons']%2 != 0 and dag.features['FA_Carbons']%2 == 0:
        return False
    #remove if odd odd
    #if dag.features['Carbons']%2 != 0 and dag.features['FA_Carbons']%2 != 0:
    #    return False
    #remove if even even
    #if dag.features['Carbons']%2 == 0 and dag.features['FA_Carbons']%2 == 0:
    #    return False
    #elif dag.features['Carbons']%2 != 0 and dag.features['FA_Carbons']%2 !=0:
    #    return False
    else:
        return True

def PL_rules_passed(PL):
    carbons = PL.lipid.features['Carbons']
    db = PL.lipid.features['Double_Bonds']
    if carbons < 35 and db > 4:
        return False
    else:
        return True

#make names list functions =========================================================            
def make_ester_names(lipid_group, chains, output_folder= False):
    """
    Used to make the names list for the esters
    That is CE, DE, LE, SE or OxE
    """
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for fa in chains:
        group = lipid_group
        mode = "+"
        name = lipid_group+"("+fa+")"
        lipid_class = Lipid.from_group(group, name, mode)
        ms1 = lipid_class.ms1_mass()
        ms2 = lipid_class.ms2_mass()
        names_dct['Precursor'].append(ms1)
        names_dct['Fragment'].append(ms2)
        names_dct['Name'].append(name)
        names_dct['Group'].append(group)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,lipid_group+'.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_specific_PL_names(lipid_group, chains, output_folder = False):
    """
    makes PL names
    """
    names_dct = OrderedDict([('Precursor',[]),
                      ('Fragment',[]),
                      ('Name',[]),
                      ('Group',[]),
                      ])
    used_names = []
    for cp in chains:
        lipid = pos_PL_builder(cp[0],cp[1], group = lipid_group)
        if (PL_rules_passed(lipid)) and (not lipid.name in used_names): 
            names_dct['Precursor'].append(lipid.ms1_mass)
            names_dct['Fragment'].append(lipid.ms2_mass)
            names_dct['Name'].append(lipid.name)
            names_dct['Group'].append(lipid.group)
            used_names.append(lipid.name)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df
    else:
        output_file = os.path.join(output_folder,lipid_group+'.csv')
        names_df.to_csv(output_file, index = False)
        return names_df        
        
def make_lysoPL_names(lipid_group, chains, output_folder = False):
    """
    makes PL names
    """
    names_dct = OrderedDict([('Precursor',[]),
                      ('Fragment',[]),
                      ('Name',[]),
                      ('Group',[]),
                      ])
    used_names = []
    for chain in chains:
        lipid = pos_lysoPL_builder(chain, group = lipid_group)
        if not lipid.name in used_names: 
            names_dct['Precursor'].append(lipid.ms1_mass)
            names_dct['Fragment'].append(lipid.ms2_mass)
            names_dct['Name'].append(lipid.name)
            names_dct['Group'].append(lipid.group)
            used_names.append(lipid.name)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,lipid_group+'.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
        
    
def make_MAG_names(chains, output_folder= True):
    """
    used to make the names list for MAG
    """
    lipid_group = "MAG"
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for fa in chains:
        group = lipid_group
        mode = "+"
        name = lipid_group+"("+fa+")"
        lipid_class = Lipid.from_group(group, name, mode)
        ms1 = lipid_class.ms1_mass()
        ms2 = lipid_class.ms2_mass()
        names_dct['Precursor'].append(ms1)
        names_dct['Fragment'].append(ms2)
        names_dct['Name'].append(name)
        names_dct['Group'].append(group)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'MAG.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_DAG_names(chains, output_folder = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for cp in chains:
        dag_pair = DAG_builder(cp[0], cp[1])
        if dag_pair.dag1_dct['Name'] == dag_pair.dag2_dct['Name']:
            dag_list = [dag_pair.dag1_dct]
        else:
            dag_list = [dag_pair.dag1_dct, dag_pair.dag2_dct]
        for dag in dag_list:
            for key in names_dct:
                names_dct[key].append(dag[key])
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'DAG.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_specific_DAG_names(chains, output_folder = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for cp in chains:
        dag_pair = DAG_builder(cp[0], cp[1])
        if dag_pair.dag1_dct['Name'] == dag_pair.dag2_dct['Name']:
            dag_list = [dag_pair.dag1_dct]
        else:
            dag_list = [dag_pair.dag1_dct, dag_pair.dag2_dct]
        for dag in dag_list:
            lipid_dag = Lipid.from_group("DAG",dag['Name'],mode = "+", mass_pair=MassPair(dag['Precursor'],dag['Fragment']))
            if DAG_rules_passed(lipid_dag):  
                for key in names_dct:
                    names_dct[key].append(dag[key])
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'DAG.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_TAG_names(chains, output_folder=False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    used_names = []
    tags_to_check = []
    for ct in chains:
        tag_build = TAG_builder(ct[0],ct[1],ct[2])
        tags_dct = OrderedDict()
        tags_dct['tag1'] = {'dct':tag_build.tag1_dct,'lipid':tag_build.tag1}
        tags_dct['tag2'] = {'dct':tag_build.tag2_dct,'lipid':tag_build.tag2}
        tags_dct['tag3'] = {'dct':tag_build.tag3_dct,'lipid':tag_build.tag3}
        for tag_num in tags_dct.keys():
            if not tags_dct[tag_num]['dct']['Name'] in used_names:
                used_names.append(tags_dct[tag_num]['dct']['Name'])
                tags_to_check.append(tags_dct[tag_num])
    for tag in tags_to_check:
        for key in names_dct.keys():
            names_dct[key].append(tag['dct'][key])
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'TAG.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_specific_TAG_names(chains, output_folder=False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    used_names = []
    tags_to_check = []
    for ct in chains:
        tag_build = TAG_builder(ct[0],ct[1],ct[2])
        tags_dct = OrderedDict()
        tags_dct['tag1'] = {'dct':tag_build.tag1_dct,'lipid':tag_build.tag1}
        tags_dct['tag2'] = {'dct':tag_build.tag2_dct,'lipid':tag_build.tag2}
        tags_dct['tag3'] = {'dct':tag_build.tag3_dct,'lipid':tag_build.tag3}
        for tag_num in tags_dct.keys():
            if not tags_dct[tag_num]['dct']['Name'] in used_names:
                used_names.append(tags_dct[tag_num]['dct']['Name'])
                tags_to_check.append(tags_dct[tag_num])
    for tag in tags_to_check:
        if TAG_rules_passed(tag['lipid']):
            for key in names_dct.keys():
                names_dct[key].append(tag['dct'][key])
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'TAG.csv')
        names_df.to_csv(output_file, index = False)
        return names_df

def make_fatty_acyl_names(chains, group, output_folder=False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    used_names = []
    for chain in chains:
        builder = pos_Fatty_Acyl_builder(chain, group)
        lipid = builder.lipid
        if not lipid.name in used_names:
            names_dct['Precursor'].append(lipid.ms1_mass())
            names_dct['Fragment'].append(lipid.ms2_mass())
            names_dct['Name'].append(lipid.name)
            names_dct['Group'].append(group)
    names_df = pd.DataFrame(names_dct)
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,group+'.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
        
def make_pos_Cer_names(chains, output_folder = False):
    """
    chains should be like ['34:1(d18:1,16:0)','34:1(d18:1,16:1)',...]
    the first one will be taken as the chain used in the backbone and denoted as d.
    """
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for chain in chains:
        name = 'Cer'+chain
        add_cer = Lipid.from_group('Cer',name,mode = '+')
        names_dct['Precursor'].append(add_cer.ms1_mass())
        names_dct['Fragment'].append(add_cer.ms2_mass())
        names_dct['Name'].append(add_cer.name)
        names_dct['Group'].append('Cer')
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'Cer.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
        
        
def make_pos_HexCer_names(group, chains, output_folder = False):
    """
    chains should be like [34:0,34:1,32:3....
    the first one will be taken as the chain used in the backbone and denoted as d.
    """
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for c in chains:
        name = group + c
        l = Lipid.from_group(group, name, mode = '+')
        names_dct['Precursor'].append(l.ms1_mass())
        names_dct['Fragment'].append(l.ms2_mass())
        names_dct['Name'].append(l.name)
        names_dct['Group'].append(l.group)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,group+'.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
def make_one_off_lipids(output_folder = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    lipids_to_add = []  
    lipids_to_add.append(Lipid.from_group('Farnesol','Farnesol','+'))
    lipids_to_add.append(Lipid.from_group('Geranylgeraniol','Geranylgeraniol','+'))
    lipids_to_add.append(Lipid.from_group('SP','Sphingosine','+'))
    lipids_to_add.append(Lipid.from_group('SN','Sphinganine','+'))
    lipids_to_add.append(Lipid.from_group('S1P','Sphingosine-1-phosphate','+'))
    lipids_to_add.append(Lipid.from_group('PSY','Psychosine','+'))
    
    for lipid in lipids_to_add:
        names_dct['Precursor'].append(lipid.ms1_mass())
        names_dct['Fragment'].append(lipid.ms2_mass())
        names_dct['Name'].append(lipid.name)
        names_dct['Group'].append(lipid.group)
    names_df = pd.DataFrame(names_dct)
    if output_folder == False:
        return names_df       
    else:
        output_file = os.path.join(output_folder,'other.csv')
        names_df.to_csv(output_file, index = False)
        return names_df
    
        

#chian list makers ===========================================================
    
def single_chain_list_maker(chain_df_list = [highchains,midchains,lowchains]):
    """
    used to make a list of the chains to be put in to the single chain lipids
    """
    chains = []
    for df in chain_df_list:
        chains.extend(df.FA.tolist())
    return chains

def double_chain_list_maker(combos = [(highchains,highchains),
                                      (highchains,midchains),
                                      (highchains,lowchains),
                                      (midchains,midchains)]):
    """
    takes a list of tuples of dfs and makes a list of combinations of 2 chains
    Used primarily for the DAG
    """
    chains = []
    for combo in combos:
        #pdb.set_trace()
        if combo[0].equals(combo[1]):
            #combinations with repeats
            l = combo[0].FA.tolist()
            chains.extend(list(itertools.combinations_with_replacement(l,2)))
        else:
            #matrix product
            l1 = combo[0].FA.tolist()
            l2 = combo[1].FA.tolist()
            chains.extend(list(itertools.product(l1,l2)))
    return chains
    
    
def tripple_chain_list_all_combo(include_lists = [highchains, midchains, lowchains]):
    """
    combination of all in lists, no repeats
    """
    chains = []
    for lst in include_lists:
        chains.extend(lst.FA.tolist())
    chain_groups = list(itertools.combinations_with_replacement(chains,3))
    return chain_groups
    
def tripple_chain_list_maker(combos = [(highchains,highchains,highchains),
                                  (highchains,highchains,midchains),
                                  (highchains,midchains,midchains),
                                  (highchains,highchains,lowchains),
                                  ]):
    """
    includes combinations only from each option
    """
    chains = []
    for combo in combos:
        #pdb.set_trace()
        if combo[0].equals(combo[1]) and combo[1].equals(combo[2]):
            #combinations with repeats
            l = combo[0].FA.tolist()
            chains.extend(list(itertools.combinations_with_replacement(l,3)))
        else:
            #matrix product
            l1 = combo[0].FA.tolist()
            l2 = combo[1].FA.tolist()
            l3 = combo[2].FA.tolist()
            chains.extend(list(itertools.product(l1,l2,l3)))
    return chains

def chain_list_combine_Cer(backbone, Rgroup):
    """
    Takes two lists backbone and Rgroup will return sum of items of combinations from the two lists
    eg backbone = ['16:0','18:0'] Rgroup = ['14:0','16:0','18:0'] will return 
    ['30:0(d16:0,14:0)', '32:0(d16:0,16:0)', '34:0(d16:0,18:0)', '32:0(d18:0,14:0)', '34:0(d18:0,16:0)', '36:0(d18:0,18:0)']
    used for Cer
    """
    if type(backbone) == type(pd.DataFrame()):
        backbone = backbone['FA'].tolist()
    if type(Rgroup) == type(pd.DataFrame()):
        Rgroup = Rgroup['FA'].tolist()
    noC = []
    nodb = []
    total_list = list(itertools.product(backbone, Rgroup))
    out = []
    for p in total_list:
        c1 = int(p[0].split(':')[0])
        c2 = int(p[1].split(':')[0])
        db1 = int(p[0].split(':')[1])
        db2 = int(p[1].split(':')[1])
        total = str(c1+c2) + ':' + str(db1+db2)
        chains = '(d' + str(c1) + ':' + str(db1) + ','  + str(c2) + ':' + str(db2) +  ')'
        complete = total+chains
        if complete not in out:
            out.append(complete)
    return out

   
def chain_list_combine_HexCer(backbone, Rgroup):
    """
    Takes two lists backbone and Rgroup will return sum of items of combinations from the two lists
    eg backbone = ['16:0','18:0'] Rgroup = ['14:0','16:0','18:0'] will return 
    ['30:0','32:0','34:0','36:0']
    used for HexCer
    """
    if type(backbone) == type(pd.DataFrame()):
        backbone = backbone['FA'].tolist()
    if type(Rgroup) == type(pd.DataFrame()):
        Rgroup = Rgroup['FA'].tolist()
    noC = []
    nodb = []
    total_list = list(itertools.product(backbone, Rgroup))
    out = []
    for p in total_list:
        c1 = int(p[0].split(':')[0])
        c2 = int(p[1].split(':')[0])
        db1 = int(p[0].split(':')[1])
        db2 = int(p[1].split(':')[1])
        to_add = str(c1+c2) + ':' + str(db1+db2)
        if to_add not in out:
            out.append(to_add)
    return out
    
#Below here will be to join a file and check overlap
def make_Cer_name(r1,r2):
    c1 = int(r1.split(':')[0])
    c2 = int(r2.split(':')[0])
    db1 = int(r1.split(':')[1])
    db1 = int(r2.split(':')[1])
    name = str(c1+c2) + ':' + str(db1+db2) + '(d' + str(c1) + ':' + str(db1) + ','  + str(c2) + ':' + str(db2) +  ')'
    return name
    
def make_HexCer_name(r1,r2):
    c1 = int(r1.split(':')[0])
    c2 = int(r2.split(':')[0])
    db1 = int(r1.split(':')[1])
    db1 = int(r2.split(':')[1])
    name = str(c1+c2) + ':' + str(db1+db2)
    return name

def Cer_Names():
    #d16_1 = ['']
    d18_0 = ['16:0','18:0','20:0','22:0','24:1','24:0','26:2']
    d18_1 = ['14:0','16:0','18:0','20:0','22:0','23:0','24:1','24:0','25:0']
    d18_2 = ['22:0','23:1','24:2']
    l1 = chain_list_combine_Cer(['18:0'],d18_0)
    l2 = chain_list_combine_Cer(['18:1'],d18_1)
    l3 = chain_list_combine_Cer(['18:2'],d18_2)
    return l1+l2+l3
    
def HexCer_Names():
    #d16_1 = ['']
    d18_0 = ['16:0','18:0','20:0','22:0','24:1','24:0','26:2']
    d18_1 = ['14:0','16:0','18:0','20:0','22:0','23:0','24:1','24:0','25:0']
    d18_2 = ['22:0','23:1','24:2']
    l1 = chain_list_combine_HexCer(['18:0'],d18_0)
    l2 = chain_list_combine_HexCer(['18:1'],d18_1)
    l3 = chain_list_combine_HexCer(['18:2'],d18_2)
    names = l1[:]
    for ls2 in l2:
        if not ls2 in names:
            names.append(ls2)
    for ls3 in l3:
        if not ls3 in names:
            names.append(ls3)
    return names


def concat_file(file_loc):
    """
    this will concat every thing in tha file that has .csv at the end of the
    file name.
    """
    all_files = os.listdir(file_loc)
    name_files = list([x for x in all_files if x.endswith('.csv')])
    if 'overlaps.csv' in name_files:
        name_files.remove('overlaps.csv')
    if 'complete.csv' in name_files:
        name_files.remove('complete.csv')
    if 'complete_duplicates_removed.csv' in name_files:
        name_files.remove('complete_duplicates_removed.csv')
    df = pd.DataFrame()
    for f in name_files:
        temp_df = pd.read_csv(os.path.join(file_loc,f))
        df = df.append(temp_df, ignore_index = True)
    df.sort_values(['Precursor','Fragment'], inplace = True)
    df.reset_index(inplace = True, drop = True)
    return df
        
def check_overlap_make_complete(name_file):
    df = concat_file(name_file)
    df.to_csv(os.path.join(name_file,'complete.csv'), index = False)
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag, accuracy=.3)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    overlaps.to_csv(os.path.join(name_file,'overlaps.csv'), index = False)
    return overlaps

def check_overlap(df):
    df = df.copy()
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag, accuracy=.3)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    try:
        overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    except KeyError:
        overlaps = pd.DataFrame()
    return overlaps

def check_overlap_from_csv(file_loc, accuracy = .3):
    df = pd.read_csv(file_loc)
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag, accuracy=accuracy)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    try:
        overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    except KeyError:
        overlaps = pd.DataFrame()
    return overlaps
    
def find(df, a, b, accuracy = .3):
    '''
    used to find a pre and fragment with accuracy
    '''  
    a1 = a-accuracy
    a2 = a+accuracy
    b1 = b-accuracy
    b2 = b+accuracy    
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    if check.empty and for_isotope == True:
        return None
    else:
        return check.index

if __name__ == '__main__':
    if True:
        only_high = single_chain_list_maker([highchains])
        esters = {'CE':single_chain_list_maker([highchains, midchains, lowchains]),
                  'DE':only_high,
                  'LE':only_high,
                  'SE':only_high,
                  'OxE':only_high,
                 }
        for ester in esters:
            make_ester_names(ester, esters[ester], output_folder=output_folder)
    if True:
        make_MAG_names(single_chain_list_maker([highchains, midchains]), output_folder=output_folder)
    if True:
        make_specific_DAG_names(double_chain_list_maker(), output_folder=output_folder)
    if True:
        tags = make_specific_TAG_names(tripple_chain_list_maker(), output_folder=output_folder)
    if True:
        PLs = {'PA':double_chain_list_maker(combos = [(HS,HP)]),
              'PC':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LS),(HS,LP),(HP,LS)]),
              'PE':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LS),(HS,LP),(HP,LS)]),
              'PG':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              'PI':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              'PS':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              'SM':double_chain_list_maker(combos = [(SM_R1, HS),(SM_R1, HP),(SM_R1, LP)])
              }
        for PL in PLs:
            make_specific_PL_names(PL, PLs[PL], output_folder=output_folder)
    if True:
        chain_list1 = pd.concat([HS,HP,LS,LP], ignore_index=True)
        chain_list2 = pd.concat([HS,HP], ignore_index=True)
        lysoPLs = {'LPA':chain_list1.FA.tolist(),
               'LPC':chain_list1.FA.tolist(),
               'LPE':chain_list1.FA.tolist(),
               'LPG':chain_list2.FA.tolist(),
               'LPI':chain_list2.FA.tolist(),
               'LPS':chain_list2.FA.tolist(),
        
        }
        for lysoPL in lysoPLs:
            make_lysoPL_names(lysoPL, lysoPLs[lysoPL], output_folder=output_folder)
    if True:
        chain_list1 = highchains.FA.tolist() + midchains.FA.tolist()
        chain_list2 = highchains.FA.tolist()
        fa = {'Fatty_Acyl_Ester': chain_list1,
              'Ethanolamide':chain_list2,
              'Seratonin':chain_list2,
              'Dopamine':chain_list2,
              'Carnitine_Ester':chain_list1,        
        }
        for group in fa:
            make_fatty_acyl_names(fa[group], group, output_folder=output_folder)
    if True:
        cer_chains = chain_list_combine_Cer(cer_r1, cer_r2)
        x= make_pos_Cer_names(Cer_Names(), output_folder=output_folder)
        hexcer_chains = chain_list_combine_HexCer(cer_r1, cer_r2)
        y= make_pos_HexCer_names('HexCer', HexCer_Names(), output_folder=output_folder)
    if True:
        #for random one off lipids
        make_one_off_lipids(output_folder=output_folder)        
    if True:
       #makes a complete file from list of files and makes overlaps
        check_overlap_make_complete(output_folder)
    
    if True:
        remove_dupe_file = os.path.join(output_folder,r'complete_duplicates_removed.csv')
        to_remove = ['DAG(34:1) NL-14:1','DAG(34:0) NL-14:0','DAG(35:1) NL-15:1',
                     'DAG(35:0) NL-15:0','DE(16:1)','DE(16:0)','DAG(36:1) NL-16:1',
                     'DAG(36:0) NL-16:0','DAG(37:1) NL-17:1','OxE(16:1)',
                     'DAG(37:0) NL-17:0','OxE(16:0)','DE(18:2)','DAG(38:3) NL-18:3',
                     'DE(18:1)','DAG(38:2) NL-18:2','DE(18:0)','DAG(38:1) NL-18:1',
                     'SE(16:1)','DAG(38:0) NL-18:0','SE(16:0)','OxE(18:2)',
                     'OxE(18:1)','OxE(18:0)','DAG(40:5) NL-20:5','DAG(40:4) NL-20:4',
                     'DAG(40:3) NL-20:3','SE(18:2)','DAG(40:1) NL-20:1','SE(18:1)',
                     'DAG(40:0) NL-20:0','SE(18:0)','DAG(42:6) NL-22:6',
                     'DAG(42:5) NL-22:5','PE(36:7)','PE(37:7)','PC(36:7)','PG(37:7)',
                     'PC(37:7)','PS(37:7)','PI(37:7)']
                     
        to_change = {'DAG(38:1) NL-16:1':['DAG(38:1) NL-16:1 or SE(16:1)', 'DAG'],
                     'DAG(40:1) NL-18:1':['DAG(38:0) NL-16:0 or SE(16:0)', 'DAG'],
                     'DAG(40:1) NL-18:1':['DAG(40:2) NL-18:2 or SE(18:2)', 'DAG'],
                     'DAG(40:1) NL-18:1':['DAG(40:1) NL-18:1 or SE(18:1)', 'DAG'],
                     'DAG(40:0) NL-18:0':['DAG(40:0) NL-18:0 or SE(18:0)', 'DAG']}
        complete = concat_file(output_folder)
        for remove_name in to_remove:
            complete = complete[complete.Name != remove_name]
        for change_name in to_change.keys():
            indexs_found = complete[complete.Name == change_name].index
            if len(indexs_found) == 1:
                index = indexs_found[0]
                replace = complete.loc[index].copy()
                complete.loc[index]
                replace['Name'] = to_change[change_name][0]
                replace['Group'] = to_change[change_name][1]
                complete.loc[index] = replace
        complete.to_csv(remove_dupe_file, index = False)
        ol = check_overlap(complete)