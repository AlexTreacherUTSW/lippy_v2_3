from __future__ import division
import numpy as np
from numba import jit, autojit, float64, float32, int64

def entropy(arr):
    p = np.array(arr)
    if np.sum(p) != 1:
        p = p / np.sum(p)
    log_p = np.log2(p)
    shannon_entropy = -np.dot(p, log_p)
    return shannon_entropy

def KL_divergence(arr1, arr2):
    p, q = normalize(np.array(arr1)), normalize(np.array(arr2))
    H_p = entropy(p)
    H_pq = -np.dot(p, np.log2(q))
    return H_pq - H_p

def bootstrap_estimate(arr, func, **kwargs):
    bs_size = kwargs.get('bootstrap_size')
    num_bs = kwargs.get('num_bootstraps')
    if bs_size is None:
        num_bs = 100
        bs_size = 1000
    if np.sum(arr) != 1:
        p = arr / np.sum(arr)
    else:
        p = arr
    experiments = np.random.multinomial(bs_size, p, size=num_bs)
    bs_estimate = np.apply_along_axis(func, 1, experiments)
#    bs_estimate = [func(np.random.multinomial(bs_size, p)) 
#                   for i in range(num_bs)]
    mean = np.mean(bs_estimate)
    bounds = kwargs.get('bounds')
    if bounds is None:
        bounds = [2.5, 97.5]
    else:
        bounds.sort()
    lower = np.percentile(bs_estimate, bounds[0])
    upper = np.percentile(bs_estimate, bounds[1])
    return np.array([mean, lower, upper])

def normalize(arr):
    return arr / arr.sum()

@jit(float64[:](float64[:], int64, int64))
def KL_bootstrap(arr, bs_size, num_bs):
    if np.sum(arr) != 1:
        p = arr / np.sum(arr)
    else:
        p = arr
    #naive_entropy = entropy(p)
    bs_estimate = []
    for i in range(num_bs):
        bs_estimate.append(KL_divergence(np.random.multinomial(bs_size, p), p))
    mean = np.mean(bs_estimate)
    bounds = [2.5, 97.5]
    lower = np.percentile(bs_estimate, bounds[0])
    upper = np.percentile(bs_estimate, bounds[1])
    return np.array([mean, lower, upper])
    
