# -*- coding: utf-8 -*-
"""
Created on Thu Dec 29 08:04:01 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
from LipPy_v2_3 import MS_classes as ms
from LipPy_v2_3.Lookups import database_tables
import experiment_class  as ec
import mysql.connector
import pdb
               
db_info = database_tables.db_login_info

old_db_info = {'host':'129.112.144.138',
               'user':'LipPy',
               'password':'MolGen',
               'lipid_db':'utswlipiddb'}

cnx = mysql.connector.connect(user = db_info['user'], 
                              password = db_info['password'],
                              host= db_info['host'],
                              database = db_info['lipid_db'])
"""                             
old_cnx = mysql.connector.connect(user = old_db_info['user'], 
                              password = old_db_info['password'],
                              host= old_db_info['host'],
                              database = old_db_info['lipid_db'])
"""
                              
query = "SELECT * FROM meta_sample WHERE internal_standard = 'splash'"

#def update_experiment_standards(experiment_id, standard, name_table, cnx)

experiment_id = 1

q1 = "SELECT * FROM meta_sample WHERE experiment_id = " + str(experiment_id)


def change_internal_standard_value(old_value, new_value, cnx):
    q1 = "SELECT * FROM meta_sample WHERE internal_standard = '" + str(old_value) + "'";
    data = pd.read_sql_query(q1, cnx)
    data.internal_standard = str(new_value)
    pdb.set_trace()
    for sid in data.sample_id.tolist():
        q2 = "UPDATE meta_sample SET internal_standard = " + str(new_value) +" WHERE sample_id = " + str(sid)
        cursor = cnx.cursor()
        cursor.execute(q2)
        cnx.commit()

def change_internal_standard_value_for_controls(new_value, cnx):
    for gn in [-1,-2,-3,-4]:
        q1 = "SELECT * FROM meta_sample WHERE group_number = " + str(gn) + ";"
        data = pd.read_sql_query(q1, cnx)
        data.internal_standard = str(new_value)
        pdb.set_trace()
        for sid in data.sample_id.tolist():
            q2 = "UPDATE meta_sample SET internal_standard = '" + str(new_value) +"' WHERE sample_id = " + str(sid)
            cursor = cnx.cursor()
            cursor.execute(q2)
            cnx.commit()

def change_standard_volume(sample_id, new_value, cnx):
    q1 = "UPDATE meta_sample SET standard_volume = '" + str(new_value) + "' WHERE sample_id = " + str(sample_id);
    cursor = cnx.cursor()
    cursor.execute(q1)
    cnx.commit() 
    
def change_sample_amount(sample_id, new_value, cnx):
    q1 = "UPDATE meta_sample SET sample_amount = '" + str(new_value) + "' WHERE sample_id = " + str(sample_id);
    cursor = cnx.cursor()
    cursor.execute(q1)
    cnx.commit()
    
def update_standard_info(experiment_id, name_table, standard_name):
    if type(name_table) == str:
        nt = pd.read_csv(name_table)
    else:
        nt = name_table.copy()
    rename = {'Name': 'sample_name',
              'Group_Name': 'group_name',
              'Group': 'group_number',
              'Sample_Number': 'sample_number',
              'Sample_Amount':'sample_amount',
              'Standard_Volume':'standard_volume'}
    nt.rename(columns = rename, inplace = True)
    nt = nt[nt.group_number > 0]
    for i in nt.index:
        sample_name = nt.loc[i]['sample_name']
        q1 = "SELECT sample_id FROM meta_sample WHERE experiment_id = %s and sample_name = '%s'" %(experiment_id, sample_name)
        sample = pd.read_sql_query(q1, cnx)
        assert sample.shape[0] == 1
        sample_id = sample.iloc[0]['sample_id']        
        sample_amount = nt.loc[i]['sample_amount']
        standard_volume = nt.loc[i]['standard_volume']
        q2 = "UPDATE meta_sample SET sample_amount = %s, standard_volume = %s, internal_standard = '%s' WHERE sample_id = %s;" %(sample_amount, standard_volume, standard_name, sample_id)
        cursor = cnx.cursor()
        cursor.execute(q2)
        cnx.commit()
        
def change_internal_standard_value_old(old_value, new_value, cnx):
    q1 = "SELECT * FROM meta_sample WHERE internal_standard = '" + str(old_value) + "'";
    data = pd.read_sql_query(q1, cnx)
    data.internal_standard = str(new_value)
    pdb.set_trace()
    for sid in data.sample_id.tolist():
        q2 = "UPDATE meta_sample SET internal_standard = " + str(new_value) +" WHERE sample_id = " + str(sid)
        cursor = cnx.cursor()
        cursor.execute(q2)
        cnx.commit()

def change_internal_standard_value_for_controls_old(new_value, cnx):
    for gn in [-1,-2,-3,-4]:
        q1 = "SELECT * FROM sample_info WHERE group_number = " + str(gn) + ";"
        data = pd.read_sql_query(q1, cnx)
        data.internal_standard = str(new_value)
        pdb.set_trace()
        for sid in data.sample_id.tolist():
            q2 = "UPDATE sample_info SET internal_standard = '" + str(new_value) +"' WHERE sample_id = " + str(sid)
            cursor = cnx.cursor()
            cursor.execute(q2)
            cnx.commit()

def change_standard_volume_old(sample_id, new_value, cnx):
    q1 = "UPDATE sample_info SET standard_volume = '" + str(new_value) + "' WHERE sample_id = " + str(sample_id);
    cursor = cnx.cursor()
    cursor.execute(q1)
    cnx.commit() 
    
def change_sample_amount_old(sample_id, new_value, cnx):
    q1 = "UPDATE sample_info SET sample_amount = '" + str(new_value) + "' WHERE sample_id = " + str(sample_id);
    cursor = cnx.cursor()
    cursor.execute(q1)
    cnx.commit()
    
def update_standard_info_old(experiment_id, name_table, standard_name):
    if type(name_table) == str:
        nt = pd.read_csv(name_table)
    else:
        nt = name_table.copy()
    rename = {'Name': 'sample_name',
              'Group_Name': 'group_name',
              'Group': 'group_number',
              'Sample_Number': 'sample_number',
              'Sample_Amount':'sample_amount',
              'Standard_Volume':'standard_volume'}
    nt.rename(columns = rename, inplace = True)
    nt = nt[nt.group_number > 0]
    for i in nt.index:
        #pdb.set_trace()
        sample_name = nt.loc[i]['sample_name']
        q1 = "SELECT sample_id FROM sample_info WHERE experiment_id = %s and sample_name = '%s'" %(experiment_id, sample_name)
        sample = pd.read_sql_query(q1, old_cnx)
        assert sample.shape[0] == 1
        sample_id = sample.iloc[0]['sample_id']        
        sample_amount = nt.loc[i]['sample_amount']
        standard_volume = nt.loc[i]['standard_volume']
        q2 = "UPDATE sample_info SET sample_amount = %s, standard_volume = %s, internal_standard = '%s' WHERE sample_id = %s;" %(sample_amount, standard_volume, standard_name, sample_id)
        cursor = old_cnx.cursor()
        cursor.execute(q2)
        old_cnx.commit()
        
def update_standatd_info_from_file_old(input_path):
    files = os.listdir(input_path)
    for f in files:
        if 'namesfile' in f.lower():
            name_table = os.path.join(input_path,f)
            break
    sample_meta = pd.read_csv(os.path.join(input_path, 'sample_meta.csv'))
    control_meta = pd.read_csv(os.path.join(input_path, 'control_meta.csv'))
    experimnet_meta = pd.read_csv(os.path.join(input_path, 'experiment_meta.csv'))
    standard = list(set(sample_meta['internal_standard']))[0]
    experiment_id = list(set(experimnet_meta['experiment_id']))[0]
    if not standard in ['None', None, 0, 'Null', 'null']:
        update_standard_info_old(experiment_id, name_table, standard)
    else:
        print experiment_id
        
        
