import os, sys
import time
import pandas as pd
import numpy as np
from collections import OrderedDict
import json
import mysql
import Lookups.database_tables as dt
import experiment_class as ex
import ast
import pdb
import subprocess
from pipeline_experiment_job import *

start1 = time.time()
job_id = 48
job_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                        password = dt.db_login_info['password'],
                        host = dt.db_login_info['host'],
                        database = dt.db_login_info['job_db'])

lipid_cnx=mysql.connector.connect(user = dt.db_login_info['user'], 
                        password = dt.db_login_info['password'],
                        host = dt.db_login_info['host'],
                        database = dt.db_login_info['lipid_db'])
