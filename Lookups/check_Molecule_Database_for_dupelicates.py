# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 11:39:47 2015

@author: Alex
"""

import pandas as pd
import numpy as np
import shelve
import os
import LipPy_v2_0.MS_classes as ms
from LipPy_v2_0.MS_classes import concat_names_standards
import LipPy_v2_0.Stats.LipidFunctions as lf
from LipPy_v2_0.Lookups import name_lookup
import collections


os.chdir('D:\\Alex Treacher\\Lipid_Explorer_v2_0')

posname = name_lookup['+']
negname = name_lookup['-']

create_files = True

def find(df, a, b, accuracy = .3):
    '''
    used in iso_correction to find the desired parent and daughter isotope by precursor and fragment size
    if it is found return [ture, index] if not found return [false, none]
    '''
    df = df   
    a1 = a-accuracy
    a2 = a+accuracy
    b1 = b-accuracy
    b2 = b+accuracy    
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    if check.empty:
        return None
    else:
        return list(check.index)
                
pos_dupe_df = pd.DataFrame()
already_checked=[]
for i in posname.index:
    if i not in already_checked:
        parent = posname['Precursor'][i]
        fragment = posname['Fragment'][i]
        found = find(posname,parent,fragment)
        if found != None:
            if len(found) > 1:
                already_checked.append(i)
                for ind in list(found):
                    pos_dupe_df = pos_dupe_df.append(posname.loc[[ind]])
                    already_checked.append(ind)
                
neg_dupe_df = pd.DataFrame()
already_checked=[]
for i in negname.index:
    if i not in already_checked:
        parent = negname['Precursor'][i]
        fragment = negname['Fragment'][i]
        found = find(negname,parent,fragment)
        if found != None:
            if len(found) > 1:
                already_checked.append(i)
                for ind in list(found):
                    neg_dupe_df = neg_dupe_df.append(negname.loc[[ind]])
                    already_checked.append(ind)