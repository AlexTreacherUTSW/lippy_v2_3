# -*- coding: utf-8 -*-
"""
Created on Fri Jul 15 10:11:14 2016

@author: Alex
"""

import os, sys
import pandas as pd
import numpy as np
from Lipids import *
from collections import OrderedDict
from mass_pairs import *
from Compounds import *
import pdb
from mass_pairs import *
import itertools

project_folder = os.path.abspath(r'D:\Alex Treacher\Projects\new_names')
chains_folder = os.path.join(project_folder, 'chains.txt')
output_folder = r'D:\Alex Treacher\Projects\new_names\for_meeting\neg2'

if not os.path.exists(output_folder):
    os.mkdir(output_folder)

chains_df = pd.read_table(chains_folder)
chains = OrderedDict()
highchains = chains_df[['High',"HighMass"]].copy()
highchains.dropna(inplace=True)
highchains.columns = ['FA','MASS']
midchains = chains_df[['Mid','MidMass']].copy()
midchains.dropna(inplace=True)
midchains.columns = ['FA','MASS']
lowchains = chains_df[['Low','LowMass']].copy()
lowchains.dropna(inplace=True)
lowchains.columns = ['FA','MASS']

PLHighSat = ["16:0","16:1","18:0","18:1"]
PLHighPUFA = ["18:2","20:4","20:5","22:5","22:6"]
PLHighTotal = PLHighSat + PLHighPUFA

PLLowSat = ["20:0","20:1","22:0","22:1","22:2","24:0","24:1","14:0","14:1","15:0","15:1","17:0",]
PLLowPUFA = ["18:3","20:3","22:4","24:5","24:6"]
PLLowTotal = PLLowSat + PLLowPUFA

HS = pd.DataFrame({'FA':PLHighSat,'MASS':['']*len(PLHighSat)})
HP = pd.DataFrame({'FA':PLHighPUFA,'MASS':['']*len(PLHighPUFA)})
LS = pd.DataFrame({'FA':PLLowSat,'MASS':['']*len(PLLowSat)})
LP = pd.DataFrame({'FA':PLLowPUFA,'MASS':['']*len(PLLowPUFA)})

SM_R1 = pd.DataFrame({'FA':["18:1","18:0","16:0","16:1","18:2"],'MASS':['','','','','']})

cer_r1 = pd.DataFrame({'FA':["18:1","18:2","18:0","16:1"],'MASS':['','','','']})
cer_r2_chains = PLHighSat+PLHighPUFA
cer_r2 = pd.DataFrame({'FA':cer_r2_chains,'MASS':['']*len(cer_r2_chains)})

CL_chains= ['16:0', '16:1', '18:0', '18:1', '18:2', '17:0', '17:1', '18:3',
            '20:0', '20:1', '20:3', '20:4', '20:5', '22:5', '22:6']

dct_template = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[])
                          ])
#builders =====================================================================
class fatty_acid_chain_builder():
    """
    class used for fatty acid composition and formula
    """
    
    def __init__(self, carbons, double_bonds):
        self.carbons = int(carbons)
        self.double_bonds = int(double_bonds)
        self.formula = self.get_formula()
        self.mass = self.get_mass()
        
    def get_formula(self):
        formula = OrderedDict()
        formula['C'] = self.carbons
        formula['O'] = 2
        formula['H'] = (self.carbons*2) - (self.double_bonds*2) - 2 + 1;
        return formula
        
    def get_mass(self):
        mass = sum(mul_dict(self.formula,MASSES).values())
        return mass
        

#chian list makers ===========================================================
    
def single_chain_list_maker(chain_df_list = [highchains,midchains,lowchains]):
    """
    used to make a list of the chains to be put in to the single chain lipids
    """
    chains = []
    for df in chain_df_list:
        chains.extend(df.FA.tolist())
    return chains

def double_chain_list_maker(combos = [(highchains,highchains),
                                      (highchains,midchains),
                                      (highchains,lowchains),
                                      (midchains,midchains)],
                            from_type = 'df'):
    """
    takes a list of tuples of dfs and makes a list of combinations of 2 chains
    Used primarily for the DAG
    """
    chains = []
    for combo in combos:
        #pdb.set_trace()
        if from_type == 'df':
            if combo[0].equals(combo[1]):
                #combinations with repeats
                l = combo[0].FA.tolist()
                chains.extend(list(itertools.combinations_with_replacement(l,2)))
            else:
                #matrix product
                l1 = combo[0].FA.tolist()
                l2 = combo[1].FA.tolist()
                chains.extend(list(itertools.product(l1,l2)))
        else:
            if set(combo[0]) == set(combo[1]):
                l = combo[0]
                chains.extend(list(itertools.combinations_with_replacement(l,2)))
            else:
                l1 = combo[0]
                l2 = combo[1]
                chains.extend(list(itertools.product(l1,l2)))
                
    return chains
    
    
def tripple_chain_list_all_combo(include_lists = [highchains, midchains, lowchains]):
    """
    combination of all in lists, no repeats
    """
    chains = []
    for lst in include_lists:
        chains.extend(lst.FA.tolist())
    chain_groups = list(itertools.combinations_with_replacement(chains,3))
    return chain_groups
    
def tripple_chain_list_maker(combos = [(highchains,highchains,highchains),
                                  (highchains,highchains,midchains),
                                  (highchains,midchains,midchains),
                                  (highchains,highchains,lowchains),
                                  ]):
    """
    includes combinations only from each option
    """
    chains = []
    for combo in combos:
        #pdb.set_trace()
        if combo[0].equals(combo[1]) and combo[1].equals(combo[2]):
            #combinations with repeats
            l = combo[0].FA.tolist()
            chains.extend(list(itertools.combinations_with_replacement(l,3)))
        else:
            #matrix product
            l1 = combo[0].FA.tolist()
            l2 = combo[1].FA.tolist()
            l3 = combo[2].FA.tolist()
            chains.extend(list(itertools.product(l1,l2,l3)))
    return chains

def chain_list_combine_Cer(backbone, Rgroup):
    """
    Takes two lists backbone and Rgroup will return sum of items of combinations from the two lists
    eg backbone = ['16:0','18:0'] Rgroup = ['14:0','16:0','18:0'] will return 
    ['30:0(d16:0,14:0)', '32:0(d16:0,16:0)', '34:0(d16:0,18:0)', '32:0(d18:0,14:0)', '34:0(d18:0,16:0)', '36:0(d18:0,18:0)']
    used for Cer
    """
    if type(backbone) == type(pd.DataFrame()):
        backbone = backbone['FA'].tolist()
    if type(Rgroup) == type(pd.DataFrame()):
        Rgroup = Rgroup['FA'].tolist()
    noC = []
    nodb = []
    total_list = list(itertools.product(backbone, Rgroup))
    out = []
    for p in total_list:
        c1 = int(p[0].split(':')[0])
        c2 = int(p[1].split(':')[0])
        db1 = int(p[0].split(':')[1])
        db2 = int(p[1].split(':')[1])
        total = str(c1+c2) + ':' + str(db1+db2)
        chains = '(d' + str(c1) + ':' + str(db1) + ','  + str(c2) + ':' + str(db2) +  ')'
        complete = total+chains
        if complete not in out:
            out.append(complete)
    return out

def chain_list_combine_HexCer(backbone, Rgroup):
    """
    Takes two lists backbone and Rgroup will return sum of items of combinations from the two lists
    eg backbone = ['16:0','18:0'] Rgroup = ['14:0','16:0','18:0'] will return 
    ['30:0','32:0','34:0','36:0']
    used for HexCer
    """
    if type(backbone) == type(pd.DataFrame()):
        backbone = backbone['FA'].tolist()
    if type(Rgroup) == type(pd.DataFrame()):
        Rgroup = Rgroup['FA'].tolist()
    noC = []
    nodb = []
    total_list = list(itertools.product(backbone, Rgroup))
    out = []
    for p in total_list:
        c1 = int(p[0].split(':')[0])
        c2 = int(p[1].split(':')[0])
        db1 = int(p[0].split(':')[1])
        db2 = int(p[1].split(':')[1])
        to_add = str(c1+c2) + ':' + str(db1+db2)
        if to_add not in out:
            out.append(to_add)
    return out
    
def chain_list_combine_CL(chains = CL_chains, major_chain = '18:2'):
    """
    makes names for the CL class
    Total of 4 chains: 3 of major and one from chains, or 2 from major and a 
        combination of 2 from chains
    """
    complete_list = []
    for chain in chains:
        complete_list.append([major_chain,major_chain,major_chain, chain])
    dual_chains = double_chain_list_maker([(chains, chains)], from_type='list')
    for dc in dual_chains:
        l = [major_chain, major_chain, dc[0], dc[1]]
        complete_list.append(l)
    return complete_list

# functions to make each lipid
def LPL_builder(chain, group):
    """
    takes a chain and returns a class of lysopc with that chain and correct masses
    """
    r1_str = str(chain)
    r1_c = int(r1_str.split(':')[0])
    r1_db = int(r1_str.split(':')[1])
    name = group+'('+str(r1_c)+':'+str(r1_db)+')'
    temp_lipid = Lipid.from_group('LPC',name,mode='-')
    ms1 = temp_lipid.ms1_mass()
    ms2 = temp_lipid.ms2_mass()
    lipid = Lipid.from_group(group,name,mode='-',mass_pair=MassPair(ms1,ms2))
    return lipid

def PL_builder(chains, group):
    """
    makes pl with the chains.
    chains should be a tuple or list like ('16:0', '16:0')
    """
    r1_str = chains[0]
    r2_str = chains[1]
    r1_c = int(r1_str.split(':')[0])
    r1_db = int(r1_str.split(':')[1])
    r2_c = int(r2_str.split(':')[0])
    r2_db = int(r2_str.split(':')[1])
    name_r1 = group+str(r1_c+r2_c)+':'+str(r1_db+r2_db)+'('+r1_str+','+r2_str+')'
    name_r2 = group+str(r1_c+r2_c)+':'+str(r1_db+r2_db)+'('+r2_str+','+r1_str+')'
    temp_lipid = Lipid.from_group(group, name_r1, '-')
    MS1_mass = temp_lipid.get_MS1(as_mass = True)
    MS2_R1 = temp_lipid.chain_formula()[0]
    MS2_R2 = temp_lipid.chain_formula()[1]
    MS2_R1_mass = formula_to_mass(MS2_R1)
    MS2_R2_mass = formula_to_mass(MS2_R2)
    pc_r1 = Lipid.from_group(group,name_r1,mode = '-', mass_pair=MassPair(MS1_mass,MS2_R1_mass))
    pc_r2 = Lipid.from_group(group,name_r2,mode = '-', mass_pair=MassPair(MS1_mass,MS2_R2_mass))
    if name_r1 == name_r2:
        return [pc_r1]
    else:
        return [pc_r1, pc_r2]

def FFA_builder(chain):
    name = 'FFA'+chain
    temp_lipid = Lipid.from_group('FFA',name, mode = '-')
    MS1_mass = temp_lipid.ms1_mass()
    MS2_mass = temp_lipid.ms2_mass()
    lipid = Lipid.from_group('FFA', name ,mode = '-', mass_pair=MassPair(MS1_mass,MS2_mass))
    return lipid

def CL_builder(chains):
    r1C = int(chains[0].split(':')[0])
    r2C = int(chains[1].split(':')[0])
    r3C = int(chains[2].split(':')[0])
    r4C = int(chains[3].split(':')[0])
    r1DB = int(chains[0].split(':')[1])
    r2DB = int(chains[1].split(':')[1])
    r3DB = int(chains[2].split(':')[1])
    r4DB = int(chains[3].split(':')[1])
    totalC = r1C+r2C+r3C+r4C
    totalDB = r1DB+r2DB+r3DB+r4DB
    total_str = str(totalC)+':'+str(totalDB)
    CLs = []
    CLnames = []
    for c in chains:
        name = 'CL('+total_str+')'+'MS2-'+c
        if not name in CLnames:
            lipid = Lipid.from_group('CL', name, mode = '-')
            lipid.calculate_mass_pair(reset=True)
            CLs.append(lipid)
            CLnames.append(name)
    return CLs
    
def GM_GD_builder(group, chains):
    """
    Makes GM or GM, the chains[0] will be the one labled with a d.   
    """
    r1C = int(chains[0].split(':')[0])
    r2C = int(chains[1].split(':')[0])
    r1DB = int(chains[0].split(':')[1])
    r2DB = int(chains[1].split(':')[1])
    name = group+'(d'+chains[0]+','+chains[1]+')'
    l = Lipid.from_group(group, name, mode = '-')
    l.calculate_mass_pair(reset=True)
    return l   

#below creates the individual names lists
def PL_name_maker(group, chain_list, output = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for chains in chain_list:
        lipids = PL_builder(chains, group)
        for lipid in lipids:
            names_dct['Precursor'].extend([lipid.ms1_mass()])
            names_dct['Fragment'].extend([lipid.ms2_mass()])
            names_dct['Name'].extend([lipid.name])
            names_dct['Group'].extend([lipid.group])
    name_df = pd.DataFrame(names_dct)
    if output != False:
        name_df.to_csv(os.path.join(output,group+'.csv'), index = False)
    return pd.DataFrame(names_dct)
    
def LPL_name_maker(group, chain_list, output = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for chain in chain_list:
        lipid = LPL_builder(chain, group)
        names_dct['Precursor'].extend([lipid.ms1_mass()])
        names_dct['Fragment'].extend([lipid.ms2_mass()])
        names_dct['Name'].extend([lipid.name])
        names_dct['Group'].extend([lipid.group])
    name_df = pd.DataFrame(names_dct)
    if output != False:
        name_df.to_csv(os.path.join(output,group+'.csv'), index = False)
    return pd.DataFrame(names_dct)

def FFA_name_maker(chain_list, output = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for chain in chain_list:
        lipid = FFA_builder(chain)
        names_dct['Precursor'].extend([lipid.ms1_mass()])
        names_dct['Fragment'].extend([lipid.ms2_mass()])
        names_dct['Name'].extend([lipid.name])
        names_dct['Group'].extend([lipid.group])
    name_df = pd.DataFrame(names_dct)
    if output != False:
        name_df.to_csv(os.path.join(output,'FFA.csv'), index = False)
    return pd.DataFrame(names_dct)
    
def CL_name_maker(chains_list, output = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    used_names = []
    for chains in chains_list:
        lipids = CL_builder(chains)
        for lipid in lipids:
            if not lipid.name in used_names:
                used_names.append(lipid.name)
                names_dct['Precursor'].extend([lipid.ms1_mass()])
                names_dct['Fragment'].extend([lipid.ms2_mass()])
                names_dct['Name'].extend([lipid.name])
                names_dct['Group'].extend([lipid.group])
    name_df = pd.DataFrame(names_dct)
    if output != False:
        name_df.to_csv(os.path.join(output,'CL.csv'), index = False)
    return name_df

def GM_GD_name_maker(group, chains_list, output = False):
    names_dct = OrderedDict([('Precursor',[]),
                          ('Fragment',[]),
                          ('Name',[]),
                          ('Group',[]),
                          ])
    for chains in chain_list:
        lipid = GM_GD_builder(group, chains)
        names_dct['Precursor'].extend([lipid.ms1_mass()])
        names_dct['Fragment'].extend([lipid.ms2_mass()])
        names_dct['Name'].extend([lipid.name])
        names_dct['Group'].extend([lipid.group])
    name_df = pd.DataFrame(names_dct)
    if output != False:
        name_df.to_csv(os.path.join(output,group+'.csv'), index = False)
    return name_df
        

#Below here will be to join a file and check overlap

def concat_file(file_loc, ignore = ['overlaps.csv','complete.csv','complete_duplicates_removed.csv']):
    """
    this will concat every thing in tha file that has .csv at the end of the
    file name, except the ones in the ignore
    """
    all_files = os.listdir(file_loc)
    name_files = list([x for x in all_files if x.endswith('.csv')])
    for igfile in ignore:
        if igfile in name_files:
            name_files.remove(igfile)
    df = pd.DataFrame()
    for f in name_files:
        temp_df = pd.read_csv(os.path.join(file_loc,f))
        df = df.append(temp_df, ignore_index = True)
    df.sort_values(['Precursor','Fragment'], inplace = True)
    df.reset_index(inplace = True, drop = True)
    return df
        
def check_overlap_make_complete(name_file):
    df = concat_file(name_file)
    df.to_csv(os.path.join(name_file,'complete.csv'), index = False)
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    overlaps.to_csv(os.path.join(name_file,'overlaps.csv'), index = False)
    return overlaps

def check_overlap(df):
    df = df.copy()
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    try:
        overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    except KeyError:
        overlaps = pd.DataFrame()
    return overlaps

def check_overlap_from_csv(file_loc):
    df = pd.read_csv(file_loc)
    overlaps = pd.DataFrame()
    for i in df.index:
        if i in df.index:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) <= 1:
                pass
            else:
                for t in found:
                    overlaps = overlaps.append(df.loc[t])
                    df.drop(t, inplace = True)
        else:
            pass
    try:
        overlaps = overlaps[['Precursor','Fragment','Name','Group']]
    except KeyError:
        overlaps = pd.DataFrame()
    return overlaps
    
def apply_priorities(name_file):
    """
    make a new dataframe with the same columns as the name_file
    make empty list of do not search (DNS) indexes
    for each value in the index  of the name file
        if the index is not in the DNS
            search for overlapping lipids
            if not found add to new_dataframe and add index to DNR
            if is found run through priority and add the result to the new_names, then add both indexs to DNR 
    
    """
    df = name_file.copy()
    new_df = pd.DataFrame(columns = ['Precursor','Fragment','Name','Group'])
    DNS = []
    for i in df.index:
        if not i in DNS:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) == 1:
                to_add = {'Precursor':df.loc[i]['Precursor'],
                          'Fragment':df.loc[i]['Fragment'],
                          'Name':df.loc[i]['Name'],
                          'Group':df.loc[i]['Group']}
                new_df = new_df.append(to_add, ignore_index = True)
                DNS.append(found[0])
            elif len(found) == 2:
                lipid0 = Lipid.from_group(df.loc[found[0]]['Group'], df.loc[found[0]]['Name'], '-')
                lipid1 = Lipid.from_group(df.loc[found[1]]['Group'], df.loc[found[1]]['Name'], '-')
                ng = get_priority(lipid0, lipid1)
                if ng == None:
                    new_df = new_df.append(df.loc[found[0]], ignore_index = True)
                    new_df = new_df.append(df.loc[found[1]], ignore_index = True)
                elif ng[1] == lipid0.group:
                    to_add = OrderedDict()
                    to_add['Precursor'] = df.loc[found[0]]['Precursor']
                    to_add['Fragment'] = df.loc[found[0]]['Fragment']
                    to_add['Name'] = ng[0]
                    to_add['Group'] = ng[1]
                    new_df = new_df.append(to_add, ignore_index = True)
                elif ng[1] == lipid1.group:
                    to_add = OrderedDict()
                    to_add['Precursor'] = df.loc[found[1]]['Precursor']
                    to_add['Fragment'] = df.loc[found[1]]['Fragment']
                    to_add['Name'] = ng[0]
                    to_add['Group'] = ng[1]
                    new_df = new_df.append(to_add, ignore_index = True)
                DNS.append(found[0])
                DNS.append(found[1])
            else:
                for fi in found:
                   new_df = new_df.append(df.loc[fi], ignore_index = True)
                   DNS.append(fi)
                #add all from found
    return new_df
    
def HH_HH_priority(lipid0, lipid1, priorities = None, HH = PLHighTotal, HL = None):
    if priorities == None:
       priorities = [['PG','PA'],['PE','PS']]
    if [lipid0.group, lipid1.group] in priorities:
       return [lipid0.name + ' or ' + lipid1.name, lipid0.group]
    elif [lipid1.group, lipid0.group] in priorities:
       return [lipid1.name + ' or ' + lipid0.name, lipid1.group]
    else:
       print 'no HH priority for ' + lipid0.name + ', ' + lipid1.name
       return None
       
def HH_HL_priority(lipid0, lipid1, priorities = None, high_chains = PLHighTotal, low_chains = PLLowTotal):
    if priorities == None:
        priorities = [['PC','PE'],['PE','PS'],['PC','PS'],['PI','PG']]
    if number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 1:
        highlipid = lipid0
        lowlipid = lipid1
    elif number_chains_in_list(lipid1, high_chains) == 2 and number_chains_in_list(lipid0, high_chains) == 1:
        highlipid = lipid1
        lowlipid = lipid0
    else:
        raise ValueError('HH_HL_NOPE ' + lipid1.name + ' ' + lipid0.name)
    test_group = [highlipid.group, lowlipid.group]
    if test_group in priorities:    
        name = highlipid.name + ' or ' + lowlipid.name
        group = highlipid.group
        return [name, group]
    else:
        return None

def HL_HH_priority(lipid0, lipid1, priorities = None, high_chains = PLHighTotal, low_chains = PLLowTotal):
    if priorities == None:
        priorities = [['PC','PS']]
    if number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 1:
        highlipid = lipid0
        lowlipid = lipid1
    elif number_chains_in_list(lipid1, high_chains) == 2 and number_chains_in_list(lipid0, high_chains) == 1:
        highlipid = lipid1
        lowlipid = lipid0
    else:
        raise ValueError('HL_HH_NOPE ' + lipid1.name + ' ' + lipid0.name)
    test_group = [lowlipid.group, highlipid.group]
    if test_group in priorities:    
        name = highlipid.name + ' or ' + lowlipid.name
        group = highlipid.group
        return [name, group]
    else:
        return None
        
def HL_HL_priority(lipid0, lipid1, priorities = None, high_chains = PLHighTotal, low_chains = PLLowTotal):
    if priorities == None:
        priorities = [['PE','PS'],['PC','PE'],['PC','PS']]
    if [lipid0.group, lipid1.group] in priorities:
       return [lipid0.name + ' or ' + lipid1.name, lipid0.group]
    elif [lipid1.group, lipid0.group] in priorities:
       return [lipid1.name + ' or ' + lipid0.name, lipid1.group]
    else:
       print 'no HL_HL priority for ' + lipid0.name + ', ' + lipid1.name
       return None
      
def LL_LL_priority(lipid0, lipid1, priorities, high_chains = PLHighTotal, low_chains = PLLowTotal):
    pass

def HH_LL_priority(lipid0, lipid1, priorities, high_chains = PLHighTotal, low_chains = PLLowTotal):
    pass
    
def get_priority(lipid0, lipid1, priorities = None, high_chains = PLHighTotal, low_chains = PLLowTotal):
    """
    This is used to test the priorites of lipids
    priorities work like this:
        the first gets priority over the second ex
        if its a HighHigh_HighHigh and PG vs PA and the priority is [PG,PA] the PG gets it
    """
    if priorities == None:
        priorities = {'HH_HH':[['PG','PA'],['PE','PS']],
                      'HH_HL':[['PC','PE'],['PE','PS'],['PC','PS'],['PI','PG'],['PC','PI']],
                      'HL_HH':[['PC','PS']],
                      'HL_HL':[['PE','PS'],['PC','PE'],['PC','PS'],['PG','PI']],
                      'LL_LL':[],
                      'HH_LL':[],}
    #if set([lipid0.group, lipid1.group]) == set(['CL','PA']):
    #    pdb.set_trace()
    if lipid0.group == 'CL' and "PL" in lipid1.get_lineage():
        ng = [lipid1.name + ' or ' + lipid0.name, lipid1.group]
        return ng
    elif "PL" in lipid0.get_lineage() and lipid1.group == 'CL':
        ng = [lipid0.name + ' or ' + lipid1.name, lipid0.group]
        return ng
    elif lipid1.group == 'CL' or lipid0.group == 'CL':
        return None
    #HH_HH
    if (number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 2) and ([lipid0.group, lipid1.group] in priorities['HH_HH'] or [lipid1.group, lipid0.group] in priorities['HH_HH']):
        ng = HH_HH_priority(lipid0, lipid1)
        return ng
    #HH_HL
    elif (number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 1) and ([lipid0.group, lipid1.group] in priorities['HH_HL'] or [lipid1.group, lipid0.group] in priorities['HH_HL']):        
        ng = HH_HL_priority(lipid0, lipid1)
        return ng
    elif (number_chains_in_list(lipid0, high_chains) == 1 and number_chains_in_list(lipid1, high_chains) == 2) and ([lipid0.group, lipid1.group] in priorities['HH_HL'] or [lipid1.group, lipid0.group] in priorities['HH_HL']):
        ng = HH_HL_priority(lipid0, lipid1)
        return ng
    #HL_HH
    elif (number_chains_in_list(lipid0, high_chains) == 1 and number_chains_in_list(lipid1, high_chains) == 2) and ([lipid0.group, lipid1.group] in priorities['HL_HH'] or [lipid1.group, lipid0.group] in priorities['HL_HH']):
        ng = HL_HH_priority(lipid0, lipid1)
        return ng
    elif (number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 1) and ([lipid0.group, lipid1.group] in priorities['HL_HH'] or [lipid1.group, lipid0.group] in priorities['HL_HH']):
        ng = HL_HH_priority(lipid0, lipid1)
        return ng
    #HL_HL             
    elif (number_chains_in_list(lipid0, high_chains) == 1 and number_chains_in_list(lipid1, high_chains) == 1) and ([lipid0.group, lipid1.group] in priorities['HL_HL'] or [lipid1.group, lipid0.group] in priorities['HL_HL']):
        ng = HL_HL_priority(lipid0, lipid1)
        return ng
    #LL_LL   
    elif (number_chains_in_list(lipid0, high_chains) == 0 and number_chains_in_list(lipid1, high_chains) == 0) and ([lipid0.group, lipid1.group] in priorities['LL_LL']) or [lipid1.group, lipid0.group] in priorities['LL_LL']:
        ng = LL_LL_priority(lipid0, lipid1)
        return ng
    #HH_LL
    elif (number_chains_in_list(lipid0, high_chains) == 2 and number_chains_in_list(lipid1, high_chains) == 0) and ([lipid0.group, lipid1.group] in priorities['HH_LL'] or [lipid1.group, lipid0.group] in priorities['HH_LL']):
        ng = HH_LL_priority(lipid0, lipid1)
        return ng
    elif (number_chains_in_list(lipid0, high_chains) == 0 and number_chains_in_list(lipid1, high_chains) == 2) and ([lipid0.group, lipid1.group] in priorities['HH_LL'] or [lipid1.group, lipid0.group] in priorities['HH_LL']):
        ng = HH_LL_priority(lipid0, lipid1)
        return ng
    else:
        ng = None
        return ng

def remove_names(name_df, to_remove):
    df = name_df.copy()
    for name in to_remove:
        df = df[df.Name != name]
    return df

def remove_overlaps(name_file):
    """
    THIS IS NOT A VALID WAY TO REMOVE OVERLAPPING MASS PAIRS,
    ONLY USE FOR TESTING
    """
    df = name_file.copy()
    new_df = pd.DataFrame(columns = ['Precursor','Fragment','Name','Group'])
    DNS = []
    for i in df.index:
        if not i in DNS:
            pre = df.loc[i]['Precursor']
            frag = df.loc[i]['Fragment']
            found = find(df, pre, frag)
            if len(found) == 1:
                to_add = {'Precursor':df.loc[i]['Precursor'],
                          'Fragment':df.loc[i]['Fragment'],
                          'Name':df.loc[i]['Name'],
                          'Group':df.loc[i]['Group']}
                new_df = new_df.append(to_add, ignore_index = True)
                DNS.append(found[0])
    return new_df
    
    
def find(df, a, b, ms1_accuracy = .3, ms2_accuracy = .3):
    '''
    used to find a pre and fragment with accuracy
    '''  
    a1 = a-ms1_accuracy 
    a2 = a+ms1_accuracy 
    b1 = b-ms2_accuracy 
    b2 = b+ms2_accuracy     
    check = df.loc[((df['Precursor'] > a1) & (df['Precursor'] < a2)) & ((df['Fragment'] > b1) & (df['Fragment'] < b2))]
    return check.index

def formula_to_mass(formula):
    return sum(mul_dict(formula,MASSES).values()) 
    
def number_chains_in_list(lipid, l):
    r1 = lipid.R1_features(as_str = True)   
    r2 = lipid.R2_features(as_str = True) 
    count = 0
    if r1 in l:
        count +=1
    if r2 in l:
        count +=1
    return count
    
    
    
mass_dif = 54

if __name__ == '__main__':
    if False:
        PLs = {'PA':double_chain_list_maker(combos = [(HS,HP)]),
              'PC':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LS),(HS,LP),(HP,LS)]),
              'PE':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LS),(HS,LP),(HP,LS)]),
              'PG':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              'PI':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              'PS':double_chain_list_maker(combos = [(HS,HS),(HP,HP),(HS,HP),(HS,LP),(HP,LS)]),
              }
        for PL in PLs:
            PL_name_maker(PL, PLs[PL], output=output_folder)
    if False:
        chain_list1 = pd.concat([HS,HP,LS,LP], ignore_index=True)
        chain_list2 = pd.concat([HS,HP], ignore_index=True)
        lysoPLs = {'LPA':chain_list1.FA.tolist(),
               'LPC':chain_list1.FA.tolist(),
               'LPE':chain_list1.FA.tolist(),
               'LPG':chain_list2.FA.tolist(),
               'LPI':chain_list2.FA.tolist(),
               'LPS':chain_list2.FA.tolist(),
        }
        for lysoPL in lysoPLs:
            LPL_name_maker(lysoPL, lysoPLs[lysoPL], output=output_folder)
    if False:
        chain_list = pd.concat([HS,HP,LS,LP], ignore_index=True).FA.tolist()
        FFA_name_maker(chain_list, output = output_folder)
    if False:
        chain_list = chain_list_combine_CL()
        CL_name_maker(chain_list ,output = output_folder)
    if False:
        backbone = pd.DataFrame({'FA' : ['18:1']})
        chain_list = double_chain_list_maker(combos = [[backbone,highchains.append(pd.DataFrame({'FA':['20:4'],'MASS':['']}), ignore_index=True)]])
        group = {'GM1':chain_list,
                 'GM2':chain_list,
                 'GM3':chain_list,
                 'GD1':chain_list,
                 'GD2':chain_list,
                 'GD3':chain_list}
        for g in group:
            GM_GD_name_maker(g, group[g], output=output_folder)
    if False:
        full =  concat_file(output_folder, ignore=['complete.csv','complete_rules.csv','overlaps.csv','overlaps_rules.csv','overlaps_after_priority.csv'])
        full.to_csv(os.path.join(output_folder,'complete.csv'), index = False)
        x = apply_priorities(full)
        x.to_csv(os.path.join(output_folder,'complete_rules.csv'), index = False)
        os.chdir(r'D:\Alex Treacher\Projects\new_names\for_meeting')
        allov = check_overlap(full)
        allov.to_csv(os.path.join(output_folder,'neg_all_overlaps.csv'), index = False)
        
        #ol = check_overlap(x)
        #ol.to_csv(r'neg_overlap_rules.csv')
        
        